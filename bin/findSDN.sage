import csv
import sys
sys.path.append("../../MyPython")
import sndlib

def findNFV(name):
	dg = sndlib.getGraph(name)

	degrees = dg.out_degree()
	closeness = dg.centrality_closeness()
	betweenness = dg.centrality_betweenness()
	# print "degrees:", degrees
	# print "closeness:", closeness
	# print "betweenness:", betweenness
	SDNs = dg.vertices()

	degSDN = sorted(SDNs, key=lambda x: degrees[x], reverse=True)
	cloSDN = sorted(SDNs, key=lambda x: closeness[x], reverse=True)
	betSDN = sorted(SDNs, key=lambda x: betweenness[x], reverse=True)

	
	with open("instances/{}_nodes.txt".format(name), 'w') as f:
		r = csv.writer(f, delimiter='\t')
		r.writerow([dg.order()])
		for u1, u2, u3 in zip(degSDN, cloSDN, betSDN):
			r.writerow([u1, u2, u3])
	print "Saved to {}".format("instances/{}_nodes.txt".format(name))

	# print degSDN, [degrees[u] for u in degSDN]
	# print cloSDN, [closeness[u] for u in cloSDN]
	# print betSDN, [betweenness[u] for u in betSDN]

	

if __name__ == "__main__":
	if len(sys.argv) < 2: 
		print "Missing parameters. Use sage findSDNs.sage {name}"
		sys.exit(-1)
	name = sys.argv[1] 
	
	findNFV(name)  