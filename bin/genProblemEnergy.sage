# -*- coding: utf-8 -*-
import random
import sys
sys.path.append("../../MyPython")
import collections
import time
import csv
import itertools
import sndlib

total = float(36456 + 9476 + 78 + 6168)
percentage = [
	9476 / total, 
	6168 / total, 
	36456 / total,
	78 / total]
chains = [[0, 1, 2, 3, 4], [0, 1, 2, 1, 0], [0, 1, 2, 5, 4], [0, 1, 5, 3, 4]]

name = sys.argv[1]
demands = sndlib.getDemands(name)

filename = "{}_demand.txt".format(name, int(time.time()))
with open("./instances/energy/" + filename, 'wb') as csvfile:
	demandsWriter = csv.writer(csvfile, delimiter='\t')
	for (s, t), d in demands.items():
		i = 0
		for i in xrange(len(chains)):
			demandsWriter.writerow([s, t, d*percentage[i]] + chains[i])
