#include "SFC.hpp"
#include <fstream>
#include <istream>
#include <iterator>

namespace SFC {

ServicePath getServicePath(const Graph::Path& _augPath, const int _n,
    const int _demandID) {
    ServicePath retval{{_augPath.front()}, {}, _demandID};
    for (auto iteU = _augPath.begin(), iteV = std::next(iteU);
         iteV != _augPath.end(); ++iteU, ++iteV) {
        const function_descriptor layerU = *iteU / _n, layerV = *iteV / _n;
        const Graph::Node baseU = *iteU % _n, baseV = *iteV % _n;
        if (layerU == layerV) {
            assert(baseU != baseV);
            retval.nPath.push_back(baseV);
        } else {
            retval.locations.push_back(baseU);
        }
    }
    return retval;
}

ServicePath::ServicePath(Graph::Path _nPath,
    std::vector<SFC::function_descriptor> _locations,
    demand_descriptor _demand)
    : nPath(std::move(_nPath))
    , locations(std::move(_locations))
    , demand(_demand) {}

std::vector<int> loadNodeCapa(const std::string& _filename, double _percent) {
    std::cout << _filename << '\n';
    std::vector<int> nodeCapas;
    std::ifstream ifs(_filename, std::ifstream::in);
    if (!ifs) {
        throw std::runtime_error(_filename + " does not exists!\n");
    } else {
        std::cout << "Opening :" << _filename << '\n';
    }

    while (ifs.good()) {
        std::string line;
        std::getline(ifs, line);
        if (!line.empty()) {
            int u, capa;
            std::stringstream lineStream(line);
            lineStream >> u >> capa;
            nodeCapas.emplace_back(capa * _percent);
        }
    }
    return nodeCapas;
}

std::pair<std::vector<Demand>, std::vector<double>>
loadDemands(const std::string& _filename, const double _percent) {
    std::cout << _filename << '\n';
    std::ifstream ifs(_filename, std::ifstream::in);
    if (!ifs) {
        std::cerr << _filename << " does not exists!\n";
        exit(-1);
    } else {
        std::cout << "Opening :" << _filename << '\n';
    }

    // for(auto ite = std::istream_iterator<double>(ifs))
    std::string line;
    std::getline(ifs, line);
    std::stringstream funcLineStream(line);
    // First line is function requirements
    std::vector<double> funcReq;
    double req;
    while (funcLineStream >> req) {
        funcReq.emplace_back(req);
    }
    // Get demands
    std::vector<Demand> demands;
    while (ifs) {
        std::getline(ifs, line);
        std::stringstream demandStream(line);
        if (!line.empty() && demandStream.good()) {
            Graph::Node s, t;
            double d;
            demandStream >> s >> t >> d;
            std::vector<int> functions;
            function_descriptor f;
            while (demandStream >> f) {
                if (f > funcReq.size()) {
                    throw std::runtime_error("Function " + std::to_string(f) + "is invalid");
                }
                functions.emplace_back(f);
            }
            demands.emplace_back(s, t, d * _percent, functions);
        }
    }
    return {demands, funcReq};
}

DiGraph loadNetwork(const std::string& _filename) {
    std::cout << _filename << '\n';
    const auto network = DiGraph::loadFromFile(_filename);
    return std::get<0>(network);
}

std::ostream& operator<<(std::ostream& _out, const Demand& _demand) {
    return _out << '{' << _demand.s << ", " << _demand.t << ", " << _demand.d
                << ", " << _demand.functions << '}';
}

std::ostream& operator<<(std::ostream& _out, const ServicePath& _sPath) {
    return _out << '{' << "nPath: " << _sPath.nPath << ", "
                << "locations: " << _sPath.locations << ", "
                << "demand: " << _sPath.demand << '}';
}

// bool operator==(const SFC::Demand& _d1, const SFC::Demand& _d2) {
//     return std::make_tuple(_d1.s, _d1.t, _d1.functions) ==
//     std::make_tuple(_d2.s, _d2.t, _d2.functions);
// }

LayeredGraph::LayeredGraph(const Instance& _inst)
    : m_inst(&_inst)
    , m_graph([&]() {
        DiGraph retval(m_inst->network.getOrder() * m_inst->maxChainSize + 1);
        // Build layers
        for (int f = 0; f < m_inst->maxChainSize + 1; ++f) {
            for (const auto& edge : m_inst->network.getEdges()) {
                retval.addEdge(m_inst->network.getOrder() * f + edge.first,
                    m_inst->network.getOrder() * f + edge.second);
            }
        }
        // Add arcs between layers
        for (int fi = 0; fi < m_inst->maxChainSize; ++fi) {
            for (const auto u : m_inst->NFVNodes) {
                retval.addEdge(m_inst->network.getOrder() * fi + u,
                    m_inst->network.getOrder() * (fi + 1) + u);
            }
        }
        return retval;
    }()) {}

void LayeredGraph::removeCrossLayerLink(const Graph::Node _u, const int _j1) {
    m_graph.removeEdge(getRealNode({_u, _j1}), getRealNode({_u, _j1 + 1}));
}

double LayeredGraph::getCrossLayerWeight(const Graph::Node _u,
    const int _j1) const {
    return m_graph.getEdgeWeight(getRealNode({_u, _j1}),
        getRealNode({_u, _j1 + 1}));
}

void LayeredGraph::setCrossLayerWeight(const Graph::Node _u, const int _j1,
    double _weight) {
    m_graph.setEdgeWeight(getRealNode({_u, _j1}), getRealNode({_u, _j1 + 1}),
        _weight);
}

void LayeredGraph::removeInLayerEdge(const Graph::Edge _edge, const int _j1) {
    m_graph.removeEdge(getRealNode({_edge.first, _j1}),
        getRealNode({_edge.second, _j1}));
}

double LayeredGraph::getInLayerWeight(const Graph::Edge _edge,
    const int _j1) const {
    return m_graph.getEdgeWeight(getRealNode({_edge.first, _j1}),
        getRealNode({_edge.second, _j1}));
}

void LayeredGraph::setInLayerWeight(const Graph::Edge _edge, const int _j1,
    double _weight) {
    m_graph.setEdgeWeight(getRealNode({_edge.first, _j1}),
        getRealNode({_edge.second, _j1}), _weight);
}

std::vector<Graph::Node> loadNFV(const std::string& _filename, int _nbSDNs,
    int _type) {
    std::ifstream ifs(_filename, std::ifstream::in);
    if (!ifs) {
        std::cerr << _filename << " does not exists!" << std::endl;
        throw std::ios_base::failure(_filename + " does not exists!");
    }
    std::string line;
    std::getline(ifs, line, '\n');
    std::stringstream lineStream(line);

    int order, u = 0;
    lineStream >> order;
    std::vector<Graph::Node> SDNs(_nbSDNs, 0);
    assert(order >= _nbSDNs);
    for (int i = 0; i < _nbSDNs; ++i) {
        line = "";
        std::getline(ifs, line);
        if (!line.empty()) {
            std::stringstream lineStream1(line);
            for (int j = 0; j < _type; ++j) {
                lineStream1 >> u;
            }
            lineStream1 >> u;
            SDNs[i] = u;
        }
    }
    return SDNs;
}

namespace OccLim {
Instance::Instance(DiGraph _network, std::vector<int> _nodeCapa,
    std::vector<Demand> _demands,
    std::vector<double> _funcCharge,
    std::vector<Graph::Node> _NFVNodes, int _nbLicenses)
    : SFC::Instance(std::move(_network), std::move(_nodeCapa),
          std::move(_demands), std::move(_funcCharge),
          std::move(_NFVNodes))
    , nbLicenses(_nbLicenses) {}
} // namespace OccLim

namespace Placed {
Instance::Instance(DiGraph _network, std::vector<int> _nodeCapa,
    std::vector<Demand> _demands,
    std::vector<double> _funcCharge,
    std::vector<Graph::Node> _NFVNodes,
    Matrix<char> _funcPlacement)
    : SFC::Instance(std::move(_network), std::move(_nodeCapa),
          std::move(_demands), std::move(_funcCharge),
          std::move(_NFVNodes))
    , funcPlacement(std::move(_funcPlacement)) {}

Instance::Instance(SFC::Instance _inst, Matrix<char> _funcPlacement)
    : SFC::Instance(std::move(_inst))
    , funcPlacement(std::move(_funcPlacement)) {}
} // namespace Placed

bool operator==(const ServicePath& _sp1, const ServicePath& _sp2) {
    return _sp1.nPath == _sp2.nPath && _sp1.locations == _sp2.locations && _sp1.demand == _sp2.demand;
}

Demand::Demand(const Graph::Node _s, const Graph::Node _t, const double _d,
    std::vector<function_descriptor> _functions)
    : s(_s)
    , t(_t)
    , d(_d)
    , functions(std::move(_functions)) {}

void showNetworkUsage(const Instance& _inst,
    const std::vector<ServicePath>& _sPaths) {
    std::vector<double> linkUsage(_inst.network.size(), 0);
    std::vector<int> nodeUsage(_inst.network.getOrder(), 0);

    for (const auto & [ nPath, locations, demandID ] : _sPaths) {
        for (auto iteU = nPath.begin(), iteV = std::next(iteU); iteV != nPath.end();
             ++iteU, ++iteV) {
            linkUsage[_inst.edgeToId(*iteU, *iteV)] += _inst.demands[demandID].d;
        }
        for (int j = 0; j < locations.size(); ++j) {
            nodeUsage[locations[j]] += _inst.nbCores(demandID, j);
        }
    }
    std::cout << "Graph::Node usage: \n";
    for (const auto& u : _inst.NFVNodes) {
        std::cout << '\t' << u << " -> " << nodeUsage[u] << " / "
                  << _inst.nodeCapa[u] << '\n';
        assert(nodeUsage[u] <= _inst.nodeCapa[u]);
    }
    std::cout << "Link usage: \n";
    int i = 0;
    for (const auto& edge : _inst.network.getEdges()) {
        std::cout << std::fixed << '\t' << edge << " -> " << linkUsage[i] << " / "
                  << _inst.network.getEdgeWeight(edge) << '\n';
        assert(_inst.network.getEdgeWeight(edge) - linkUsage[i] > -1.0e-6 || (std::cerr << std::fixed << edge << '\n'
                                                                                        << _inst.network.getEdgeWeight(edge) - linkUsage[i] << '\n',
                                                                                 false));
        ++i;
    }
}

Instance::Instance(DiGraph _network, std::vector<int> _nodeCapa,
    std::vector<Demand> _demands,
    std::vector<double> _funcCharge,
    std::vector<Graph::Node> _NFVNodes)
    : network(std::move(_network))
    , edges(network.getEdges())
    , nodeCapa(std::move(_nodeCapa))
    , funcCharge(std::move(_funcCharge))
    , demands(std::move(_demands))
    , maxChainSize(static_cast<int>(std::max_element(demands.begin(), demands.end(),
              [&](const Demand& _d1, const Demand& _d2) {
                  return _d1.functions.size() < _d2.functions.size();
              })
                           ->functions.size()))
    , NFVNodes(std::move(std::move(_NFVNodes)))
    , NFVIndices([&]() {
        std::vector<int> tmp(network.getOrder(), -1);
        for (int i = 0; i < NFVNodes.size(); ++i) {
            tmp[NFVNodes[i]] = i;
        }
        return tmp;
    }())
    , isNFV([&]() {
        std::vector<bool> tmp(network.getOrder(), false);
        for (const auto& u : NFVNodes) {
            tmp[u] = true;
        }
        return tmp;
    }())
    , nbCores([&]() {
        Matrix<double> mat(static_cast<int>(demands.size()), maxChainSize, -1);
        for (int i = 0; i < demands.size(); ++i) {
            for (int j = 0; j < demands[i].functions.size(); ++j) {
                mat(i, j) =
                    ceil(demands[i].d / funcCharge[demands[i].functions[j]]);
            }
        }
        return mat;
    }())
    , edgeToId([&]() {
        Matrix<int> edgeToId_(network.getOrder(), network.getOrder(), -1);
        int i = 0;
        for (Graph::Edge edge : edges) {
            edgeToId_(edge.first, edge.second) = i;
            ++i;
        }
        return edgeToId_;
    }()) {}

// void save(const std::vector<int>& _nodeCapa, const std::string& _filename) {
//     std::ofstream ofs(_filename);
//     for (int i = 0; i < _nodeCapa.size(); ++i) {
//         ofs << i << '\t' << _nodeCapa[i] << '\n';
//     }
// }

// void save(const std::vector<double>& _funcCharge, const std::string&
// _filename) {
//     std::ofstream ofs(_filename);
//     for (function_descriptor f = 0; f < _funcCharge.size(); ++f) {
//         ofs << f << '\t' << _funcCharge[f] << '\n';
//     }
// }

// void save(const WeightedDiGraph& _graph, const std::string& _filename) {
//     const auto& weight = get(boost::edge_weight, _graph);
//     std::ofstream ofs(_filename);
//     ofs << num_vertices(_graph) << '\n';
//     for (auto[ite, end] = edges(_graph); ite != end; ++ite) {
//         ofs << source(*ite, _graph) << '\t' << target(*ite, _graph) << '\t'
//         << get(weight, *ite) << '\n';
//     }
// }

// void save(const std::vector<Demand>& _demands, const std::vector<double>&
// _nodeResReq, const std::string& _filename) {
//     std::ofstream ofs(_filename);
//     printContainer(ofs, _nodeResReq.begin(), _nodeResReq.end(), "\t") <<
//     '\n'; for (const auto & [ s, t, d, functions ] : _demands) {
//         ofs << s << '\t' << t << '\t' << d;
//         for (const auto& f : functions) {
//             ofs << '\t' << f;
//         }
//         ofs << '\n';
//     }
// }

// bool operator==(const ServicePath& _sp1, const ServicePath& _sp2) {
//     return _sp1.nPath == _sp2.nPath && _sp1.locations == _sp2.locations &&
//     _sp1.demand == _sp2.demand;
// }

// std::vector<Graph::Node> loadNFV(const std::string& _filename,
//     const int _nbSDNs, const int _type) {
//     std::ifstream ifs(_filename, std::ifstream::in);
//     if(!ifs) {
//         std::cerr << _filename << " does not exists!" << std::endl;
//         throw std::ios_base::failure(_filename + " does not exists!");
//     }
//     std::string line;
//     std::getline(ifs, line, '\n');
//     std::stringstream lineStream(line);

//     int order, u = 0;
//     lineStream >> order;
//     std::vector<Graph::Node> SDNs(_nbSDNs, 0);
//     assert(order >= _nbSDNs);
//     for (int i = 0; i < _nbSDNs; ++i) {
//         line = "";
//         std::getline(ifs, line);
//         if(!line.empty()) {
//             std::stringstream lineStream1(line);
//             for(int j = 0; j < _type; ++j) {
//                 lineStream1 >> u;
//             }
//             lineStream1 >> u;
//             SDNs[i] = u;
//         }
//     }
//     return SDNs;
// }

// std::vector<int> loadNodeCapa(const std::string& _filename,
//     const double _percent) {
//     std::cout << _filename << '\n';
//     std::vector<int> nodeCapas;
//     std::ifstream ifs(_filename, std::ifstream::in);
//     if (!ifs) {
//         std::cerr << _filename << " does not exists!\n";
//         exit(-1);
//     } else {
//         std::cout << "Opening :" << _filename << '\n';
//     }

//     while (ifs.good()) {
//         std::string line;
//         std::getline(ifs, line);
//         if (!line.empty()) {
//             int u, capa;
//             std::stringstream lineStream(line);
//             lineStream >> u >> capa;
//             nodeCapas.emplace_back(capa * _percent);
//         }
//     }
//     return nodeCapas;
// }

// std::pair<std::vector<Demand>, std::vector<double>> loadDemands(const
// std::string& _filename,
//     const double _percent) {
//     std::cout << _filename << '\n';
//     std::ifstream ifs(_filename, std::ifstream::in);
//     if (!ifs) {
//         std::cerr << _filename << " does not exists!\n";
//         exit(-1);
//     } else {
//         std::cout << "Opening :" << _filename << '\n';
//     }

//     // for(auto ite = std::istream_iterator<double>(ifs))
//     std::string line;
//     std::getline(ifs, line);
//     std::stringstream funcLineStream(line);
//     // First line is function requirements
//     std::vector<double> funcReq;
//     while (funcLineStream) {
//         funcLineStream >> funcReq.emplace_back();
//     }
//     // Get demands
//     std::vector<Demand> demands;
//     while (ifs) {
//         std::getline(ifs, line);
//         std::stringstream demandStream(line);
//         if (!line.empty() && demandStream.good()) {
//             Graph::Node s, t;
//             double d;
//             demandStream >> s >> t >> d;
//             std::vector<function_descriptor> functions;
//             while (demandStream) {
//                 demandStream >> functions.emplace_back();
//             }
//             demands.emplace_back(s, t, d * _percent, functions);
//         }
//     }
//     return {demands, funcReq};
// }

struct WeightedEdge {
    Graph::Node u;
    Graph::Node v;
    double capa;
};

// std::istream& operator>>(std::istream& _in, WeightedEdge& _wEdge) {
//     return _in >> _wEdge.u >> _wEdge.v >> _wEdge.capa;
// }

// WeightedDiGraph loadNetwork(const std::string& _filename) {
//     std::cout << "Loading network from " << _filename << '\n';
//     std::ifstream ifs(_filename, std::ifstream::in);
//     if (!ifs) {
//         throw std::runtime_error("Invalid ifstream for " + _filename);
//     }
//     int order;
//     ifs >> order;
//     WeightedDiGraph graph(order);

//     // std::vector<Edge> edges;
//     // std::vector<int> linkCapa;
//     int e = 0;
//     for (std::istream_iterator<WeightedEdge> ite(ifs), end; ite != end;
//     ++ite) {
//         assert(ite->u < order);
//         assert(ite->v < order);
//         assert(ite->capa > 0);
//         add_edge(ite->u, ite->v,
//             WeightedDiGraph::edge_property_type(ite->capa,
//                 boost::property<boost::edge_index_t, int>(e++)),
//             graph);
//     }
//     return graph;
// }

std::vector<double> loadFunctions(const std::string& _filename) {
    std::cout << _filename << '\n';
    std::ifstream ifs(_filename, std::ifstream::in);
    if (!ifs) {
        std::cerr << _filename << " does not exists!\n";
        exit(-1);
    }
    std::vector<double> functions;
    while (ifs.good()) {
        std::string line;
        std::getline(ifs, line);
        if (!line.empty()) {
            function_descriptor f;
            double cpu;
            std::stringstream lineStream(line);
            lineStream >> f >> cpu;
            functions.push_back(cpu);
        }
    }
    return functions;
}

// std::ostream& operator<<(std::ostream& _out, const ServicePath& _sPath) {
//     _out << "{nPath: [";
//     printContainer(_out, _sPath.nPath.begin(), _sPath.nPath.end());
//     _out << "], locations: [";
//     printContainer(_out, _sPath.locations.begin(), _sPath.locations.end());
//     return _out << "],  demand #: " << _sPath.demand << '}';
// }

bool operator==(const Demand& _d1, const Demand& _d2) {
    return std::make_tuple(_d1.s, _d1.t, _d1.functions) == std::make_tuple(_d2.s, _d2.t, _d2.functions);
}

// Instance::Instance(const UncapacitatedInstance& _uinst, WeightedDiGraph
// _network,
//     std::vector<int> _nodeCapa)
//     : network(std::move(_network))
//     , nodeCapa(std::move(_nodeCapa))
//     , funcCharge(_uinst.funcCharge)
//     , demands(_uinst.demands)
//     , maxChainSize(_uinst.maxChainSize)
//     , NFVNodes(_uinst.NFVNodes)
//     , NFVIndices(_uinst.NFVIndices)
//     , isNFV(_uinst.isNFV)
//     , nbCores(_uinst.nbCores) {}

// Instance::Instance(WeightedDiGraph _network, std::vector<int> _nodeCapa,
//     std::vector<Demand> _demands,
//     std::vector<double> _funcCharge, std::vector<Graph::Node> _NFVNodes)
//     : network(std::move(_network))
//     , nodeCapa(std::move(_nodeCapa))
//     , funcCharge(std::move(_funcCharge))
//     , demands(std::move(_demands))
//     , maxChainSize(std::max_element(demands.begin(), demands.end(),
//           [&](const Demand& _d1, const Demand& _d2) {
//               return _d1.functions.size() < _d2.functions.size();
//           })
//                        ->functions.size())
//     , NFVNodes(std::move(_NFVNodes))
//     , NFVIndices([&]() {
//         std::vector<int> retval(num_vertices(network), -1);
//         for (int i = 0; i < NFVNodes.size(); ++i) {
//             retval[NFVNodes[i]] = i;
//         }
//         return retval;
//     }())
//     , isNFV([&]() {
//         std::vector<bool> retval(num_vertices(network), false);
//         for (const auto& u : NFVNodes) {
//             retval[u] = true;
//         }
//         return retval;
//     }())
//     , nbCores([&]() {
//         boost::multi_array<double, 2>
//         retval(boost::extents[demands.size()][maxChainSize]); for (int i = 0;
//         i < demands.size(); ++i) {
//             for (int j = 0; j < demands[i].functions.size(); ++j) {
//                 retval[i][j] =
//                     ceil(demands[i].d / funcCharge[demands[i].functions[j]]);
//             }
//         }
//         return retval;
//     }()) {}

// int UncapacitatedInstance::getNbCores(const int _demandID, const int _j)
// const {
//     assert(_demandID < demands.size());
//     assert(_j < demands[_demandID].functions.size());
//     assert(_j < maxChainSize);
//     return nbCores[_demandID][_j];
// }

// int Instance::getNbCores(const int _demandID, const int _j) const {
//     assert(_demandID < demands.size());
//     assert(_j < demands[_demandID].functions.size());
//     return nbCores[_demandID][_j];
// }

// UncapacitatedInstance::UncapacitatedInstance(WeightedDiGraph _network,
//     std::vector<Demand> _demands,
//     std::vector<double> _funcCharge, std::vector<Graph::Node> _NFVNodes)
//     : network(std::move(_network))
//     , funcCharge(std::move(_funcCharge))
//     , demands(std::move(_demands))
//     , maxChainSize(std::max_element(demands.begin(), demands.end(),
//           [&](const Demand& _d1, const Demand& _d2) {
//               return _d1.functions.size() < _d2.functions.size();
//           })
//                        ->functions.size())
//     , NFVNodes(std::move(_NFVNodes))
//     , NFVIndices([&]() {
//         std::vector<int> retval(num_vertices(network), -1);
//         for (int i = 0; i < NFVNodes.size(); ++i) {
//             retval[NFVNodes[i]] = i;
//         }
//         return retval;
//     }())
//     , isNFV([&]() {
//         std::vector<bool> retval(num_vertices(network), false);
//         for (const auto& u : NFVNodes) {
//             retval[u] = true;
//         }
//         return retval;
//     }())
//     , nbCores([&]() {
//         boost::multi_array<double, 2>
//         retval(boost::extents[demands.size()][maxChainSize]); for (int i = 0;
//         i < demands.size(); ++i) {
//             for (int j = 0; j < demands[i].functions.size(); ++j) {
//                 retval[i][j] =
//                     ceil(demands[i].d / funcCharge[demands[i].functions[j]]);
//             }
//         }
//         return retval;
//     }()) {}

// double Instance::getLinkCapacity(Edge _edge) const {
//     return get(get(boost::edge_weight, network), edge(_edge.first,
//     _edge.second, network).first);
// }

// double Instance::getLinkCapacity(WeightedDiGraph::edge_descriptor _ed) const
// {
//     return get(get(boost::edge_weight, network), _ed);
// }

// int Instance::getEdgeId(Graph::Node _u, Graph::Node _v) const noexcept {
//     return get(get(boost::edge_index, network), edge(_u, _v, network).first);
// }

// int Instance::getEdgeId(WeightedDiGraph::edge_descriptor _ed) const noexcept
// {
//     return get(boost::edge_index, network, _ed);
// }

// int UncapacitatedInstance::getEdgeId(Graph::Node _u, Graph::Node _v) const
// noexcept {
//     return get(get(boost::edge_index, network), edge(_u, _v, network).first);
// }

// int UncapacitatedInstance::getEdgeId(WeightedDiGraph::edge_descriptor _ed)
// const noexcept {
//     return get(boost::edge_index, network, _ed);
// }

// void checkNetworkUsage(const Instance& _inst,
//     const std::vector<ServicePath>& _sPaths) {
//     const auto ru = getResourceUsage(_inst, _sPaths);
//     std::cout << "Graph::Node usage: \n";
//     for (const auto& u : _inst.NFVNodes) {
//         std::cout << '\t' << u << " -> " << ru.nodeUsage[u] << " / "
//                   << _inst.nodeCapa[u] << '\n';
//         assert(ru.nodeUsage[u] <= _inst.nodeCapa[u]);
//     }
//     std::cout << "Link usage: \n";
//     int i = 0;
//     for (auto[ite, end] = edges(_inst.network); ite != end; ++ite) {
//         const auto ed = *ite;
//         std::cout << std::fixed << '\t' << ed << " -> " << ru.linkUsage[i]
//                   << " / " << _inst.getLinkCapacity(ed) << '\n';
//         assert([&]() {
//             if (_inst.getLinkCapacity(ed) - ru.linkUsage[i] < -1.0e-6) {
//                 std::cerr << std::fixed << ed << '\n'
//                           << _inst.getLinkCapacity(ed) - ru.linkUsage[i]
//                           << '\n';
//                 return false;
//             }
//             return true;
//         }());
//         ++i;
//     }
// }

// boost::multi_array<double, 2> getDistance(const WeightedDiGraph& _graph) {
//     boost::multi_array<double, 2>
//     distance(boost::extents[num_vertices(_graph)][num_vertices(_graph)]);
//     boost::johnson_all_pairs_shortest_paths(_graph, distance);
//     return distance;
// }

// namespace OccLim {
// Instance::Instance(WeightedDiGraph _network, std::vector<int> _nodeCapa,
//     std::vector<Demand> _demands,
//     std::vector<double> _funcCharge, std::vector<Graph::Node> _NFVNodes,
//     int _nbLicenses)
//     : SFC::Instance(std::move(_network), std::move(_nodeCapa),
//           std::move(_demands), std::move(_funcCharge),
//           std::move(_NFVNodes))
//     , nbLicenses(_nbLicenses) {}
// } // namespace OccLim
} // namespace SFC
