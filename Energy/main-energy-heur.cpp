#include <MyTimer.h>
#include <tclap/CmdLine.h>

#include "Energy/Chaining.hpp"
#include "Energy/Heuristic.hpp"

std::string getDemandFile(const std::string& name, int _i);
std::vector<Demand> loadDemands(const std::string& _filename, double _percent);
std::vector<int> loadNodeCapa(const std::string& _filename, const double _percent = 1.0);

DiGraph loadNetwork(const std::string& _filename);
std::vector<double> loadFunctions(const std::string& _filename);
void saveNodeCapas(const std::vector<int>& _nodeCapas, const std::string& _filename);
void saveTopo(const DiGraph& _graph, const std::string& _filename);

struct Param {
    std::string model;
    std::string name;
    double factor;
    int funcCoef;
    double percentDemand;
};

Param getParams(int argc, char** argv) {
    TCLAP::CmdLine cmd("Solve the SFC problem with column generation", ' ', "1.0");

    TCLAP::ValuesConstraint<std::string> vCons({"heur"});
    TCLAP::ValueArg<std::string> modelName("m", "model", "Specify the model used",
        false, "heur", &vCons);
    cmd.add(&modelName);

    TCLAP::UnlabeledValueArg<std::string> networkName("network", "Specify the network name",
        true, "", "network");
    cmd.add(&networkName);

    TCLAP::ValueArg<double> demandFactor("d", "demandFactor", "Specify the multiplicative factor for the demands",
        false, 1, "int");
    cmd.add(&demandFactor);

    TCLAP::ValueArg<int> functionChages("c", "functionChages", "Specify the index for the function charge file",
        false, 4, "int");
    cmd.add(&functionChages);

    TCLAP::ValueArg<double> percentDemand("p", "percentDemand", "Specify the percentage of demand used",
        false, -1, "double");
    cmd.add(&percentDemand);

    cmd.parse(argc, argv);

    return {modelName.getValue(), networkName.getValue(), demandFactor.getValue(),
        functionChages.getValue(), percentDemand.getValue()};
}

int main(int argc, char** argv) {
    std::cout << std::fixed;
    try {
        const auto params = getParams(argc, argv);
        std::string folderName = "./instances/energy/";
        DiGraph network = loadNetwork(folderName + params.name + "_topo.txt");
        std::vector<Demand> allDemands = loadDemands(folderName + params.name + "_demand.txt", params.factor);
        std::vector<double> funcCharge = loadFunctions(folderName + "func" + std::to_string(params.funcCoef) + ".txt");
        std::vector<int> nodeCapas = loadNodeCapa(folderName + params.name + "_nodeCapa.txt");
        std::cout << "Files loaded...\n";

        std::vector<Demand> demands;
        const int nbDemands = std::ceil(allDemands.size() * params.percentDemand / 100.0);
        if (params.percentDemand != -1) {
            demands = std::vector<Demand>(allDemands.begin(), allDemands.begin() + nbDemands);
        } else {
            demands = allDemands;
        }
        // srand(time(NULL));
        // std::random_shuffle(demands.begin(), demands.end());

        double totalNbCores = 0;
        for (int i(0); i < int(demands.size()); ++i) {
            const auto& funcs = std::get<3>(demands[i]);

            for (int j(0); j < int(funcs.size()); ++j) {
                totalNbCores += std::get<2>(demands[i]) / funcCharge[funcs[j]];
            }
        }
        std::cout << totalNbCores << " needed cores!" << '\n';

        std::vector<int> NFVNodes;
        for (Graph::Node u(0); u < network.getOrder(); ++u) {
            if (nodeCapas[u] != -1) {
                NFVNodes.push_back(u);
            }
        }
        int availableCores = 0;
        for (const auto& u : NFVNodes) {
            availableCores += nodeCapas[u];
        }
        std::cout << availableCores << " cores available !\n";

        std::cout << std::accumulate(demands.begin(), demands.end(), 0.0,
                         [&](const double& _bandwidth, const Demand& _demand) {
                             return _bandwidth + std::get<2>(_demand);
                         })
                  << " total bandwidth!\n";
        Time timer;
        timer.start();
        std::cout << "Getting initial configuration...\n";

        Heuristic heur = solveEESFC(network, demands, funcCharge, NFVNodes, nodeCapas);
        if (heur.getState() == Heuristic::State::Solved) {
            if (params.percentDemand == -1) {
                heur.save("./results/energy/" + params.name + "_" + params.model + "_" + std::to_string(params.factor) + "_" + std::to_string(params.funcCoef) + ".res",
                    timer.show(), network);
            } else {
                heur.save("./results/energy/" + params.name + "_" + std::to_string(nbDemands) + "_" + params.model + "_" + std::to_string(params.factor) + "_" + std::to_string(params.funcCoef) + ".res",
                    timer.show(), network);
            }
        } else {
            std::cout << "Proble is infeasible" << std::endl;
        }

    } catch (const TCLAP::ArgException& e) {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << '\n';
    }
    return 1;
}