#include "MyTimer.hpp"
#include "tclap/CmdLine.hpp"

#include "GenCut.hpp"

struct Param {
    // std::string model;
    std::string name;
    double factor;
    // int funcCoef;
    // double percentDemand;
};

Param getParams(int argc, char** argv) {
    TCLAP::CmdLine cmd("Solve the SFC problem with column generation", ' ', "1.0");

    // TCLAP::ValuesConstraint<std::string> vCons({ "path", "pathOne", "subpath", "pathArray", "pathOcc", "set1", "set2", "set3", "pathThread", "pathThreadBF" });
    // TCLAP::ValueArg<std::string> modelName("m", "model", "Specify the model used",
    //     false, "path", &vCons);
    // cmd.add(&modelName);

    TCLAP::UnlabeledValueArg<std::string> networkName("network", "Specify the network name",
        true, "", "network");
    cmd.add(&networkName);

    TCLAP::ValueArg<double> demandFactor("d", "demandFactor", "Specify the multiplicative factor for the demands",
        false, 1, "int");
    cmd.add(&demandFactor);

    cmd.parse(argc, argv);

    return {
        // modelName.getValue(),
        networkName.getValue(), demandFactor.getValue(),
        // functionChages.getValue(), percentDemand.getValue()
    };
}

int main(int argc, char** argv) {
    std::cout << std::fixed;
    try {
        auto params = getParams(argc, argv);

        Energy::EARInstance instance(loadNetwork("./instances/" + params.name + "_topo.txt"), loadDemands("./instances/" + params.name + "_demand.txt", params.factor));
        Time timer;
        timer.start();
        Energy::GenCut genCut(instance);
        std::vector<double> outDemands(instance.network.getOrder());
        for (const auto& demand : instance.demands) {
            outDemands[std::get<0>(demand)] += std::get<2>(demand);
        }

        for (Graph::Node u = 0; u < instance.network.getOrder(); ++u) {
            auto neighbors = instance.network.getNeighbors(u);
            std::sort(neighbors.begin(), neighbors.end(), [&](const Graph::Node& __v1, const Graph::Node& __v2) {
                return instance.network.getEdgeWeight(u, __v1) > instance.network.getEdgeWeight(u, __v2);
            });
            std::size_t i = 0;
            while (outDemands[u] > 0) {
                outDemands[u] -= instance.network.getEdgeWeight(u, neighbors[i]);
                ++i;
            }
            Energy::GenCut::Column col;
            for (const auto& v : neighbors) {
                int realE = u < v ? instance.edgeID(u, v) : instance.edgeID(v, u);
                col.edgeIDs.push_back(realE);
            }
            col.minEdge = i;
            genCut.addColumn(col);
        }
        genCut.solve();
    } catch (IloException& e) {
        std::cout << e.getMessage() << '\n';
    } catch (const TCLAP::ArgException& e) {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << '\n';
    }
    return 1;
}
