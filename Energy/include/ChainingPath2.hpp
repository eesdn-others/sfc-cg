#ifndef ENERGY_CHAINING_PATH2_HP
#define ENERGY_CHAINING_PATH2_HP

#include <chrono>
#include <cmath>
#include <ilcplex/ilocplex.h>
#include <iostream>
#include <map>
#include <thread>
#include <tuple>

#include "GenCut.hpp"
#include "SFC.hpp"
#include "ShortestPath.hpp"
#include "ShortestPathBF.hpp"
#include "cplex_utility.hpp"
#include "utility.hpp"

namespace SFC::Energy {

/*
* CG-cuts+ formulation
*/
class ChainPlacementPath2 {
    class ReducedMainProblem {
        friend ChainPlacementPath2;

      public:
        explicit ReducedMainProblem(const ChainPlacementPath2& _placement);

        ~ReducedMainProblem();

        ReducedMainProblem& operator=(const ReducedMainProblem&) = delete;
        ReducedMainProblem(const ReducedMainProblem&) = delete;
        bool checkReducedCosts() const;
        void addPath(const ServicePath& _sPath, const int _i);
        double solveInteger();
        void getNetworkUsage() const;
        void getDuals();
        bool solve();
        double getObjValue() const;

      private:
        const ChainPlacementPath2& m_placement;
        IloEnv m_env{};
        IloModel m_model;

        IloNumVarArray m_y;
        IloNumVarArray m_x;
        IloNumVarArray m_k;

        IloObjective m_obj;

        IloRangeArray m_onePathCons;
        IloRangeArray m_linkCapasCons;
        IloRangeArray m_linkUsableCons;
        IloRangeArray m_nodeCapasCons1;
        IloRangeArray m_nodeCapasCons2;

        IloNumArray m_linkCapaDuals;
        IloNumArray m_linkUsableDuals;
        IloNumArray m_inst.nodeCapaDuals;
        IloNumArray m_onePathDuals;

        // Cuts
        IloRange m_inst.networkCutCons;
        IloRangeArray m_neighborhoodCutsIn;

        IloNumArray m_valsLinkCapa;
        IloNumArray m_valsLinkUsable;
        IloNumArray m_valsNode;
        IloRangeArray m_linkUsableConsPerDemand;

        IloCplex m_solver;

        std::vector<std::pair<ServicePath, int>> m_paths;
    };

    class PricingProblem {
        friend ChainPlacementPath2;

      public:
        explicit PricingProblem(const ChainPlacementPath2& _chainPlacement);
        ~PricingProblem();

        bool solve();
        double getObjValue() const;

        void updateModel(const int _i);
        void updateDual(const std::size_t _i);
        ServicePath getServicePath();

      private:
        const ChainPlacementPath2& m_placement;
        IloEnv m_env{};

        int m_demandID;
        const std::size_t n;
        const std::size_t m;
        const std::size_t nbLayers;
        IloModel m_model;

        IloObjective m_obj;

        IloNumVarArray m_a;
        IloNumVarArray m_f;
        IloNumVarArray m_x;

        IloRangeArray m_flowConsCons;
        IloRangeArray m_usableLink;
        IloRangeArray m_linkCapaCons;
        IloRangeArray m_nodeCapaCons;

        IloRangeArray m_noLoopCons1;
        IloRangeArray m_noLoopCons2;

        IloCplex m_solver;

        ServicePath m_servicePath;

        int getIndexF(const int _e, const int _j) const;
        int getIndexA(const int _u, const int _j) const;
    };

  public:
    explicit ChainPlacementPath2(Instance& _inst);
    ChainPlacementPath2(const ChainPlacementPath2&) = default;
    ChainPlacementPath2& operator=(const ChainPlacementPath2&) = default;
    ChainPlacementPath2(ChainPlacementPath2&&) = default;
    ChainPlacementPath2& operator=(ChainPlacementPath2&&) = default;
    ~ChainPlacementPath2() = default;

    void addInitConf(const std::vector<ServicePath>& _paths);
    void addInitConf(const std::vector<std::pair<ServicePath, int>>& _paths);
    std::vector<std::pair<ServicePath, int>> getPaths() const;
    void save(const std::string& _filename, const std::pair<double, double>& _time);
    bool solveInteger();
    bool solve();
    double getObjValue() const;

  private:
    Instance const* m_inst;

    double m_intObj;
    double m_fractObj;

    ReducedMainProblem m_mainProblem;
    ThreadPool m_pool;
    std::map<std::thread::id, std::size_t> m_threadID;
    std::vector<PricingProblem> m_pps;

    friend ReducedMainProblem;
};

ChainPlacementPath2::ChainPlacementPath2(Instance& _inst)
    : m_inst(_inst)
    , m_intObj(-1)
    , m_fractObj(-1)
    , m_mainProblem(*this)
    , m_pool(std::max(1u, std::thread::hardware_concurrency()))
    // , m_pool(1)
    , m_threadID([&]() {
        std::map<std::thread::id, std::size_t> pps;
        std::size_t i = 0;
        for (const auto& id : m_pool.getIds()) {
            pps.emplace(id, i);
            ++i;
        }
        return pps;
    }())
    , m_pps([&]() {
        std::vector<PricingProblem> pps;
        pps.reserve(m_pool.size());
        for (std::size_t i = 0; i < m_pool.size(); ++i) {
            pps.emplace_back(*this);
        }
        return pps;
    }()) {}

void ChainPlacementPath2::addInitConf(const std::vector<ServicePath>& _paths) {
    std::cout << "Adding initial configuration...." << std::flush;
    std::size_t i = 0;
    for (const auto& path : _paths) {
        m_mainProblem.addPath(path, i);
        ++i;
    }
    std::cout << "done\n";
}

void ChainPlacementPath2::addInitConf(const std::vector<std::pair<ServicePath, int>>& _paths) {
    std::cout << "Adding initial configuration...." << std::flush;
    for (const auto& path : _paths) {
        m_mainProblem.addPath(path.first, path.second);
    }
    std::cout << "done\n";
}

std::vector<std::pair<ServicePath, int>> ChainPlacementPath2::getPaths() const {
    return m_mainProblem.m_paths;
}

void ChainPlacementPath2::save(const std::string& _filename, const std::pair<double, double>& _time) {
    std::ofstream ofs(_filename);
    ofs << m_intObj << '\t' << m_fractObj << '\n';
    ofs << m_mainProblem.m_paths.size() << std::endl;
    ofs << _time.first << '\t' << _time.second << std::endl;

    std::vector<double> linkUsage(m_inst.network.size(), 0);
    std::vector<double> nodeUsage(m_inst.network.getOrder(), 0);
    std::vector<int> pathsUsed(m_inst.demands.size());

    for (std::size_t i = 0; i < m_mainProblem.m_paths.size(); ++i) {
        if (m_mainProblem.m_solver.getValue(m_mainProblem.m_y[i]) != 0) {
            const auto& path = m_mainProblem.m_paths[i].first.first;
            const auto& funcPlacement = m_mainProblem.m_paths[i].first.second;
            const auto& demand = m_inst.demands[m_mainProblem.m_paths[i].second];
            pathsUsed[m_mainProblem.m_paths[i].second] = i;

            for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
                linkUsage[m_inst.edgeID(*iteU, *iteV)] += demand.d * m_mainProblem.m_solver.getValue(m_mainProblem.m_y[i]);
            }
            std::size_t j = 0;
            for (const auto& pair_NodeFuncs : funcPlacement) {
                for (const std::size_t nbFunc = pair_NodeFuncs.second.size() + j; j < nbFunc; ++j) {
                    nodeUsage[pair_NodeFuncs.first] += m_mainProblem.m_solver.getValue(m_mainProblem.m_y[i]) * m_inst.nbCores(m_mainProblem.m_paths[i].second, j);
                }
            }
        }
    }

    ofs << m_inst.network.getOrder() << '\n';
    for (Graph::Node u = 0; u < m_inst.network.getOrder(); ++u) {
        ofs << u << '\t' << int(std::ceil(nodeUsage[u])) << '\t' << m_inst.nodeCapa[u] << '\n';
    }

    ofs << m_inst.network.size() << '\n';
    std::size_t i = 0;
    for (const auto& edge : m_inst.edges) {
        ofs << edge.first << '\t' << edge.second << '\t' << linkUsage[i] << '\t' << m_inst.network.getEdgeWeight(edge);
        int xIndex;
        if (edge.first < edge.second) {
            xIndex = i;
        } else {
            xIndex = m_inst.edgeID(edge.second, edge.first);
        }
        ofs << '\t' << m_mainProblem.m_solver.getValue(m_mainProblem.m_x[xIndex]) << '\n';
        ++i;
    }

    ofs << pathsUsed.size() << '\n';
    for (const auto& index : pathsUsed) {
        const auto& sPath = m_mainProblem.m_paths[index].first;
        ofs << sPath.first.size() << '\t';
        for (const auto& u : sPath.first) {
            ofs << u << '\t';
        }
        for (const auto& pair_NodeFuncs : sPath.second) {
            for (const auto& func : pair_NodeFuncs.second) {
                ofs << pair_NodeFuncs.first << '\t' << func << '\t';
            }
        }
        ofs << '\n';
    }
    std::cout << "Results saved to :" << _filename << '\n';
}

bool ChainPlacementPath2::solveInteger() {
    if (!solve()) {
        return false;
    }
    m_intObj = m_mainProblem.solveInteger();
    return m_intObj != -1;
}

bool ChainPlacementPath2::solve() {
    bool foundNewColumn;
    std::vector<std::future<std::pair<ServicePath, std::size_t>>> futures;
    futures.reserve(m_inst.demands.size());
    do {
        foundNewColumn = false;
        futures.clear();
        if (m_mainProblem.solve()) {
            m_fractObj = m_mainProblem.getObjValue();
            std::cout << "Reduced Main Problem Objective Value = " << m_fractObj << '\n';
            m_mainProblem.getDuals();
            assert(m_mainProblem.checkReducedCosts());
            std::cout << "Searching for paths..." << std::flush;
            for (std::size_t i = 0;
                 i < m_inst.demands.size();
                 ++i) {
                futures.emplace_back(m_pool.enqueue([&, this, i]() {
                    auto& pp = m_pps[m_threadID.at(std::this_thread::get_id())];
                    pp.updateDual(i);
                    if (pp.solve() && pp.getObjValue() < -Energy::RC_EPS) {
                        return std::make_pair(pp.getServicePath(), i);
                    }
                    return std::make_pair(ServicePath(), i);
                }));
            }
            std::cout << "getting paths..." << std::flush;
            for (auto& fut : futures) {
                auto sPath = fut.get();
                if (!sPath.first.first.empty()) {
                    foundNewColumn = true;
                    m_mainProblem.addPath(sPath.first, sPath.second);
                }
            }
            std::cout << "done!\n";
        } else {
            m_mainProblem.m_solver.exportModel("rm.lp");
            std::cout << "No solution found for RMP" << '\n';
        }
    } while (foundNewColumn);
    return m_fractObj != -1;
}

double ChainPlacementPath2::getObjValue() const {
    return m_mainProblem.getObjValue();
}

//####################################################################################################################
//####################################################################################################################
//####################################################################################################################
//####################################################################################################################
//####################################################################################################################

ChainPlacementPath2::PricingProblem::PricingProblem(const ChainPlacementPath2& _chainPlacement)
    : m_placement(_chainPlacement)
    , m_env()
    , m_demandID(0)
    , n(m_placement.m_inst.network.getOrder())
    , m(m_placement.m_inst.network.size())
    , nbLayers(m_placement.m_inst.maxChainSize + 1)
    , m_model(m_env)
    , m_obj(IloAdd(m_model, IloMinimize(m_env)))
    , m_a([&]() {
        IloNumVarArray a(m_env, m_placement.m_inst.NFVNodes.size() * (nbLayers - 1));
        for (const auto u : m_placement.m_inst.NFVNodes) {
            for (std::size_t j = 0; j < nbLayers - 1)
                ;
            ++j;
            {
                a[getIndexA(u, j)] = IloAdd(m_model,
                    IloNumVar(m_env, 0, 1, ILOBOOL));
                setIloName(a[getIndexA(u, j)], "a" + toString(std::make_tuple(j, u)));
            }
        }
        return a;
    }())
    , m_f([&]() {
        IloNumVarArray f(m_env, m * nbLayers, 0, 1, ILOBOOL);
        for (std::size_t e = 0; e < m; ++e) {
            for (std::size_t j = 0; j < nbLayers; ++j) {
                setIloName(f[getIndexF(e, j)], "f" + toString(std::make_tuple(m_placement.m_inst.edges[e], j)));
            }
        }
        return IloAdd(m_model, f);
    }())
    , m_x([&]() {
        IloNumVarArray x(m_env, m, 0, 1, ILOBOOL);
        for (std::size_t e = 0; e < m; ++e) {
            setIloName(x[e], "x(" + toString(m_placement.m_inst.edges[e]) + ')');
        }
        return IloAdd(m_model, x);
    }())
    , m_flowConsCons([&]() {
        IloRangeArray flowConsCons(m_env, n * nbLayers, 0.0, 0.0);
        for (Graph::Node u = 0; u < m_placement.m_inst.network.getOrder(); ++u) {
            for (std::size_t j = 0; j < nbLayers; ++j) {
                IloNumVarArray vars(m_env);
                IloNumArray vals(m_env);
                for (const auto v : m_placement.m_inst.network.getNeighbors(u)) {
                    vars.add(m_f[getIndexF(m_placement.m_inst.edgeID(u, v), j)]);
                    vals.add(1.0);
                    vars.add(m_f[getIndexF(m_placement.m_inst.edgeID(v, u), j)]);
                    vals.add(-1.0);
                }
                if (m_placement.m_inst.isNFV[u]) {
                    if (j > 0) {
                        vars.add(m_a[getIndexA(u, j - 1)]);
                        vals.add(-1.0);
                    }
                    if (j < nbLayers - 1) {
                        vars.add(m_a[getIndexA(u, j)]);
                        vals.add(1.0);
                    }
                }
                flowConsCons[n * j + u] = IloAdd(m_model,
                    IloRange(m_env, 0.0, IloScalProd(vars, vals), 0.0));
                vars.end();
                vals.end();
            }
        }
        return flowConsCons;
    }())
    , m_usableLink([&]() {
        IloRangeArray usableLink(m_env, m);
        for (std::size_t e = 0; e < m; ++e) {
            const auto& edge = m_placement.m_inst.edges[e];
            if (edge.first < edge.second) {
                IloNumVarArray vars(m_env);
                IloNumArray vals(m_env);
                for (std::size_t j = 0; j < m_placement.m_inst.maxChainSize + 1; ++j) {
                    vars.add(m_f[getIndexF(e, j)]);
                    vars.add(m_f[getIndexF(m_placement.m_inst.edgeID(edge.second, edge.first), j)]);
                    vals.add(-1.0);
                    vals.add(-1.0);
                }
                vars.add(m_x[e]);
                vals.add(vals.getSize());
                usableLink[e] = IloRange(m_env, 0.0, IloScalProd(vars, vals), vars.getSize() - 2);
                // std::cout << usableLink[e] << '\n';

                vars.end();
                vals.end();
            }
        }
        return IloAdd(m_model, usableLink);
    }())
    , m_linkCapaCons([&]() {
        IloRangeArray linkCapaCons(m_env, m, 0.0, 0.0);
        for (int e = 0; e < m_placement.m_inst.edges.size())
            ;
        ++e;
        {
            linkCapaCons[e].setUB(m_placement.m_inst.network.getEdgeWeight(m_placement.m_inst.edges[e]));
            setIloName(linkCapaCons[e], "linkCapaCons(" + toString(e) + ")");
        }
        return IloAdd(m_model, linkCapaCons);
    }())
    , m_nodeCapaCons([&]() {
        IloRangeArray nodeCapaCons(m_env, n);
        for (const auto u : m_placement.m_inst.NFVNodes) {
            nodeCapaCons[u] = IloAdd(m_model,
                IloRange(m_env, 0.0, m_placement.m_inst.nodeCapa[u]));
            setIloName(nodeCapaCons[u], "nodeCapaCons(" + std::to_string(u) + ")");
        }
        return nodeCapaCons;
    }())
    , m_noLoopCons1([&]() {
        IloRangeArray noLoopCons(m_env, n * nbLayers);
        for (Graph::Node u = 0; u < m_placement.m_inst.network.getOrder(); ++u) {
            for (std::size_t j = 0; j < nbLayers; ++j) {
                IloNumVarArray vars(m_env);
                for (const auto v : m_placement.m_inst.network.getNeighbors(u)) {
                    vars.add(m_f[getIndexF(m_placement.m_inst.edgeID(u, v), j)]);
                }
                if (j < nbLayers - 1) {
                    vars.add(m_a[getIndexA(u, j)]);
                }
                noLoopCons[n * j + u] = IloAdd(m_model, IloRange(m_env, 0.0, IloSum(vars), 1.0));
            }
        }
        return noLoopCons;
    }())
    , m_noLoopCons2([&]() {
        IloRangeArray noLoopCons(m_env, n * nbLayers);
        for (Graph::Node u = 0; u < m_placement.m_inst.network.getOrder(); ++u) {
            for (std::size_t j = 0; j < nbLayers; ++j) {
                IloNumVarArray vars(m_env);
                for (const auto v : m_placement.m_inst.network.getNeighbors(u)) {
                    vars.add(m_f[getIndexF(m_placement.m_inst.edgeID(v, u), j)]);
                }
                if (j > 0) {
                    vars.add(m_a[getIndexA(u, j - 1)]);
                }
                noLoopCons[n * j + u] = IloAdd(m_model, IloRange(m_env, 0.0, IloSum(vars), 1.0));
            }
        }
        return noLoopCons;
    }())
    , m_solver([&]() {
        IloCplex solver(m_model);
        solver.setParam(IloCplex::Threads, 1);
        solver.setParam(IloCplex::EpRHS, 1e-9);
        solver.setOut(m_env.getNullStream());
        solver.setWarning(m_env.getNullStream());
        return solver;
    }())
    , m_servicePath() {
    m_env.setNormalizer(false);
    // m_solver.addLazyConstraints(m_noLoopCons1);
    // m_solver.addLazyConstraints(m_noLoopCons2);
}

ChainPlacementPath2::PricingProblem::~PricingProblem() {
    m_env.end();
}

int ChainPlacementPath2::PricingProblem::getIndexF(const int _e, const int _j) const {
    assert(0 <= _e && _e < m_placement.m_inst.network.size());
    assert(0 <= _j && _j < nbLayers);
    return m * _j + _e;
}

int ChainPlacementPath2::PricingProblem::getIndexA(const int _u, const int _j) const {
    assert(0 <= _u && _u < m_placement.m_inst.network.getOrder());
    assert(0 <= _j && _j < nbLayers);
    assert(m_placement.m_inst.NFVIndices[_u] != -1);
    return m_placement.m_inst.NFVNodes.size() * _j + m_placement.m_inst.NFVIndices[_u];
}

void ChainPlacementPath2::PricingProblem::updateModel(const int _i) {
    int nbFunctions = m_placement.m_inst.demands[m_demandID].functions.size();
    m_flowConsCons[n * 0 + m_placement.m_inst.demands[m_demandID].s].setBounds(0.0, 0.0);
    m_flowConsCons[n * nbFunctions + m_placement.m_inst.demands[m_demandID].t].setBounds(0.0, 0.0);

    m_demandID = _i;
    nbFunctions = m_placement.m_inst.demands[m_demandID].functions.size();
    m_flowConsCons[n * 0 + m_placement.m_inst.demands[m_demandID].s].setBounds(1.0, 1.0);
    m_flowConsCons[n * nbFunctions + m_placement.m_inst.demands[m_demandID].t].setBounds(-1.0, -1.0);

    for (int e = 0; e < m_placement.m_inst.edges.size())
        ;
    ++e;
    {
        IloNumVarArray vars(m_env);
        IloNumArray vals(m_env);

        for (std::size_t j = 0; j <= nbFunctions)
            ;
        ++j;
        {
            vars.add(m_f[getIndexF(e, j)]);
            vals.add(m_placement.m_inst.demands[m_demandID].d);
        }
        for (int j = nbFunctions + 1; j < nbLayers; ++j) {
            vars.add(m_f[getIndexF(e, j)]);
            vals.add(0);
        }
        m_linkCapaCons[e].setLinearCoefs(vars, vals);

        vars.end();
        vals.end();
    }

    for (const auto u : m_placement.m_inst.NFVNodes) {
        IloNumVarArray vars(m_env);
        IloNumArray vals(m_env);

        for (std::size_t j = 0; j < nbFunctions)
            ;
        ++j;
        {
            vars.add(m_a[getIndexA(u, j)]);
            vals.add(m_placement.m_inst.nbCores(m_demandID, j));
        }
        for (int j = nbFunctions; j < nbLayers - 1)
            ;
        ++j;
        {
            vars.add(m_a[getIndexA(u, j)]);
            vals.add(0);
        }
        m_nodeCapaCons[u].setLinearCoefs(vars, vals);
        setIloName(m_nodeCapaCons[u], "m_nodeCapaCons(" + std::to_string(u) + ")");

        vars.end();
        vals.end();
    }
}

void ChainPlacementPath2::PricingProblem::updateDual(const std::size_t _i) {
    updateModel(_i);
    const auto& d = m_placement.m_inst.demands[m_demandID].d;
    const auto& functions = m_placement.m_inst.demands[m_demandID].functions;
    const int nbFunctions = m_placement.m_inst.demands[m_demandID].functions.size();

    IloNumVarArray vars(m_env, m / 2 + m * nbLayers + m_placement.m_inst.NFVNodes.size() * nbFunctions);
    IloNumArray vals(m_env, m / 2 + m * nbLayers + m_placement.m_inst.NFVNodes.size() * nbFunctions);
    int index = 0;
    for (std::size_t e = 0; e < m; ++e) {
        if (m_placement.m_inst.edges[e].first < m_placement.m_inst.edges[e].second) {
            vars[index] = m_x[e];
            vals[index] = -m_placement.m_mainProblem.m_linkUsableDuals[m_placement.m_inst.network.size() * m_demandID + e];
            ++index;
        }
    }

    for (std::size_t e = 0; e < m; ++e) {
        for (std::size_t j = 0; j <= nbFunctions; ++j) {
            vars[index] = m_f[getIndexF(e, j)];
            vals[index] = m_placement.m_inst.energyCons.propLink * d / m_placement.m_inst.network.getEdgeWeight(m_placement.m_inst.edges[e])
                          - d * m_placement.m_mainProblem.m_linkCapaDuals[e];
            ++index;
        }
    }

    for (const auto u : m_placement.m_inst.NFVNodes) {
        for (std::size_t j = 0; j < functions.size())
            ;
        ++j;
        {
            vars[index] = m_a[getIndexA(u, j)];
            vals[index] = -m_placement.m_mainProblem.m_inst.nodeCapaDuals[m_placement.m_inst.NFVIndices[u]] * m_placement.m_inst.nbCores(m_demandID, j);
            ++index;
        }
        for (int j = functions.size(); j < nbLayers - 1)
            ;
        ++j;
        {
            vars[index] = m_a[getIndexA(u, j)];
            vals[index] = 0;
            ++index;
        }
    }
    m_obj.setConstant(-m_placement.m_mainProblem.m_onePathDuals[m_demandID]);
    m_obj.setLinearCoefs(vars, vals);
    m_solver.extract(m_model);

    vars.end();
    vals.end();
}

ServicePath ChainPlacementPath2::PricingProblem::getServicePath() {
    return m_servicePath;
}

bool ChainPlacementPath2::PricingProblem::solve() {
    if (!m_solver.solve()) {
        return false;
    }
    if (m_solver.getObjValue() > -Energy::RC_EPS) {
        return true;
    }
    const auto& funcs = m_placement.m_inst.demands[m_demandID].functions;
    const auto& t = m_placement.m_inst.demands[m_demandID].t;
    std::vector<std::pair<Graph::Node, std::vector<FunctionDescriptor>>> installs;

    std::vector<IloNumVarArray> cycles;
    IloNumArray aVal(m_env);
    IloNumArray fVal(m_env);
    IloNumArray xVal(m_env);
    Graph::Path path;
    do {
        cycles.clear();
        path.clear();
        installs.clear();

        m_solver.getValues(aVal, m_a);
        m_solver.getValues(fVal, m_f);
        m_solver.getValues(xVal, m_x);

        // std::cout << "Func placement:";
        // for (const auto u : m_placement.m_inst.NFVNodes) {
        //     for (std::size_t j = 0; j < nbLayers - 1); ++j; {
        //         if(aVal[getIndexA(u, j)]) {
        //             std::cout << u << ", " << j << " -> " << - m_placement.m_mainProblem.m_inst.nodeCapaDuals[m_placement.m_inst.NFVIndices[u]] * m_placement.m_inst.nbCores(m_demandID, j) << '\t';
        //         }
        //     }
        // }
        // std::cout << '\n';

        // std::cout << "Link flows:";
        // for (std::size_t e = 0; e < m; ++e) {
        //     for (std::size_t j = 0; j < nbLayers; ++j) {
        //         if(fVal[getIndexF(e, j)] > Energy::RC_EPS) {
        //             std::cout << m_placement.m_inst.edges[e] << ", " << j
        //             << " -> " << m_placement.m_inst.energyCons.propLink * m_placement.m_inst.demands[m_demandID].d / m_placement.m_inst.network.getEdgeWeight(m_placement.m_inst.edges[e])
        //                   - m_placement.m_inst.demands[m_demandID].d * m_placement.m_mainProblem.m_linkCapaDuals[e] << '\t';
        //         }
        //     }
        // }
        // std::cout << '\n';

        // std::cout << "Link state:";
        // for(int e = 0; e < m_placement.m_inst.network.size(); ++e) {
        //     if(xVal[e] > Energy::RC_EPS) {
        //         std::cout << m_placement.m_inst.edges[e] << " -> " << m_placement.m_mainProblem.m_linkUsableDuals[m_placement.m_inst.network.size() * m_demandID + e] << '\t';
        //     }
        // }
        // std::cout << '\n';

        // Rebuild the path
        Graph::Node u = m_placement.m_inst.demands[m_demandID].s;
        path.push_back(u);
        std::size_t j = 0;

        while (u != t || j < funcs.size())
            ;
        {
            if (j < int(funcs.size()) && m_placement.m_inst.isNFV[u] && aVal[getIndexA(u, j)] > Energy::RC_EPS) {
                if (installs.empty() || installs.back().first != u) {
                    installs.emplace_back(u, std::vector<FunctionDescriptor>());
                }
                installs.back().second.push_back(funcs[j]);
                ++j;
            } else {
                for (const auto v : m_placement.m_inst.network.getNeighbors(u)) {
                    if (fVal[getIndexF(m_placement.m_inst.edgeID(u, v), j)] > Energy::RC_EPS) {
                        fVal[getIndexF(m_placement.m_inst.edgeID(u, v), j)] = 0;
                        xVal[m_placement.m_inst.edgeID(u, v)] = 0;
                        xVal[m_placement.m_inst.edgeID(v, u)] = 0;
                        path.push_back(v);
                        u = v;
                        break;
                    }
                }
            }
        }
        assert(path.back() == t);

        // Check for cycles

        // for(int e = 0; e < m_placement.m_inst.network.size(); ++e) {
        //     if(fVal[getIndexF(e, j)] > Energy::RC_EPS) {
        //         std::cout << m_placement.m_inst.edges[e] << ", " << j  << " -> " << m_placement.m_mainProblem.m_linkUsableDuals[m_placement.m_inst.network.size() * m_demandID + e] << '\t';
        //     }
        // }
        // std::cout <<  '\n';

        for (std::size_t j = 0; j < nbLayers; ++j) {
            for (int e = 0; e < m_placement.m_inst.network.size(); ++e) {
                if (m_placement.m_inst.edges[e].first < m_placement.m_inst.edges[e].second) {
                    if (fVal[getIndexF(e, j)] > Energy::RC_EPS) {
                        // std::cout << "Found a cycle starting from " << m_placement.m_inst.edges[e].first << " for demand " << m_placement.m_inst.demands[m_demandID] << '\n';
                        fVal[getIndexF(e, j)] = 0;
                        cycles.emplace_back(m_env);
                        cycles.back().add(m_f[getIndexF(e, j)]);
                        Graph::Node u = m_placement.m_inst.edges[e].second;
                        const Graph::Node t = m_placement.m_inst.edges[e].first;
                        while (u != t) {
                            // std::cout << u << "\t";
                            for (const auto v : m_placement.m_inst.network.getNeighbors(u)) {
                                if (fVal[getIndexF(m_placement.m_inst.edgeID(u, v), j)] > Energy::RC_EPS) {
                                    fVal[getIndexF(m_placement.m_inst.edgeID(u, v), j)] = 0;
                                    cycles.back().add(m_f[getIndexF(m_placement.m_inst.edgeID(u, v), j)]);
                                    u = v;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        if (!cycles.empty()) {
            DBOUT(std::cout << "Found " << cycles.size() << " cycles "
                            << " for demand " << m_placement.m_inst.demands[m_demandID] << '\n';)
            for (const auto& cycle : cycles) {
                IloRange rng(m_env, 0.0, IloSum(cycle), cycle.getSize() - 1);
                // std::cout << rng << std::endl;
                m_model.add(rng);
            }
            // std::cout << "Resolving...\n";
            if (!m_solver.solve()) {
                return false;
            }
            if (m_solver.getObjValue() > -Energy::RC_EPS) {
                return true;
            }
        }

    } while (!cycles.empty());

    // std::cout << '\n' << ServicePath(path, installs) << " -> " << getObjValue() << '\n';
    // for(std::size_t i = 0; i < aVal.getSize()); ++i; {
    //     if(aVal[i]) {
    //         std::cout << m_a[i].getName() << '(' << aVal[i] << ")\t";
    //     }
    // }
    // std::cout << '\n';
    // for(std::size_t i = 0; i < fVal.getSize()); ++i; {
    //     if(fVal[i] > Energy::RC_EPS) {
    //         std::cout << m_f[i].getName() << '(' << fVal[i] << ")\t";
    //     }
    // }
    // std::cout << '\n';
    // for(std::size_t i = 0; i < xVal.getSize()); ++i; {
    //     if(xVal[i]) {
    //         std::cout << m_x[i].getName() << '(' << xVal[i] << ")\t";
    //     }
    // }
    // std::cout << '\n';
    aVal.end();
    xVal.end();
    fVal.end();
    // std::cout << m_placement.m_inst.demands[m_demandID] << " -> " << ServicePath(path, installs) << '\n';
    m_servicePath = ServicePath(path, installs);
    return true;
}

double ChainPlacementPath2::PricingProblem::getObjValue() const {
    return m_solver.getObjValue();
}

//####################################################################################################################
//####################################################################################################################
//####################################################################################################################
//####################################################################################################################
//####################################################################################################################

ChainPlacementPath2::ReducedMainProblem::ReducedMainProblem(const ChainPlacementPath2& _placement)
    : m_placement(_placement)
    , m_env()
    , m_model(m_env)
    , m_y(IloAdd(m_model, IloNumVarArray(m_env)))
    , m_x([&]() {
        IloNumVarArray x(m_env, m_placement.m_inst.network.size());
        int e = 0;
        for (const Graph::Edge& edge : m_placement.m_inst.edges) {
            if (edge.first < edge.second) {
                x[e] = IloAdd(m_model, IloNumVar(m_env, 0.0, 1.0));
                setIloName(x[e], "x" + toString(edge));
            }
            ++e;
        }
        return x;
    }())
    , m_k([&]() {
        IloNumVarArray k(m_env, m_placement.m_inst.NFVNodes.size());
        for (const auto u : m_placement.m_inst.NFVNodes) {
            k[m_placement.m_inst.NFVIndices[u]] = IloNumVar(m_env, -IloInfinity, m_placement.m_inst.nodeCapa[u]);
            setIloName(k[m_placement.m_inst.NFVIndices[u]], "k(" + toString(u) + ")");
        }
        return IloAdd(m_model, k);
    }())
    , m_obj([&]() {
        IloNumVarArray vars(m_env);
        IloNumArray vals(m_env);
        int e = 0;
        for (const Graph::Edge& edge : m_placement.m_inst.edges) {
            if (edge.first < edge.second) {
                vars.add(m_x[e]);
                vals.add(2 * m_placement.m_inst.energyCons.activeLink);
            }
            ++e;
        }
        for (const auto u : m_placement.m_inst.NFVNodes) {
            vars.add(m_k[m_placement.m_inst.NFVIndices[u]]);
            vals.add(m_placement.m_inst.energyCons.activeCore);
        }
        auto obj = IloAdd(m_model, IloMinimize(m_env, IloScalProd(vars, vals)));
        vars.end();
        vals.end();
        return obj;
    }())
    , m_onePathCons([&]() {
        IloRangeArray onePathCons(m_env, m_placement.m_inst.demands.size(), 1.0, IloInfinity);
        for (std::size_t i = 0; i < m_placement.m_inst.demands.size(); ++i) {
            setIloName(onePathCons[i], "onePathCons" + toString(m_placement.m_inst.demands[i]));
        }
        return IloAdd(m_model, onePathCons);
    }())
    , m_linkCapasCons([&]() {
        IloRangeArray linkCapaCons(m_env, m_placement.m_inst.network.size(), 0.0, IloInfinity);
        int e = 0;
        for (const Graph::Edge& edge : m_placement.m_inst.edges) {
            linkCapaCons[e].setUB(m_placement.m_inst.network.getEdgeWeight(edge));
            setIloName(linkCapaCons[e], "linkCapas2" + toString(edge));
            ++e;
        }
        return IloAdd(m_model, linkCapaCons);
    }())
    , m_linkUsableCons([&]() {
        IloRangeArray linkUsableCons(m_env, m_placement.m_inst.network.size() * m_placement.m_inst.demands.size(), -IloInfinity, 0.0);
        int e = 0;
        for (const auto& edge : m_placement.m_inst.edges) {
            const int realE = edge.first < edge.second ? e : m_placement.m_inst.edgeID(edge.second, edge.first);
            for (std::size_t i = 0; i < m_placement.m_inst.demands.size(); ++i) {
                linkUsableCons[m_placement.m_inst.network.size() * i + realE].setLinearCoef(m_x[realE], -1.0);
                setIloName(linkUsableCons[m_placement.m_inst.network.size() * i + realE], "linkUsableCons" + toString(std::make_pair(i, e)));
            }
            ++e;
        }
        return IloAdd(m_model, linkUsableCons);
    }())
    , m_nodeCapasCons1([&]() {
        IloRangeArray nodeCapasCons(m_env, m_placement.m_inst.NFVNodes.size());
        for (const auto u : m_placement.m_inst.NFVNodes) {
            const int i = m_placement.m_inst.NFVIndices[u];
            nodeCapasCons[i] = IloAdd(m_model, IloRange(m_env, -IloInfinity, -m_k[i], 0.0));
            setIloName(nodeCapasCons[i], "nodeCapasCons1" + std::to_string(u));
        }
        return nodeCapasCons;
    }())
    , m_nodeCapasCons2([&]() {
        IloRangeArray nodeCapasCons(m_env, m_placement.m_inst.NFVNodes.size());
        for (const auto u : m_placement.m_inst.NFVNodes) {
            const int i = m_placement.m_inst.NFVIndices[u];
            nodeCapasCons[i] = IloAdd(m_model, IloRange(m_env, 0.0, m_k[i], m_placement.m_inst.nodeCapa[u]));
            setIloName(nodeCapasCons[i], "nodeCapasCons2" + std::to_string(u));
        }
        return nodeCapasCons;
    }())
    , m_linkCapaDuals(m_env)
    , m_linkUsableDuals(m_env, m_placement.m_inst.network.size() * m_placement.m_inst.demands.size())
    , m_inst.nodeCapaDuals(m_env)
    , m_onePathDuals(m_env)
    , m_inst.networkCutCons([&]() {
        IloNumVarArray vars(m_env);
        IloNumArray vals(m_env);
        int e = 0;
        for (const Graph::Edge& edge : m_placement.m_inst.edges) {
            if (edge.first < edge.second) {
                vars.add(m_x[e]);
                vals.add(1.0);
            }
            ++e;
        }
        auto cut = IloAdd(m_model, IloRange(m_env,
                                       m_placement.m_inst.network.getOrder() - 1, IloScalProd(vars, vals), m_placement.m_inst.network.size() / 2,
                                       "networkCutCons"));
        vars.end();
        vals.end();
        return cut;
    }())
    , m_neighborhoodCutsIn([&]() {
        IloRangeArray neighborhoodCutsIn(m_env, m_placement.m_inst.network.getOrder());

        for (Graph::Node u = 0; u < m_placement.m_inst.network.getOrder(); ++u) {
            IloNumVarArray vars(m_env);
            IloNumArray vals(m_env);

            for (const auto v : m_placement.m_inst.network.getNeighbors(u)) {
                if (v < u) {
                    vars.add(m_x[m_placement.m_inst.edgeID(v, u)]);
                } else {
                    vars.add(m_x[m_placement.m_inst.edgeID(u, v)]);
                }
                vals.add(1.0);
            }
            int traffic = m_placement.m_inst.traffic[u].first;
            int nbEdges = 0;
            for (const auto& edge : m_placement.m_inst.sortedNeighbors[u]) {
                traffic -= m_placement.m_inst.network.getEdgeWeight(edge);
                ++nbEdges;
                if (traffic <= 0) {
                    break;
                }
            }
            m_neighborhoodCutsIn[u] = IloAdd(m_model,
                IloRange(m_env, nbEdges, IloScalProd(vars, vals), IloInfinity));

            vars.end();
            vals.end();
        }
        return neighborhoodCutsIn;
    }())
    , m_valsLinkCapa(m_env, m_placement.m_inst.network.size())
    , m_valsLinkUsable(m_env, m_placement.m_inst.network.size())
    , m_valsNode(m_env, m_placement.m_inst.NFVNodes.size())
    , m_linkUsableConsPerDemand(m_env, m_placement.m_inst.network.size())
    , m_solver([&]() {
        IloCplex solver(m_model);
        // solver.setParam(IloCplex::EpGap, 0.005);
        // solver.setParam(IloCplex::VarSel, 3);
        // solver.setParam(IloCplex::Threads, 0);
        solver.setParam(IloCplex::EpRHS, 1e-9);
        // solver.setParam(IloCplex::RootAlg, 4);
        solver.setParam(IloCplex::BarCrossAlg, -1);
        solver.setOut(m_env.getNullStream());
        return solver;
    }())
    , m_paths() {
    m_env.setNormalizer(false);
}

ChainPlacementPath2::ReducedMainProblem::~ReducedMainProblem() {
    m_env.end();
}

bool ChainPlacementPath2::ReducedMainProblem::checkReducedCosts() const {
    std::vector<double> linkUsage(m_placement.m_inst.network.size());
    std::vector<double> linkCapa(m_placement.m_inst.network.size());
    std::vector<double> nodeCapa(m_placement.m_inst.network.getOrder());

    for (int k = 0; k < m_paths.size())
        ;
    ++k;
    {
        std::fill(linkUsage.begin(), linkUsage.end(), 0.0);
        std::fill(linkCapa.begin(), linkCapa.end(), 0.0);
        std::fill(nodeCapa.begin(), nodeCapa.end(), 0.0);
        const int i = m_paths[k].second;

        for (auto iteU = m_paths[k].first.first.begin(), iteV = std::next(iteU); iteV != m_paths[k].first.first.end(); ++iteU, ++iteV) {
            const int e = m_placement.m_inst.edgeID(*iteU, *iteV);
            const int realE = *iteU < *iteV ? e : m_placement.m_inst.edgeID(*iteV, *iteU);
            linkCapa[e] += m_placement.m_inst.energyCons.propLink * m_placement.m_inst.demands[i].d / m_placement.m_inst.network.getEdgeWeight(*iteU, *iteV)
                           - m_placement.m_inst.demands[i].d * m_linkCapaDuals[e];
            linkUsage[realE] = -m_linkUsableDuals[m_placement.m_inst.network.size() * i + realE];
        }
        std::size_t j = 0;
        for (const auto& pair_NodeFuncs : m_paths[k].first.second) {
            for (const int nbFunc = pair_NodeFuncs.second.size() + j; j < nbFunc; ++j) {
                const int u = pair_NodeFuncs.first;
                nodeCapa[m_placement.m_inst.NFVIndices[u]] += -m_inst.nodeCapaDuals[m_placement.m_inst.NFVIndices[u]] * m_placement.m_inst.nbCores(i, j);
            }
        }

        double myRC = -m_onePathDuals[i]
                      + std::accumulate(linkUsage.begin(), linkUsage.end(), 0.0)
                      + std::accumulate(linkCapa.begin(), linkCapa.end(), 0.0)
                      + std::accumulate(nodeCapa.begin(), nodeCapa.end(), 0.0);

        // std::cout << "Reduced cost of " << m_paths[k] << " is " << myRC << ' ' << m_solver.getReducedCost(m_y[k]) << '\n'
        //         << -m_onePathDuals[i] << '\n'
        //         << linkUsage << '\n'
        //         << linkCapa << '\n'
        //         << nodeCapa << '\n';
        if (std::abs(myRC - m_solver.getReducedCost(m_y[k])) > Energy::RC_EPS) {
            return false;
        }
    }
    return true;
}

void ChainPlacementPath2::ReducedMainProblem::addPath(const ServicePath& _sPath, const int _i) {
    const auto pair = std::make_pair(_sPath, _i);
    assert([&]() {
        const auto ite = std::find(m_paths.begin(), m_paths.end(), pair);
        if (ite == m_paths.end()) {
            return true;
        } else {
            std::cout << pair << " is already present!" << '\n';
            return false;
        }
    }());

    assert(_sPath.first.front() == m_placement.m_inst.demands[_i].s);
    assert(_sPath.first.back() == m_placement.m_inst.demands[_i].t);

    double objCoef = 0.0;

    // For each link in the _sPath.first, add to the corresponding constraints (link capa & link usage)
    for (std::size_t e = 0; e < m_placement.m_inst.network.size(); ++e) {
        m_valsLinkCapa[e] = 0;
        m_valsLinkUsable[e] = 0;
        m_linkUsableConsPerDemand[e] = m_linkUsableCons[m_placement.m_inst.network.size() * _i + e];
    }

    for (auto iteU = _sPath.first.begin(), iteV = std::next(iteU); iteV != _sPath.first.end(); ++iteU, ++iteV) {
        const int e = m_placement.m_inst.edgeID(*iteU, *iteV);
        const int realE = *iteU < *iteV ? e : m_placement.m_inst.edgeID(*iteV, *iteU);
        m_valsLinkCapa[e] += m_placement.m_inst.demands[_i].d;
        m_valsLinkUsable[realE] = 1.0;
        objCoef += m_placement.m_inst.energyCons.propLink * m_placement.m_inst.demands[_i].d / m_placement.m_inst.network.getEdgeWeight(*iteU, *iteV);
    }

    for (std::size_t i = 0; i < m_valsNode.getSize())
        ;
    ++i;
    {
        m_valsNode[i] = 0;
    }

    std::size_t j = 0;
    for (const auto& pair_NodeFuncs : _sPath.second) {
        for (const std::size_t nbFunc = pair_NodeFuncs.second.size() + j; j < nbFunc; ++j) {
            assert(0 <= pair_NodeFuncs.first && pair_NodeFuncs.first < m_placement.m_inst.network.getOrder());
            assert(std::find(_sPath.first.begin(), _sPath.first.end(), pair_NodeFuncs.first) != _sPath.first.end());
            m_valsNode[m_placement.m_inst.NFVIndices[pair_NodeFuncs.first]] += m_placement.m_inst.nbCores(_i, j);
        }
    }

    m_y.add(
        m_onePathCons[_i](1.0)
        + m_linkCapasCons(m_valsLinkCapa)
        + m_linkUsableConsPerDemand(m_valsLinkUsable)
        + m_nodeCapasCons1(m_valsNode) + m_obj(objCoef));
    setIloName(m_y[m_y.getSize() - 1], "y" + toString(std::make_tuple(m_placement.m_inst.demands[_i], _sPath)));
    m_paths.emplace_back(_sPath, _i);
}

double ChainPlacementPath2::ReducedMainProblem::solveInteger() {
    m_solver.setOut(std::cout);
    m_model.add(IloConversion(m_env, m_y, ILOBOOL));
    m_model.add(IloConversion(m_env, m_k, ILOINT));

    int e = 0;
    for (const Graph::Edge& edge : m_placement.m_inst.edges) {
        if (edge.first < edge.second) {
            m_model.add(IloConversion(m_env, m_x[e], ILOBOOL));
        }
        ++e;
    }

    if (m_solver.solve()) {
        auto obj = m_solver.getObjValue();
        std::cout << "Final obj value: " << obj << "\tFrac Obj Value: " << m_placement.m_fractObj
                  << "\t# Paths: " << m_paths.size() << '\n';
        getNetworkUsage();
        return obj;
    } else {
        m_solver.exportModel("mp.sav");
        return -1.0;
    }
}

void ChainPlacementPath2::ReducedMainProblem::getNetworkUsage() const {
    std::vector<double> linkUsage(m_placement.m_inst.network.size(), 0);
    std::vector<std::size_t> nodeUsage(m_placement.m_inst.network.getOrder(), 0);
    std::vector<IloNumVar> pathsUsed(m_placement.m_inst.demands.size(), nullptr);
    std::vector<int> active(m_placement.m_inst.network.size(), 0);

    for (std::size_t i = 0; i < m_paths.size(); ++i) {
        if (m_solver.getValue(m_y[i]) > RC_EPS) {
            const auto& path = m_paths[i].first.first;
            const auto& demand = m_placement.m_inst.demands[m_paths[i].second];
            const auto d = demand.d;
            pathsUsed[m_paths[i].second] = m_y[i];

            for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
                linkUsage[m_placement.m_inst.edgeID(*iteU, *iteV)] += d * m_solver.getValue(m_y[i]);
                active[m_placement.m_inst.edgeID(*iteU, *iteV)] = 1;
                active[m_placement.m_inst.edgeID(*iteV, *iteU)] = 1;
            }
        }
    }
    for (const auto u : m_placement.m_inst.NFVNodes) {
        nodeUsage[u] = m_solver.getValue(m_k[m_placement.m_inst.NFVIndices[u]]);
    }
    std::cout << "Node usage: \n";
    double nodeEnergy = 0;
    for (const auto u : m_placement.m_inst.NFVNodes) {
        nodeEnergy += nodeUsage[u] * m_placement.m_inst.energyCons.activeCore;
        std::cout << '\t' << u << " -> " << nodeUsage[u] << " / " << m_placement.m_inst.nodeCapa[u]
                  << " -> " << nodeUsage[u] * m_placement.m_inst.energyCons.activeCore << '\n';
        assert(nodeUsage[u] <= m_placement.m_inst.nodeCapa[u]);
    }
    std::cout << '\t' << nodeEnergy << '\n';
    double linkEnergy = 0.0;
    std::cout << "Link usage: \n";
    int e = 0;
    for (const auto& edge : m_placement.m_inst.edges) {
        int realE = edge.first < edge.second ? e : m_placement.m_inst.edgeID(edge.second, edge.first);
        double energy = m_placement.m_inst.energyCons.propLink * linkUsage[e] / m_placement.m_inst.network.getEdgeWeight(edge);

        if (active[e]) {
            energy += m_placement.m_inst.energyCons.activeLink;
        }
        linkEnergy += energy;
        assert(active[e] == m_solver.getValue(m_x[realE]));
        std::cout << '\t' << edge << " -> " << linkUsage[e] << " / " << m_placement.m_inst.network.getEdgeWeight(edge)
                  << " -> " << energy << " ->" << active[e] << "/" << bool(m_solver.getValue(m_x[realE])) << '\n';
        assert(m_placement.m_inst.network.getEdgeWeight(edge) - linkUsage[e] > -RC_EPS
               || (std::cerr << m_placement.m_inst.network.getEdgeWeight(edge) - linkUsage[e] << '\n', false));
        ++e;
    }
    std::cout << nodeEnergy << " + " << linkEnergy << " = " << nodeEnergy + linkEnergy << '\n';
}

void ChainPlacementPath2::ReducedMainProblem::getDuals() {
    m_solver.getDuals(m_linkUsableDuals, m_linkUsableCons);
    m_solver.getDuals(m_linkCapaDuals, m_linkCapasCons);
    m_solver.getDuals(m_inst.nodeCapaDuals, m_nodeCapasCons1);
    m_solver.getDuals(m_onePathDuals, m_onePathCons);
}

bool ChainPlacementPath2::ReducedMainProblem::solve() {
    return m_solver.solve();
}

double ChainPlacementPath2::ReducedMainProblem::getObjValue() const {
    return m_solver.getObjValue();
}
} // namespace SFC::Energy

#endif