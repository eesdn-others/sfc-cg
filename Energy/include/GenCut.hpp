#ifndef GEN_CUT_HPP
#define GEN_CUT_HPP

#include <ilcplex/ilocplex.h>

#include "ShortestPath.hpp"
#include "cplex_utility.hpp"
#include "utility.hpp"
#include <Matrix.hpp>

#include "Energy.h"
#include "SFC.hpp"

namespace SFC::Energy {

class GenCut {

  public:
    struct Column {
        Column()
            : edgeIDs()
            , minEdge() {}
        Column(const std::vector<int>& _edgeIDs, int _minEdge)
            : edgeIDs(_edgeIDs)
            , minEdge(_minEdge) {}
        std::vector<int> edgeIDs;
        int minEdge;
    };

  private:
    class ReducedMainProblem {
        friend GenCut;

      public:
        ReducedMainProblem(const GenCut& _placement)
            : m_placement(_placement)
            , m_env()
            , m_model(m_env)
            , m_y(IloAdd(m_model, IloNumVarArray(m_env)))
            , m_obj(IloAdd(m_model, IloMaximize(m_env)))
            , m_edgeConstraints(IloAdd(m_model, IloRangeArray(m_env, m_placement.m_instance.network.size(), 0.0, 1.0)))
            , m_solver([&]() {
                IloCplex solver(m_model);
                solver.setParam(IloCplex::Threads, 1);
                // #if defined(NDEBUG)
                solver.setOut(m_env.getNullStream());
                // #endif
                return solver;
            }())
            , m_columns() {}

        ~ReducedMainProblem() {
            m_env.end();
        }

        ReducedMainProblem& operator=(const ReducedMainProblem&) = delete;
        ReducedMainProblem(const ReducedMainProblem&) = delete;

        void checkReducedCosts() const {
            // for(std::size_t i = 0; i < m_columns); ++i; {
            // 	double cplexRC = m_solver.getReducedCost(m_y[i]);
            // }
        }

        void addColumn(const Column _col) {
#if !defined(NDEBUG)
            std::cout << "addColumn" << _col.edgeIDs << " >= " << _col.minEdge << '\n';
#endif
            assert([&]() {
                const auto ite = std::find(m_columns.begin(), m_columns.end(), _col);
                if (ite == m_columns.end()) {
                    return true;
                } else {
                    // std::cout << _col << " is already present!" << '\n';
                    return false;
                }
            }());

            IloNumColumn cplexCol(m_env);
            cplexCol += m_obj(_col.minEdge);
            for (const auto& edgeID : _col.edgeIDs) {
                cplexCol += m_edgeConstraints[edgeID](1.0);
            }
            m_y.add(IloNumVar(cplexCol));
            m_columns.push_back(_col);
        }

        double solveInteger() {
            // Change variable type
            m_model.add(IloConversion(m_env, m_y, ILOBOOL));

            if (m_solver.solve()) {
                auto obj = m_solver.getObjValue();
                std::cout << "Final obj value: " << obj << "\t# Columns: " << m_columns.size() << '\n';
                return obj;
            } else if (m_solver.getStatus() == IloAlgorithm::Infeasible
                       || m_solver.getStatus() == IloAlgorithm::InfeasibleOrUnbounded) {
                std::cout << '\n'
                          << "No solution - starting Conflict refinement" << '\n';

                IloConstraintArray infeas(m_env);
                IloNumArray preferences(m_env);

                // infeas.add(m_onePathCons);
                // for (IloInt i = 0; i < m_k.getSize(); ++i) {
                // 	if ( m_k[i].getType() != IloNumVar::Bool ) {
                // 		infeas.add(IloBound(m_k[i], IloBound::Lower));
                // 		infeas.add(IloBound(m_k[i], IloBound::Upper));
                // 	}
                // }

                // for (IloInt i = 0; i < infeas.getSize(); ++i) {
                // 	preferences.add(1.0);  // user may wish to assign unique preferences
                // }

                if (m_solver.refineConflict(infeas, preferences)) {
                    IloCplex::ConflictStatusArray conflict = m_solver.getConflict(infeas);
                    m_env.getImpl()->useDetailedDisplay(IloTrue);
                    std::cout << "Conflict :" << '\n';
                    for (IloInt i = 0; i < infeas.getSize(); ++i) {
                        if (conflict[i] == IloCplex::ConflictMember)
                            std::cout << "Proved  : " << infeas[i] << '\n';
                        if (conflict[i] == IloCplex::ConflictPossibleMember)
                            std::cout << "Possible: " << infeas[i] << '\n';
                    }
                } else {
                    std::cout << "Conflict could not be refined\n\n";
                }
            }
            return m_solver.getObjValue();
        }

        std::vector<Column> getCuts() const {
            return m_columns;
        }

      private:
        const GenCut& m_placement;
        IloEnv m_env{};
        IloModel m_model;

        IloNumVarArray m_y;

        IloObjective m_obj;

        IloRangeArray m_edgeConstraints;

        IloCplex m_solver;

        std::vector<Column> m_columns;
    };

    class PricingProblem {
        friend GenCut;

      public:
        PricingProblem(const GenCut& _genCut)
            : m_placement(_genCut)
            , m_env()
            , m_model(m_env)
            , m_x(IloAdd(m_model, IloNumVarArray(m_env, m_placement.m_instance.network.getOrder(), 0.0, 1.0, ILOBOOL)))
            , m_z(IloAdd(m_model, IloNumVarArray(m_env, m_placement.m_instance.demands.size(), 0.0, 1.0, ILOBOOL)))
            , m_e(IloAdd(m_model, IloNumVarArray(m_env, m_placement.m_instance.network.size(), 0.0, 1.0, ILOBOOL)))
            , m_minEdge(IloAdd(m_model, IloNumVarArray(m_env, m_placement.m_instance.network.size(), 0.0, 1.0, ILOBOOL)))
            , m_subMinEdge(IloAdd(m_model, IloNumVarArray(m_env, m_placement.m_instance.network.size(), 0.0, 1.0, ILOBOOL)))
            , m_obj([&]() {
                IloNumVarArray vars(m_env);
                IloNumArray vals(m_env);
                for (int e = 0; e < m_placement.m_instance.network.size(); ++e) {
                    vars.add(m_minEdge[e]);
                    vals.add(1);
                }
                return IloAdd(m_model, IloMaximize(m_env, IloScalProd(vars, vals)));
            }())
            , m_flowCons([&]() {
                IloRangeArray flowCons(m_env, m_placement.m_instance.demands.size());
                for (std::size_t i = 0; i < m_placement.m_instance.demands.size(); ++i) {
                    const auto& demand = m_placement.m_instance.demands[i];
                    flowCons[i] = IloRange(m_env, -1.0, m_x[demand.s] - m_x[demand.t] - 2 * m_z[i], 0.0);
                    setIloName(flowCons[i], "flowCons(" + std::to_string(i) + ")");
                }
                return IloAdd(m_model, flowCons);
            }())
            , m_cutCons([&]() {
                IloRangeArray cutCons(m_env, m_placement.m_instance.network.size());
                int e = 0;
                for (const auto& edge : m_placement.m_instance.network.getEdges()) {
                    cutCons[e] = IloRange(m_env, -1.0, m_x[edge.first] - m_x[edge.second] - 2 * m_e[e], 0.0);
                    ++e;
                }
                return IloAdd(m_model, cutCons);
            }())
            , m_minEdgesCons([&]() {
                IloNumVarArray vars(m_env);
                IloNumArray vals(m_env);
                for (std::size_t i = 0; i < m_placement.m_instance.demands.size(); ++i) {
                    vars.add(m_z[i]);
                    vals.add(-m_placement.m_instance.demands[i].d);
                }
                int e = 0;
                for (const auto& edge : m_placement.m_instance.network.getEdges()) {
                    vars.add(m_minEdge[e]);
                    vals.add(m_placement.m_instance.network.getEdgeWeight(edge));
                    ++e;
                }
                return IloAdd(m_model, IloRange(m_env, 0.0, IloScalProd(vars, vals), IloInfinity));
            }())
            , m_subMinEdgesCons([&]() {
                IloNumVarArray vars(m_env);
                IloNumArray vals(m_env);
                for (std::size_t i = 0; i < m_placement.m_instance.demands.size(); ++i) {
                    vars.add(m_z[i]);
                    vals.add(m_placement.m_instance.demands[i].d);
                }
                int e = 0;
                for (const auto& edge : m_placement.m_instance.network.getEdges()) {
                    vars.add(m_subMinEdge[e]);
                    vals.add(-m_placement.m_instance.network.getEdgeWeight(edge));
                    ++e;
                }
                return IloAdd(m_model, IloRange(m_env, RC_EPS, IloScalProd(vars, vals), IloInfinity));
            }())
            , m_sizeCons([&]() {
                IloNumVarArray vars(m_env);
                IloNumArray vals(m_env);

                for (int e = 0; e < m_placement.m_instance.network.size(); ++e) {
                    vars.add(m_subMinEdge[e]);
                    vals.add(-1.0);
                    vars.add(m_minEdge[e]);
                    vals.add(1.0);
                }
                return IloAdd(m_model, IloRange(m_env, 1.0, IloScalProd(vars, vals), 1.0));
            }())
            , m_usageMinCons([&]() {
                IloRangeArray usageMinCons(m_env, m_placement.m_instance.network.size());
                for (int e = 0; e < m_placement.m_instance.network.size(); ++e) {
                    usageMinCons[e] = IloRange(m_env, -IloInfinity, m_minEdge[e] - m_e[e], 0.0);
                }
                return IloAdd(m_model, usageMinCons);
            }())
            , m_usageSubMinCons([&]() {
                IloRangeArray usageMinCons(m_env, m_placement.m_instance.network.size());
                for (int e = 0; e < m_placement.m_instance.network.size(); ++e) {
                    usageMinCons[e] = IloRange(m_env, -IloInfinity, m_subMinEdge[e] - m_minEdge[e], 0.0);
                }
                return IloAdd(m_model, usageMinCons);
            }())
            , m_solver() {}

        ~PricingProblem() {
            m_env.end();
        }

        void updateDual(const IloNumArray& _edgeDuals) {
            IloNumVarArray vars(m_env);
            IloNumArray vals(m_env);
            int e = 0;
            for (const auto& edge : m_placement.m_instance.network.getEdges()) {
                int realE = edge.first < edge.second ? e : m_placement.m_instance.edgeID(edge.second, edge.first);
                vars.add(m_e[e]);
                vals.add(-_edgeDuals[realE]);
                ++e;
            }
            m_obj.setLinearCoefs(vars, vals);
            m_solver = IloCplex(m_model);
            // std::cout << m_obj << '\n';
            m_solver.setOut(m_env.getNullStream());
            m_solver.setParam(IloCplex::Threads, 1);
        }

        Column getColumn() const {
            IloNumArray eVal(m_env);
            m_solver.getValues(m_e, eVal);
            IloNumArray eMinVal(m_env);
            m_solver.getValues(m_minEdge, eMinVal);
            IloNumArray eSubMinVal(m_env);
            m_solver.getValues(m_subMinEdge, eSubMinVal);

            Column col{{}, 0};
            int e = 0;
            const auto& edges = m_placement.m_instance.network.getEdges();
            for (const auto& edge : edges) {
                int realE = edge.first < edge.second ? e : m_placement.m_instance.edgeID(edge.second, edge.first);
                if (eMinVal[e] > RC_EPS) {
                    ++col.minEdge;
                }
                if (eVal[e] > RC_EPS) {
                    col.edgeIDs.push_back(realE);
                }
                ++e;
            }

            IloNumArray zVal(m_env);
            m_solver.getValues(m_z, zVal);
            double totalFlow = 0.0;
            for (std::size_t i = 0; i < m_placement.m_instance.demands.size(); ++i) {
                if (zVal[i] > RC_EPS) {
                    totalFlow += m_placement.m_instance.demands[i].d;
                }
            }

            double totalCapa = 0.0;
            double totalCapaSub = 0.0;
            e = 0;
            for (const auto& edge : edges) {
                if (eMinVal[e] > RC_EPS) {
                    totalCapa += m_placement.m_instance.network.getEdgeWeight(edge);
                }
                if (eSubMinVal[e] > RC_EPS) {
                    totalCapaSub += m_placement.m_instance.network.getEdgeWeight(edge);
                }
                ++e;
            }
            std::cout << totalFlow << "/(" << totalCapa << ", " << totalCapaSub << ")\n";

            IloNumArray xVal(m_env);
            m_solver.getValues(m_x, xVal);

            double flow = 0.0;
            for (std::size_t i = 0; i < m_placement.m_instance.demands.size(); ++i) {
                const auto& demand = m_placement.m_instance.demands[i];
                const auto s = demand.s,
                           t = demand.t;
                if (int(xVal[s]) != int(xVal[t])) {
                    flow += demand.d;
                }
            }
            assert(flow == totalFlow);

            std::vector<Graph::Edge> cut;
            for (e = 0; e < edges.size(); ++e) {
                if (eVal[e] > RC_EPS) {
                    cut.push_back(edges[e]);
                }
            }
            std::sort(cut.begin(), cut.end(), [&](const Graph::Edge& __edge1, const Graph::Edge& __edge2) {
                return m_placement.m_instance.network.getEdgeWeight(__edge1) > m_placement.m_instance.network.getEdgeWeight(__edge2);
            });
            int minEdge = 0;
            while (flow > 0) {
                flow -= m_placement.m_instance.network.getEdgeWeight(cut[minEdge]);
                minEdge++;
            }
            assert(minEdge == col.minEdge);
            return col;
        }

        double getRealObjValue() const {
            return m_solver.getObjValue();
        }

      private:
        const GenCut& m_placement;
        IloEnv m_env{};

        IloModel m_model;

        // Variables
        IloNumVarArray m_x;
        IloNumVarArray m_z;
        IloNumVarArray m_e;
        IloNumVarArray m_minEdge;
        IloNumVarArray m_subMinEdge;

        IloObjective m_obj;

        // Constraints
        IloRangeArray m_flowCons;
        IloRangeArray m_cutCons;

        IloRange m_minEdgesCons;
        IloRange m_subMinEdgesCons;
        IloRange m_sizeCons;

        IloRangeArray m_usageMinCons;
        IloRangeArray m_usageSubMinCons;

        IloCplex m_solver;
    };

  public:
    GenCut(const Instance& _instance)
        : m_instance(_instance)
        , m_intObj(-1.0)
        , m_fractObj(-1.0)
        , m_mainProblem(*this)
        , m_pp(*this) {
    }

    std::vector<Column> getColumns() const {
        return m_mainProblem.m_columns;
    }

    void save(const std::string& _filename, const std::pair<double, double>& /*_time*/) {
        std::ofstream ofs(_filename);
        std::cout << "Results saved to :" << _filename << '\n';
    }

    void solveInteger() {
        m_mainProblem.m_solver.setOut(std::cout);
        m_intObj = m_mainProblem.solveInteger();
    }

    void addColumn(const Column& _col) {
        m_mainProblem.addColumn(_col);
    }

    std::vector<Column> getCuts() const {
        return m_mainProblem.getCuts();
    }

    void solve() {
        bool reducedCost;
        do {
            reducedCost = false;
            if (m_mainProblem.m_solver.solve()) {
                m_fractObj = m_mainProblem.m_solver.getObjValue();
                std::cout << "Reduced Main Problem Objective Value = " << m_fractObj << '\n';

                IloEnv env;
                IloNumArray edgeDuals(env);
                m_mainProblem.m_solver.getDuals(edgeDuals, m_mainProblem.m_edgeConstraints);
                // int e = 0;
                // for(const auto& edge : m_instance.network.getEdges()) {
                // 	std::cout << edge << " -> " << edgeDuals[e] << '\n';
                // 	++e;
                // }
                m_pp.updateDual(edgeDuals);

                if (m_pp.m_solver.solve()) {
                    const auto rc = m_pp.getRealObjValue();
                    if (rc > RC_EPS) {
                        reducedCost = true;
                        const auto col = m_pp.getColumn();
                        std::cout << "Found col with RC=" << rc << " -> " << col.edgeIDs << " >= " << col.minEdge << '\n';
                        m_mainProblem.addColumn(m_pp.getColumn());
                    }
                } else {
                    m_pp.m_solver.exportModel("pp.lp");
                }
            } else {
                m_mainProblem.m_solver.exportModel("rmp.lp");
                std::cout << "No solution found for RMP, exorting to rmp.lp" << '\n';
            }

        } while (reducedCost);
        m_intObj = m_mainProblem.solveInteger();
    }

  private:
    const Instance m_instance;

    double m_intObj;
    double m_fractObj;

    ReducedMainProblem m_mainProblem;
    PricingProblem m_pp;

    friend ReducedMainProblem;
};

bool operator==(const GenCut::Column& _col1, const GenCut::Column& _col2) {
    return _col1.minEdge == _col2.minEdge && _col1.edgeIDs == _col2.edgeIDs;
}
} // namespace SFC::Energy
#endif