#ifndef AUGMENTED_GRAPH_HPP
#define AUGMENTED_GRAPH_HPP

#include <map>

#include <DiGraph.hpp>
#include <Matrix.hpp>
#include <ShortestPath.hpp>
#include <utility.hpp>

#include "Energy.hpp"
#include "SFC.hpp"

namespace SFC::Energy {
class Heuristic {
  public:
    enum State {
        Solved,
        NotSolved,
        Infeasible
    };

    Heuristic(const Instance& _inst);
    Heuristic(const Heuristic& _other) = default;
    Heuristic& operator=(Heuristic& _other) = default;

    Heuristic(Heuristic&& _other) = default;
    Heuristic& operator=(Heuristic&& _other) = default;
    ~Heuristic() = default;

    void solve();
    void save(const std::string& _filename, const std::pair<double, double>& _time, const DiGraph& _network) const;
    bool getAllShortestPaths();
    bool placeFunctions();
    double getEnergyUsed() const;
    bool checkSolution() const;
    std::vector<Graph::Edge> getRemovablesEdges(const Matrix<char>& _isRemovable) const;
    void removeEdge(const Graph::Edge& _edge);
    void clear();
    std::vector<ServicePath> getSPath() const;
    State getState() const;

  private:
    // Problem Input
    Instance const* m_inst;

    // Problem data structures
    DiGraph m_flowGraph;
    std::vector<double> m_nodeUsage;
    ShortestPath<DiGraph> m_shortestPath;
    std::vector<Graph::Path> m_paths;
    Matrix<std::vector<int>> m_pathsOnEdge;
    std::vector<Graph::Node> m_chainLocations;
    std::map<std::vector<function_descriptor>, std::vector<int>> m_chainsToDemands;
    State m_state;
};

Heuristic::Heuristic(const Instance& _inst)
    : m_inst(&_inst)
    , m_flowGraph([&]() {
        DiGraph flowGraph(m_inst->network.getOrder());
        for (const auto& edge : m_inst->network.getEdges()) {
            flowGraph.addEdge(edge.first, edge.second, 0);
        }
        return flowGraph;
    }())
    , m_nodeUsage(m_inst->network.getOrder())
    , m_shortestPath(m_flowGraph)
    , m_paths(m_inst->demands.size())
    , m_pathsOnEdge(m_inst->network.getOrder(), m_inst->network.getOrder())
    , m_chainLocations(m_inst->demands.size(), -1)
    , m_chainsToDemands([&]() {
        std::map<std::vector<function_descriptor>, std::vector<int>> chainsToDemands;
        for (std::size_t i = 0; i < m_inst->demands.size(); ++i) {
            chainsToDemands[m_inst->demands[i].functions].push_back(i);
        }
        return chainsToDemands;
    }())
    , m_state(State::NotSolved) {
}

void Heuristic::solve() {
    m_state = getAllShortestPaths() && placeFunctions() ? State::Solved : State::Infeasible;
}

void Heuristic::save(const std::string& _filename, const std::pair<double, double>& _time, const DiGraph& _network) const {
    std::ofstream ofs(_filename);
    ofs << getEnergyUsed() << '\t' << -1 << '\n';
    ofs << -1 << '\n';
    ofs << _time.first << '\t' << _time.second << '\n';

    ofs << m_inst->network.getOrder() << '\n';
    for (Graph::Node u = 0; u < m_inst->network.getOrder(); ++u) {
        ofs << u << '\t' << std::ceil(m_nodeUsage[u]) << '\t' << m_inst->nodeCapa[u] << '\n';
    }

    ofs << _network.size() << '\n';
    for (const auto& edge : _network.getEdges()) {
        if (m_flowGraph.hasEdge(edge)) {
            ofs << std::fixed << edge.first << '\t' << edge.second
                << '\t' << m_flowGraph.getEdgeWeight(edge) << '\t' << _network.getEdgeWeight(edge);
            ofs << '\t' << (m_flowGraph.getEdgeWeight(edge) > 0.0) << '\n';
        } else {
            ofs << std::fixed << edge.first << '\t' << edge.second
                << '\t' << 0 << '\t' << _network.getEdgeWeight(edge);
            ofs << '\t' << (m_flowGraph.getEdgeWeight(edge) > 0.0 || m_flowGraph.getEdgeWeight(edge.second, edge.first) > 0.0) << '\n';
        }
    }

    ofs << m_paths.size() << '\n';
    for (std::size_t i = 0; i < m_inst->demands.size(); ++i) {
        ofs << m_paths[i].size() << '\t';
        for (const auto& u : m_paths[i]) {
            ofs << u << '\t';
        }
        for (const auto& func : m_inst->demands[i].functions) {
            ofs << m_chainLocations[i] << '\t' << func << '\t';
        }
        ofs << '\n';
    }
    std::cout << "Results saved to :" << _filename << '\n';
}

bool Heuristic::getAllShortestPaths() {
    std::size_t i = 0;
    for (const auto& demand : m_inst->demands) {
        // if(m_paths[i].empty()) {
        m_shortestPath.clear();
        m_paths[i] = m_shortestPath.getShortestPathNbArcs(demand.s, demand.t,
            [&](const int u, const int v) {
                // std::cout << m_inst->network.getEdgeWeight(u, v) << '\t' << m_flowGraph.getEdgeWeight(u, v) << '\t' << demand.d << '\n';
                return m_inst->network.getEdgeWeight(u, v) - m_flowGraph.getEdgeWeight(u, v) > demand.d;
            });
        if (!m_paths[i].empty()) {
            for (auto iteU = m_paths[i].begin(), iteV = std::next(iteU); iteV != m_paths[i].end(); ++iteU, ++iteV) {
                m_flowGraph.setEdgeWeight(*iteU, *iteV, m_flowGraph.getEdgeWeight(*iteU, *iteV) + demand.d);
                m_pathsOnEdge(*iteU, *iteV).emplace_back(i);
            }

        } else {
            std::cout << "No path found for " << demand << '\n';
            return false;
        }
        // }
        ++i;
    }
    return true;
}

bool Heuristic::placeFunctions() {
    int totalNbDemandsAssigned = 0;
    // For each chains
    for (const auto& pair_chainDemands : m_chainsToDemands) {
        const auto& ints = pair_chainDemands.second;
        const int nbToAssigned = std::count_if(ints.begin(), ints.end(), [&](const int i) {
            return m_chainLocations[i] == -1;
        });
        // Count number of demands going though each node
        std::vector<int> nodeOcc(m_inst->network.getOrder());
        for (const int i : ints) {
            const Graph::Path& path = m_paths[i];
            for (const Graph::Node u : path) {
                ++nodeOcc[u];
            }
        }
        std::vector<int> nodes(m_inst->network.getOrder());
        std::iota(nodes.begin(), nodes.end(), 0);
        std::sort(nodes.begin(), nodes.end(), [&](const int u, const int v) {
            return nodeOcc[u] > nodeOcc[v];
        });
        int nbDemandsAssigned = 0;
        for (const Graph::Node u : nodes) {
            if (nbDemandsAssigned == nbToAssigned) {
                break;
            }
            // If not all demands have their chain assigned to a node
            for (const int i : ints) {
                assert(!m_paths[i].empty());
                if (m_chainLocations[i] == -1 && std::find(m_paths[i].begin(), m_paths[i].end(), u) != m_paths[i].end()) {
                    double totalNbCores = 0.0; // @TODO: Move to class member
                    assert(m_inst->demands[i].functions == pair_chainDemands.first);
                    for (std::size_t j = 0; j < m_inst->demands[i].functions.size(); ++j) {
                        totalNbCores += m_inst->nbCores(i, j);
                    }
                    if (totalNbCores + m_nodeUsage[u] <= m_inst->nodeCapa[u]) {

                        m_chainLocations[i] = u;
                        m_nodeUsage[u] += totalNbCores;
                        assert(m_nodeUsage[u] <= m_inst->nodeCapa[u]);
                        ++totalNbDemandsAssigned;
                        ++nbDemandsAssigned;
                    } else {
                        // std::cout << (totalNbCores + m_nodeUsage[u]) << " > " << m_inst->nodeCapa[u] << '\n';
                    }
                }
            }
        }
        if (nbDemandsAssigned < nbToAssigned) {
            std::cout << "Not enough demand assigned: " << nbDemandsAssigned << " over " << nbToAssigned << '\n';
            return false;
        }
    }
    for (std::size_t i = 0; i < m_inst->demands.size(); ++i) {
        assert(m_chainLocations[i] != -1);
    }
    assert(totalNbDemandsAssigned == m_inst->demands.size());
    return true;
}

double Heuristic::getEnergyUsed() const {
    double energyUsed = 0.0;
    for (const auto& edge : m_flowGraph.getEdges()) {
        double linkEnergy = 0;
        if (m_flowGraph.getEdgeWeight(edge) > 0.0) {
            linkEnergy += m_inst->energyCons.activeLink;
            if (!m_flowGraph.hasEdge(edge.second, edge.first)) {
                linkEnergy += m_inst->energyCons.activeLink;
            }
        }
        linkEnergy += m_inst->energyCons.propLink * m_flowGraph.getEdgeWeight(edge) / m_inst->network.getEdgeWeight(edge);
        energyUsed += linkEnergy;
    }
    for (Graph::Node u = 0; u < m_inst->network.getOrder(); ++u) {
        energyUsed += std::ceil(m_nodeUsage[u]) * m_inst->energyCons.activeCore;
    }
    return energyUsed;
}

bool Heuristic::checkSolution() const {
    DiGraph graph(m_inst->network.getOrder());
    std::vector<double> nodeUsage(m_inst->network.getOrder());

    for (const auto& edge : m_inst->network.getEdges()) {
        graph.addEdge(edge.first, edge.second, 0);
    }
    for (std::size_t i = 0; i < m_inst->demands.size(); ++i) {
        assert(!m_paths[i].empty());
        assert(m_paths[i].front() == m_inst->demands[i].s);
        assert(m_paths[i].back() == m_inst->demands[i].t);

        for (auto iteU = m_paths[i].begin(), iteV = std::next(iteU); iteV != m_paths[i].end(); ++iteU, ++iteV) {
            graph.addEdgeWeight(*iteU, *iteV, m_inst->demands[i].d);
            assert(graph.getEdgeWeight(*iteU, *iteV) <= m_inst->network.getEdgeWeight(*iteU, *iteV) || (std::cout << Graph::Edge(*iteU, *iteV) << " -> " << graph.getEdgeWeight(*iteU, *iteV) << ", " << m_inst->network.getEdgeWeight(*iteU, *iteV) << '\n', false));
        }
        const auto& node = m_chainLocations[i];
        assert(node != -1);
        assert(std::find(m_paths[i].begin(), m_paths[i].end(), node) != m_paths[i].end()); // Check chains is installed on the right node
        for (std::size_t j = 0; j < m_inst->demands[i].functions.size(); ++j) {
            nodeUsage[node] += m_inst->nbCores(i, j);
            assert(nodeUsage[node] <= m_inst->nodeCapa[node]);
        }
    }
    return true;
}

std::vector<Graph::Edge> Heuristic::getRemovablesEdges(const Matrix<char>& _isRemovable) const {
    std::vector<Graph::Edge> edges;
    edges.reserve(m_flowGraph.size());
    for (const auto& edge : m_flowGraph.getEdges()) {
        if (edge.first < edge.second && _isRemovable(edge.first, edge.second)) {
            edges.push_back(edge);
        }
    }
    std::sort(edges.begin(), edges.end(), [&](const auto& _e1, const auto& _e2) {
        return m_flowGraph.getEdgeWeight(_e1) + m_flowGraph.getEdgeWeight(_e1.second, _e1.first)
               < m_flowGraph.getEdgeWeight(_e2) + m_flowGraph.getEdgeWeight(_e2.second, _e2.first);
    });
    return edges;
}

void Heuristic::removeEdge(const Graph::Edge& _edge) {
    // std::vector< std::pair<ServicePath, int> > sPaths;
    // for(auto i : m_pathsOnEdge(_edge.first, _edge.second)) {
    //     for (auto iteU = m_paths[i].begin(), iteV = std::next(iteU); iteV != m_paths[i].end(); ++iteU, ++iteV) {
    //         m_flowGraph.setEdgeWeight(*iteU, *iteV, m_flowGraph.getEdgeWeight(*iteU, *iteV) - m_inst->demands[i].d);
    //     }
    //     for(std::size_t j = 0; j < m_inst->demands[i].functions.size()); ++j; {
    //         m_nodeUsage[m_chainLocations[i]] -= m_inst->nbCores(i, j);
    //     }
    //     ServicePath sPath = { m_paths[i], std::vector< std::pair< Graph::Node, std::vector<function_descriptor>> >{{m_chainLocations[i], m_inst->demands[i].functions}}};
    //     sPaths.emplace_back(sPath, i);
    //     m_paths[i].clear();
    //     m_chainLocations[i] = -1;
    // }
    m_flowGraph.removeEdge(_edge);
    m_flowGraph.removeEdge(_edge.second, _edge.first);
    clear();
    // m_pathsOnEdge(_edge.first, _edge.second).clear();
}

void Heuristic::clear() {
    for (const auto& edge : m_flowGraph.getEdges()) {
        m_flowGraph.addEdge(edge.first, edge.second, 0);
    }

    std::fill(m_nodeUsage.begin(), m_nodeUsage.end(), 0.0);
    m_shortestPath.clear();
    for (auto& p : m_paths) {
        p.clear();
    }
    std::fill(m_chainLocations.begin(), m_chainLocations.end(), -1);
}

std::vector<ServicePath> Heuristic::getSPath() const {
    std::vector<ServicePath> sPaths;
    sPaths.reserve(m_inst->demands.size());
    for (std::size_t i = 0; i < m_inst->demands.size(); ++i) {
        std::vector<std::pair<Graph::Node, std::vector<function_descriptor>>> installs;
        assert(m_chainLocations[i] != -1);
        sPaths.emplace_back(m_paths[i], std::vector<Graph::Node>(m_inst->demands[i].functions.size(), m_chainLocations[i]), i);
    }
    return sPaths;
}

Heuristic::State Heuristic::getState() const {
    return m_state;
}

Heuristic solveEESFC(const Instance& _inst);
Heuristic solveEESFC(const Instance& _inst) {
    Matrix<char> removableEdges(_inst.network.getOrder(), _inst.network.getOrder(), true);
    Heuristic heur(_inst);

    heur.solve();
    if (heur.getState() == Heuristic::State::Solved) {
        Heuristic sol(heur);
        double energyUsed = sol.getEnergyUsed();
        assert(sol.checkSolution());
        while (true) {
            std::vector<Graph::Edge> edgesToRemove = heur.getRemovablesEdges(removableEdges);
            if (edgesToRemove.empty()) {
                break;
            }
            for (const auto& edge : edgesToRemove) {
                heur.removeEdge(edge);
                heur.solve();
                std::cout << "Trying to remove " << edge << "..." << std::flush;
                if (heur.getState() == Heuristic::State::Infeasible) { // Edge is not removable
                    std::cout << "failure!\n";
                    removableEdges(edge.first, edge.second) = false;
                    heur = sol;
                    assert(heur.checkSolution());
                } else {
                    energyUsed = sol.getEnergyUsed();
                    std::cout << "success: " << edge << " -> " << energyUsed << '\n';
                    sol = heur;
                    assert(sol.checkSolution());
                    break;
                }
            }
        }
        assert(sol.checkSolution());
        return sol;
    } else {
        return heur;
    }
}

std::vector<ServicePath> loadFromDisk(const std::string& _filename, const std::vector<Demand>& _demands);
std::vector<ServicePath> loadFromDisk(const std::string& _filename, const std::vector<Demand>& _demands) {
    std::vector<ServicePath> sPaths;

    std::ifstream ifs(_filename);
    if (!ifs) {
        std::cerr << _filename << " does not exists!" << std::endl;
        throw std::ios_base::failure(_filename + " does not exists!");
    } else {
        std::string line;
        std::size_t i = 0;
        while (std::getline(ifs, line)) {
            std::stringstream linestream(line);
            int pathLength;
            Graph::Path path;
            Graph::Node chainLocation;

            linestream >> pathLength;
            path.reserve(pathLength);
            for (std::size_t j = 0; j < pathLength; ++j) {
                Graph::Node u;
                linestream >> u;
                path.push_back(u);
            }
            linestream >> chainLocation;
            sPaths.emplace_back(path, std::vector<Graph::Node>(_demands[i].functions.size(), chainLocation), i);
            ++i;
        }
    }
    return sPaths;
}
} // namespace SFC::Energy

#endif
