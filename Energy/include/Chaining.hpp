
#ifndef ENERGY_CHAINING_PATH_HP
#define ENERGY_CHAINING_PATH_HP

#include <chrono>
#include <cmath>
#include <ilcplex/ilocplex.h>
#include <iostream>
#include <map>
#include <thread>
#include <tuple>

#include <DiGraph.hpp>
#include <ShortestPath.hpp>
#include <ShortestPathBF.hpp>
#include <ThreadPool.hpp>
#include <cplex_utility.hpp>
#include <utility.hpp>

#include "Energy.hpp"
#include "SFC.hpp"

namespace SFC::Energy {
class Placement_Path {
  public:
    using Column = SFC::ServicePath;
    using Solution = Solution<int>;
    Placement_Path(const Instance* _inst);
    Placement_Path& operator=(const Placement_Path&) = default;
    Placement_Path(const Placement_Path&) = default;
    Placement_Path& operator=(Placement_Path&&) = default;
    Placement_Path(Placement_Path&&) = default;
    ~Placement_Path();

    // void addCut(const GenCut::Column& _col);
    bool checkReducedCosts() const;
    void addColumns(const std::vector<Column>& _sPaths);
    bool solveInteger();
    void getNetworkUsage() const;
    void getDuals();
    bool solve();
    double getObjValue() const;
    inline int getNbPricing() const {
        return m_inst->demands.size();
    }

    inline int getNbColumns() const {
        return m_paths.size();
    }

    inline double getNodeCapaDual(Graph::Node _u) const {
        return m_nodeCapasDuals[m_inst->NFVIndices[_u]];
    }

    inline double getLinkCapaDual(int _e) const {
        return m_linkCapasDuals[_e];
    }
    inline double getOnePathDual(SFC::demand_descriptor _demandID) const {
        return m_onePathDuals[_demandID];
    }

    inline void setEdge(const Graph::Edge& _edge, bool _state);

  private:
    const Instance* m_inst;
    IloEnv m_env{};
    IloModel m_model = IloModel(m_env);

    IloNumVarArray m_y;
    IloNumVarArray m_x;
    IloNumVarArray m_k;

    IloObjective m_obj;

    IloRangeArray m_onePathCons;
    IloRangeArray m_linkCapasCons;

    IloRangeArray m_nodeCapasCons1;
    IloRangeArray m_nodeCapasCons2;

    IloNumVarArray m_dummyPath;

    IloNumArray m_linkCapasDuals;
    IloNumArray m_nodeCapasDuals;
    IloNumArray m_onePathDuals;
    // Cuts
    IloRange m_networkCutCons;
    IloRangeArray m_neighborhoodCutsIn;

    IloNumArray m_valsLink;
    IloNumArray m_valsNode;

    IloCplex m_solver;

    std::vector<ServicePath> m_paths{};

    double m_fractObj = -1.0;
    double m_intObj = -1.0;
};

class PricingProblem {
  public:
    using Column = SFC::ServicePath;
    PricingProblem(const Instance* _inst);
    PricingProblem& operator=(const PricingProblem&) = default;
    PricingProblem(const PricingProblem&) = default;
    PricingProblem& operator=(PricingProblem&&) = default;
    PricingProblem(PricingProblem&&) = default;
    ~PricingProblem();
    void updateDual(const Placement_Path& _rmp);
    void setPricingID(demand_descriptor _demandID);
    bool solve();
    double getObjValue() const;

    Column getColumn() const;

  private:
    const Instance* m_inst;
    IloEnv m_env {};

    int m_demandID {0};
    const int n;
    const int m;
    const int nbLayers;
    IloModel m_model {IloModel(m_env)};

    IloObjective m_obj;

    IloNumVarArray m_a;
    IloNumVarArray m_f;

    IloRangeArray m_flowConsCons;
    IloRangeArray m_linkCapaCons;
    IloRangeArray m_nodeCapaCons;

    IloCplex m_solver;

    int getIndexF(const int _e, const int _j) const;
    int getIndexA(const int _u, const int _j) const;
};

class PricingProblemBF {
  public:
    using Column = SFC::ServicePath;
    explicit PricingProblemBF(const Instance* _inst);
    PricingProblemBF(const PricingProblemBF&) = default;
    PricingProblemBF(PricingProblemBF&&) = default;
    PricingProblemBF& operator=(const PricingProblemBF&) = default;
    PricingProblemBF& operator=(PricingProblemBF&&) = default;
    ~PricingProblemBF() = default;

    void updateDual(const Placement_Path& _rmp);
    void setPricingID(demand_descriptor _demandID);

    bool solve();
    double getObjValue() const;
    Column getColumn() const;

  private:
    const Instance* m_inst;
    int n;
    int m;
    int nbLayers;

    int m_demandID;

    DiGraph m_layeredGraph;
    ShortestPathBellmanFord<DiGraph> m_bf;
    Graph::Path m_path;
    double m_objValue;
};

// void save(const std::string& _filename, const std::pair<double, double>& _time) {
//     std::ofstream ofs(_filename);
//     ofs << m_intObj << '\t' << m_fractObj << '\n';
//     ofs << m_mainProblem.m_paths.size() << std::endl;
//     ofs << _time.first << '\t' << _time.second << std::endl;

//     std::vector<double> linkUsage(m_inst->network.size(), 0);
//     std::vector<int> nodeUsage(m_inst->network.getOrder(), 0);
//     std::vector<int> pathsUsed(m_inst->demands.size());

//     for (int i = 0; i < m_mainProblem.m_paths.size(); ++i) {
//         if (m_mainProblem.m_solver.getValue(m_mainProblem.m_y[i]) > RC_EPS) {
//             const auto& path = m_mainProblem.m_paths[i].first.first;
//             const auto& funcPlacement = m_mainProblem.m_paths[i].first.second;
//             const auto& demand = m_inst->demands[m_mainProblem.m_paths[i].second];
//             const auto d = demand.d;
//             pathsUsed[m_mainProblem.m_paths[i].second] = i;

//             for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
//                 linkUsage[m_inst->edgeID(*iteU, *iteV)] += d
//                                                            * m_mainProblem.m_solver.getValue(m_mainProblem.m_y[i]);
//             }
//             int j = 0;
//             for (const auto& pair_NodeFuncs : funcPlacement) {
//                 for (const int nbFunc = pair_NodeFuncs.second.size() + j; j < nbFunc; ++j) {
//                     nodeUsage[pair_NodeFuncs.first] += m_mainProblem.m_solver.getValue(m_mainProblem.m_y[i])
//                                                        * m_inst->nbCores(m_mainProblem.m_paths[i].second, j);
//                 }
//             }
//         }
//     }

//     ofs << m_inst->network.getOrder() << '\n';
//     for (Graph::Node u = 0; u < m_inst->network.getOrder(); ++u) {
//         ofs << u << '\t' << nodeUsage[u] << '\t' << m_inst->nodeCapa[u] << '\n';
//     }

//     ofs << m_inst->network.size() << '\n';
//     int i = 0;
//     for (const auto& edge : m_inst->edges) {
//         ofs << std::fixed << edge.first << '\t' << edge.second << '\t' << linkUsage[i] << '\t' << m_inst->network.getEdgeWeight(edge);
//         int xIndex;
//         if (edge.first < edge.second) {
//             xIndex = i;
//         } else {
//             xIndex = m_inst->edgeID(edge.second, edge.first);
//         }
//         ofs << '\t' << m_mainProblem.m_solver.getValue(m_mainProblem.m_x[xIndex]) << '\n';
//         ++i;
//     }

//     ofs << pathsUsed.size() << '\n';
//     for (const auto& index : pathsUsed) {
//         const auto& sPath = m_mainProblem.m_paths[index].first;
//         ofs << sPath.first.size() << '\t';
//         for (const auto& u : sPath.first) {
//             ofs << u << '\t';
//         }
//         for (const auto& pair_NodeFuncs : sPath.second) {
//             for (const auto& func : pair_NodeFuncs.second) {
//                 ofs << pair_NodeFuncs.first << '\t' << func << '\t';
//             }
//         }
//         ofs << '\n';
//     }
//     std::cout << "Results saved to :" << _filename << '\n';
// }

//####################################################################################################################
//####################################################################################################################
//################################################ Placement_Path ################################################
//####################################################################################################################
//####################################################################################################################

Placement_Path::Placement_Path(const Instance* _inst)
    : m_inst(_inst)
    , m_y(IloAdd(m_model, IloNumVarArray(m_env)))
    , m_x([&]() {
        IloNumVarArray x(m_env, m_inst->network.size());
        int e = 0;
        for (const Graph::Edge& edge : m_inst->edges) {
            if (edge.first < edge.second) {
                x[e] = IloAdd(m_model, IloNumVar(m_env, 0.0, 1.0));
                setIloName(x[e], "x" + toString(edge));
            }
            ++e;
        }
        return x;
    }())
    , m_k([&]() {
        IloNumVarArray k(m_env, m_inst->NFVNodes.size());
        for (const auto& u : m_inst->NFVNodes) {
            k[m_inst->NFVIndices[u]] = IloNumVar(m_env, 0.0, IloInfinity);
            setIloName(k[m_inst->NFVIndices[u]], "k(" + toString(u) + ")");
        }
        return IloAdd(m_model, k);
    }())
    , m_obj([&]() {
        IloNumVarArray vars(m_env);
        IloNumArray vals(m_env);
        int e = 0;
        for (const Graph::Edge& edge : m_inst->edges) {
            if (edge.first < edge.second) {
                vars.add(m_x[e]);
                vals.add(2 * m_inst->energyCons.activeLink);
            }
            ++e;
        }
        for (const auto& u : m_inst->NFVNodes) {
            vars.add(m_k[m_inst->NFVIndices[u]]);
            vals.add(m_inst->energyCons.activeCore);
        }
        auto obj = IloAdd(m_model, IloMinimize(m_env, IloScalProd(vars, vals)));
        vars.end();
        vals.end();
        return obj;
    }())
    , m_onePathCons([&]() {
        IloRangeArray onePathCons(m_env, m_inst->demands.size(), 1.0, 1.0);
        for (int i = 0; i < m_inst->demands.size(); ++i) {
            setIloName(onePathCons[i], "onePathCons" + toString(m_inst->demands[i]));
        }
        return IloAdd(m_model, onePathCons);
    }())
    , m_linkCapasCons([&]() {
        IloRangeArray linkCapaCons(m_env, m_inst->network.size(), -IloInfinity, 0.0);
        int e = 0;
        for (const Graph::Edge& edge : m_inst->edges) {
            const int realE = edge.first < edge.second ? e : m_inst->edgeID(edge.second, edge.first);
            linkCapaCons[e].setLinearCoef(m_x[realE], -m_inst->network.getEdgeWeight(edge));
            setIloName(linkCapaCons[e], "linkCapasCons" + toString(edge));
            ++e;
        }
        return IloAdd(m_model, linkCapaCons);
    }())
    , m_nodeCapasCons1([&]() {
        IloRangeArray nodeCapasCons(m_env, m_inst->NFVNodes.size());
        for (const auto& u : m_inst->NFVNodes) {
            const int i = m_inst->NFVIndices[u];
            nodeCapasCons[i] = IloAdd(m_model, IloRange(m_env, -IloInfinity, -m_k[i], 0.0));
            setIloName(nodeCapasCons[i], "nodeCapasCons1" + std::to_string(u));
        }
        return nodeCapasCons;
    }())
    , m_nodeCapasCons2([&]() {
        IloRangeArray nodeCapasCons(m_env, m_inst->NFVNodes.size());
        for (const auto& u : m_inst->NFVNodes) {
            const int i = m_inst->NFVIndices[u];
            nodeCapasCons[i] = IloAdd(m_model,
                IloRange(m_env, 0.0, m_k[i], m_inst->nodeCapa[u]));
            setIloName(nodeCapasCons[i], "nodeCapasCons2" + std::to_string(u));
        }
        return nodeCapasCons;
    }())
    , m_dummyPath([&]() {
        IloNumVarArray retval(m_env);
        for (int i = 0; i < m_inst->demands.size(); ++i) {
            retval.add(m_onePathCons[i](1.0) + m_obj(2 * m_inst->demands[i].d * m_inst->network.getOrder()));
            // setIloName(retval[retval.getSize() - 1], "dummyPaths" + toString(m_inst->demands[i]));
        }
        return retval;
    }())
    , m_linkCapasDuals(m_env)
    , m_nodeCapasDuals(m_env)
    , m_onePathDuals(m_env)
    , m_networkCutCons([&]() {
        IloNumVarArray vars(m_env);
        int e = 0;
        for (const Graph::Edge& edge : m_inst->edges) {
            if (edge.first < edge.second) {
                vars.add(m_x[e]);
            }
            ++e;
        }
        IloRange cut(m_env, m_inst->network.getOrder() - 1, IloSum(vars),
            m_inst->network.size() / 2, "networkCutCons");
        vars.end();
        return cut;
    }())
    , m_neighborhoodCutsIn([&]() {
        IloRangeArray neighborhoodCutsIn(m_env, m_inst->network.getOrder());

        for (Graph::Node u = 0; u < m_inst->network.getOrder(); ++u) {
            IloNumVarArray vars(m_env);
            for (const auto& v : m_inst->network.getNeighbors(u)) {
                if (v < u) {
                    vars.add(m_x[m_inst->edgeID(v, u)]);
                } else {
                    vars.add(m_x[m_inst->edgeID(u, v)]);
                }
            }

            int traffic = static_cast<int>(m_inst->traffic[u].first);
            int nbEdges = 0;
            for (const auto& edge : m_inst->sortedNeighbors[u]) {
                traffic -= m_inst->network.getEdgeWeight(edge);
                ++nbEdges;
                if (traffic <= 0) {
                    break;
                }
            }
            m_neighborhoodCutsIn[u] = IloRange(m_env, nbEdges, IloSum(vars), IloInfinity);
            vars.end();
        }

        return neighborhoodCutsIn;
    }())
    , m_valsLink(m_env, m_inst->network.size())
    , m_valsNode(m_env, m_inst->NFVNodes.size())
    , m_solver([&]() {
        IloCplex solver(m_model);
        // solver.setParam(IloCplex::EpGap, 0.0077);
        // solver.setParam(IloCplex::VarSel, 3);
        // solver.setParam(IloCplex::Threads, 1);
        solver.setParam(IloCplex::EpRHS, 1e-9);
        solver.setOut(m_env.getNullStream());
        return solver;
    }())
    , m_paths() {
}

Placement_Path::~Placement_Path() {
    m_env.end();
}

void Placement_Path::setEdge(const Graph::Edge& _edge, bool _state) {
    assert(_edge.first < _edge.second);
    m_x[m_inst->edgeID(_edge.first, _edge.second)].setBounds(_state, _state);
}

// void Placement_Path::addCut(const GenCut::Column& _col) {
//     const auto& edges = m_inst->network.getEdges();
//     IloNumVarArray vars(m_env);
//     IloNumArray vals(m_env);
//     for (const int e : _col.edgeIDs) {
//         const int realE = edges[e].first < edges[e].second ? e : m_inst->edgeID(edges[e].second, edges[e].first);
//         vars.add(m_x[realE]);
//         vals.add(1.0);
//     }
//     IloRange cut = IloScalProd(vars, vals) >= _col.minEdge;
//     vars.end();
//     vals.end();
//     std::cout << cut << '\n';
//     m_model.add(cut);
// }

bool Placement_Path::checkReducedCosts() const {
    for (int k = 0; k < m_paths.size(); ++k) {
        const int i = m_paths[k].demand;
        double myRC = -m_onePathDuals[i];

        for (auto iteU = m_paths[k].nPath.begin(), iteV = std::next(iteU);
             iteV != m_paths[k].nPath.end(); ++iteU, ++iteV) {
            const int e = m_inst->edgeID(*iteU, *iteV);

            myRC += m_inst->energyCons.propLink
                        * (m_inst->demands[i].d / m_inst->network.getEdgeWeight(*iteU, *iteV))
                    - m_inst->demands[i].d * m_linkCapasDuals[e];
        }

        for (int j = 0; j < m_paths[k].locations.size(); ++j) {
            myRC += -m_nodeCapasDuals[m_inst->NFVIndices[m_paths[k].locations[j]]]
                    * m_inst->nbCores(i, j);
        }
        if (std::abs(myRC - m_solver.getReducedCost(m_y[k])) > RC_EPS) {
            std::cout << myRC << ' ' << m_solver.getReducedCost(m_y[k]) << '\n';
            return false;
        }
    }
    return true;
}

void Placement_Path::addColumns(const std::vector<ServicePath>& _sPaths) {
    IloNumColumnArray columns(m_env);
    columns.setSize(_sPaths.size());
    int idx = 0;
    for (const auto& _sPath : _sPaths) {
        assert([&]() {
            const auto ite = std::find(m_paths.begin(), m_paths.end(), _sPath);
            if (ite == m_paths.end()) {
                return true;
            } else {
                std::cout << _sPath << " is already present!" << '\n';
                return false;
            }
        }());

        const auto& path = _sPath.nPath;
        const auto& funcPlacement = _sPath.locations;
        const auto& demand = m_inst->demands[_sPath.demand];
        const auto d = demand.d;

        assert(path.front() == demand.s);
        assert(path.back() == demand.t);

        double objCoef = 0.0;
        for (int e = 0; e < m_inst->network.size(); ++e) {
            m_valsLink[e] = 0;
        }
        for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
            m_valsLink[m_inst->edgeID(*iteU, *iteV)] += d;
            objCoef += m_inst->energyCons.propLink
                       * (d / m_inst->network.getEdgeWeight(*iteU, *iteV));
        }
        std::fill(begin(m_valsNode), end(m_valsNode), 0.0);

        for (int j = 0; j < funcPlacement.size(); ++j) {
            m_valsNode[m_inst->NFVIndices[funcPlacement[j]]] += m_inst->nbCores(_sPath.demand, j);
        }

        columns[idx] = m_onePathCons[_sPath.demand](1.0)
                       + m_linkCapasCons(m_valsLink)
                       + m_nodeCapasCons1(m_valsNode)
                       + m_obj(objCoef);
        ++idx;
    }
    m_y.add(IloNumVarArray(m_env, columns));
    // setIloName(m_y[m_y.getSize() - 1], "y" + toString(std::make_tuple(m_inst->demands[demandID], _sPath)));
    m_paths.reserve(m_paths.size() + _sPaths.size());
    m_paths.insert(m_paths.end(), _sPaths.begin(), _sPaths.end());
}

bool Placement_Path::solveInteger() {
    m_model.add(IloConversion(m_env, m_y, ILOBOOL));
    m_model.add(IloConversion(m_env, m_k, ILOINT));
    for (int i = 0; i < m_inst->demands.size(); ++i) {
        m_model.add(IloRange(m_env, 0.0, IloNumExprArg(m_dummyPath[i]), 0.0));
    }

    int e = 0;
    for (const Graph::Edge& edge : m_inst->edges) {
        if (edge.first < edge.second) {
            m_model.add(IloConversion(m_env, m_x[e], ILOBOOL));
        }
        ++e;
    }

    m_solver.extract(m_model);
    if (m_solver.solve() == IloFalse) {
        m_solver.exportModel("mp.sav");
        return false;
    }
    m_intObj = m_solver.getObjValue();
    std::cout << "Final obj value: " << m_intObj << "\tFrac Obj Value: "
              << m_fractObj << "\t# Paths: " << m_paths.size() << '\n';
    // getNetworkUsage();
    return true;
}

void Placement_Path::getNetworkUsage() const {
    std::vector<double> linkUsage(m_inst->network.size(), 0);
    std::vector<IloNumVar> pathsUsed(m_inst->demands.size(), nullptr);
    std::vector<int> active(m_inst->network.size(), 0);

    for (int i = 0; i < m_paths.size(); ++i) {
        if (m_solver.getValue(m_y[i]) > RC_EPS) {
            const auto& path = m_paths[i].nPath;
            const auto& demand = m_inst->demands[m_paths[i].demand];
            const auto d = demand.d;
            pathsUsed[m_paths[i].demand] = m_y[i];

            for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
                linkUsage[m_inst->edgeID(*iteU, *iteV)] += d * m_solver.getValue(m_y[i]);
                active[m_inst->edgeID(*iteU, *iteV)] = 1;
                active[m_inst->edgeID(*iteV, *iteU)] = 1;
            }
        }
    }

    // Get node usage
    IloNumArray nodeUsage(m_env);
    m_solver.getValues(nodeUsage, m_k);
    for (const auto& u : m_inst->NFVNodes) {
        nodeUsage[u] = m_solver.getValue(m_k[m_inst->NFVIndices[u]]);
    }
    std::cout << "Node usage: \n";
    double nodeEnergy = 0;
    for (const auto& u : m_inst->NFVNodes) {
        nodeEnergy += nodeUsage[u] * m_inst->energyCons.activeCore;
        std::cout << '\t' << u << " -> " << nodeUsage[u] << " / " << m_inst->nodeCapa[u]
                  << " -> " << nodeUsage[u] * m_inst->energyCons.activeCore << '\n';
        assert(nodeUsage[u] <= m_inst->nodeCapa[u]);
    }
    std::cout << '\t' << nodeEnergy << '\n';

    // Get link usage
    double linkEnergy = 0.0;
    std::cout << "Link usage: \n";
    int e = 0;
    for (const auto& edge : m_inst->edges) {
        int realE = edge.first < edge.second ? e : m_inst->edgeID(edge.second, edge.first);
        double energy = m_inst->energyCons.propLink
                        * linkUsage[e] / m_inst->network.getEdgeWeight(edge);

        if (active[e]) {
            energy += m_inst->energyCons.activeLink;
        }
        linkEnergy += energy;
        std::cout << std::fixed << '\t' << edge << " -> "
                  << linkUsage[e] << " / " << m_inst->network.getEdgeWeight(edge)
                  << " -> " << energy << " ->" << active[e] << "/" << m_solver.getValue(m_x[realE]) << '\n';
        assert(m_inst->network.getEdgeWeight(edge) - linkUsage[e] > -RC_EPS
               || (std::cerr << m_inst->network.getEdgeWeight(edge) - linkUsage[e] << '\n', false));
        assert(epsilon_equal<double>()(active[e], m_solver.getValue(m_x[realE])));
        ++e;
    }
    std::cout << nodeEnergy << " + " << linkEnergy << " = " << nodeEnergy + linkEnergy << '\n';
}

void Placement_Path::getDuals() {
    m_solver.getDuals(m_linkCapasDuals, m_linkCapasCons);
    m_solver.getDuals(m_nodeCapasDuals, m_nodeCapasCons1);
    m_solver.getDuals(m_onePathDuals, m_onePathCons);
}

bool Placement_Path::solve() {
    m_solver.extract(m_model);
    if (m_solver.solve() == IloFalse) {
        m_solver.exportModel("rm.lp");
        return false;
    }
    m_fractObj = m_solver.getObjValue();
    getDuals();
    assert(checkReducedCosts());
    m_solver.clear();
    return true;
}

double Placement_Path::getObjValue() const {
    return m_fractObj;
}
//####################################################################################################################
//####################################################################################################################
//################################################# PricingProblemBF #################################################
//####################################################################################################################
//####################################################################################################################

PricingProblemBF::PricingProblemBF(const Instance* _inst)
    : m_inst(_inst)
    , n(m_inst->network.getOrder())
    , m(m_inst->network.size())
    , nbLayers(m_inst->maxChainSize + 1)
    , m_demandID(0)
    , m_layeredGraph([=]() {
        DiGraph layeredGraph(n * nbLayers);
        for (const auto& u : m_inst->NFVNodes) {
            for (int j = 0; j < nbLayers - 1; ++j) {
                layeredGraph.addEdge(n * j + u, n * (j + 1) + u);
            }
        }
        for (int j = 0; j < nbLayers; ++j) {
            for (const auto& edge : m_inst->network.getEdges()) {
                layeredGraph.addEdge(n * j + edge.first, n * j + edge.second);
            }
        }
        return layeredGraph;
    }())
    , m_bf(m_layeredGraph)
    , m_path()
    , m_objValue(0) {}

void PricingProblemBF::setPricingID(const int _demandID) {
    m_demandID = _demandID;
}

void PricingProblemBF::updateDual(const Placement_Path& _rmp) {
#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
    std::cout << "updateDual(" << m_inst->demands[_i] << ")\n";
#endif

    for (const auto& u : m_inst->NFVNodes) {
        for (int j = 0; j < nbLayers - 1; ++j) {
            m_layeredGraph.setEdgeWeight(n * j + u, n * (j + 1) + u,
                -_rmp.getNodeCapaDual(u) * m_inst->nbCores(m_demandID, j));
        }
    }
    const auto edges = m_inst->network.getEdges();
    for (int j = 0; j < nbLayers; ++j) {
        for (int e = 0; e < m; ++e) {
            m_layeredGraph.setEdgeWeight(n * j + edges[e].first, n * j + edges[e].second,
                m_inst->energyCons.propLink
                        * (m_inst->demands[m_demandID].d / m_inst->network.getEdgeWeight(m_inst->edges[e]))
                    - m_inst->demands[m_demandID].d * _rmp.getLinkCapaDual(e));
        }
    }
    m_objValue = -_rmp.getOnePathDual(m_demandID);
}

bool PricingProblemBF::solve() {
    m_bf.clear();
    const Graph::Node dest = n * (m_inst->demands[m_demandID].functions.size()) + m_inst->demands[m_demandID].t;
    m_path = m_bf.getShortestPath(m_inst->demands[m_demandID].s, dest);
    m_objValue += m_bf.getDistance(dest);
    return !m_path.empty();
}

double PricingProblemBF::getObjValue() const {
    return m_objValue;
}

ServicePath PricingProblemBF::getColumn() const {
    return SFC::getServicePath(m_path, n, m_demandID);
}

//####################################################################################################################
//####################################################################################################################
//####################################################################################################################
//####################################################################################################################
//####################################################################################################################

PricingProblem::PricingProblem(const Instance* _inst)
    : m_inst(_inst)
    , n(m_inst->network.getOrder())
    , m(m_inst->network.size())
    , nbLayers(m_inst->maxChainSize + 1)
    , m_model(m_env)
    , m_obj(IloAdd(m_model, IloMinimize(m_env)))
    , m_a([&]() {
        IloNumVarArray a(m_env, m_inst->NFVNodes.size() * (nbLayers - 1));
        for (const auto& u : m_inst->NFVNodes) {
            for (int j(0); j < nbLayers - 1; ++j) {
                a[getIndexA(u, j)] = IloAdd(m_model,
                    IloNumVar(m_env, 0, 1, ILOBOOL));
                setIloName(a[getIndexA(u, j)], "a" + toString(std::make_tuple(j, u)));
            }
        }
        return a;
    }())
    , m_f([&]() {
        IloNumVarArray f(m_env, m * nbLayers, 0, 1, ILOBOOL);
        for (int e(0); e < m; ++e) {
            for (int j(0); j < nbLayers; ++j) {
                setIloName(f[getIndexF(e, j)],
                    "f" + toString(std::make_tuple(m_inst->edges[e], j)));
            }
        }
        return IloAdd(m_model, f);
    }())
    , m_flowConsCons([&]() {
        IloRangeArray flowConsCons(m_env, n * nbLayers, 0.0, 0.0);
        for (Graph::Node u(0); u < m_inst->network.getOrder(); ++u) {
            for (int j(0); j < nbLayers; ++j) {
                IloNumVarArray vars(m_env);
                IloNumArray vals(m_env);
                for (const auto& v : m_inst->network.getNeighbors(u)) {
                    vars.add(m_f[getIndexF(m_inst->edgeID(u, v), j)]);
                    vals.add(1.0);
                    vars.add(m_f[getIndexF(m_inst->edgeID(v, u), j)]);
                    vals.add(-1.0);
                }
                if (m_inst->isNFV[u]) {
                    if (j > 0) {
                        vars.add(m_a[getIndexA(u, j - 1)]);
                        vals.add(-1.0);
                    }
                    if (j < nbLayers - 1) {
                        vars.add(m_a[getIndexA(u, j)]);
                        vals.add(1.0);
                    }
                }
                flowConsCons[n * j + u] = IloAdd(m_model,
                    IloRange(m_env, 0.0, IloScalProd(vars, vals), 0.0));
            }
        }
        return flowConsCons;
    }())
    , m_linkCapaCons([&]() {
        IloRangeArray linkCapaCons(m_env, m, 0.0, 0.0);
        for (int e(0); e < m_inst->edges.size(); ++e) {
            linkCapaCons[e].setUB(m_inst->network.getEdgeWeight(m_inst->edges[e]));
            setIloName(linkCapaCons[e], "linkCapaCons(" + toString(e) + ")");
        }
        return IloAdd(m_model, linkCapaCons);
    }())
    , m_nodeCapaCons([&]() {
        IloRangeArray nodeCapaCons(m_env, n);
        for (const auto& u : m_inst->NFVNodes) {
            nodeCapaCons[u] = IloAdd(m_model,
                IloRange(m_env, 0.0, m_inst->nodeCapa[u]));
            setIloName(nodeCapaCons[u], "nodeCapaCons(" + std::to_string(u) + ")");
        }
        return nodeCapaCons;
    }())
    , m_solver([&]() {
        IloCplex solver(m_model);
        solver.setParam(IloCplex::Threads, 1);
        solver.setOut(m_env.getNullStream());
        solver.setParam(IloCplex::EpRHS, 1e-9);
        solver.setWarning(m_env.getNullStream());
        return solver;
    }()) {}

PricingProblem::~PricingProblem() {
    m_env.end();
}

int PricingProblem::getIndexF(const int _e, const int _j) const {
    assert(0 <= _e && _e < m_inst->network.size());
    assert(0 <= _j && _j < nbLayers);
    return m * _j + _e;
}

int PricingProblem::getIndexA(const int _u, const int _j) const {
    assert(0 <= _u && _u < m_inst->network.getOrder());
    assert(0 <= _j && _j < nbLayers);
    assert(m_inst->NFVIndices[_u] != -1);
    return m_inst->NFVNodes.size() * _j + m_inst->NFVIndices[_u];
}

void PricingProblem::setPricingID(SFC::demand_descriptor _demandID) {
    int nbFunctions = m_inst->demands[m_demandID].functions.size();
    m_flowConsCons[n * 0 + m_inst->demands[m_demandID].s].setBounds(0.0, 0.0);
    m_flowConsCons[n * nbFunctions + m_inst->demands[m_demandID].t].setBounds(0.0, 0.0);

    m_demandID = _demandID;
    nbFunctions = m_inst->demands[m_demandID].functions.size();
    m_flowConsCons[n * 0 + m_inst->demands[m_demandID].s].setBounds(1.0, 1.0);
    m_flowConsCons[n * nbFunctions
                   + m_inst->demands[m_demandID].t]
        .setBounds(-1.0, -1.0);

    for (int e(0); e < m_inst->edges.size(); ++e) {
        IloNumVarArray vars(m_env);
        IloNumArray vals(m_env);
        for (int j(0); j <= nbFunctions; ++j) {
            vars.add(m_f[getIndexF(e, j)]);
            vals.add(m_inst->demands[m_demandID].d);
        }
        for (int j(nbFunctions + 1); j < nbLayers; ++j) {
            vars.add(m_f[getIndexF(e, j)]);
            vals.add(0);
        }
        m_linkCapaCons[e].setLinearCoefs(vars, vals);
        vars.end();
        vals.end();
    }

    for (const auto& u : m_inst->NFVNodes) {
        IloNumVarArray vars(m_env);
        IloNumArray vals(m_env);
        for (int j(0); j < nbFunctions; ++j) {
            vars.add(m_a[getIndexA(u, j)]);
            vals.add(m_inst->nbCores(m_demandID, j));
        }
        for (int j(nbFunctions); j < nbLayers - 1; ++j) {
            vars.add(m_a[getIndexA(u, j)]);
            vals.add(0);
        }
        m_nodeCapaCons[u].setLinearCoefs(vars, vals);
        vars.end();
        vals.end();
        setIloName(m_nodeCapaCons[u], "m_nodeCapaCons(" + std::to_string(u) + ")");
    }
}

void PricingProblem::updateDual(const Placement_Path& _rmp) {
    const auto& d = m_inst->demands[m_demandID].d;
    const auto& functions = m_inst->demands[m_demandID].functions;
    const int nbFunctions = m_inst->demands[m_demandID].functions.size();
    IloNumVarArray vars(m_env);
    IloNumArray vals(m_env);

    for (int e(0); e < m; ++e) {
        for (int j(0); j <= nbFunctions; ++j) {
            vars.add(m_f[getIndexF(e, j)]);
            vals.add(m_inst->energyCons.propLink
                         * (d / m_inst->network.getEdgeWeight(m_inst->edges[e]))
                     - d * _rmp.getLinkCapaDual(e));
        }
    }

    for (const auto& u : m_inst->NFVNodes) {
        for (int j = 0; j < functions.size(); ++j) {
            vals.add(-_rmp.getNodeCapaDual(u)
                     * m_inst->nbCores(m_demandID, j));
            vars.add(m_a[getIndexA(u, j)]);
        }
        for (int j(functions.size()); j < nbLayers - 1; ++j) {
            vals.add(0);
            vars.add(m_a[getIndexA(u, j)]);
        }
    }
    m_obj.setConstant(-_rmp.getOnePathDual(m_demandID));
    m_obj.setLinearCoefs(vars, vals);
    vars.end();
    vals.end();
}

bool PricingProblem::solve() {
    return m_solver.solve();
}

double PricingProblem::getObjValue() const {
    return m_solver.getObjValue();
}

ServicePath PricingProblem::getColumn() const {
    const auto& funcs = m_inst->demands[m_demandID].functions;
    const auto& t = m_inst->demands[m_demandID].t;

    Graph::Node u = m_inst->demands[m_demandID].s;
    ServicePath retval{{u}, {}, m_demandID};
    int j = 0;

    IloNumArray aVal(m_env);
    m_solver.getValues(aVal, m_a);

    while (u != t || j < funcs.size()) {
        if (j < funcs.size() && m_inst->isNFV[u] 
            && epsilon_equal<double>()(aVal[getIndexA(u, j)], IloTrue)) {
            retval.locations.push_back(u);
            ++j;
        } else {
            for (const auto& v : m_inst->network.getNeighbors(u)) {
                if (m_solver.getValue(
                        m_f[getIndexF(m_inst->edgeID(u, v), j)])
                    > 0) {
                    retval.nPath.push_back(v);
                    u = v;
                    break;
                }
            }
        }
    }

    assert(retval.nPath.back() == t);
    return retval;
}
} // namespace SFC::Energy

#endif
