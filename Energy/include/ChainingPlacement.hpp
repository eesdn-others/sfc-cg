#pragma once

#include <chrono>
#include <cmath>
#include <ilcplex/ilocplex.h>
#include <iostream>
#include <map>
#include <thread>
#include <tuple>

#include <ShortestPath.hpp>
#include <ShortestPathBF.hpp>
#include <ThreadPool.hpp>
#include <cplex_utility.hpp>
#include <utility.hpp>

#include "Energy.h"
#include "SFC.hpp"

namespace SFC::Energy {
class ChainingPlacementFixedEdge {
    class ReducedMainProblem {
        friend ChainingPlacementFixedEdge;

      public:
        ReducedMainProblem(const ChainingPlacementFixedEdge& _placement);
        ReducedMainProblem& operator=(const ReducedMainProblem&) = delete;
        ReducedMainProblem(const ReducedMainProblem&) = delete;
        ~ReducedMainProblem();

        void checkReducedCosts() const;
        void addPath(const ServicePath& _sPath, const int _i);
        double solveInteger();
        void getNetworkUsage() const;
        void getDuals();
        bool solve();
        double getObjValue() const;

        Graph::Edge getLessUsedEdge() const;
        std::vector<Graph::Edge> getEdgesByUsage() const;
        void removeEdge(const Graph::Edge& _edge);
        void restoreEdge(const Graph::Edge& _edge);

      private:
        const ChainingPlacementFixedEdge& m_placement;
        IloEnv m_env{};
        IloModel m_model;
        IloModel m_integerModel;

        IloNumVarArray m_y;
        IloNumVarArray m_k;

        IloObjective m_obj;

        IloRangeArray m_onePathCons;
        IloRangeArray m_linkCapasCons;

        IloRangeArray m_nodeCapasCons1;
        IloRangeArray m_nodeCapasCons2;

        IloNumArray m_linkCapasDuals;
        IloNumArray m_nodeCapasDuals;
        IloNumArray m_onePathDuals;
        // Cuts
        // IloRange m_inst.networkCutCons;
        // IloRangeArray m_neighborhoodCutsIn;

        IloNumArray m_valsLink;
        IloNumArray m_valsNode;

        IloCplex m_solver;

        std::vector<std::pair<ServicePath, int>> m_paths;
        std::vector<char> m_removable;
    };

    class PricingProblem {
        friend ChainingPlacementFixedEdge;

      public:
        PricingProblem(const ChainingPlacementFixedEdge& _chainPlacement);
        ~PricingProblem();
        void updateModel(const int _i);
        void updateDual(const std::size_t _i);
        bool solve();
        double getObjValue() const;
        ServicePath getServicePath() const;

        void removeEdge(const Graph::Edge& _edge);
        void restoreEdge(const Graph::Edge& _edge);

      private:
        const ChainingPlacementFixedEdge& m_placement;
        IloEnv m_env{};

        int m_demandID;
        const std::size_t n;
        const std::size_t m;
        const std::size_t nbLayers;
        IloModel m_model;

        IloObjective m_obj;

        IloNumVarArray m_a;
        IloNumVarArray m_f;

        IloRangeArray m_flowConsCons;
        IloRangeArray m_linkCapaCons;
        IloRangeArray m_nodeCapaCons;

        IloCplex m_solver;

        int getIndexF(const int _e, const int _j) const;
        int getIndexA(const int _u, const int _j) const;
    };

  public:
    ChainingPlacementFixedEdge(const Instance& _inst);

    ChainingPlacementFixedEdge(const ChainingPlacementFixedEdge&) = default;
    ChainingPlacementFixedEdge& operator=(const ChainingPlacementFixedEdge&) = default;
    ChainingPlacementFixedEdge(ChainingPlacementFixedEdge&&) = default;
    ChainingPlacementFixedEdge& operator=(ChainingPlacementFixedEdge&&) = default;
    ~ChainingPlacementFixedEdge() = default;

    void addInitConf(const std::vector<ServicePath>& _paths);
    void addInitConf(const std::vector<std::pair<ServicePath, int>>& _paths);
    std::vector<std::pair<ServicePath, int>> getPaths() const;
    void save(const std::string& _filename, const std::pair<double, double>& _time);
    bool solveInteger();
    bool solve();

    double getObjValue() const;

    Graph::Edge getLessUsedEdge() const;
    std::vector<Graph::Edge> getEdgesByUsage() const;
    void removeEdge(const Graph::Edge& _edge);
    void restoreEdge(const Graph::Edge& _edge);

  private:
    Instance const* m_inst;

    double m_intObj;
    double m_fractObj;
    ReducedMainProblem m_mainProblem;
    ThreadPool m_pool;
    std::map<std::thread::id, std::size_t> m_threadID;
    std::vector<PricingProblem> m_pps;

    friend ReducedMainProblem;
};

ChainingPlacementFixedEdge::ChainingPlacementFixedEdge(const Instance& _inst)
    : m_inst(&_inst)
    , m_intObj(-1)
    , m_fractObj(-1)
    , m_mainProblem(*this)
    , m_pool(std::max(1u, std::thread::hardware_concurrency()))
    , m_threadID([&]() {
        std::map<std::thread::id, std::size_t> pps;
        std::size_t i = 0;
        for (const auto& id : m_pool.getIds()) {
            pps.emplace(id, i);
            ++i;
        }
        return pps;
    }())
    , m_pps([&]() {
        std::vector<PricingProblem> pps;
        std::size_t concurentThreadsSupported = std::max(1u, std::thread::hardware_concurrency());
        pps.reserve(concurentThreadsSupported);
        for (std::size_t i = 0; i < concurentThreadsSupported; ++i) {
            pps.emplace_back(*this);
        }
        return pps;
    }()) {}

Graph::Edge ChainingPlacementFixedEdge::getLessUsedEdge() const {
    return m_mainProblem.getLessUsedEdge();
}

std::vector<Graph::Edge> ChainingPlacementFixedEdge::getEdgesByUsage() const {
    return m_mainProblem.getEdgesByUsage();
}
void ChainingPlacementFixedEdge::removeEdge(const Graph::Edge& _edge) {
    m_mainProblem.removeEdge(_edge);
    for (auto& pp : m_pps) {
        pp.removeEdge(_edge);
    }
}

void ChainingPlacementFixedEdge::restoreEdge(const Graph::Edge& _edge) {
    m_mainProblem.restoreEdge(_edge);
    for (auto& pp : m_pps) {
        pp.restoreEdge(_edge);
    }
}

double ChainingPlacementFixedEdge::getObjValue() const {
    return m_mainProblem.getObjValue();
}

void ChainingPlacementFixedEdge::addInitConf(const std::vector<ServicePath>& _paths) {
    std::cout << "Adding initial configuration...." << std::flush;
    std::size_t i = 0;
    for (auto& path : _paths) {
        m_mainProblem.addPath(path, i);
        ++i;
    }
    std::cout << "done\n";
}

void ChainingPlacementFixedEdge::addInitConf(const std::vector<std::pair<ServicePath, int>>& _paths) {
    std::cout << "Adding initial configuration...." << std::flush;
    for (auto& path : _paths) {
        m_mainProblem.addPath(path.first, path.second);
    }
    std::cout << "done\n";
}

std::vector<std::pair<ServicePath, int>> ChainingPlacementFixedEdge::getPaths() const {
    return m_mainProblem.m_paths;
}

void ChainingPlacementFixedEdge::save(const std::string& _filename, const std::pair<double, double>& _time) {
    std::ofstream ofs(_filename);
    ofs << m_intObj << '\t' << m_fractObj << '\n';
    ofs << m_mainProblem.m_paths.size() << std::endl;
    ofs << _time.first << '\t' << _time.second << std::endl;

    std::vector<double> linkUsage(m_inst.network.size(), 0);
    std::vector<int> nodeUsage(m_inst.network.getOrder(), 0);
    std::vector<int> pathsUsed(m_inst.demands.size());

    for (std::size_t i = 0; i < m_mainProblem.m_paths.size(); ++i) {
        if (m_mainProblem.m_solver.getValue(m_mainProblem.m_y[i + m_inst.demands.size()]) > 0.0) {
            const auto& path = m_mainProblem.m_paths[i].first.first;
            const auto& funcPlacement = m_mainProblem.m_paths[i].first.second;
            const auto& demand = m_inst.demands[m_mainProblem.m_paths[i].second];
            const auto d = demand.d;
            pathsUsed[m_mainProblem.m_paths[i].second] = i;

            for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
                linkUsage[m_inst.edgeID(*iteU, *iteV)] += d
                                                          * m_mainProblem.m_solver.getValue(m_mainProblem.m_y[i + m_inst.demands.size()]);
            }
            std::size_t j = 0;
            for (const auto& pair_NodeFuncs : funcPlacement) {
                for (const std::size_t nbFunc = pair_NodeFuncs.second.size() + j; j < nbFunc; ++j) {
                    nodeUsage[pair_NodeFuncs.first] += m_mainProblem.m_solver.getValue(m_mainProblem.m_y[i + m_inst.demands.size()])
                                                       * m_inst.nbCores(m_mainProblem.m_paths[i].second, j);
                }
            }
        }
    }

    ofs << m_inst.network.getOrder() << '\n';
    for (Graph::Node u = 0; u < m_inst.network.getOrder(); ++u) {
        ofs << u << '\t' << nodeUsage[u] << '\t' << m_inst.nodeCapa[u] << '\n';
    }

    ofs << m_inst.network.size() << '\n';
    std::size_t i = 0;
    for (const auto& edge : m_inst.edges) {
        ofs << std::fixed << edge.first << '\t' << edge.second << '\t' << linkUsage[i] << '\t' << m_inst.network.getEdgeWeight(edge);
        // int xIndex;
        // if (edge.first < edge.second) {
        //     xIndex = i;
        // } else {
        //     xIndex = m_inst.edgeID(edge.second, edge.first);
        // }
        ofs << '\t' << "@TODO" << '\n';
        ++i;
    }

    ofs << pathsUsed.size() << '\n';
    for (const auto& index : pathsUsed) {
        const auto& sPath = m_mainProblem.m_paths[index].first;
        ofs << sPath.first.size() << '\t';
        for (const auto& u : sPath.first) {
            ofs << u << '\t';
        }
        for (const auto& pair_NodeFuncs : sPath.second) {
            for (const auto& func : pair_NodeFuncs.second) {
                ofs << pair_NodeFuncs.first << '\t' << func << '\t';
            }
        }
        ofs << '\n';
    }
    std::cout << "Results saved to :" << _filename << '\n';
}

bool ChainingPlacementFixedEdge::solveInteger() {
    if (solve()) {
        m_intObj = m_mainProblem.solveInteger();
        return m_intObj != -1;
    } else {
        return false;
    }
}

bool ChainingPlacementFixedEdge::solve() {
    bool reducedCost;
    do {
        reducedCost = false;
        if (m_mainProblem.solve()) {
            m_fractObj = m_mainProblem.getObjValue();
            std::cout << "Reduced Main Problem Objective Value = " << m_fractObj << '\n';
            m_mainProblem.getDuals();
            std::vector<std::future<std::pair<ServicePath, std::size_t>>> futures;
            futures.reserve(m_inst.demands.size());
            for (std::size_t i = 0;
                 i < m_inst.demands.size();
                 ++i) {
                futures.emplace_back(
                    m_pool.enqueue([&, this, i]() {
                        // PricingProblem pp(*this);
                        auto& pp = m_pps[m_threadID.at(std::this_thread::get_id())];
                        pp.updateDual(i);
                        if (pp.solve() && pp.getObjValue() < -1.0e-9) {
                            return std::make_pair(pp.getServicePath(), i);
                        } else {
                            return std::make_pair(ServicePath(), i);
                        }
                    }));
            }
            for (auto& fut : futures) {
                auto sPath = fut.get();
                if (!sPath.first.first.empty()) {
                    reducedCost = true;
                    m_mainProblem.addPath(sPath.first, sPath.second);
                }
            }
        } else {
            m_mainProblem.m_solver.exportModel("rm.lp");
            std::cout << "No solution found for RMP" << '\n';
        }
    } while (reducedCost);
    return m_fractObj != -1;
}

//####################################################################################################################
//####################################################################################################################
//######################################### ReducedMainProblem #######################################################
//####################################################################################################################
//####################################################################################################################

ChainingPlacementFixedEdge::ReducedMainProblem::ReducedMainProblem(const ChainingPlacementFixedEdge& _placement)
    : m_placement(_placement)
    , m_env()
    , m_model(m_env)
    , m_integerModel(m_env)
    , m_y([&]() {
        IloNumVarArray y(m_env, m_placement.m_inst.demands.size());
        for (std::size_t i = 0; i < m_placement.m_inst.demands.size(); ++i) {
            y[i] = IloNumVar(m_env, 0.0, IloInfinity);
            setIloName(y[i], "dummyY(" + toString(i) + ")");
            m_integerModel.add(y[i] == 0.0);
        }
        return IloAdd(m_model, y);
    }())
    , m_k([&]() {
        IloNumVarArray k(m_env, m_placement.m_inst.NFVNodes.size());
        for (const auto& u : m_placement.m_inst.NFVNodes) {
            k[m_placement.m_inst.NFVIndices[u]] = IloNumVar(m_env, 0.0, IloInfinity);
            setIloName(k[m_placement.m_inst.NFVIndices[u]], "k(" + toString(u) + ")");
        }
        m_integerModel.add(IloConversion(m_env, k, ILOINT));
        return IloAdd(m_model, k);
    }())
    , m_obj([&]() {
        IloNumVarArray vars(m_env);
        IloNumArray vals(m_env);
        for (const auto& u : m_placement.m_inst.NFVNodes) {
            vars.add(m_k[m_placement.m_inst.NFVIndices[u]]);
            vals.add(m_placement.m_inst.energyCons.activeCore);
        }
        for (std::size_t i = 0; i < m_placement.m_inst.demands.size(); ++i) {
            vars.add(m_y[i]);
            vals.add(100);
        }
        auto obj = IloAdd(m_model, IloMinimize(m_env, IloScalProd(vars, vals)));
        obj.setConstant(m_placement.m_inst.energyCons.activeLink * m_placement.m_inst.network.size());
        vars.end();
        vals.end();
        return obj;
    }())
    , m_onePathCons([&]() {
        IloRangeArray onePathCons(m_env, m_placement.m_inst.demands.size());
        for (std::size_t i = 0; i < m_placement.m_inst.demands.size(); ++i) {
            onePathCons[i] = IloRange(m_env, 1.0, IloNumExpr(m_y[i]), 1.0);
            setIloName(onePathCons[i], "onePathCons(" + toString(m_placement.m_inst.demands[i]) + ')');
        }
        return IloAdd(m_model, onePathCons);
    }())
    , m_linkCapasCons([&]() {
        IloRangeArray linkCapaCons(m_env, m_placement.m_inst.network.size(), 0.0, 0.0);
        int e = 0;
        for (const Graph::Edge& edge : m_placement.m_inst.edges) {
            // const int realE = edge.first < edge.second ? e : m_placement.m_inst.edgeID(edge.second, edge.first);
            linkCapaCons[e].setBounds(0.0, m_placement.m_inst.network.getEdgeWeight(edge));
            setIloName(linkCapaCons[e], "linkCapasCons(" + toString(edge) + ')');
            ++e;
        }
        return IloAdd(m_model, linkCapaCons);
    }())
    , m_nodeCapasCons1([&]() {
        IloRangeArray nodeCapasCons(m_env, m_placement.m_inst.NFVNodes.size());
        for (const auto& u : m_placement.m_inst.NFVNodes) {
            const int i = m_placement.m_inst.NFVIndices[u];
            nodeCapasCons[i] = IloAdd(m_model, IloRange(m_env, -IloInfinity, -m_k[i], 0.0));
            setIloName(nodeCapasCons[i], "nodeCapasCons1(" + std::to_string(u) + ')');
        }
        return nodeCapasCons;
    }())
    , m_nodeCapasCons2([&]() {
        IloRangeArray nodeCapasCons(m_env, m_placement.m_inst.NFVNodes.size());
        for (const auto& u : m_placement.m_inst.NFVNodes) {
            const int i = m_placement.m_inst.NFVIndices[u];
            nodeCapasCons[i] = IloAdd(m_model,
                IloRange(m_env, 0.0, m_k[i], m_placement.m_inst.nodeCapa[u]));
            setIloName(nodeCapasCons[i], "nodeCapasCons2(" + std::to_string(u) + ')');
        }
        return nodeCapasCons;
    }())
    , m_linkCapasDuals(m_env)
    , m_nodeCapasDuals(m_env)
    , m_onePathDuals(m_env)
    , m_valsLink(m_env, m_placement.m_inst.network.size())
    , m_valsNode(m_env, m_placement.m_inst.NFVNodes.size())
    , m_solver([&]() {
        IloCplex solver(m_env);
        solver.setParam(IloCplex::EpGap, 0.0077);
        solver.setParam(IloCplex::VarSel, 3);
        solver.setParam(IloCplex::Threads, 1);
        solver.setParam(IloCplex::EpRHS, 1e-9);
        solver.setOut(m_env.getNullStream());
        return solver;
    }())
    , m_paths()
    , m_removable(m_placement.m_inst.network.size(), true) {
    m_integerModel.add(m_model);
}

ChainingPlacementFixedEdge::ReducedMainProblem::~ReducedMainProblem() {
    m_env.end();
}

Graph::Edge ChainingPlacementFixedEdge::ReducedMainProblem::getLessUsedEdge() const {
    IloNumArray yVal(m_env);
    m_solver.getValues(yVal, m_y);
    std::vector<double> linkUsage(m_placement.m_inst.network.size(), 0.0);
    for (int k = 0; k < m_paths.size())
        ;
    ++k;
    {
        if (yVal[k + m_placement.m_inst.demands.size()]) {
            for (auto iteU = m_paths[k].first.first.begin(), iteV = std::next(iteU); iteV != m_paths[k].first.first.end(); ++iteU, ++iteV) {
                assert(m_linkCapasCons[m_placement.m_inst.edgeID(*iteU, *iteV)].getUB() != 0.0);
                linkUsage[m_placement.m_inst.edgeID(*iteU, *iteV)] += 1.0; //m_placement.m_inst.demands[m_paths[k].second].d;

                assert(m_linkCapasCons[m_placement.m_inst.edgeID(*iteV, *iteU)].getUB() != 0.0);
                linkUsage[m_placement.m_inst.edgeID(*iteV, *iteU)] += 1.0; //m_placement.m_inst.demands[m_paths[k].second].d;
            }
        }
    }

    int e = 0;
    for (; e < m_placement.m_inst.network.size(); ++e) {
        if (m_removable[e] && m_linkCapasCons[e].getUB() != 0.0) {
            break;
        }
    }
    int minE = e++;
    for (; e < m_placement.m_inst.network.size(); ++e) {
        if (m_removable[e]
            && m_linkCapasCons[e].getUB() != 0
            && linkUsage[e] < linkUsage[minE]) {
            minE = e;
        }
    }
    Graph::Edge edge = m_placement.m_inst.edges[minE];
    if (edge.first >= edge.second) {
        std::swap(edge.first, edge.second);
    }
    return edge;
}

std::vector<Graph::Edge> ChainingPlacementFixedEdge::ReducedMainProblem::getEdgesByUsage() const {
    IloNumArray yVal(m_env);
    m_solver.getValues(yVal, m_y);
    std::vector<double> linkUsage(m_placement.m_inst.network.size(), 0.0);
    for (int k = 0; k < m_paths.size())
        ;
    ++k;
    {
        if (yVal[k + m_placement.m_inst.demands.size()]) {
            for (auto iteU = m_paths[k].first.first.begin(), iteV = std::next(iteU); iteV != m_paths[k].first.first.end(); ++iteU, ++iteV) {
                assert(m_linkCapasCons[m_placement.m_inst.edgeID(*iteU, *iteV)].getUB() != 0.0);
                linkUsage[m_placement.m_inst.edgeID(*iteU, *iteV)] += m_placement.m_inst.demands[m_paths[k].second].d;

                assert(m_linkCapasCons[m_placement.m_inst.edgeID(*iteV, *iteU)].getUB() != 0.0);
                linkUsage[m_placement.m_inst.edgeID(*iteV, *iteU)] += m_placement.m_inst.demands[m_paths[k].second].d;
            }
        }
    }

    std::vector<Graph::Edge> edges;

    for (int e = 0; e < m_placement.m_inst.network.size(); ++e) {
        if (m_placement.m_inst.edges[e].first < m_placement.m_inst.edges[e].second
            && m_removable[e]
            && m_linkCapasCons[e].getUB() != 0.0) {
            edges.emplace_back(m_placement.m_inst.edges[e]);
        }
    }
    std::sort(edges.begin(), edges.end(), [&](const Graph::Edge& _e1, const Graph::Edge& _e2) {
        return linkUsage[m_placement.m_inst.edgeID(_e1.first, _e1.first)] < linkUsage[m_placement.m_inst.edgeID(_e2.first, _e2.first)];
    });
    return edges;
}

void ChainingPlacementFixedEdge::ReducedMainProblem::removeEdge(const Graph::Edge& _edge) {
    m_obj.setConstant(m_obj.getConstant() - 2 * m_placement.m_inst.energyCons.activeLink);
    m_linkCapasCons[m_placement.m_inst.edgeID(_edge.first, _edge.second)].setBounds(0.0, 0.0);
    m_linkCapasCons[m_placement.m_inst.edgeID(_edge.second, _edge.first)].setBounds(0.0, 0.0);
}

void ChainingPlacementFixedEdge::ReducedMainProblem::restoreEdge(const Graph::Edge& _edge) {
    m_obj.setConstant(m_obj.getConstant() + 2 * m_placement.m_inst.energyCons.activeLink);
    m_linkCapasCons[m_placement.m_inst.edgeID(_edge.first, _edge.second)].setBounds(0.0, m_placement.m_inst.network.getEdgeWeight(_edge));
    m_linkCapasCons[m_placement.m_inst.edgeID(_edge.second, _edge.first)].setBounds(0.0, m_placement.m_inst.network.getEdgeWeight(_edge));

    m_removable[m_placement.m_inst.edgeID(_edge.first, _edge.second)] = false;
    m_removable[m_placement.m_inst.edgeID(_edge.second, _edge.first)] = false;
}

void ChainingPlacementFixedEdge::ReducedMainProblem::checkReducedCosts() const {
    for (int k = 0; k < m_paths.size())
        ;
    ++k;
    {
        const int i = m_paths[k].second;
        double myRC = -m_onePathDuals[i];

        for (auto iteU = m_paths[k].first.first.begin(), iteV = std::next(iteU);
             iteV != m_paths[k].first.first.end(); ++iteU, ++iteV) {
            const int e = m_placement.m_inst.edgeID(*iteU, *iteV);
            myRC += m_placement.m_inst.energyCons.propLink
                        * (m_placement.m_inst.demands[i].d / m_placement.m_inst.network.getEdgeWeight(*iteU, *iteV))
                    - m_placement.m_inst.demands[i].d * m_linkCapasDuals[e];
        }

        std::size_t j = 0;
        for (const auto& pair_NodeFuncs : m_paths[k].first.second) {
            for (const std::size_t nbFunc = pair_NodeFuncs.second.size() + j; j < nbFunc; ++j) {
                const int u = pair_NodeFuncs.first;
                myRC += -m_nodeCapasDuals[m_placement.m_inst.NFVIndices[u]]
                        * m_placement.m_inst.nbCores(i, j);
            }
        }
        assert(std::abs(myRC - m_solver.getReducedCost(m_y[k + m_placement.m_inst.demands.size()])) < 1.0e-9 || (std::cout << myRC << ' ' << m_solver.getReducedCost(m_y[k + m_placement.m_inst.demands.size()]) << '\n', false));
    }
}

void ChainingPlacementFixedEdge::ReducedMainProblem::addPath(const ServicePath& _sPath, const int _i) {
    const auto pair = std::make_pair(_sPath, _i);
    assert([&]() {
        const auto ite = std::find(m_paths.begin(), m_paths.end(), pair);
        if (ite == m_paths.end()) {
            return true;
        } else {
            std::cout << pair << " is already present!" << '\n';
            return false;
        }
    }());

    const auto& path = _sPath.first;
    const auto& funcPlacement = _sPath.second;
    const auto& demand = m_placement.m_inst.demands[_i];
    const auto d = demand.d;

    assert(path.front() == demand.s);
    assert(path.back() == demand.t);

    double objCoef = 0.0;
    for (int e(0); e < m_placement.m_inst.network.size())
        ;
    ++e;
    {
        m_valsLink[e] = 0;
    }
    for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
        m_valsLink[m_placement.m_inst.edgeID(*iteU, *iteV)] += d;
        objCoef += m_placement.m_inst.energyCons.propLink
                   * (d / m_placement.m_inst.network.getEdgeWeight(*iteU, *iteV));
    }
    for (std::size_t i = 0; i < m_valsNode.getSize())
        ;
    ++i;
    {
        m_valsNode[i] = 0.0;
    }
    std::size_t j = 0;
    for (const auto& pair_NodeFuncs : funcPlacement) {
        for (const std::size_t nbFunc = pair_NodeFuncs.second.size() + j; j < nbFunc; ++j) {
            assert(0 <= pair_NodeFuncs.first && pair_NodeFuncs.first < m_placement.m_inst.network.getOrder());
            assert(std::find(path.begin(), path.end(), pair_NodeFuncs.first) != path.end());
            m_valsNode[m_placement.m_inst.NFVIndices[pair_NodeFuncs.first]] += m_placement.m_inst.nbCores(_i, j);
        }
    }

    m_y.add(
        m_onePathCons[_i](1.0)
        + m_linkCapasCons(m_valsLink)
        + m_nodeCapasCons1(m_valsNode)
        + m_obj(objCoef));
    setIloName(m_y[m_y.getSize() - 1], "y" + toString(std::make_tuple(demand, _sPath)));
    m_integerModel.add(IloConversion(m_env, m_y[m_y.getSize() - 1], ILOBOOL));
    m_paths.emplace_back(_sPath, _i);
}

double ChainingPlacementFixedEdge::ReducedMainProblem::solveInteger() {
    m_solver.extract(m_integerModel);
    if (m_solver.solve()) {
        double obj = m_solver.getObjValue();
        std::cout << "Final obj value: " << obj << "\tFrac Obj Value: "
                  << m_placement.m_fractObj << "\t# Paths: " << m_paths.size() << '\n';
        // getNetworkUsage();
        return obj;
    } else {
        m_solver.exportModel("mp.lp");
        return -1.0;
    }
}

void ChainingPlacementFixedEdge::ReducedMainProblem::getNetworkUsage() const {
    std::vector<double> linkUsage(m_placement.m_inst.network.size(), 0);
    std::vector<IloNumVar> pathsUsed(m_placement.m_inst.demands.size(), nullptr);
    std::vector<char> active(m_placement.m_inst.network.size(), 0);

    for (std::size_t i = 0; i < m_paths.size(); ++i) {
        if (m_solver.getValue(m_y[i + m_placement.m_inst.demands.size()]) > 0.0) {
            const auto& path = m_paths[i].first.first;
            const auto& demand = m_placement.m_inst.demands[m_paths[i].second];
            const auto d = demand.d;
            pathsUsed[m_paths[i].second] = m_y[i + m_placement.m_inst.demands.size()];

            for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
                linkUsage[m_placement.m_inst.edgeID(*iteU, *iteV)] += d * m_solver.getValue(m_y[i + m_placement.m_inst.demands.size()]);
            }
        }
    }

    for (int e = 0; e < m_placement.m_inst.network.size(); ++e) {
        active[e] = m_linkCapasCons[e].getUB() != 0.0;
    }
    // Get node usage
    IloNumArray nodeUsage(m_env);
    m_solver.getValues(nodeUsage, m_k);
    for (const auto& u : m_placement.m_inst.NFVNodes) {
        nodeUsage[u] = m_solver.getValue(m_k[m_placement.m_inst.NFVIndices[u]]);
    }
    std::cout << "Node usage: \n";
    double nodeEnergy = 0;
    for (const auto& u : m_placement.m_inst.NFVNodes) {
        nodeEnergy += nodeUsage[u] * m_placement.m_inst.energyCons.activeCore;
        std::cout << '\t' << u << " -> " << nodeUsage[u] << " / " << m_placement.m_inst.nodeCapa[u]
                  << " -> " << nodeUsage[u] * m_placement.m_inst.energyCons.activeCore << '\n';
        assert(nodeUsage[u] <= m_placement.m_inst.nodeCapa[u]);
    }
    std::cout << '\t' << nodeEnergy << '\n';

    // Get link usage
    double linkEnergy = 0.0;
    std::cout << "Link usage: \n";
    int e = 0;
    for (const auto& edge : m_placement.m_inst.edges) {
        // int realE = edge.first < edge.second ? e : m_placement.m_inst.edgeID(edge.second, edge.first);
        double energy = m_placement.m_inst.energyCons.propLink
                        * linkUsage[e] / m_placement.m_inst.network.getEdgeWeight(edge);

        if (active[e]) {
            energy += m_placement.m_inst.energyCons.activeLink;
        }
        linkEnergy += energy;
        std::cout << std::fixed << '\t' << edge << " -> "
                  << linkUsage[e] << " / " << m_placement.m_inst.network.getEdgeWeight(edge)
                  << " -> " << energy << '\n';
        assert(m_placement.m_inst.network.getEdgeWeight(edge) - linkUsage[e] > -1.0e-6
               || (std::cerr << m_placement.m_inst.network.getEdgeWeight(edge) - linkUsage[e] << '\n', false));
        ++e;
    }
    std::cout << nodeEnergy << " + " << linkEnergy << " = " << nodeEnergy + linkEnergy << '\n';
}

void ChainingPlacementFixedEdge::ReducedMainProblem::getDuals() {
    m_solver.getDuals(m_linkCapasDuals, m_linkCapasCons);
    m_solver.getDuals(m_nodeCapasDuals, m_nodeCapasCons1);
    m_solver.getDuals(m_onePathDuals, m_onePathCons);
}

bool ChainingPlacementFixedEdge::ReducedMainProblem::solve() {
    m_solver.extract(m_model);
    return m_solver.solve();
}

double ChainingPlacementFixedEdge::ReducedMainProblem::getObjValue() const {
    return m_solver.getObjValue();
}
//####################################################################################################################
//####################################################################################################################
//####################################################################################################################
//####################################################################################################################
//####################################################################################################################

ChainingPlacementFixedEdge::PricingProblem::PricingProblem(const ChainingPlacementFixedEdge& _chainPlacement)
    : m_placement(_chainPlacement)
    , m_env()
    , m_demandID(0)
    , n(m_placement.m_inst.network.getOrder())
    , m(m_placement.m_inst.network.size())
    , nbLayers(m_placement.m_inst.maxChainSize + 1)
    , m_model(m_env)
    , m_obj(IloAdd(m_model, IloMinimize(m_env)))
    , m_a([&]() {
        IloNumVarArray a(m_env, m_placement.m_inst.NFVNodes.size() * (nbLayers - 1));
        for (const auto& u : m_placement.m_inst.NFVNodes) {
            for (int j(0); j < nbLayers - 1)
                ;
            ++j;
            {
                a[getIndexA(u, j)] = IloAdd(m_model,
                    IloNumVar(m_env, 0, 1, ILOBOOL));
                setIloName(a[getIndexA(u, j)], "a" + toString(std::make_tuple(j, u)));
            }
        }
        return a;
    }())
    , m_f([&]() {
        IloNumVarArray f(m_env, m * nbLayers, 0, 1, ILOBOOL);
        for (int e(0); e < m; ++e) {
            for (int j(0); j < nbLayers)
                ;
            ++j;
            {
                setIloName(f[getIndexF(e, j)],
                    "f" + toString(std::make_tuple(m_placement.m_inst.edges[e], j)));
            }
        }
        return IloAdd(m_model, f);
    }())
    , m_flowConsCons([&]() {
        IloRangeArray flowConsCons(m_env, n * nbLayers, 0.0, 0.0);
        for (Graph::Node u(0); u < m_placement.m_inst.network.getOrder())
            ;
        ++u;
        {
            for (int j(0); j < nbLayers)
                ;
            ++j;
            {
                IloNumVarArray vars(m_env);
                IloNumArray vals(m_env);
                for (const auto& v : m_placement.m_inst.network.getNeighbors(u)) {
                    vars.add(m_f[getIndexF(m_placement.m_inst.edgeID(u, v), j)]);
                    vals.add(1.0);
                    vars.add(m_f[getIndexF(m_placement.m_inst.edgeID(v, u), j)]);
                    vals.add(-1.0);
                }
                if (m_placement.m_inst.isNFV[u]) {
                    if (j > 0) {
                        vars.add(m_a[getIndexA(u, j - 1)]);
                        vals.add(-1.0);
                    }
                    if (j < nbLayers - 1) {
                        vars.add(m_a[getIndexA(u, j)]);
                        vals.add(1.0);
                    }
                }
                flowConsCons[n * j + u] = IloAdd(m_model,
                    IloRange(m_env, 0.0, IloScalProd(vars, vals), 0.0));
            }
        }
        return flowConsCons;
    }())
    , m_linkCapaCons([&]() {
        IloRangeArray linkCapaCons(m_env, m, 0.0, 0.0);
        for (int e(0); e < m_placement.m_inst.edges.size())
            ;
        ++e;
        {
            linkCapaCons[e].setUB(m_placement.m_inst.network.getEdgeWeight(m_placement.m_inst.edges[e]));
            setIloName(linkCapaCons[e], "linkCapaCons(" + toString(e) + ")");
        }
        return IloAdd(m_model, linkCapaCons);
    }())
    , m_nodeCapaCons([&]() {
        IloRangeArray nodeCapaCons(m_env, n);
        for (const auto& u : m_placement.m_inst.NFVNodes) {
            nodeCapaCons[u] = IloAdd(m_model,
                IloRange(m_env, 0.0, m_placement.m_inst.nodeCapa[u]));
            setIloName(nodeCapaCons[u], "nodeCapaCons(" + std::to_string(u) + ")");
        }
        return nodeCapaCons;
    }())
    , m_solver([&]() {
        IloCplex solver(m_model);
        solver.setParam(IloCplex::Threads, 1);
        solver.setOut(m_env.getNullStream());
        solver.setWarning(m_env.getNullStream());
        return solver;
    }()) {}

ChainingPlacementFixedEdge::PricingProblem::~PricingProblem() {
    m_env.end();
}

void ChainingPlacementFixedEdge::PricingProblem::removeEdge(const Graph::Edge& _edge) {
    m_linkCapaCons[m_placement.m_inst.edgeID(_edge.first, _edge.second)].setBounds(0.0, 0.0);
    m_linkCapaCons[m_placement.m_inst.edgeID(_edge.second, _edge.first)].setBounds(0.0, 0.0);
}

void ChainingPlacementFixedEdge::PricingProblem::restoreEdge(const Graph::Edge& _edge) {
    m_linkCapaCons[m_placement.m_inst.edgeID(_edge.first, _edge.second)].setBounds(0.0, m_placement.m_inst.network.getEdgeWeight(_edge));
    m_linkCapaCons[m_placement.m_inst.edgeID(_edge.second, _edge.first)].setBounds(0.0, m_placement.m_inst.network.getEdgeWeight(_edge));
}

int ChainingPlacementFixedEdge::PricingProblem::getIndexF(const int _e, const int _j) const {
    assert(0 <= _e && _e < m_placement.m_inst.network.size());
    assert(0 <= _j && _j < nbLayers);
    return m * _j + _e;
}

int ChainingPlacementFixedEdge::PricingProblem::getIndexA(const int _u, const int _j) const {
    assert(0 <= _u && _u < m_placement.m_inst.network.getOrder());
    assert(0 <= _j && _j < nbLayers);
    assert(m_placement.m_inst.NFVIndices[_u] != -1);
    return m_placement.m_inst.NFVNodes.size() * _j + m_placement.m_inst.NFVIndices[_u];
}

void ChainingPlacementFixedEdge::PricingProblem::updateModel(const int _i) {
    int nbFunctions = m_placement.m_inst.demands[m_demandID].functions.size();
    m_flowConsCons[n * 0 + m_placement.m_inst.demands[m_demandID].s].setBounds(0.0, 0.0);
    m_flowConsCons[n * nbFunctions + m_placement.m_inst.demands[m_demandID].t].setBounds(0.0, 0.0);

    m_demandID = _i;
    nbFunctions = m_placement.m_inst.demands[m_demandID].functions.size();
    m_flowConsCons[n * 0 + m_placement.m_inst.demands[m_demandID].s].setBounds(1.0, 1.0);
    m_flowConsCons[n * nbFunctions
                   + m_placement.m_inst.demands[m_demandID].t]
        .setBounds(-1.0, -1.0);

    for (int e(0); e < m_placement.m_inst.edges.size())
        ;
    ++e;
    {
        IloNumVarArray vars(m_env);
        IloNumArray vals(m_env);
        for (int j(0); j <= nbFunctions)
            ;
        ++j;
        {
            vars.add(m_f[getIndexF(e, j)]);
            vals.add(m_placement.m_inst.demands[m_demandID].d);
        }
        for (int j(nbFunctions + 1); j < nbLayers; ++j) {
            vars.add(m_f[getIndexF(e, j)]);
            vals.add(0);
        }
        m_linkCapaCons[e].setLinearCoefs(vars, vals);
        vars.end();
        vals.end();
    }

    for (const auto& u : m_placement.m_inst.NFVNodes) {
        IloNumVarArray vars(m_env);
        IloNumArray vals(m_env);
        for (int j(0); j < nbFunctions)
            ;
        ++j;
        {
            vars.add(m_a[getIndexA(u, j)]);
            vals.add(m_placement.m_inst.nbCores(m_demandID, j));
        }
        for (int j(nbFunctions); j < nbLayers - 1)
            ;
        ++j;
        {
            vars.add(m_a[getIndexA(u, j)]);
            vals.add(0);
        }
        m_nodeCapaCons[u].setLinearCoefs(vars, vals);
        vars.end();
        vals.end();
        setIloName(m_nodeCapaCons[u], "m_nodeCapaCons(" + std::to_string(u) + ")");
    }
}

void ChainingPlacementFixedEdge::PricingProblem::updateDual(const std::size_t _i) {
    updateModel(_i);
    const auto& d = m_placement.m_inst.demands[m_demandID].d;
    const auto& functions = m_placement.m_inst.demands[m_demandID].functions;
    const int nbFunctions = m_placement.m_inst.demands[m_demandID].functions.size();
    IloNumVarArray vars(m_env);
    IloNumArray vals(m_env);

    for (int e(0); e < m; ++e) {
        for (int j(0); j <= nbFunctions; ++j) {
            vars.add(m_f[getIndexF(e, j)]);
            vals.add(m_placement.m_inst.energyCons.propLink
                         * (d / m_placement.m_inst.network.getEdgeWeight(m_placement.m_inst.edges[e]))
                     - d * m_placement.m_mainProblem.m_linkCapasDuals[e]);
        }
    }

    for (const auto& u : m_placement.m_inst.NFVNodes) {
        for (std::size_t j = 0; j < functions.size())
            ;
        ++j;
        {
            vals.add(-m_placement.m_mainProblem.m_nodeCapasDuals[m_placement.m_inst.NFVIndices[u]]
                     * m_placement.m_inst.nbCores(m_demandID, j));
            vars.add(m_a[getIndexA(u, j)]);
        }
        for (int j(functions.size()); j < nbLayers - 1)
            ;
        ++j;
        {
            vals.add(0);
            vars.add(m_a[getIndexA(u, j)]);
        }
    }
    m_obj.setConstant(-m_placement.m_mainProblem.m_onePathDuals[_i]);
    m_obj.setLinearCoefs(vars, vals);
    vars.end();
    vals.end();
}

bool ChainingPlacementFixedEdge::PricingProblem::solve() {
    return m_solver.solve();
}

double ChainingPlacementFixedEdge::PricingProblem::getObjValue() const {
    return m_solver.getObjValue();
}

ServicePath ChainingPlacementFixedEdge::PricingProblem::getServicePath() const {
#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
    std::cout << "getServicePath() -> " << m_placement.m_inst.demands[m_demandID] << '\n';
#endif
    const auto& funcs = m_placement.m_inst.demands[m_demandID].functions;
    const auto& t = m_placement.m_inst.demands[m_demandID].t;
    std::vector<std::pair<Graph::Node, std::vector<FunctionDescriptor>>> installs;

    Graph::Node u = m_placement.m_inst.demands[m_demandID].s;
    std::size_t j = 0;
    Graph::Path path{u};

    IloNumArray aVal(m_env);
    m_solver.getValues(aVal, m_a);

    IloNumArray fVal(m_env);
    m_solver.getValues(fVal, m_f);

    while (u != t || j < funcs.size())
        ;
    {
        if (j < int(funcs.size()) && m_placement.m_inst.isNFV[u] && aVal[getIndexA(u, j)]) {
            if (installs.empty() || installs.back().first != u) {
                installs.emplace_back(u, std::vector<FunctionDescriptor>());
            }
            installs.back().second.push_back(funcs[j]);
            ++j;
        } else {
            for (const auto& v : m_placement.m_inst.network.getNeighbors(u)) {
                if (fVal[getIndexF(m_placement.m_inst.edgeID(u, v), j)] > 0) {
                    fVal[getIndexF(m_placement.m_inst.edgeID(u, v), j)] = 0;
                    path.push_back(v);
                    u = v;
                    break;
                }
            }
        }
    }

    assert(path.back() == t);
    return ServicePath(path, installs);
}
} // namespace SFC::Energy