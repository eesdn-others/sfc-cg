#ifndef ENERGY_H
#define ENERGY_H

#include <fstream>

#include <iostream>

#include "DiGraph.hpp"
#include "SFC.hpp"
#include "utility.hpp"

namespace SFC::Energy {
static constexpr double RC_EPS = 1.0e-6;

struct Consumption {
    Consumption() = default;
    Consumption(const Consumption&) = default;
    Consumption(Consumption&&) = default;
    Consumption& operator=(const Consumption&) = default;
    Consumption& operator=(Consumption&&) = default;
    static Consumption fromFile(const std::string& _filename);
    double activeLink;
    double propLink;
    double activeCore;
};

inline Consumption Consumption::fromFile(const std::string& _filename) {
    std::cout << "Getting energy consumption from " << _filename << '\n';
    std::ifstream ifs(_filename);
    Consumption cons;
    ifs >> cons.activeLink >> cons.propLink >> cons.activeCore;
    return cons;
}

struct Instance {
    Instance(DiGraph _network, std::vector<int> _nodeCapa, std::vector<Demand> _demands,
        std::vector<double> _funcCharge, std::vector<int> _NFVNodes, Consumption _energyCons);

    bool checkSolution(const std::vector<ServicePath>& sPaths);
    void save(const std::vector<ServicePath>& sPaths, const std::string& _filename, const std::pair<double, double>& _time);

    DiGraph network;
    std::vector<Graph::Edge> edges;
    std::vector<int> nodeCapa;
    std::vector<double> funcCharge;
    std::vector<Demand> demands;

    std::vector<std::pair<double, double>> traffic;
    std::vector<std::vector<Graph::Edge>> sortedNeighbors;

    int maxChainSize;
    std::vector<int> NFVNodes;
    Energy::Consumption energyCons;

    std::vector<int> NFVIndices;
    std::vector<int> isNFV;
    Matrix<double> nbCores;
    Matrix<int> edgeID;
};

template <typename AdditionalInformation = unused>
class Solution {
  public:
    Solution(const Instance* _inst, double _objValue, double _lowerBound, std::vector<ServicePath> _paths,
        AdditionalInformation _additionalInformation = AdditionalInformation())
        : inst(_inst)
        , objValue(_objValue)
        , lowerBound(_lowerBound)
        , paths(std::move(_paths))
        , additionalInformation(std::move(_additionalInformation)) {}

    void save(const std::string& _filename, const std::pair<double, double>& _time) {
        std::ofstream ofs(_filename);
        ofs << objValue << '\t' << lowerBound << '\n';
        ofs << additionalInformation << std::endl;
        ofs << _time.first << '\t' << _time.second << std::endl;

        std::vector<double> linkUsage(inst->network.size(), 0);
        std::vector<int> nodeUsage(inst->network.getOrder(), 0);

        for (const auto & [ nPath, locations, demandID ] : paths) {
            for (auto iteU = nPath.begin(), iteV = std::next(iteU); iteV != nPath.end();
                 ++iteU, ++iteV) {
                linkUsage[inst->edgeID(*iteU, *iteV)] += inst->demands[demandID].d;
            }
            for (int j = 0; j < locations.size(); ++j) {
                nodeUsage[locations[j]] += inst->nbCores(demandID, j);
            }
        }

        ofs << inst->network.getOrder() << '\n';
        for (Graph::Node u = 0; u < inst->network.getOrder(); ++u) {
            ofs << u << '\t' << nodeUsage[u] << '\t' << inst->nodeCapa[u] << '\n';
        }

        ofs << inst->network.size() << '\n';
        int e = 0;
        for (const auto& edge : inst->network.getEdges()) {
            ofs << std::fixed << edge.first << '\t' << edge.second << '\t'
                << linkUsage[e] << '\t' << inst->network.getEdgeWeight(edge) << '\n';
            ++e;
        }

        ofs << paths.size() << '\n';
        for (const auto & [ nPath, locations, demandID ] : paths) {
            ofs << nPath.size() << '\t';
            for (const auto& u : nPath) {
                ofs << u << '\t';
            }

            for (int j = 0; j < locations.size(); ++j) {
                ofs << locations[j] << '\t' << inst->demands[demandID].functions[j] << '\t';
            }

            ofs << '\n';
        }
        std::cout << "Results saved to :" << _filename << '\n';
    }

  private:
    const Instance* inst;
    double objValue;
    double lowerBound;
    std::vector<ServicePath> paths;
    AdditionalInformation additionalInformation;
};

Instance::Instance(DiGraph _network, std::vector<int> _nodeCapa, std::vector<Demand> _demands,
    std::vector<double> _funcCharge, std::vector<int> _NFVNodes, Consumption _energyCons)
    : network(std::move(_network))
    , edges(network.getEdges())
    , nodeCapa(std::move(_nodeCapa))
    , funcCharge(std::move(_funcCharge))
    , demands(std::move(_demands))
    , traffic([&]() {
        std::vector<std::pair<double, double>> tmp(network.getOrder());
        for (std::size_t i = 0; i < demands.size(); ++i) {
            tmp[demands[i].t].first += demands[i].d;
            tmp[demands[i].s].second += demands[i].d;
        }
        return tmp;
    }())
    , sortedNeighbors([&]() {
        std::vector<std::vector<Graph::Edge>> tmp(network.getOrder());
        for (Graph::Node u = 0; u < network.getOrder(); ++u) {
            for (const auto& v : network.getNeighbors(u)) {
                tmp[u].emplace_back(u, v);
            }

            std::sort(tmp[u].begin(), tmp[u].end(),
                [&](const Graph::Edge& _e1, const Graph::Edge& _e2) {
                    return network.getEdgeWeight(_e1) > network.getEdgeWeight(_e2);
                });
        }
        return tmp;
    }())
    , maxChainSize(std::max_element(demands.begin(), demands.end(),
          [&](const Demand& _d1, const Demand& _d2) {
              return _d1.functions.size() > _d2.functions.size();
          })
                       ->functions.size())
    , NFVNodes(std::move(_NFVNodes))
    , energyCons(std::move(_energyCons))
    , NFVIndices([&]() {
        std::vector<int> tmp(network.getOrder(), -1);
        for (std::size_t i = 0; i < NFVNodes.size(); ++i) {
            tmp[NFVNodes[i]] = i;
        }
        return tmp;
    }())
    , isNFV([&]() {
        std::vector<int> tmp(network.getOrder(), 0);
        for (const auto& u : NFVNodes) {
            tmp[u] = 1;
        }
        return tmp;
    }())
    , nbCores([&]() {
        Matrix<double> retval(demands.size(), maxChainSize, -1);

        for (int i = 0; i < demands.size(); ++i) {
            for (int j(0); j < demands[i].functions.size(); ++j) {
                retval(i, j) = demands[i].d / funcCharge[demands[i].functions[j]];
                if (retval(i, j) < 1e-9) {
                    std::cout << "Warning: Function " << j << " of demand " << i << " use very little cores: " << retval(i, j) << '\n';
                }
            }
        }
        return retval;
    }())
    , edgeID([&]() {
        Matrix<int> tmp(network.getOrder(), network.getOrder(), -1);
        std::size_t i = 0;
        for (const Graph::Edge& edge : edges) {
            tmp(edge.first, edge.second) = i;
            ++i;
        }
        return tmp;
    }()) {}

bool Instance::checkSolution(const std::vector<ServicePath>& sPaths) {
    if (sPaths.size() != demands.size()) {
        std::cerr << "Number of paths is different from number of demands: " << sPaths.size() << " vs. " << demands.size() << '\n';
        return false;
    }
    std::vector<double> linkUsage(network.size(), 0.0);
    std::vector<char> active(network.size(), false);
    std::vector<double> nodeUsage(network.getOrder(), 0.0);

    for (std::size_t i = 0; i < sPaths.size(); ++i) {
        if (sPaths[i].nPath.front() != demands[i].s
            || sPaths[i].nPath.back() != demands[i].t) {
            std::cerr << "Path src/dst is different from demand src/dst: " << std::make_pair(sPaths[i].nPath.front(), sPaths[i].nPath.back()) << " vs. " << std::make_pair(demands[i].s, demands[i].t) << '\n';
            return false;
        }
        const auto& path = sPaths[i].nPath;
        const auto& locations = sPaths[i].locations;

        for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
            linkUsage[edgeID(*iteU, *iteV)] += demands[i].d;
            active[edgeID(*iteU, *iteV)] = true;
            active[edgeID(*iteV, *iteU)] = true;
        }

        for (std::size_t j = 0; j < locations.size(); ++j) {
            nodeUsage[locations[j]] += nbCores(i, j);
        }
    }

    for (auto& coreNb : nodeUsage) {
        coreNb = std::ceil(coreNb);
    }

    std::cout << "Node usage: \n";
    double nodeEnergy = 0.0;
    for (const auto& u : NFVNodes) {
        nodeEnergy += nodeUsage[u] * energyCons.activeCore;
        std::cout << '\t' << u << " -> " << nodeUsage[u] << " / " << nodeCapa[u] << " -> " << nodeUsage[u] * energyCons.activeCore << '\n';
        if (nodeUsage[u] > nodeCapa[u]) {
            return false;
        }
    }
    std::cout << '\t' << nodeEnergy << '\n';

    // Get link usage
    double linkEnergy = 0.0;
    std::cout << "Link usage: \n";
    int e = 0;
    for (const auto& edge : edges) {
        double energy = energyCons.propLink * linkUsage[e] / network.getEdgeWeight(edge);
        if (active[e]) {
            energy += energyCons.activeLink;
        }
        linkEnergy += energy;
        std::cout << std::fixed << '\t' << edge << " -> " << linkUsage[e] << " / " << network.getEdgeWeight(edge) << " -> " << energy << " ->" << static_cast<bool>(active[e]) << '\n';
        if (network.getEdgeWeight(edge) < linkUsage[e]) {
            return false;
        }
        ++e;
    }
    std::cout << nodeEnergy << " + " << linkEnergy << " = " << nodeEnergy + linkEnergy << '\n';
    return true;
}

void Instance::save(const std::vector<ServicePath>& _sPaths, const std::string& _filename, const std::pair<double, double>& _time) {
    std::ofstream ofs(_filename);
    ofs << 0 << '\t' << 0 << '\n';
    ofs << _sPaths.size() << std::endl;
    ofs << _time.first << '\t' << _time.second << std::endl;

    std::vector<double> linkUsage(network.size(), 0);
    std::vector<double> nodeUsage(network.getOrder(), 0);
    // std::vector<int> pathsUsed(demands.size());
    std::vector<char> activeLink(network.size(), 0);

    for (std::size_t i = 0; i < _sPaths.size(); ++i) {
        const auto& locations = _sPaths[i].locations;
        const auto& demand = demands[i];

        for (auto iteU = _sPaths[i].nPath.begin(), iteV = std::next(iteU); iteV != _sPaths[i].nPath.end(); ++iteU, ++iteV) {
            linkUsage[edgeID(*iteU, *iteV)] += demand.d;
            activeLink[edgeID(*iteU, *iteV)] = activeLink[edgeID(*iteV, *iteU)] = 1;
        }

        for (std::size_t j = 0; j < locations.size(); ++j) {
            nodeUsage[locations[j]] += nbCores(i, j);
        }
    }

    ofs << network.getOrder() << '\n';
    for (Graph::Node u = 0; u < network.getOrder(); ++u) {
        ofs << u << '\t' << int(std::ceil(nodeUsage[u])) << '\t' << nodeCapa[u] << '\n';
    }

    ofs << network.size() << '\n';
    std::size_t i = 0;
    for (const auto& edge : edges) {
        ofs << edge.first << '\t' << edge.second << '\t' << linkUsage[i] << '\t' << network.getEdgeWeight(edge);
        int xIndex;
        if (edge.first < edge.second) {
            xIndex = i;
        } else {
            xIndex = edgeID(edge.second, edge.first);
        }
        ofs << '\t' << bool(activeLink[xIndex]) << '\n';
        ++i;
    }

    ofs << _sPaths.size() << '\n';
    for (const auto& sPath : _sPaths) {
        ofs << sPath.nPath.size() << '\t';
        for (const auto& u : sPath.nPath) {
            ofs << u << '\t';
        }
        for (std::size_t j = 0; j < _sPaths[i].locations.size(); ++j) {
            ofs << _sPaths[i].locations[j] << '\t' << demands[_sPaths[i].demand].functions[j] << '\t';
        }
        ofs << '\n';
    }
    std::cout << "Results saved to :" << _filename << '\n';
}
} // namespace SFC::Energy

std::ostream& operator<<(std::ostream& _out, const SFC::Energy::Consumption& _cons);
std::ostream& operator<<(std::ostream& _out, const SFC::Energy::Consumption& _cons) {
    return _out << "[activeLink:" << _cons.activeLink << ", propLink: " << _cons.propLink << ", activeCore:" << _cons.activeCore << "]";
}
#endif
