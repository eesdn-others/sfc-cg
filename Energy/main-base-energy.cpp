#include "EnergyPartition.hpp"
#include "EnergySub.hpp"

#include <MyTimer>

std::vector<Demand> loadDemands(const std::string&, double);
DiGraph loadNetwork(const std::string&);

int main(int argc, char** argv) {
    try {
        std::string name{argv[1]};
        double percent = std::stod(argv[2]);

        const DiGraph network = loadNetwork("./instances/" + name + "_topo.txt");
        const std::vector<Demand> demands = loadDemands("./instances/" + name + "_demand.txt", percent);

        EnergyPartition energy{network, demands};
        energy.solve();

        std::cout << "--------------------------------------------------------------------------------" << std::endl;
        std::cout << "--------------------------------------------------------------------------------" << std::endl;
        std::cout << "--------------------------------------------------------------------------------" << std::endl;
        std::cout << "--------------------------------------------------------------------------------" << std::endl;
        std::cout << "--------------------------------------------------------------------------------" << std::endl;

        ClassicalEnergy cEnergy{network, demands};
        cEnergy.solve();

        // std::cout << "--------------------------------------------------------------------------------" << std::endl;
        // std::cout << "--------------------------------------------------------------------------------" << std::endl;
        // std::cout << "--------------------------------------------------------------------------------" << std::endl;
        // std::cout << "--------------------------------------------------------------------------------" << std::endl;
        // std::cout << "--------------------------------------------------------------------------------" << std::endl;

        // auto optPaths = cEnergy.getPaths();
        // EnergySub energy2 { network, demands };
        // energy2.solve();

    } catch (IloException& e) {
        std::cout << e.getMessage() << std::endl;
    }
}

std::vector<Demand> loadDemands(const std::string& _filename, double _percent) {
    std::vector<Demand> demands;
    std::ifstream ifs(_filename, std::ifstream::in);
    if (!ifs) {
        std::cerr << _filename << " does not exists!" << std::endl;
        exit(-1);
    }
    while (ifs.good()) {
        std::string line;
        std::getline(ifs, line);
        if (line != "") {
            int s, t;
            double d;
            std::stringstream lineStream(line);
            lineStream >> s >> t >> d;
            demands.emplace_back(s, t, _percent * d);
        }
    }
    return demands;
}

DiGraph loadNetwork(const std::string& _filename) {
    auto network = DiGraph::loadFromFile(_filename);
    return std::get<0>(network);
}