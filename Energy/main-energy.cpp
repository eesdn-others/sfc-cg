#include <MyTimer.hpp>
#include <tclap/CmdLine.hpp>

#include "ColumnGeneration.hpp"
#include "Energy.hpp"
#include "Heuristic.hpp"
#include "SFC.hpp"
#include <ilcplex/ilocplex.h>
// Path
#include "Chaining.hpp"
// #include "ChainingPath2.hpp"
// #include "ChainingPath3.hpp"
// #include "ChainingPlacement.hpp"
// #include "GenCut.hpp"

#include <numeric>

struct Param {
    std::string model;
    std::string name;
    int nbThreads;
    double factor;
    double percentDemand;
    int energyCons;
};

Param getParams(int argc, char** argv);
Param getParams(int argc, char** argv) {
    TCLAP::CmdLine cmd("Solve the SFC problem with column generation", ' ', "1.0");

    TCLAP::ValuesConstraint<std::string> vCons({"path", "pathBF"
        //"checkSolution", , "path2", "path3", "pathGen", "heur", "heur_path", "path_fromILPHeur", "path2_fromILPHeur", "path3_fromILPHeur"
    });
    TCLAP::ValueArg<std::string> modelName("m", "model", "Specify the model used",
        false, "path", &vCons);
    cmd.add(&modelName);

    TCLAP::UnlabeledValueArg<std::string> networkName("network", "Specify the network name",
        true, "", "network");
    cmd.add(&networkName);

    TCLAP::ValueArg<double> demandFactor("d", "demandFactor", "Specify the multiplicative factor for the demands",
        false, 1, "int");
    cmd.add(&demandFactor);

    TCLAP::ValueArg<int> nbThreads(
        "j", "nbThreads", "Specify the number of threads",
        false, 1, "int");
    cmd.add(&nbThreads);

    TCLAP::ValueArg<int> energyCons("e", "energyCons", "Specify the index for the energy consumption file",
        false, 1, "int");
    cmd.add(&energyCons);

    TCLAP::ValueArg<double> percentDemand("p", "percentDemand", "Specify the percentage of demand used",
        false, 100, "double");
    cmd.add(&percentDemand);

    cmd.parse(argc, argv);

    return {modelName.getValue(), networkName.getValue(), nbThreads.getValue(),
        demandFactor.getValue(), percentDemand.getValue(), energyCons.getValue()};
}

std::vector<SFC::ServicePath> getServicePathsFromILPHeur(const std::vector<SFC::Demand>& _demands, const std::string& _name, const double& _factor, const int _funcCoef);

int main(int argc, char** argv) {
    try {
        const auto params = getParams(argc, argv);
        std::string folderName = "./instances/";
        DiGraph<double> network = SFC::loadNetwork(folderName + params.name + "_topo.txt");
        // std::vector<double> funcCharge = SFC::loadFunctions(folderName + "func" + std::to_string(params.funcCoef) + ".txt");
        std::vector<int> nodeCapas = SFC::loadNodeCapa(folderName + params.name + "_nodeCapa.txt");
        SFC::Energy::Consumption energyCons = SFC::Energy::Consumption::fromFile(folderName + "energy" + std::to_string(params.energyCons) + ".txt");
        std::cout << "Files loaded...\n";

        auto[demands, funcChargeName] = SFC::loadDemands(folderName + params.name + "_demand.txt", params.factor);
        const int nbDemands = static_cast<int>(std::ceil(demands.size() * params.percentDemand / 100.0));
        demands.erase(demands.begin() + nbDemands, demands.end());
        const auto& funcCharge = funcChargeName;
        const double totalNbCores = std::accumulate(demands.begin(), demands.end(), 0.0,
            [&](const double t__nbCores, const SFC::Demand& t_demand) {
                // Sum of cores needed for the demand
                return t__nbCores + std::accumulate(t_demand.functions.begin(), t_demand.functions.end(), 0.0, 
                    [&](const double t_nbCores, const int t_func) {
                    return t_nbCores + t_demand.d / funcCharge[t_func];
                });
            });
        std::cout << totalNbCores << " needed cores!" << '\n';

        std::vector<int> NFVNodes;
        for (Graph::Node u = 0; u < network.getOrder(); ++u) {
            if (nodeCapas[u] != -1) {
                NFVNodes.push_back(u);
            }
        }
        int availableCores = 0;
        for (const auto& u : NFVNodes) {
            availableCores += nodeCapas[u];
        }
        std::cout << availableCores << " cores available !\n";

        std::cout << std::accumulate(demands.begin(), demands.end(), 0.0,
                         [&](const double& _bandwidth, const SFC::Demand& _demand) {
                             return _bandwidth + _demand.d;
                         })
                  << " total bandwidth!\n";

        SFC::Energy::Instance instance(network, nodeCapas, demands, funcCharge, NFVNodes, energyCons);

        Time timer;
        timer.start();
        std::string filename = params.percentDemand != 100.0
                                   ? "./results/energy/" + params.name + "_" + std::to_string(nbDemands) + "_" + params.model + "_" + std::to_string(params.factor) + ".res"
                                   : "./results/energy/" + params.name + "_" + params.model + "_" + std::to_string(params.factor) + ".res";

        if (params.model == "path") {
            SFC::Energy::Heuristic heur = SFC::Energy::solveEESFC(instance);
             if (heur.getState() == SFC::Energy::Heuristic::State::Solved) {
                 std::cout << "Energy used: " << heur.getEnergyUsed() << std::endl;
                 if (!heur.checkSolution()) {
                     std::cerr << "Solution invalid!\n";
                     exit(-1);
                 }
                 
                 ColumnGenerationModel<SFC::Energy::Instance, SFC::Energy::Placement_Path> cgModel(&instance);
                 cgModel.addInitConf(heur.getSPath());
            if (cgModel.solve<SFC::Energy::PricingProblem>(params.nbThreads)) {
                //cgModel.getSolution().save(filename, timer.get());
            }
             }
            
        } else if (params.model == "pathBF") {
            ColumnGenerationModel<SFC::Energy::Instance, SFC::Energy::Placement_Path> cgModel(&instance);
            if (cgModel.solve<SFC::Energy::PricingProblemBF>(params.nbThreads)) {
                //cgModel.getSolution().save(filename, timer.get());
            }
        }
        // if (params.model == "heur") {
        //     Energy::Heuristic heur = Energy::solveEESFC(instance);
        //     if (heur.getState() == Energy::Heuristic::State::Solved) {
        //         std::cout << "Energy used: " << heur.getEnergyUsed() << std::endl;
        //         assert(heur.checkSolution());
        //         heur.save(filename, timer.show(), network);
        //     } else {
        //         std::cout << "No solution found!\n";
        //         return -1;
        //     }
        // } else if (params.model == "path") {
        //     std::cout << "Getting initial configuration...\n";
        //     Energy::Heuristic heur = Energy::solveEESFC(instance);
        //     if (heur.getState() == Energy::Heuristic::State::Solved) {
        //         std::cout << "Energy used: " << heur.getEnergyUsed() << std::endl;
        //         if (!heur.checkSolution()) {
        //             std::cerr << "Solution invalid!\n";
        //             exit(-1);
        //         }
        //         Energy::ChainPlacement css(&instance);
        //         css.addInitConf(heur.getSPath());
        //         if (css.solveInteger()) {
        //             css.save(filename, timer.show());
        //         }
        //     } else {
        //         std::cout << "No initial configuration\n";
        //         return -1;
        //     }
        // } else if (params.model == "path_fromILPHeur") {
        //     std::cout << "Getting initial configuration...\n";
        //     Energy::ChainPlacement css(&instance);
        //     auto sPaths = getServicePathsFromILPHeur(demands, params.name, params.factor, params.funcCoef);
        //     instance.checkSolution(sPaths);
        //     css.addInitConf(sPaths);
        //     if (css.solveInteger()) {
        //         css.save(filename, timer.show());
        //     }
        // } else if (params.model == "checkSolution") {
        //     std::cout << "Loading solution...\n";
        //     auto sPaths = getServicePathsFromILPHeur(demands, params.name, params.factor, params.funcCoef);
        //     std::cout << "Checking solution...\n";
        //     instance.checkSolution(sPaths);
        //     instance.save(sPaths, filename, timer.show());
        // } else if (params.model == "path2_fromILPHeur") {
        //     std::cout << "Getting initial configuration...\n";
        //     Energy::ChainPlacementPath2 css(&instance);
        //     auto sPaths = getServicePathsFromILPHeur(demands, params.name, params.factor, params.funcCoef);
        //     instance.checkSolution(sPaths);
        //     css.addInitConf(sPaths);
        //     if (css.solveInteger()) {
        //         css.save(filename, timer.show());
        //     }
        // } else if (params.model == "path3_fromILPHeur") {
        //     std::cout << "Getting initial configuration...\n";
        //     Energy::ChainPlacement3 css(&instance);
        //     auto sPaths = getServicePathsFromILPHeur(demands, params.name, params.factor, params.funcCoef);
        //     instance.checkSolution(sPaths);
        //     css.addInitConf(sPaths);
        //     if (css.solveInteger()) {
        //         css.save(filename, timer.show());
        //     }
        // } else if (params.model == "pathGen") {
        //     std::cout << "Getting initial configuration...\n";
        //     Energy::Heuristic heur = Energy::solveEESFC(instance);
        //     if (heur.getState() == Energy::Heuristic::State::Solved) {
        //         std::cout << "Energy used: " << heur.getEnergyUsed() << std::endl;
        //         assert(heur.checkSolution());
        //         Energy::GenCut genCut(instance);
        //         std::vector<double> outDemands(instance.network.getOrder());
        //         for (const auto& demand : instance.demands) {
        //             outDemands[demand.s] += demand.d;
        //         }

        //         for (Graph::Node u = 0; u < instance.network.getOrder(); ++u) {
        //             auto neighbors = instance.network.getNeighbors(u);
        //             std::sort(neighbors.begin(), neighbors.end(), [&](const Graph::Node& __v1, const Graph::Node& __v2) {
        //                 return instance.network.getEdgeWeight(u, __v1) > instance.network.getEdgeWeight(u, __v2);
        //             });
        //             std::size_t i = 0;
        //             while (outDemands[u] > 0) {
        //                 outDemands[u] -= instance.network.getEdgeWeight(u, neighbors[i]);
        //                 ++i;
        //             }
        //             Energy::GenCut::Column col;
        //             for (const auto& v : neighbors) {
        //                 int realE = u < v ? instance.edgeID(u, v) : instance.edgeID(v, u);
        //                 col.edgeIDs.push_back(realE);
        //             }
        //             col.minEdge = i;
        //             genCut.addColumn(col);
        //         }
        //         genCut.solve();

        //         Energy::ChainPlacement css(&instance);
        //         for (const auto& col : genCut.getCuts()) {
        //             css.addCut(col);
        //         }
        //         css.addInitConf(heur.getSPath());
        //         if (css.solveInteger()) {
        //             css.save(filename, timer.show());
        //         }
        //     }
        // } else if (params.model == "path2") {
        //     std::cout << "Getting initial configuration...\n";
        //     Energy::Heuristic heur = Energy::solveEESFC(instance);
        //     if (heur.getState() == Energy::Heuristic::State::Solved) {
        //         std::cout << "Energy used: " << heur.getEnergyUsed() << std::endl;
        //         assert(heur.checkSolution());
        //         Energy::ChainPlacementPath2 css(&instance);
        //         css.addInitConf(heur.getSPath());
        //         if (css.solveInteger()) {
        //             css.save(filename, timer.show());
        //         }
        //     } else {
        //         std::cout << "No model specified!\n";
        //         return -1;
        //     }
        // } else if (params.model == "path3") {
        //     std::cout << "Getting initial configuration...\n";
        //     Energy::Heuristic heur = Energy::solveEESFC(instance);
        //     if (heur.getState() == Energy::Heuristic::State::Solved) {
        //         std::cout << "Energy used: " << heur.getEnergyUsed() << std::endl;
        //         assert(heur.checkSolution());
        //         Energy::ChainPlacement3 css(&instance);
        //         css.addInitConf(heur.getSPath());
        //         if (css.solveInteger()) {
        //             css.save(filename, timer.show());
        //         }
        //     } else {
        //         std::cout << "No initial configuration\n";
        //         return -1;
        //     }
        // } else if (params.model == "heur_path") {
        //     std::cout << "Getting initial configuration...\n";
        //     Energy::Heuristic heur = Energy::solveEESFC(instance);
        //     if (heur.getState() == Energy::Heuristic::State::Solved) {
        //         std::cout << "Energy used: " << heur.getEnergyUsed() << std::endl;
        //         assert(heur.checkSolution());
        //         Energy::ChainingPlacementFixedEdge css(instance);
        //         // css.addInitConf(heur.getSPath());

        //         if (!css.solveInteger()) {
        //             std::cerr << "No solution found!\n";
        //             exit(-1);
        //         }
        //         // auto edgesToRemove = css.getEdgesByUsage();
        //         double bestSol = css.getObjValue();
        //         for (std::size_t i = 0; i < instance.network.size() / 2; ++i) {
        //             // while(!edgesToRemove.empty()) {
        //             // css is in solved state
        //             auto edge = css.getLessUsedEdge();
        //             // for(const auto& edge : edgesToRemove) {
        //             css.removeEdge(edge);
        //             if (!css.solveInteger()
        //                 || css.getObjValue() > bestSol) {
        //                 std::cout << "Failed to remove " << edge << "\n";
        //                 css.restoreEdge(edge);
        //                 css.solveInteger();
        //             } else {
        //                 bestSol = css.getObjValue();
        //                 std::cout << "Removed " << edge << "\n";
        //                 // edgesToRemove = css.getEdgesByUsage();
        //                 // break;
        //             }
        //             // }
        //         }
        //         css.save(filename, timer.show());                               }
        //     } else {
        //         std::cout << "No initial configuration\n";
        //         return -1;
        //     }
        // } else {
        //     std::cout << "No initial configuration\n";
        //     return -1;
        // }
    } catch (IloException& e) {
        std::cout << e.getMessage() << '\n';
    } catch (const TCLAP::ArgException& e) {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << '\n';
    }
}
std::vector<SFC::ServicePath> getServicePathsFromILPHeur(const std::vector<SFC::Demand>& _demands, const std::string& _name, const double& _factor, const int _funcCoef);
std::vector<SFC::ServicePath> getServicePathsFromILPHeur(const std::vector<SFC::Demand>& _demands, const std::string& _name, const double& _factor, const int _funcCoef) {
    const std::string filename = "./results/energy/" + _name + "_ILPHeur_" + std::to_string(_factor) + "_" + std::to_string(_funcCoef) + ".res";
    std::cout << "Opening " << filename << '\n';
    std::ifstream ifs(filename);
    std::vector<SFC::ServicePath> sPaths;
    if (!ifs.good()) {
        std::cout << "ifs not good\n";
        return sPaths;
    }
    sPaths.reserve(_demands.size());
    double totalEnergy, linkEnergy, nodeEnergy;
    ifs >> totalEnergy >> linkEnergy >> nodeEnergy;
    std::size_t i = 0;
    do {
        ifs >> i;
        std::cout << i << '\n';
        int pathSize;
        ifs >> pathSize;
        Graph::Path path(pathSize);
        for (int k = 0; k < pathSize; ++k) {
            ifs >> path[k];
        }

        std::vector<int> locations;
        Graph::Node loc;
        for (std::size_t j = 0; j < _demands[i].functions.size(); ++j) {
            ifs >> loc;
            locations.push_back(loc);
        }
        sPaths.emplace_back(path, locations, i);
        ++i;
    } while (i < _demands.size() && ifs.good());
    return sPaths;
}
