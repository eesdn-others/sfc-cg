from subprocess import call
import sys

orders = {"atlanta": 15, "internet2": 10, "germany50": 50}

percents = {"germany50": [1, 2.25, 3.5, 5.25 ,6], 
	"germany50_HW": [1, 2.25, 3.5, 5.25 ,6], 
	"atlanta": [1, 2.25, 3.5, 5.25 ,6], 
	"atlanta_HW": [1, 2.25, 3.5, 5.25 ,6], 
	"ta2": [1, 2.75, 4.5, 6.25, 8], 
	"zib54": [1, 3, 5, 7, 9],
	"abilene": [1],
	"pdh": [1, 2.25, 3.5, 5.25 ,6],
	"pdh_HW": [1, 2.25, 3.5, 5.25 ,6]#[1, 2.75, 4.5, 6.25, 8]
}


funcCoefs = {"germany50": 5, 
	"germany50_HW": 5, 
	"atlanta": 6,
	"atlanta_HW": 6,
	"ta2": 5, 
	"zib54": 5,
	"abilene": 5,
	"pdh": 5,
	"pdh_HW": 5
}

names = [sys.argv[1]] if sys.argv[1] != "all" else ["atlanta_HW", "germany50_HW", "pdh_HW"]#, "atlanta", "germany50", "pdh"]
models = [sys.argv[2]] if sys.argv[2] != "all" else ["path", "path2", "path3"]

for name in names:
	for percent in percents[name]:
		heur_command = "./HeurNRJ {} -c {} -d {}".format(name, funcCoefs[name], percent)
		print heur_command
		call(heur_command, shell=True)
	
	for model in models:
		for percent in percents[name]:
			LP_command = "./LP_NRJ {} -c {} -d {} -m {}".format(name, funcCoefs[name], percent, model)
			print LP_command
			call(LP_command, shell=True)
