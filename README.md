# Introduction

This repository contains algorithms for the Service Function Chaining routing and placement problem.

It features all the algorithms published in [Optimal network service chain provisioning](https://hal.inria.fr/hal-01920951/document) and 
[Energy-efficient service function chain provisioning](https://hal.inria.fr/hal-01920960/document).

# Compiling

It requires CMake 3.3+ and a compiler compliant with the C++17 standard.

To compile follow:

```bash
mkdir build
cd build
cmake ..
make LP
```