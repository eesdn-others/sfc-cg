#ifndef LOCALISATION_HPP
#define LOCALISATION_HPP

#include <SFC.hpp>
#include <ShortestPath.hpp>
#include <cplex_utility.hpp>

namespace SFC::OccLim {
class Localisation {
  public:
    Localisation(Instance& _instance);
    ~Localisation();
    bool solve();
    IloNumArray getLocalisation() const;

  private:
    inline int getIndexX(const Graph::Node _u, const int _i, const int _j) const;
    inline int getIndexB(const Graph::Node _u, const function_descriptor _f) const;
    Instance& m_instance;

    IloEnv m_env{};
    IloModel m_model;

    IloNumVarArray m_x;
    IloNumVarArray m_b;

    IloObjective m_obj;

    IloRangeArray m_oneNodePerDemands;
    IloRangeArray m_nodeCapacities;
    IloRangeArray m_nbLicenses;
    IloRangeArray m_demandNodeLinkage;
    IloSOS1Array m_oneNodePerDemandsSOS1;

    IloCplex m_solver;
};

Localisation::Localisation(Instance& _instance)
    : m_instance(_instance)
    , m_env()
    , m_model(m_env)
    , m_x([&]() {
        IloNumVarArray x(m_env, m_instance.network.getOrder() * m_instance.demands.size() * m_instance.maxChainSize);
        for (const auto& u : m_instance.NFVNodes) {
            for (int i = 0; i < m_instance.demands.size(); ++i) {
                for (int j = 0; j < m_instance.demands[i].functions.size(); ++j) {
                    x[getIndexX(u, i, j)] = IloAdd(m_model, IloNumVar(m_env, 0.0, 1.0, ILOBOOL));
                    setIloName(x[getIndexX(u, i, j)], "x" + toString(std::make_pair(i, j)));
                }
            }
        }
        return x;
    }())
    , m_b([&]() {
        IloNumVarArray b(m_env, m_instance.NFVNodes.size() * m_instance.funcCharge.size());
        for (const auto& u : m_instance.NFVNodes) {
            for (int f = 0; f < m_instance.funcCharge.size(); ++f) {
                b[getIndexB(u, f)] = IloAdd(m_model, IloNumVar(m_env, 0.0, 1.0, ILOBOOL));
                setIloName(b[getIndexB(u, f)], "b" + toString(std::make_pair(u, f)));
            }
        }
        return b;
    }())
    , m_obj([&]() {
        IloNumVarArray vars(m_env);
        IloNumArray vals(m_env);
        ShortestPath<DiGraph> sp(m_instance.network);
        for (const auto& u : m_instance.NFVNodes) {
            for (int i = 0; i < m_instance.demands.size(); ++i) {
                for (int j = 0; j < m_instance.demands[i].functions.size(); ++j) {
                    vars.add(m_x[getIndexX(u, i, j)]);
                    //@TODO: Implement a real all shortest paths
                    sp.clear();
                    int dist = sp.getShortestPath(m_instance.demands[i].s, u).size();
                    sp.clear();
                    dist += sp.getShortestPath(u, m_instance.demands[i].t).size();
                    vals.add(dist);
                }
            }
        }
        auto obj = IloMinimize(m_env, IloScalProd(vars, vals));
        vars.end();
        vals.end();
        return IloAdd(m_model, obj);
    }())
    , m_oneNodePerDemands([&]() {
        IloRangeArray oneNodePerDemands(m_env, m_instance.demands.size() * m_instance.maxChainSize);
        for (int i = 0; i < m_instance.demands.size(); ++i) {
            for (int j = 0; j < m_instance.demands[i].functions.size(); ++j) {
                IloNumVarArray vars(m_env);
                for (const auto& u : m_instance.NFVNodes) {
                    vars.add(m_x[getIndexX(u, i, j)]);
                }
                oneNodePerDemands[m_instance.demands.size() * j + i] = IloAdd(m_model, IloRange(m_env, 1.0, IloSum(vars), 1.0));
                vars.end();
            }
        }
        return oneNodePerDemands;
    }())
    , m_nodeCapacities([&]() {
        IloRangeArray nodeCapacities(m_env, m_instance.NFVNodes.size());
        for (const auto& u : m_instance.NFVNodes) {
            const int index = m_instance.NFVIndices[u];
            IloNumVarArray vars(m_env);
            IloNumArray vals(m_env);
            for (int i = 0; i < m_instance.demands.size(); ++i) {
                for (int j = 0; j < m_instance.demands[i].functions.size(); ++j) {
                    vars.add(m_x[getIndexX(u, i, j)]);
                    vals.add(m_instance.nbCores(i, j));
                }
            }
            nodeCapacities[index] = IloAdd(m_model, IloRange(m_env, 0.0, IloScalProd(vars, vals), m_instance.nodeCapa[u]));
            vars.end();
            vals.end();
        }
        return nodeCapacities;
    }())
    , m_nbLicenses([&]() {
        IloRangeArray nbLicenses(m_env, m_instance.funcCharge.size());
        for (int f = 0; f < m_instance.funcCharge.size(); ++f) {
            IloNumVarArray vars(m_env);
            for (const auto& u : m_instance.NFVNodes) {
                vars.add(m_b[getIndexB(u, f)]);
            }
            // nbLicenses[f] = IloAdd(m_model, IloRange(m_env, m_instance.nbLicenses, IloSum(vars), m_instance.nbLicenses));
            nbLicenses[f] = IloAdd(m_model, IloRange(m_env, 0.0, IloSum(vars), m_instance.nbLicenses));
            vars.end();
        }
        return nbLicenses;
    }())
    , m_demandNodeLinkage([&]() {
        IloRangeArray demandNodeLinkage(m_env, m_instance.network.getOrder() * m_instance.demands.size() * m_instance.maxChainSize);
        for (const auto& u : m_instance.NFVNodes) {
            for (int i = 0; i < m_instance.demands.size(); ++i) {
                for (int j = 0; j < m_instance.demands[i].functions.size(); ++j) {
                    const function_descriptor f = m_instance.demands[i].functions[j];
                    demandNodeLinkage[getIndexX(u, i, j)] = IloAdd(m_model, IloRange(m_env, 0.0, m_b[getIndexB(u, f)] - m_x[getIndexX(u, i, j)], IloInfinity));
                }
            }
        }
        return demandNodeLinkage;
    }())
    , m_oneNodePerDemandsSOS1([&]() {
        IloSOS1Array oneNodePerDemandsSOS(m_env, m_instance.demands.size() * m_instance.maxChainSize);
        for (int i = 0; i < m_instance.demands.size(); ++i) {
            for (int j = 0; j < m_instance.demands[i].functions.size(); ++j) {
                IloNumVarArray vars(m_env);
                for (const auto& u : m_instance.NFVNodes) {
                    vars.add(m_x[getIndexX(u, i, j)]);
                }
                oneNodePerDemandsSOS[m_instance.demands.size() * j + i] = IloAdd(m_model, IloSOS1(m_env, vars));
            }
        }
        return oneNodePerDemandsSOS;
    }())
    , m_solver([&]() {
        IloCplex solver(m_model);
        // solver.setOut(m_env.getNullStream());
        solver.setParam(IloCplex::Threads, 1);
        for (IloInt i = 0; i < m_b.getSize(); ++i) {
            solver.setPriority(m_b[i], 2);
        }
        return solver;
    }()) {}

bool Localisation::solve() {
    return m_solver.solve();
}

IloNumArray Localisation::getLocalisation() const {
    IloNumArray b(m_env);
    m_solver.getValues(b, m_b);

    // Check node capas
    for (const auto& u : m_instance.NFVNodes) {
        // const int index = m_instance.NFVIndices[u];
        double charge = 0.0;
        for (int i = 0; i < m_instance.demands.size(); ++i) {
            for (int j = 0; j < m_instance.demands[i].functions.size(); ++j) {
                if (epsilon_equal<double>()(m_solver.getValue(m_x[getIndexX(u, i, j)]), 1.0)) {
                    charge += m_instance.nbCores(i, j);
                }
            }
        }
        std::cout << charge << " / " << m_instance.nodeCapa[u] << '\n';
    }

    std::vector<int> nbCores(m_instance.funcCharge.size(), 0);
    for (int i = 0; i < m_instance.demands.size(); ++i) {
        for (int j = 0; j < m_instance.demands[i].functions.size(); ++j) {
            nbCores[m_instance.demands[i].functions[j]] += m_instance.nbCores(i, j);
        }
    }

    std::cout << m_solver.getObjValue() << '\n';
    for (int f = 0; f < m_instance.funcCharge.size(); ++f) {
        std::cout << "Function " << f << " on ";
        int totalCapa = 0;
        for (const auto& u : m_instance.NFVNodes) {
            if (epsilon_equal<double>()(b[getIndexB(u, f)], 1.0)) {
                std::cout << u << ", ";
                totalCapa += m_instance.nodeCapa[u];
            }
        }
        std::cout << " -> " << nbCores[f] << '/' << totalCapa << '\n';
    }
    return b;
}

Localisation::~Localisation() {
    m_env.end();
}

int Localisation::getIndexX(const Graph::Node _u, const int _i, const int _j) const {
    assert(_u < m_instance.network.getOrder());
    assert(_i < m_instance.demands.size());
    assert(_j < m_instance.maxChainSize);
    return m_instance.network.getOrder() * m_instance.demands.size() * _j
           + m_instance.network.getOrder() * _i
           + m_instance.NFVIndices[_u];
}

inline int Localisation::getIndexB(const Graph::Node _u, const function_descriptor _f) const {
    assert(_f < m_instance.funcCharge.size());
    assert(_u < m_instance.network.getOrder());
    return m_instance.NFVNodes.size() * _f + m_instance.NFVIndices[_u];
}
} // namespace SFC::OccLim
#endif
