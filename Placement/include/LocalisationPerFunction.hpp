#ifndef LOCALISATIONPERFUNCTION_HPP
#define LOCALISATIONPERFUNCTION_HPP

#include <SFC.hpp>
#include <ShortestPath.hpp>
#include <cplex_utility.hpp>

namespace SFC::OccLim {
class LocalisationPerFunction {
  public:
    LocalisationPerFunction(Instance& _instance);
    ~LocalisationPerFunction();
    bool solve();
    IloNumArray getLocalisation() const;
    double getUsedCapacities(const Graph::Node& _u);
    void setFunction(const function_descriptor _f);
    void updateCapacities();

  private:
    inline int getIndexX(const Graph::Node _u, const int _i, const int _j) const;
    inline int getIndexB(const Graph::Node _u, const function_descriptor _f) const;
    Instance& m_instance;
    function_descriptor m_function;

    IloEnv m_env{};
    IloModel m_model;

    IloNumVarArray m_x;
    IloNumVarArray m_b;

    IloObjective m_obj;

    IloRangeArray m_oneNodePerDemands;
    IloRangeArray m_nodeCapacities;
    IloRangeArray m_nbLicenses;
    IloRangeArray m_demandNodeLinkage;

    IloCplex m_solver;
};

LocalisationPerFunction::LocalisationPerFunction(Instance& _instance)
    : m_instance(_instance)
    , m_function(-1)
    , m_env()
    , m_model(m_env)
    , m_x([&]() {
        IloNumVarArray x(m_env, m_instance.network.getOrder() * m_instance.demands.size() * m_instance.maxChainSize);
        for (const auto& u : m_instance.NFVNodes) {
            for (int i = 0; i < m_instance.demands.size(); ++i) {
                for (int j = 0; j < m_instance.demands[i].functions.size(); ++j) {
                    x[getIndexX(u, i, j)] = IloAdd(m_model, IloNumVar(m_env, 0.0, 1.0, ILOBOOL));
                    setIloName(x[getIndexX(u, i, j)], "x" + toString(std::make_pair(i, j)));
                }
            }
        }
        return x;
    }())
    , m_b([&]() {
        IloNumVarArray b(m_env, m_instance.NFVNodes.size() * m_instance.funcCharge.size());
        for (const auto& u : m_instance.NFVNodes) {
            for (int f = 0; f < m_instance.funcCharge.size(); ++f) {
                b[getIndexB(u, f)] = IloAdd(m_model, IloNumVar(m_env, 0.0, 1.0, ILOBOOL));
                setIloName(b[getIndexB(u, f)], "b" + toString(std::make_pair(u, f)));
            }
        }
        return b;
    }())
    , m_obj([&]() {
        IloNumVarArray vars(m_env);
        IloNumArray vals(m_env);
        ShortestPath<DiGraph> sp(m_instance.network);
        for (const auto& u : m_instance.NFVNodes) {
            for (int i = 0; i < _instance.demands.size(); ++i) {
                for (int j = 0; j < m_instance.demands[i].functions.size(); ++j) {
                    vars.add(m_x[getIndexX(u, i, j)]);
                    //@TODO: Implement a real all shortest paths
                    sp.clear();
                    int dist = sp.getShortestPath(m_instance.demands[i].s, u).size();
                    sp.clear();
                    dist += sp.getShortestPath(u, m_instance.demands[i].t).size();
                    vals.add(dist);
                }
            }
        }

        auto obj = IloAdd(m_model, IloMinimize(m_env, IloScalProd(vars, vals)));
        vars.end();
        vals.end();
        return obj;
    }())
    , m_oneNodePerDemands([&]() {
        IloRangeArray oneNodePerDemands(m_env, m_instance.demands.size() * m_instance.maxChainSize);
        for (int i = 0; i < m_instance.demands.size(); ++i) {
            for (int j = 0; j < m_instance.demands[i].functions.size(); ++j) {
                IloNumVarArray vars(m_env);
                for (const auto& u : m_instance.NFVNodes) {
                    vars.add(m_x[getIndexX(u, i, j)]);
                }
                oneNodePerDemands[m_instance.demands.size() * j + i] = IloAdd(m_model, IloRange(m_env, 0.0, IloSum(vars), 0.0));
                vars.end();
            }
        }
        return oneNodePerDemands;
    }())
    , m_nodeCapacities([&]() {
        IloRangeArray nodeCapacities(m_env, m_instance.NFVNodes.size());
        for (const auto& u : m_instance.NFVNodes) {
            const int index = m_instance.NFVIndices[u];
            IloNumVarArray vars(m_env);
            IloNumArray vals(m_env);
            for (int i = 0; i < m_instance.demands.size(); ++i) {
                for (int j = 0; j < m_instance.demands[i].functions.size(); ++j) {
                    vars.add(m_x[getIndexX(u, i, j)]);
                    vals.add(m_instance.nbCores(i, j));
                }
            }
            nodeCapacities[index] = IloAdd(m_model, IloRange(m_env, 0.0, IloScalProd(vars, vals), m_instance.nodeCapa[u]));
            vars.end();
            vals.end();
        }
        return nodeCapacities;
    }())
    , m_nbLicenses([&]() {
        IloRangeArray nbLicenses(m_env, m_instance.funcCharge.size());
        for (int f = 0; f < m_instance.funcCharge.size(); ++f) {
            IloNumVarArray vars(m_env);
            for (const auto& u : m_instance.NFVNodes) {
                vars.add(m_b[getIndexB(u, f)]);
            }
            nbLicenses[f] = IloAdd(m_model, IloRange(m_env, m_instance.nbLicenses, IloSum(vars), m_instance.nbLicenses));
            vars.end();
        }
        return nbLicenses;
    }())
    , m_demandNodeLinkage([&]() {
        IloRangeArray demandNodeLinkage(m_env, m_instance.network.getOrder() * m_instance.demands.size() * m_instance.maxChainSize);
        for (const auto& u : m_instance.NFVNodes) {
            for (int i = 0; i < m_instance.demands.size(); ++i) {
                for (int j = 0; j < m_instance.demands[i].functions.size(); ++j) {
                    const int f = m_instance.demands[i].functions[j];
                    demandNodeLinkage[getIndexX(u, i, j)] = IloAdd(m_model, IloRange(m_env, 0.0, m_b[getIndexB(u, f)] - m_x[getIndexX(u, i, j)], IloInfinity));
                }
            }
        }
        return demandNodeLinkage;
    }())
    , m_solver([&]() {
        IloCplex solver(m_model);
        solver.setWarning(m_env.getNullStream());
        solver.setOut(m_env.getNullStream());
        for (IloInt i = 0; i < m_b.getSize(); ++i) {
            solver.setPriority(m_b[i], 2);
        }
        return solver;
    }()) {
}

bool LocalisationPerFunction::solve() {
    return m_solver.solve();
}

double LocalisationPerFunction::getUsedCapacities(const Graph::Node& _u) {
    IloNumArray x(m_env);
    m_solver.getValues(x, m_x);

    double usedCapa = 0.0;
    for (int i = 0; i < m_instance.demands.size(); ++i) {
        for (int j = 0; j < m_instance.demands[i].functions.size(); ++j) {
            if (epsilon_equal<double>()(x[getIndexX(_u, i, j)], IloTrue)) {
                usedCapa += m_instance.nbCores(i, j);
            }
        }
    }
    x.end();
    return usedCapa;
}

void LocalisationPerFunction::setFunction(const function_descriptor _f) {
    m_function = _f;
    for (int i = 0; i < m_instance.demands.size(); ++i) {
        for (int j = 0; j < m_instance.demands[i].functions.size(); ++j) {
            const int value = m_instance.demands[i].functions[j] == m_function ? 1.0 : 0.0;
            m_oneNodePerDemands[m_instance.demands.size() * j + i].setBounds(value, value);
        }
    }
}

void LocalisationPerFunction::updateCapacities() {
    for (const auto& u : m_instance.NFVNodes) {
        const int index = m_instance.NFVIndices[u];
        m_nodeCapacities[index].setBounds(0.0, m_instance.nodeCapa[u]);
    }
}

/**
Return an boolean array of size n representing the location of the function on the network
b[u] = 1 iff f is installed of u
*/
IloNumArray LocalisationPerFunction::getLocalisation() const {
    IloNumArray bVal(m_env);
    IloNumArray bResult(m_env);

    m_solver.getValues(bVal, m_b);
    for (const auto& u : m_instance.NFVNodes) {
        bResult.add(bVal[getIndexB(u, m_function)]);
    }
    bVal.end();
    return bResult;
}

LocalisationPerFunction::~LocalisationPerFunction() {
    m_env.end();
}

int LocalisationPerFunction::getIndexX(const Graph::Node _u, const int _i, const int _j) const {
    assert(_u < m_instance.network.getOrder());
    assert(_i < m_instance.demands.size());
    assert(_j < m_instance.maxChainSize);
    return m_instance.network.getOrder() * m_instance.demands.size() * _j
           + m_instance.network.getOrder() * _i
           + m_instance.NFVIndices[_u];
}

inline int LocalisationPerFunction::getIndexB(const Graph::Node _u, const function_descriptor _f) const {
    assert(_f < m_instance.funcCharge.size());
    assert(_u < m_instance.network.getOrder());
    return m_instance.NFVNodes.size() * _f + m_instance.NFVIndices[_u];
}
} // namespace SFC::OccLim

#endif
