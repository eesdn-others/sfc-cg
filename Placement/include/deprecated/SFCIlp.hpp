#ifndef SFC_ILP_HPP
#define SFC_ILP_HPP

#include <ilcplex/ilocplex.h>

#include <cplex_utility.hpp>
#include <utility.hpp>

#include "SFC.hpp"

class SFCIlp {
  public:
    SFCIlp(const DiGraph& _network, const std::vector<int> _nodeCapa, const std::vector<Demand>& _demands, const std::vector<double>& _funcCharge, const std::vector<int>& _NFVNodes)
        : m_inst.network(_network)
        , m_inst.nodeCapa(_nodeCapa)
        , m_funcCharge(_funcCharge)
        , m_inst.demands(_demands)
        , m_inst.maxChainSize(std::max_element(m_inst.demands.begin(), m_inst.demands.end(),
              [&](const Demand& _d1, const Demand& _d2) {
                  return _d1.functions.size() < _d2.functions.size();
              })
                             ->functions.size())
        , m_inst.NFVNodes(_NFVNodes)
        , m_isNFV([&]() {
            std::vector<int> isNFV(m_inst.network.getOrder(), 0);
            for (const auto& u : m_inst.NFVNodes) {
                isNFV[u] = 1;
            }
            return isNFV;
        }())
        , m_inst.nbCores([&]() {
            Matrix<double> mat(m_inst.demands.size(), m_inst.maxChainSize, -1);

            for (int i = 0; i < int(m_inst.demands.size()); ++i) {
                const auto& funcs = m_inst.demands[i].functions;

                for (int j = 0; j < int(funcs.size()); ++j) {
                    mat(i, j) = ceil(m_inst.demands[i].d / m_funcCharge[funcs[j]]);
                }
            }
            return mat;
        }())
        , m_inst.edgeToId([&]() {
            Matrix<int> edgeToId(m_inst.network.getOrder(), m_inst.network.getOrder(), -1);
            const auto edges = m_inst.network.getEdges();
            for (int i = 0; i < int(edges.size()); ++i) {
                edgeToId(edges[i].first, edges[i].second) = i;
            }
            return edgeToId;
        }())
        , m_inst.edges(m_inst.network.getEdges())
        , m_intObj(-1)
        , m_fractObj(-1)
        , m_env()
        , m_model(m_env)
        , m_f([&]() {
            IloNumVarArray f(m_env, m_inst.network.size() * (m_inst.maxChainSize + 1) * m_inst.demands.size());
            for (int e = 0; e < m_inst.network.size(); ++e) {
                for (int i = 0; i < int(m_inst.demands.size()); ++i) {
                    for (int j = 0; j <= int(m_inst.demands[i].functions.size()); ++j) {
                        f[getIndexF(e, i, j)] = IloAdd(m_model, IloNumVar(m_env, 0.0, 1.0, ILOBOOL));
                        setIloName(f[getIndexF(e, i, j)], "f" + toString(std::make_tuple(m_inst.edges[e], i, j)));
                    }
                }
            }
            return f;
        }())
        , m_a([&]() {
            IloNumVarArray a(m_env, m_inst.network.getOrder() * m_inst.maxChainSize * m_inst.demands.size());
            for (const auto& u : m_inst.NFVNodes) {
                for (int i = 0; i < int(m_inst.demands.size()); ++i) {
                    for (int j = 0; j < int(m_inst.demands[i].functions.size()); ++j) {
                        const int aIndex = getIndexA(u, i, j);
                        a[aIndex] = IloAdd(m_model, IloNumVar(m_env, 0.0, 1.0, ILOBOOL));
                        setIloName(a[aIndex], "a" + toString(std::make_tuple(u, i, j)));
                    }
                }
            }
            return a;
        }())
        , m_obj([&]() {
            IloNumVarArray vars(m_env);
            IloNumArray vals(m_env);
            for (int e = 0; e < m_inst.network.size(); ++e) {
                for (int i = 0; i < int(m_inst.demands.size()); ++i) {
                    for (int j = 0; j <= int(m_inst.demands[i].functions.size()); ++j) {
                        vars.add(m_f[getIndexF(e, i, j)]);
                        vals.add(m_inst.demands[i].d);
                    }
                }
            }
            return IloAdd(m_model, IloMinimize(m_env, IloScalProd(vars, vals)));
        }())
        , m_flowConservationConstraints([&]() {
            IloRangeArray flowCons = IloRangeArray(m_env, m_inst.network.getOrder() * (m_inst.maxChainSize + 1) * m_inst.demands.size());
            for (Graph::Node u = 0; u < m_inst.network.getOrder(); ++u) {
                for (int i = 0; i < int(m_inst.demands.size()); ++i) {
                    for (int j = 0; j <= int(m_inst.demands[i].functions.size()); ++j) {
                        IloNumVarArray vars(m_env);
                        IloNumArray vals(m_env);
                        for (const auto& v : m_inst.network.getNeighbors(u)) {
                            // Outgoing flows
                            vars.add(m_f[getIndexF(m_inst.edgeToId(u, v), i, j)]);
                            vals.add(1.0);
                            // Incoming flows
                            vars.add(m_f[getIndexF(m_inst.edgeToId(v, u), i, j)]);
                            vals.add(-1.0);
                        }
                        if (m_isNFV[u]) {
                            if (j < int(m_inst.demands[i].functions.size())) { // Going next layer
                                vars.add(m_a[getIndexA(u, i, j)]);
                                vals.add(1.0);
                            }
                            if (j > 0) { // Coming from last layer
                                vars.add(m_a[getIndexA(u, i, j - 1)]);
                                vals.add(-1.0);
                            }
                        }
                        const int fIndex = getIndexFC(u, i, j);
                        flowCons[getIndexFC(u, i, j)] = IloAdd(m_model, IloScalProd(vars, vals) == 0);

                        setIloName(flowCons[fIndex], "flowCons" + toString(std::make_tuple(u, i, j)));
                    }
                }
            }

            for (int i = 0; i < int(m_inst.demands.size()); ++i) {
                flowCons[getIndexFC(m_inst.demands[i].s, i, 0)].setBounds(1.0, 1.0);
                flowCons[getIndexFC(m_inst.demands[i].t, i, m_inst.demands[i].functions.size())].setBounds(-1.0, -1.0);
            }
            return flowCons;
        }())
        , m_linkCapacityConstraitns([&]() {
            IloRangeArray linkCapaConstraints(m_env, m_inst.network.size(), 0.0, 0.0);
            for (int e = 0; e < m_inst.network.size(); ++e) {
                IloNumVarArray vars(m_env);
                IloNumArray vals(m_env);
                for (int i = 0; i < int(m_inst.demands.size()); ++i) {
                    for (int j = 0; j <= int(m_inst.demands[i].functions.size()); ++j) {
                        vars.add(m_f[getIndexF(e, i, j)]);
                        vals.add(m_inst.demands[i].d);
                    }
                }
                linkCapaConstraints[e] = (IloScalProd(vars, vals) <= m_inst.network.getEdgeWeight(m_inst.edges[e]));
                setIloName(linkCapaConstraints[e], "linkCapa" + toString(m_inst.edges[e]));
            }
            return IloAdd(m_model, linkCapaConstraints);
        }())
        , m_nodeCapacityConstraints([]() {
            IloRangeArray nodeCapaConstraints(m_env, m_inst.network.getOrder(), 0.0, 0.0);
            for (const auto& u : m_inst.NFVNodes) {
                IloNumVarArray vars(m_env);
                IloNumArray vals(m_env);
                for (int i = 0; i < int(m_inst.demands.size()); ++i) {
                    for (int j = 0; j < int(m_inst.demands[i].functions.size()); ++j) {
                        vars.add(m_a[getIndexA(u, i, j)]);
                        vals.add(m_inst.nbCores(i, j));
                    }
                }
                nodeCapaConstraints[u] = IloAdd(m_model, IloScalProd(vars, vals) <= m_inst.nodeCapa[u]);
                setIloName(nodeCapaConstraints[u], "nodeCapa" + toString(u));
            }
            return nodeCapaConstraints;
        }())
        , m_solver(m_model) {
    }

    bool solve() {
#if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 0
        std::cout << m_model << '\n';
#endif
        if (m_solver.solve()) {
            m_intObj = m_solver.getObjValue();
            std::cout << "Solution found :" << m_intObj << '\n';

#if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 0
            // Check locations
            for (Graph::Node u = 0; u < m_inst.network.getOrder(); ++u) {
                for (int i = 0; i < int(m_inst.demands.size()); ++i) {
                    for (int j = 0; j < int(m_inst.demands[i].functions.size()); ++j) {
                        const int aIndex = getIndexA(u, i, j);
                        if (m_solver.getValue(m_a[aIndex])) {
                            std::cout << m_a[aIndex] << " -> " << m_solver.getValue(m_a[aIndex]) << '\n';
                        }
                    }
                }
            }
            // Check flows
            for (int e = 0; e < m_inst.network.size(); ++e) {
                for (int i = 0; i < int(m_inst.demands.size()); ++i) {
                    for (int j = 0; j <= int(m_inst.demands[i].functions.size()); ++j) {
                        const int fIndex = getIndexF(e, i, j);
                        if (m_solver.getValue(m_f[fIndex])) {
                            std::cout << m_f[fIndex] << " -> " << m_solver.getValue(m_f[fIndex]) << '\n';
                        }
                    }
                }
            }
#endif
            return true;
        } else {
            std::cout << "No solution found!\n";
            m_solver.exportModel("ILP.lp");
            return false;
        }
    }

    void save(const std::string& _filename, const std::pair<double, double>& _time) {
        std::ofstream ofs(_filename);
        ofs << m_intObj << '\t' << m_fractObj << '\n';
        ofs << _time.first << '\t' << _time.second << std::endl;
    }

  private:
    // Data
    const DiGraph m_inst.network;
    const std::vector<int> m_inst.nodeCapa;
    const std::vector<double> m_funcCharge;
    const std::vector<Demand> m_inst.demands;
    const int m_inst.maxChainSize;
    const std::vector<int> m_inst.NFVNodes;
    const std::vector<int> m_isNFV;
    const Matrix<double> m_inst.nbCores;
    const Matrix<int> m_inst.edgeToId;
    const std::vector<Graph::Edge> m_inst.edges;

    // Results
    double m_intObj;
    double m_fractObj;
    // ILP
    IloEnv m_env { };
    IloModel m_model;

    IloNumVarArray m_f;
    IloNumVarArray m_a;

    IloObjective m_obj;

    IloRangeArray m_flowConservationConstraints;
    IloRangeArray m_linkCapacityConstraitns;
    IloRangeArray m_nodeCapacityConstraints;

    IloCplex m_solver;

    int getIndexA(const int _u, const int _i, const int _j) {
        assert(m_isNFV[_u]);
        assert(0 <= _i && _i < int(m_inst.demands.size()));
        assert(0 <= _j && _j < int(m_inst.demands[_i].functions.size()));
        return _j + _i * m_inst.maxChainSize + _u * m_inst.demands.size() * m_inst.maxChainSize;
    }

    int getIndexFC(const int _u, const int _i, const int _j) {
        assert(0 <= _u && _u < m_inst.network.getOrder());
        assert(0 <= _i && _i < int(m_inst.demands.size()));
        assert(0 <= _j && _j <= int(m_inst.demands[_i].functions.size()));
        return _j + _i * (m_inst.maxChainSize + 1) + _u * m_inst.demands.size() * (m_inst.maxChainSize + 1);
    }

    int getIndexF(const int _e, const int _i, const int _j) {
        assert(0 <= _e && _e < m_inst.network.size());
        assert(0 <= _i && _i < int(m_inst.demands.size()));
        assert(0 <= _j && _j <= int(m_inst.demands[_i].functions.size()));
        return _j + _i * (m_inst.maxChainSize + 1) + _e * m_inst.demands.size() * (m_inst.maxChainSize + 1);
    }
};

#endif