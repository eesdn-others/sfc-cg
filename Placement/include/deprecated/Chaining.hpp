#ifndef CHAINING_PATH_HPP
#define CHAINING_PATH_HPP

#include <chrono>
#include <cmath>
#include <ilcplex/ilocplex.h>
#include <iostream>
#include <map>
#include <tuple>

#include "DiGraph.hpp"
#include "SFC.hpp"
#include "ShortestPath.hpp"
#include "cplex_utility.hpp"
#include "utility.hpp"

#define RC_EPS 1.0e-6

class ChainingPath {
    class ReducedMainProblem {
        friend ChainingPath;

      public:
        explicit ReducedMainProblem(const ChainingPath& _placement)
            : m_placement(_placement)
            , m_env()
            , m_model(m_env)
            , m_y(IloAdd(m_model, IloNumVarArray(m_env)))
            , m_obj(IloAdd(m_model, IloMinimize(m_env)))
            , m_onePathCons([&]() {
                IloRangeArray onePathCons(m_env, m_placement.m_inst.demands.size(), 1.0, 1.0);
                for (DemandID i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
                    setIloName(onePathCons[i], "onePathCons" + toString(m_placement.m_inst.demands[i]));
                }
                return IloAdd(m_model, onePathCons);
            }())
            , m_linkCapasCons([&]() {
                IloRangeArray linkCapaCons(m_env, m_placement.m_inst.network.size(), 0.0, 0.0);
                int e = 0;
                for (const Graph::Edge& edge : m_placement.m_inst.edges) {
                    linkCapaCons[e].setBounds(0.0, m_placement.m_inst.network.getEdgeWeight(edge));
                    setIloName(linkCapaCons[e], "linkCapas" + toString(edge));
                    ++e;
                }
                return IloAdd(m_model, linkCapaCons);
            }())
            , m_nodeCapasCons([&]() {
                IloRangeArray nodeCapasCons(m_env, m_placement.m_inst.NFVNodes.size());
                for (const auto& u : m_placement.m_inst.NFVNodes) {
                    const int i = m_placement.m_inst.NFVIndices[u];
                    nodeCapasCons[i] = IloAdd(m_model, IloRange(m_env, 0.0, m_placement.m_inst.nodeCapa[u]));
                    setIloName(nodeCapasCons[i], "nodeCapasCons" + std::to_string(u));
                }
                return nodeCapasCons;
            }())
            , m_solver([&]() {
                IloCplex solver(m_model);
                // #if defined(NDEBUG)
                solver.setOut(m_env.getNullStream());
                // #endif
                solver.setParam(IloCplex::Threads, 1);
                return solver;
            }()) {
        }

        ~ReducedMainProblem() {
            m_env.end();
        }

        ReducedMainProblem& operator=(const ReducedMainProblem&) = delete;
        ReducedMainProblem(const ReducedMainProblem&) = delete;

        void addPath(const ServicePath& _sPath) {
            assert([&]() {
                const auto ite = std::find(m_paths.begin(), m_paths.end(), _sPath);
                if (ite != m_paths.end()) {
                    std::cout << _sPath << " is already present!" << '\n';
                    return false;
                }
                return true;
            }());

            const auto& path = _sPath.first;
            const auto& funcPlacement = _sPath.second;

            const auto& demand = m_placement.m_inst.demands[_i];
            const auto d = demand.d;

            IloNumArray linkCharge(m_env, m_placement.m_inst.network.size());
            for (int e = 0; e < int(m_placement.m_inst.network.size()); ++e) {
                linkCharge[e] = 0;
            }
            for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
                linkCharge[m_placement.m_inst.edgeToId(*iteU, *iteV)] += d;
            }
            /****************************************************************************************************/

            /***************************************** Node constraints *****************************************/
            IloNumArray nodeCharge(m_env, m_placement.m_inst.NFVNodes.size());

            for (IloInt i = 0; i < nodeCharge.getSize(); ++i) {
                nodeCharge[i] = IloNum(0.0);
            }

            int j = 0;
            for (const auto& pair_NodeFuncs : funcPlacement) {
                for (const int nbFunc = pair_NodeFuncs.second.size() + j; j < nbFunc; ++j) {
                    nodeCharge[m_placement.m_inst.NFVIndices[pair_NodeFuncs.first]] += m_placement.m_inst.nbCores(_i, j);
                }
            }
            IloNumColumn inc = m_onePathCons[_i](1.0)
                               + m_obj(d * (path.size() - 1))
                               + m_linkCapasCons(linkCharge)
                               + m_nodeCapasCons(nodeCharge);

            auto yVar = IloNumVar(inc);
            setIloName(yVar, "y" + toString(std::make_tuple(demand, _sPath)));
            m_y.add(yVar);
            m_paths.emplace_back(_sPath, _i);
        }

        double solveInteger() {
            m_model.add(IloConversion(m_env, m_y, ILOBOOL));
            if (!m_solver.solve()) {
                return -1.0;
            }
            auto obj = m_solver.getObjValue();
            std::cout << "Final obj value: " << obj << "\t# Paths: " << m_paths.size() << '\n';
            getNetworkUsage(m_solver);
            return obj;
        }

        void getNetworkUsage(const IloCplex& _sol) const {
            std::vector<double> linkUsage(m_placement.m_inst.network.size(), 0);
            std::vector<int> nodeUsage(m_placement.m_inst.network.getOrder(), 0);
            std::vector<IloNumVar> pathsUsed(m_placement.m_inst.demands.size(), nullptr);

            for (int i = 0; i < int(m_paths.size()); ++i) {
                if (_sol.getValue(m_y[i]) > RC_EPS) {
                    const auto& path = m_paths[i].first.first;
                    const auto& funcPlacement = m_paths[i].first.second;
                    const auto& demand = m_placement.m_inst.demands[m_paths[i].second];
                    const auto d = demand.d;
                    pathsUsed[m_paths[i].second] = m_y[i];

                    for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
                        linkUsage[m_placement.m_inst.edgeToId(*iteU, *iteV)] += d * _sol.getValue(m_y[i]);
                    }
                    int j = 0;
                    for (const auto& pair_NodeFuncs : funcPlacement) {
                        for (const int nbFunc = pair_NodeFuncs.second.size() + j; j < nbFunc; ++j) {
                            nodeUsage[pair_NodeFuncs.first] += _sol.getValue(m_y[i]) * m_placement.m_inst.nbCores(m_paths[i].second, j);
                        }
                    }
                }
            }
            std::cout << "Node usage: \n";
            for (const auto& u : m_placement.m_inst.NFVNodes) {
                std::cout << '\t' << u << " -> " << nodeUsage[u] << " / " << m_placement.m_inst.nodeCapa[u] << '\n';
                assert(nodeUsage[u] <= m_placement.m_inst.nodeCapa[u]);
            }
            std::cout << "Link usage: \n";
            int i = 0;
            for (const auto& edge : m_placement.m_inst.network.getEdges()) {
                std::cout << std::fixed << '\t' << edge << " -> " << linkUsage[i] << " / " << m_placement.m_inst.network.getEdgeWeight(edge) << '\n';
                assert(m_placement.m_inst.network.getEdgeWeight(edge) - linkUsage[i] > -1.0e-6
                       || (std::cerr << std::fixed << m_linkCapasCons[i] << '\n'
                                     << m_placement.m_inst.network.getEdgeWeight(edge) - linkUsage[i] << '\n',
                              false));
                ++i;
            }
        }

      private:
        const ChainingPath& m_placement;
        IloEnv m_env { };
        IloModel m_model;

        IloNumVarArray m_y;

        IloObjective m_obj;

        IloRangeArray m_onePathCons;
        IloRangeArray m_linkCapasCons;
        IloRangeArray m_nodeCapasCons;

        IloCplex m_solver;

        std::vector<std::pair<ServicePath, int>> m_paths;
    };

    class PricingProblem {
        friend ChainingPath;

      public:
        explicit PricingProblem(const ChainingPath& _chainPlacement)
            : m_placement(_chainPlacement)
            , m_env()
            , m_demandID(0)
            , n(m_placement.m_inst.network.getOrder())
            , m(m_placement.m_inst.network.size())
            , nbLayers(m_placement.m_inst.maxChainSize + 1)
            , m_model(m_env)
            , m_obj(IloAdd(m_model, IloMinimize(m_env)))
            , m_a([&]() {
                IloNumVarArray a(m_env, m_placement.m_inst.NFVNodes.size() * (nbLayers - 1));
                for (const auto& u : m_placement.m_inst.NFVNodes) {
                    for (int j(0); j < int(nbLayers - 1); ++j) {
                        a[getIndexA(u, j)] = IloAdd(m_model,
                            IloNumVar(m_env, 0, 1, ILOBOOL));
                        setIloName(a[getIndexA(u, j)], "a" + toString(std::make_tuple(j, u)));
                        std::cout << "a" + toString(std::make_tuple(j, u)) << std::endl;
                    }
                }
                return a;
            }())
            , m_f([&]() {
                IloNumVarArray f(m_env, m * nbLayers, 0, 1, ILOBOOL);
                for (int e = 0; e < m; ++e) {
                    for (int j(0); j < int(nbLayers); ++j) {
                        setIloName(f[getIndexF(e, j)], "f" + toString(std::make_tuple(m_placement.m_inst.edges[e], j)));
                    }
                }
                return IloAdd(m_model, f);
            }())
            , m_flowConsCons([&]() {
                IloRangeArray flowConsCons(m_env, n * nbLayers, 0.0, 0.0);
                for (Graph::Node u(0); u < int(m_placement.m_inst.network.getOrder()); ++u) {
                    for (int j(0); j < int(nbLayers); ++j) {
                        IloNumVarArray vars(m_env);
                        IloNumArray vals(m_env);
                        for (const auto& v : m_placement.m_inst.network.getNeighbors(u)) {
                            vars.add(m_f[getIndexF(m_placement.m_inst.edgeToId(u, v), j)]);
                            vals.add(1.0);
                            vars.add(m_f[getIndexF(m_placement.m_inst.edgeToId(v, u), j)]);
                            vals.add(-1.0);
                        }
                        if (m_placement.m_isNFV[u]) {
                            if (j > 0) {
                                vars.add(m_a[getIndexA(u, j - 1)]);
                                vals.add(-1.0);
                            }
                            if (j < nbLayers - 1) {
                                vars.add(m_a[getIndexA(u, j)]);
                                vals.add(1.0);
                            }
                        }
                        flowConsCons[n * j + u] = IloAdd(m_model,
                            IloRange(m_env, 0.0, IloScalProd(vars, vals), 0.0));
                    }
                }
                return flowConsCons;
            }())
            , m_linkCapaCons([&]() {
                IloRangeArray linkCapaCons(m_env, m, 0.0, 0.0);
                for (int e = 0; e < int(m_placement.m_inst.edges.size()); ++e) {
                    linkCapaCons[e].setUB(m_placement.m_inst.network.getEdgeWeight(m_placement.m_inst.edges[e]));
                    setIloName(linkCapaCons[e], "linkCapaCons(" + toString(e) + ")");
                }
                return IloAdd(m_model, linkCapaCons);
            }())
            , m_nodeCapaCons([&]() {
                IloRangeArray nodeCapaCons(m_env, n);
                for (const auto& u : m_placement.m_inst.NFVNodes) {
                    nodeCapaCons[u] = IloAdd(m_model,
                        IloRange(m_env, 0.0, m_placement.m_inst.nodeCapa[u]));
                    setIloName(nodeCapaCons[u], "nodeCapaCons(" + std::to_string(u) + ")");
                }
                return nodeCapaCons;
            }())
            , m_solver(m_env) {}

        PricingProblem(const PricingProblem&) = default;
        PricingProblem(PricingProblem&&) = default;
        PricingProblem operator=(const PricingProblem&) = default;
        PricingProblem operator=(PricingProblem&&) = default;

        ~PricingProblem() {
            m_env.end();
        }

        int getIndexF(const int _e, const int _j) const {
            assert(0 <= _e && _e < m_placement.m_inst.network.size());
            assert(0 <= _j && _j < nbLayers);
            return m * _j + _e;
        }

        int getIndexA(const int _u, const int _j) const {
            assert(0 <= _u && _u < m_placement.m_inst.network.getOrder());
            assert(m_placement.m_inst.NFVIndices[_u] != -1);
            assert(0 <= _j && _j < nbLayers);
            return m_placement.m_inst.NFVNodes.size() * _j + m_placement.m_inst.NFVIndices[_u];
        }

        void updateModel(const int _i) {
            // m_solver.end();
            int nbFunctions = m_placement.m_inst.demands[m_demandID].functions.size();
            m_flowConsCons[n * 0 + m_placement.m_inst.demands[m_demandID].s].setBounds(0.0, 0.0);
            m_flowConsCons[n * nbFunctions + m_placement.m_inst.demands[m_demandID].t].setBounds(0.0, 0.0);

            m_demandID = _i;
            nbFunctions = m_placement.m_inst.demands[m_demandID].functions.size();
            m_flowConsCons[n * 0 + m_placement.m_inst.demands[m_demandID].s].setBounds(1.0, 1.0);
            m_flowConsCons[n * nbFunctions + m_placement.m_inst.demands[m_demandID].t].setBounds(-1.0, -1.0);

            for (int e = 0; e < int(m_placement.m_inst.edges.size()); ++e) {
                IloNumVarArray vars(m_env);
                IloNumArray vals(m_env);
                for (int j(0); j <= int(nbFunctions); ++j) {
                    vars.add(m_f[getIndexF(e, j)]);
                    vals.add(m_placement.m_inst.demands[m_demandID].d);
                }
                for (int j(nbFunctions + 1); j < nbLayers; ++j) {
                    vars.add(m_f[getIndexF(e, j)]);
                    vals.add(0);
                }
                m_linkCapaCons[e].setLinearCoefs(vars, vals);
            }

            for (const auto& u : m_placement.m_inst.NFVNodes) {
                IloNumVarArray vars(m_env);
                IloNumArray vals(m_env);
                for (int j(0); j < int(nbFunctions); ++j) {
                    vars.add(m_a[getIndexA(u, j)]);
                    vals.add(m_placement.m_inst.nbCores(m_demandID, j));
                }
                for (int j(nbFunctions); j < int(nbLayers - 1); ++j) {
                    vars.add(m_a[getIndexA(u, j)]);
                    vals.add(0);
                }
                m_nodeCapaCons[u].setLinearCoefs(vars, vals);
                setIloName(m_nodeCapaCons[u], "m_nodeCapaCons(" + std::to_string(u) + ")");
            }
        }

        void updateDual(const int _i, const IloCplex& _sol) {
            updateModel(_i);
            const auto& d = m_placement.m_inst.demands[m_demandID].d;
            const auto& functions = m_placement.m_inst.demands[m_demandID].functions;
            const int nbFunctions = functions.size();
            IloNumVarArray vars(m_env);
            IloNumArray vals(m_env);

            IloNumArray linkCapaDuals(m_env);
            _sol.getDuals(linkCapaDuals, m_placement.m_mainProblem.m_linkCapasCons);
            IloNumArray nodeCapaDuals(m_env);
            _sol.getDuals(nodeCapaDuals, m_placement.m_mainProblem.m_nodeCapasCons);

            for (int e = 0; e < m; ++e) {
                for (int j(0); j <= nbFunctions; ++j) {
                    vars.add(m_f[getIndexF(e, j)]);
                    vals.add(d - d * linkCapaDuals[e]);
                }
            }

            for (const auto& u : m_placement.m_inst.NFVNodes) {
                for (int j = 0; j < int(functions.size()); ++j) {
                    vars.add(m_a[getIndexA(u, j)]);
                    vals.add(-nodeCapaDuals[m_placement.m_inst.NFVIndices[u]] * m_placement.m_inst.nbCores(m_demandID, j));
                }
            }
            m_obj.setConstant(-_sol.getDual(m_placement.m_mainProblem.m_onePathCons[m_demandID]));
            m_obj.setLinearCoefs(vars, vals);
            m_solver.extract(m_model);
            m_solver.setParam(IloCplex::Threads, 1);
            // #if defined(NDEBUG)
            m_solver.setOut(m_env.getNullStream());
            // #endif
        }

        ServicePath getServicePath() const {
#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
            std::cout << "getServicePath() -> " << m_placement.m_inst.demands[m_demandID] << '\n';
#endif
            const auto& funcs = m_placement.m_inst.demands[m_demandID].functions;
            const auto& t = m_placement.m_inst.demands[m_demandID].t;
            std::vector<std::pair<Graph::Node, std::vector<function_descriptor>>> installs;

            Graph::Node u = m_placement.m_inst.demands[m_demandID].s;
            int j = 0;
            Graph::Path path{u};

            IloNumArray aVal(m_env);
            m_solver.getValues(aVal, m_a);

            while (u != t || j < funcs.size()); {
                if (j < int(funcs.size()) && m_placement.m_isNFV[u] && aVal[getIndexA(u, j)]) {
                    if (installs.empty() || installs.back().first != u) {
                        installs.emplace_back(u, std::vector<function_descriptor>());
                    }
                    installs.back().second.push_back(funcs[j]);
                    ++j;
                } else {
                    for (const auto& v : m_placement.m_inst.network.getNeighbors(u)) {
                        if (m_solver.getValue(m_f[getIndexF(m_placement.m_inst.edgeToId(u, v), j)]) > 0) {
                            path.push_back(v);
                            u = v;
                            break;
                        }
                    }
                }
            }

            assert(path.back() == t);
            return ServicePath(path, installs);
        }

      private:
        const ChainingPath& m_placement;
        IloEnv m_env { };

        int m_demandID;
        const int n;
        const int m;
        const int nbLayers;
        IloModel m_model;

        IloObjective m_obj;

        IloNumVarArray m_a;
        IloNumVarArray m_f;

        IloRangeArray m_flowConsCons;
        IloRangeArray m_linkCapaCons;
        IloRangeArray m_nodeCapaCons;

        IloCplex m_solver;
    };

  public:
    ChainingPath(DiGraph _network, std::vector<int> _nodeCapa, std::vector<Demand> _demands, std::vector<double> _funcCharge, std::vector<int> _NFVNodes)
        : m_inst.network(std::move(_network))
        , m_inst.edges(m_inst.network.getEdges())
        , m_inst.nodeCapa(std::move(_nodeCapa))
        , m_funcCharge(std::move(_funcCharge))
        , m_inst.demands(std::move(_demands))
        , m_inst.maxChainSize(*std::max_element(m_inst.demands.begin(.functions, m_inst.demands.end(),
                                               [&](const Demand& _d1, const Demand& _d2) {
                                                   return _d1.functions.size() < _d2.functions.size();
                                               }))
                              .size())
        , m_inst.NFVNodes(std::move(_NFVNodes))
        , m_inst.NFVIndices([&]() {
            std::vector<int> NFVIndices(m_inst.network.getOrder(), -1);
            for (int i = 0; i < m_inst.NFVNodes.size()); ++i; {
                NFVIndices[m_inst.NFVNodes[i]] = i;
            }
            return NFVIndices;
        }())
        , m_isNFV([&]() {
            std::vector<int> isNFV(m_inst.network.getOrder(), 0);
            for (const auto& u : m_inst.NFVNodes) {
                isNFV[u] = 1;
            }
            return isNFV;
        }())
        , m_inst.nbCores([&]() {
            Matrix<double> mat(m_inst.demands.size(), m_inst.maxChainSize, -1);

            for (int i(0); i < int(m_inst.demands.size()); ++i) {
                const auto& funcs = m_inst.demands[i].functions;

                for (int j(0); j < int(funcs.size()); ++j) {
                    mat(i, j) = ceil(m_inst.demands[i].d / m_funcCharge[funcs[j]]);
                }
            }
            return mat;
        }())
        , m_intObj(-1)
        , m_fractObj(-1)
        , m_inst.edgeToId([&]() {
            Matrix<int> edgeToId(m_inst.network.getOrder(), m_inst.network.getOrder(), -1);
            int i = 0;
            for (Graph::Edge edge : m_inst.network.getEdges()) {
                edgeToId(edge.first, edge.second) = i;
                ++i;
            }
            return edgeToId;
        }())
        , m_mainProblem(*this)
        , m_pp(*this) {
    }

    ChainingPath(const ChainingPath&) = default;
    ChainingPath(ChainingPath&&) = default;
    ChainingPath& operator=(const ChainingPath&) = default;
    ChainingPath& operator=(ChainingPath&&) = default;
    ~ChainingPath() = default;

    void addInitConf(const std::vector<ServicePath>& _paths) {
        int i = 0;
        for (auto& path : _paths) {
            m_mainProblem.addPath(path, i);
            ++i;
        }
    }

    void save(const std::string& _filename, const std::pair<double, double>& _time) {
        std::ofstream ofs(_filename);
        ofs << m_intObj << '\t' << m_fractObj << '\n';
        ofs << m_mainProblem.m_paths.size() << std::endl;
        ofs << _time.first << '\t' << _time.second << std::endl;

        std::vector<double> linkUsage(m_inst.network.size(), 0);
        std::vector<int> nodeUsage(m_inst.network.getOrder(), 0);
        std::vector<int> pathsUsed(m_inst.demands.size());

        for (int i = 0; i < int(m_mainProblem.m_paths.size()); ++i) {
            if (m_mainProblem.m_solver.getValue(m_mainProblem.m_y[i]) != 0) {
                const auto& path = m_mainProblem.m_paths[i].first.first;
                const auto& funcPlacement = m_mainProblem.m_paths[i].first.second;
                const auto& demand = m_inst.demands[m_mainProblem.m_paths[i].second];
                const auto d = demand.d;
                pathsUsed[m_mainProblem.m_paths[i].second] = i;

                for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
                    linkUsage[m_inst.edgeToId(*iteU, *iteV)] += d * m_mainProblem.m_solver.getValue(m_mainProblem.m_y[i]);
                }
                int j = 0;
                for (const auto& pair_NodeFuncs : funcPlacement) {
                    for (const int nbFunc = pair_NodeFuncs.second.size() + j; j < nbFunc; ++j) {
                        nodeUsage[pair_NodeFuncs.first] += m_mainProblem.m_solver.getValue(m_mainProblem.m_y[i]) * m_inst.nbCores(m_mainProblem.m_paths[i].second, j);
                    }
                }
            }
        }

        ofs << m_inst.network.getOrder() << '\n';
        for (Graph::Node u = 0; u < m_inst.network.getOrder(); ++u) {
            ofs << u << '\t' << nodeUsage[u] << '\t' << m_inst.nodeCapa[u] << '\n';
        }

        ofs << m_inst.network.size() << '\n';
        int i = 0;
        for (const auto& edge : m_inst.network.getEdges()) {
            ofs << std::fixed << edge.first << '\t' << edge.second << '\t' << linkUsage[i] << '\t' << m_inst.network.getEdgeWeight(edge) << '\n';
            ++i;
        }

        ofs << pathsUsed.size() << '\n';
        for (const auto& index : pathsUsed) {
            const auto& sPath = m_mainProblem.m_paths[index].first;
            ofs << sPath.first.size() << '\t';
            for (const auto& u : sPath.first) {
                ofs << u << '\t';
            }
            for (const auto& pair_NodeFuncs : sPath.second) {
                for (const auto& func : pair_NodeFuncs.second) {
                    ofs << pair_NodeFuncs.first << '\t' << func << '\t';
                }
            }
            ofs << '\n';
        }
        std::cout << "Results saved to :" << _filename << '\n';
    }

    bool solveInteger() {
        if (solve()) {
            m_intObj = m_mainProblem.solveInteger();
            return m_intObj != -1;
        } else {
            return false;
        }
    }

    bool solve() {
        bool reducedCost;
        std::vector<std::pair<ServicePath, int>> toAdd;
        toAdd.reserve(m_inst.demands.size());
        do {
            reducedCost = false;
            toAdd.clear();
            if (m_mainProblem.m_solver.solve()) {
                m_fractObj = m_mainProblem.m_solver.getObjValue();
                std::cout << "Reduced Main Problem Objective Value = " << m_fractObj << '\n';

                for (DemandID i(0); i < int(m_inst.demands.size()); ++i) {
/* For each demands, search for a shortest path in the augmented graph */
                    m_pp.updateDual(i, m_mainProblem.m_solver);
                    if (m_pp.m_solver.solve()) {
                        const auto rc = m_pp.m_solver.getObjValue();

                        if (rc < -RC_EPS) {
                            const ServicePath sPath = m_pp.getServicePath();
                            reducedCost = true;
                            toAdd.emplace_back(sPath, i);
                            if (!m_allDemands) {
                                break;
                            }
                        }
                    } else {
                        m_pp.m_solver.exportModel("pp.lp");
                        std::cout << i << "th pp no solution" << '\n';
                    }
                }
                std::cout << '\n';
                for (auto& p : toAdd) {
                    m_mainProblem.addPath(p.first, p.second);
                }
            } else {
                m_mainProblem.m_solver.exportModel("rm.lp");
                std::cout << "No solution found for RMP" << '\n';
            }

        } while (reducedCost);
        m_intObj = m_mainProblem.solveInteger();
        return m_intObj > 0;
    }

  private:
    const DiGraph m_inst.network;
    const std::vector<Graph::Edge> m_inst.edges;
    const std::vector<int> m_inst.nodeCapa;
    const std::vector<double> m_funcCharge;
    const std::vector<Demand> m_inst.demands;
    const bool m_allDemands;

    int m_inst.maxChainSize;
    const std::vector<int> m_inst.NFVNodes;
    const std::vector<int> m_inst.NFVIndices;
    const std::vector<int> m_isNFV;
    const Matrix<double> m_inst.nbCores;
    double m_intObj;
    double m_fractObj;

    const Matrix<int> m_inst.edgeToId;

    ReducedMainProblem m_mainProblem;
    PricingProblem m_pp;

    friend ReducedMainProblem;
};

#endif