// #ifndef AUGMENTED_GRAPH_HPP
// #define AUGMENTED_GRAPH_HPP

// #include <BinaryHeap.hpp>
// #include <DiGraph.hpp>
// #include <Matrix.hpp>
// #include <utility.hpp>

// #include "SFC.hpp"

// class AugmentedGraphILP {
//   public:
//     AugmentedGraphILP(const DiGraph& _network, const std::vector<double>& _funcCharge, const std::vector<int>& _NFVNodes,
//         const std::vector<int>& _nodeCapa, const int _occLimit)
//         : m_inst.network(_network)
//         , m_inst.edges(m_inst.network.getEdges())
//         , m_funcCharge(_funcCharge)
//         , m_inst.NFVNodes(_NFVNodes)
//         , m_isNFV([&]() {
//             std::vector<int> isNFV(m_inst.network.getOrder(), false);
//             for (const auto& u : m_inst.NFVNodes) {
//                 isNFV[u] = true;
//             }
//             return isNFV;
//         }())
//         , m_inst.nodeCapa(_nodeCapa)
//         , m_nbLayers(m_funcCharge.size() + 1)
//         , m_inst.edgeToId([&]() {
//             Matrix<int> edgeToId(m_inst.network.getOrder(), m_inst.network.getOrder(), -1);
//             int i = 0;
//             for (Graph::Edge edge : m_inst.network.getEdges()) {
//                 edgeToId(edge.first, edge.second) = i;
//                 ++i;
//             }
//             return edgeToId;
//         }())
//         , m_flowGraph([&]() {
//             DiGraph flowGraph(m_inst.network);
//             for (auto& edge : flowGraph.getEdges()) {
//                 flowGraph.setEdgeWeight(edge, 0);
//             }
//             return flowGraph;
//         }())
//         , m_nodeUsage(m_inst.network.getOrder(), 0)
//         , m_maxFuncOccurences(_occLimit)
//         , m_funcOccurences(m_funcCharge.size(), 0)
//         , m_funcPlacement(m_funcCharge.size(), m_inst.network.getOrder(), 0) {}

//     AugmentedGraphILP(const DiGraph& _network, const std::vector<double>& _funcCharge, const std::vector<int>& _NFVNodes)
//         : AugmentedGraphILP(_network, _funcCharge, _NFVNodes, std::vector<int>(_network.getOrder(), 0), _network.getOrder()) {}

//     AugmentedGraphILP(const AugmentedGraphILP& _other)
//         : m_inst.network(_other.m_inst.network)
//         , m_inst.edges(_other.m_inst.edges)
//         , m_funcCharge(_other.m_funcCharge)
//         , m_inst.NFVNodes(_other.m_inst.NFVNodes)
//         , m_isNFV(_other.m_isNFV)
//         , m_inst.nodeCapa(_other.m_inst.nodeCapa)
//         , m_nbLayers(_other.m_nbLayers)
//         , m_inst.edgeToId(_other.m_inst.edgeToId)
//         , m_flowGraph(_other.m_flowGraph)
//         , m_nodeUsage(_other.m_nodeUsage)
//         , m_maxFuncOccurences(_other.m_maxFuncOccurences)
//         , m_funcOccurences(_other.m_funcOccurences)
//         , m_funcPlacement(_other.m_funcPlacement) {}

//     AugmentedGraphILP(AugmentedGraphILP&& _other)
//         : m_inst.network(std::move(_other.m_inst.network))
//         , m_inst.edges(std::move(_other.m_inst.edges))
//         , m_funcCharge(std::move(_other.m_funcCharge))
//         , m_inst.NFVNodes(std::move(_other.m_inst.NFVNodes))
//         , m_isNFV(std::move(_other.m_isNFV))
//         , m_inst.nodeCapa(std::move(_other.m_inst.nodeCapa))
//         , m_nbLayers(std::move(_other.m_nbLayers))
//         , m_inst.edgeToId(std::move(_other.m_inst.edgeToId))
//         , m_flowGraph(std::move(_other.m_flowGraph))
//         , m_nodeUsage(std::move(_other.m_nodeUsage))
//         , m_maxFuncOccurences(std::move(_other.m_maxFuncOccurences))
//         , m_funcOccurences(std::move(_other.m_funcOccurences))
//         , m_funcPlacement(std::move(_other.m_funcPlacement)) {}

//     AugmentedGraphILP& operator=(const AugmentedGraphILP&) = delete;
//     AugmentedGraphILP& operator=(AugmentedGraphILP&&) = delete;

//     void showNetworkUsage() const {
//         std::cout << m_nodeUsage << '\n';
//         std::cout << m_flowGraph.getEdges() << std::endl;
//     }

//     std::vector<int> getNodesSortedByUsage() const {
//         std::vector<Graph::Node> nodes(m_inst.nodeCapa.size());
//         for (Graph::Node u = 0; u < int(m_inst.nodeCapa.size()); ++u) {
//             nodes[u] = u;
//         }
//         std::sort(nodes.begin(), nodes.end(), [&](const Graph::Node& _u, const Graph::Node& _v) {
//             return m_nodeUsage[_u] < m_nodeUsage[_v];
//         });
//         return nodes;
//     }

//     std::vector<ServicePath> getInitialConfiguration(const std::vector<Demand>& _demands) {
//         std::vector<ServicePath> initConf;
//         initConf.reserve(_demands.size());
//         int i = 0;
//         for (auto& demand : _demands) {
//             /* For each demands, search for a shortest path in the augmented graph */
//             const std::vector<function_descriptor>& functions = std::get<3>(demand);
//             const double d = std::get<2>(demand);

//             // Update edge and node usage
//             const auto sPath = PathSolver(*this, demand).getServicePath();
//             ;
//             for (const auto& pair_NodeFuncs : sPath.second) {
//                 const Graph::Node& u = pair_NodeFuncs.first;
//                 for (const auto& func : pair_NodeFuncs.second) {
//                     m_nodeUsage[u] += ceil(d / m_funcCharge[func]);
//                     if (m_funcPlacement(func, u) == 0) {
//                         m_funcPlacement(func, u) = 1;
//                         m_funcOccurences[func] += 1;
//                         assert(m_funcOccurences[func] <= m_maxFuncOccurences);
//                     }
//                     assert(m_nodeUsage[u] <= m_inst.nodeCapa[u]
//                            || (std::cout << "m_nodeUsage[" << u << "] is full (" << m_nodeUsage[u] << '/' << m_inst.nodeCapa[u] << ")\n", false));
//                 }
//             }

//             for (auto iteU = std::begin(sPath.first), iteV = std::next(iteU); iteV != std::end(sPath.first); ++iteU, ++iteV) {
//                 const Graph::Edge edge(*iteU, *iteV);
//                 m_flowGraph.setEdgeWeight(edge, m_flowGraph.getEdgeWeight(edge) + d);
//                 assert(m_flowGraph.getEdgeWeight(edge) < m_inst.network.getEdgeWeight(edge)
//                        || (std::cout << "m_flowGraph[" << edge << "] is full ("
//                                      << m_flowGraph.getEdgeWeight(edge) << '/' << m_inst.network.getEdgeWeight(edge) << ")\n",
//                               false));
//             }
//             // Add service path
//             initConf.push_back(sPath);
// #if !defined(NDEBUG) && 0
//             std::cout << demand << " -> " << initConf.back() << '\n';
// #endif
//             ++i;
//         }
// #if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 2
//         std::cout << m_funcPlacement << '\n';
//         std::cout << m_funcOccurences << '\n';
// #endif
//         return initConf;
//     }

//   private:
//     class PathSolver {
//       public:
//         PathSolver(const AugmentedGraphILP& _augGraph, const Demand& _demand)
//             : m_parentClass(_augGraph)
//             , m_env()
//             , n(m_parentClass.m_inst.network.getOrder())
//             , m(m_parentClass.m_inst.network.size())
//             , nbLayers(std::get<3>(_demand).size() + 1)
//             , m_demand(_demand)
//             , m_model(m_env)
//             , m_a([&]() {
//                 IloNumVarArray a(m_env, n * (nbLayers - 1));
//                 for (const auto& u : m_parentClass.m_inst.NFVNodes) {
//                     for (int j(0); j < int(nbLayers - 1); ++j) {
//                         a[n * j + u] = IloAdd(m_model, IloNumVar(m_env, 0.0, 1.0, ILOBOOL));
//                         setIloName(a[n * j + u], "a" + toString(std::make_tuple(j, u)));
//                     }
//                 }
//                 return a;
//             }())
//             , m_f([&]() {
//                 IloNumVarArray f(m_env, m * nbLayers);
//                 for (int e(0); e < m; ++e) {
//                     for (int j(0); j < int(nbLayers); ++j) {
//                         f[getIndexF(e, j)] = IloAdd(m_model, IloNumVar(m_env, 0, 1, ILOBOOL));
//                         setIloName(f[getIndexF(e, j)], "f" + toString(std::make_tuple(m_parentClass.m_inst.edges[e], j)));
//                     }
//                 }
//                 return IloAdd(m_model, f);
//             }())
//             , m_obj([&]() {
//                 IloNumVarArray vars(m_env);
//                 IloNumArray vals(m_env);
//                 for (const auto& u : m_parentClass.m_inst.NFVNodes) {
//                     for (int j(0); j < int(nbLayers - 1); ++j) {
//                         vars.add(m_a[n * j + u]);
//                         vals.add(std::pow(2, (ceil(std::get<2>(m_demand))
//                                                  / m_parentClass.m_funcCharge[std::get<3>(m_demand)[j]])
//                                                  / float(m_parentClass.m_inst.nodeCapa[u])));
//                     }
//                 }
//                 for (int e(0); e < m; ++e) {
//                     for (int j(0); j < int(nbLayers); ++j) {
//                         vars.add(m_f[getIndexF(e, j)]);
//                         vals.add(std::pow(2, std::get<2>(m_demand) / float(m_parentClass.m_inst.network.getEdgeWeight(m_parentClass.m_inst.edges[e]))));
//                     }
//                 }
//                 return IloAdd(m_model, IloMinimize(m_env, IloScalProd(vars, vals)));
//             }())
//             , m_flowConsCons([&]() {
//                 IloRangeArray flowConsCons(m_env, n * nbLayers);

//                 for (Graph::Node u(0); u < int(m_parentClass.m_inst.network.getOrder()); ++u) {
//                     for (int j(0); j < int(nbLayers); ++j) {
//                         IloNumVarArray vars(m_env);
//                         IloNumArray vals(m_env);
//                         for (const auto& v : m_parentClass.m_inst.network.getNeighbors(u)) {
//                             vars.add(m_f[getIndexF(m_parentClass.m_inst.edgeToId(u, v), j)]);
//                             vals.add(1.0);
//                             vars.add(m_f[getIndexF(m_parentClass.m_inst.edgeToId(v, u), j)]);
//                             vals.add(-1.0);
//                         }
//                         if (m_parentClass.m_isNFV[u]) {
//                             if (j > 0) {
//                                 vars.add(m_a[n * (j - 1) + u]);
//                                 vals.add(-1.0);
//                             }
//                             if (j < nbLayers - 1) {
//                                 vars.add(m_a[n * j + u]);
//                                 vals.add(1.0);
//                             }
//                         }
//                         flowConsCons[n * j + u] = IloRange(m_env, 0.0, IloScalProd(vars, vals), 0.0);
//                     }
//                 }
//                 flowConsCons[n * 0 + std::get<0>(m_demand)].setBounds(1.0, 1.0);
//                 flowConsCons[n * std::get<3>(m_demand).size() + std::get<1>(m_demand)].setBounds(-1.0, -1.0);
//                 return IloAdd(m_model, flowConsCons);
//             }())
//             , m_linkCapaCons([&]() {
//                 IloRangeArray linkCapaCons(m_env, m);
//                 for (int e(0); e < int(m_parentClass.m_inst.edges.size()); ++e) {
//                     IloNumVarArray vars(m_env);
//                     IloNumArray vals(m_env);
//                     for (int j(0); j < nbLayers; ++j) {
//                         vars.add(m_f[getIndexF(e, j)]);
//                         vals.add(std::get<2>(m_demand));
//                     }
//                     linkCapaCons[e] = IloRange(m_env, 0.0, IloScalProd(vars, vals),
//                         (m_parentClass.m_inst.network.getEdgeWeight(m_parentClass.m_inst.edges[e]) - m_parentClass.m_flowGraph.getEdgeWeight(m_parentClass.m_inst.edges[e])));
//                     setIloName(linkCapaCons[e], "linkCapaCons(" + toString(e) + ")");
//                 }
//                 return IloAdd(m_model, linkCapaCons);
//             }())
//             , m_nodeCapaCons([&]() {
//                 IloRangeArray nodeCapaCons(m_env, n);
//                 for (const auto& u : m_parentClass.m_inst.NFVNodes) {
//                     IloNumVarArray vars(m_env);
//                     IloNumArray vals(m_env);
//                     for (int j(0); j < std::get<3>(m_demand).size()); ++j; {
//                         vars.add(m_a[n * j + u]);
//                         vals.add(std::get<2>(m_demand) / m_parentClass.m_funcCharge[j]);
//                     }
//                     nodeCapaCons[u] = IloAdd(m_model, IloRange(m_env, 0.0, IloScalProd(vars, vals),
//                                                           (m_parentClass.m_inst.nodeCapa[u] - m_parentClass.m_nodeUsage[u])));
//                     setIloName(nodeCapaCons[u], "nodeCapaCons(" + std::to_string(u) + ")");
//                 }
//                 return nodeCapaCons;
//             }())
//             , m_solver([&]() {
//                 IloCplex solver(m_model);
//                 solver.setOut(m_env.getNullStream());
//                 return solver;
//             }()) {}

//         ~PathSolver() {
//             m_env.end();
//         }

//         inline int getIndexF(const int _e, const int _j) const {
//             assert(0 <= _e && _e < m);
//             assert(0 <= _j && _j < nbLayers);
//             return m * _j + _e;
//         }

//         ServicePath getServicePath() {
// #if defined(LOG_LEVEL) && LOG_LEVEL <= 1
//             std::cout << "getServicePath() -> " << m_demand << '\n';
// #endif
//             const auto& funcs = std::get<3>(m_demand);
//             const auto& t = std::get<1>(m_demand);
//             std::vector<std::pair<Graph::Node, std::vector<function_descriptor>>> installs;

//             Graph::Node u = std::get<0>(m_demand);
//             int j = 0;
//             Graph::Path path{u};

//             IloNumArray aVal(m_env);
//             if (m_solver.solve()) {
//                 m_solver.getValues(aVal, m_a);
//                 int nbFunctions = 0;
//                 while (u != t || j < funcs.size()); {
//                     if (j < funcs.size()) && m_parentClass.m_isNFV[u] && aVal[n * j + u]; {
//                         if (installs.empty() || installs.back().first != u) {
//                             installs.emplace_back(u, std::vector<function_descriptor>());
//                         }
//                         installs.back().second.push_back(funcs[j]);
//                         ++j;
//                     } else {
//                         for (const auto& v : m_parentClass.m_inst.network.getNeighbors(u)) {
//                             if (m_solver.getValue(m_f[getIndexF(m_parentClass.m_inst.edgeToId(u, v), j)]) > 0) {
//                                 path.push_back(v);
//                                 u = v;
//                                 break;
//                             }
//                         }
//                     }
//                 }
//                 assert(path.back() == t);
//                 assert(j == funcs.size());
//             } else {
//                 std::cout << "No solution found for " << m_demand << '\n';
//                 m_solver.exportModel("demand.lp");
//                 return ServicePath({}, installs);
//             }
//             std::cout << m_demand << '\t' << path << '\t' << installs << std::endl;
//             return ServicePath(path, installs);
//         }

//       private:
//         const AugmentedGraphILP& m_parentClass;
//         IloEnv m_env { };

//         const int n;
//         const int m;
//         const int nbLayers;
//         Demand m_demand;

//         IloModel m_model;

//         IloNumVarArray m_a;
//         IloNumVarArray m_f;

//         IloObjective m_obj;

//         IloRangeArray m_flowConsCons;
//         IloRangeArray m_linkCapaCons;
//         IloRangeArray m_nodeCapaCons;

//         IloCplex m_solver;
//     };

//     const DiGraph& m_inst.network;
//     const std::vector<Graph::Edge> m_inst.edges;
//     const std::vector<double>& m_funcCharge;
//     const std::vector<int>& m_inst.NFVNodes;
//     const std::vector<int> m_isNFV;
//     std::vector<int> m_inst.nodeCapa;
//     const int m_nbLayers;
//     Matrix<int> m_inst.edgeToId;

//     DiGraph m_flowGraph;
//     std::vector<int> m_nodeUsage;
//     int m_maxFuncOccurences;
//     std::vector<int> m_funcOccurences;
//     Matrix<int> m_funcPlacement;

//     friend PathSolver;
// };

// #endif