// #ifndef CHAINING_PATH2_HPP
// #define CHAINING_PATH2_HPP

// #include <chrono>
// #include <cmath>
// #include <ilcplex/ilocplex.h>
// #include <iostream>
// #include <map>
// #include <tuple>

// #include "DiGraph.hpp"
// #include "SFC.hpp"
// #include "ShortestPath.hpp"
// #include "cplex_utility.hpp"
// #include "utility.hpp"

// #define RC_EPS 1.0e-6

// class ChainingPath2 {
//     class MasterProblem {
//         friend ChainingPath2;

//       public:
//         MasterProblem(ChainingPath2& _placement)
//             : m_placement(_placement)
//             , m_model(m_placement.m_env)
//             , m_y(IloAdd(m_model,
//                   IloNumVarArray(m_placement.m_env)))
//             , m_obj(IloAdd(m_model,
//                   IloMinimize(m_placement.m_env)))
//             , m_onePathCons(IloAdd(m_model,
//                   IloRangeArray(m_placement.m_env, m_placement.m_inst.demands.size(),
//                       1.0, 1.0)))
//             , m_linkCapasCons(IloRangeArray(m_placement.m_env, m_placement.m_inst.network.size(),
//                   0.0, 0.0))
//             , m_nodeCapasCons(IloRangeArray(m_placement.m_env, m_placement.m_inst.network.getOrder(),
//                   -IloInfinity, 0.0))
//             , m_solver(m_model)
//             , m_paths() {
//             std::cout << "Building main problem...\n";
//             std::cout << "Building one path constraints...\n";
//             for (DemandID i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                 setIloName(m_onePathCons[i], "m_onePathCons" + toString(m_placement.m_inst.demands[i]));
//             }
//             std::cout << "Building link capa constraints...\n";
//             int e = 0;
//             for (Graph::Edge edge : m_placement.m_inst.network.getEdges()) {
//                 m_linkCapasCons[e].setBounds(0.0, m_placement.m_inst.network.getEdgeWeight(edge));
//                 setIloName(m_linkCapasCons[e], "linkCapas" + toString(edge));
//                 m_model.add(m_linkCapasCons[e]);
//                 ++e;
//             }

//             std::cout << "Building node capa constraints...\n";
//             for (const auto& u : m_placement.m_inst.NFVNodes) {
//                 m_nodeCapasCons[u].setUB(m_placement.m_inst.nodeCapa[u]);
//                 setIloName(m_nodeCapasCons[u], "m_nodeCapasCons" + std::to_string(u));
//             }
//             IloAdd(m_model, m_nodeCapasCons);
//             m_solver.setOut(m_placement.m_env.getNullStream());
//         }

//         MasterProblem& operator=(const MasterProblem&) = delete;
//         MasterProblem(const MasterProblem&) = delete;
//         MasterProblem& operator=(MasterProblem&&) = delete;
//         MasterProblem(MasterProblem&&) = delete;
//         ~MasterProblem() = default

//         void addPath(const ServicePath& _sPath, const int _i) {
//             const auto pair = std::make_pair(_sPath, _i);
// #if !defined(NDEBUG)
//             std::cout << "addPath" << pair << '\n';
// #endif
//             assert([&]() {
//                 const auto ite = std::find(m_paths.begin(), m_paths.end(), pair);
//                 if (ite == m_paths.end()) {
//                     return true;
//                 } else {
//                     std::cout << pair << " is already present!" << '\n';
//                     return false;
//                 }
//             }());

//             const auto& path = _sPath.first;
//             const auto& funcPlacement = _sPath.second;

//             const auto& demand = m_placement.m_inst.demands[_i];
//             const auto d = std::get<2>(demand);
//             // const auto funcs = demand.functions;

//             IloNumArray valsLink(m_placement.m_env, m_placement.m_inst.network.size());
//             for (int e(0); e < int(m_placement.m_inst.network.size()); ++e) {
//                 valsLink[e] = 0;
//             }
//             for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
//                 valsLink[m_placement.m_inst.edgeToId.at(Graph::Edge(*iteU, *iteV))] = d;
//             }
//             IloNumArray valsNode(m_placement.m_env, m_placement.m_inst.network.getOrder());
//             for (const auto& u : m_placement.m_inst.NFVNodes) {
//                 valsNode[u] = 0;
//             }
//             int j = 0;
//             for (const auto& pair_NodeFuncs : funcPlacement) {
//                 for (const auto& func : pair_NodeFuncs.second) {
//                     valsNode[pair_NodeFuncs.first] += m_placement.m_inst.nbCores(_i, j);
//                     ++j;
//                 }
//             }

//             IloNumColumn inc = m_onePathCons[_i](1.0)
//                                + m_obj(d * (path.size() - 1))
//                                + m_linkCapasCons(valsLink)
//                                + m_nodeCapasCons(valsNode);

//             auto yVar = IloNumVar(inc);
//             setIloName(yVar, "y" + toString(std::make_tuple(demand, _sPath)));
//             m_y.add(yVar);
//             m_paths.emplace_back(_sPath, _i);
//         }

//         double solveInteger() {
//             m_model.add(IloConversion(m_placement.m_env, m_y, ILOBOOL));
//             if (m_solver.solve()) {
//                 auto obj = m_solver.getObjValue();
//                 std::cout << "Final obj value: " << obj << "\t# Paths: " << m_paths.size() << '\n';
//                 getNetworkUsage(m_solver);
//                 return obj;
//             } else {
//                 return -1.0;
//             }
//         }

//         void getNetworkUsage(const IloCplex& _sol) const {
//             std::vector<double> linkUsage(m_placement.m_inst.network.size(), 0);
//             std::vector<int> nodeUsage(m_placement.m_inst.network.getOrder(), 0);
//             std::vector<IloNumVar> pathsUsed(m_placement.m_inst.demands.size(), nullptr);

//             for (int i = 0; i < int(m_paths.size()); ++i) {
//                 if (_sol.getValue(m_y[i]) > RC_EPS) {
//                     const auto& path = m_paths[i].first.first;
//                     const auto& funcPlacement = m_paths[i].first.second;
//                     const auto& demand = m_placement.m_inst.demands[m_paths[i].second];
//                     const auto d = std::get<2>(demand);
//                     pathsUsed[m_paths[i].second] = m_y[i];

//                     for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
//                         linkUsage[m_placement.m_inst.edgeToId.at(Graph::Edge(*iteU, *iteV))] += d * _sol.getValue(m_y[i]);
//                     }
//                     int j = 0;
//                     for (const auto& pair_NodeFuncs : funcPlacement) {
//                         for (const auto& func : pair_NodeFuncs.second) {
//                             nodeUsage[pair_NodeFuncs.first] += _sol.getValue(m_y[i]) * m_placement.m_inst.nbCores(m_paths[i].second, j);
//                             ++j;
//                         }
//                     }
//                 }
//             }
//             std::cout << "Node usage: \n";
//             for (const auto& u : m_placement.m_inst.NFVNodes) {
//                 std::cout << '\t' << u << " -> " << nodeUsage[u] << " / " << m_placement.m_inst.nodeCapa[u] << '\n';
//                 assert(nodeUsage[u] <= m_placement.m_inst.nodeCapa[u]);
//             }
//             std::cout << "Link usage: \n";
//             int i = 0;
//             for (const auto& edge : m_placement.m_inst.network.getEdges()) {
//                 std::cout << std::fixed << '\t' << edge << " -> " << linkUsage[i] << " / " << m_placement.m_inst.network.getEdgeWeight(edge) << '\n';
//                 assert(m_placement.m_inst.network.getEdgeWeight(edge) - linkUsage[i] > -1.0e-6
//                        || (std::cerr << std::fixed << m_linkCapasCons[i] << '\n'
//                                      << m_placement.m_inst.network.getEdgeWeight(edge) - linkUsage[i] << '\n',
//                               false));
//                 ++i;
//             }
//             // std::cout << "Paths usage: \n";
//             // for(const auto& path : pathsUsed) {
//             // 	std::cout << '\t' << path << '\n';
//             // }
//         }

//       private:
//         ChainingPath2& m_placement;
//         IloModel m_model;
//         IloNumVarArray m_y;

//         IloObjective m_obj;

//         IloRangeArray m_onePathCons;
//         IloRangeArray m_linkCapasCons;
//         IloRangeArray m_nodeCapasCons;

//         IloCplex m_solver;

//         std::vector<std::pair<ServicePath, int>> m_paths;
//     };

//     class PricingProblem {
//         friend ChainingPath2;

//       public:
//         PricingProblem(ChainingPath2& _chainPlacement)
//             : m_placement(_chainPlacement)
//             , n(m_placement.m_inst.network.getOrder())
//             , m(m_placement.m_inst.network.size())
//             , nbLayers(m_placement.m_inst.maxChainSize + 1)
//             , m_model(m_placement.m_env)
//             , m_obj(IloAdd(m_model, IloMinimize(m_placement.m_env)))
//             , m_a(IloAdd(m_model, IloNumVarArray(m_placement.m_env, m_placement.m_inst.demands.size() * n * (nbLayers - 1), 0, 1, ILOBOOL)))
//             , m_f(IloAdd(m_model, IloNumVarArray(m_placement.m_env, m_placement.m_inst.demands.size() * m * nbLayers, 0, 1, ILOBOOL)))
//             , m_flowConsCons(IloAdd(m_model, IloRangeArray(m_placement.m_env, m_placement.m_inst.demands.size() * n * nbLayers, 0.0, 0.0)))
//             , m_linkCapaCons(IloAdd(m_model, IloRangeArray(m_placement.m_env, m_placement.m_inst.demands.size() * m, 0.0, 0.0)))
//             , m_nodeCapaCons(IloAdd(m_model, IloRangeArray(m_placement.m_env, m_placement.m_inst.demands.size() * n)))
//             ,
//             // m_maxIn( IloAdd(m_model, IloRangeArray(m_placement.m_env, n, 0.0, 1.0) )),
//             // m_maxOut( IloAdd(m_model, IloRangeArray(m_placement.m_env, n, 0.0, 1.0) )),
//             m_solver() {
//             const std::vector<Graph::Edge> edges(m_placement.m_inst.network.getEdges());

//             //std::cout << "Building flow cons...\n";
//             for (Graph::Node u(0); u < int(m_placement.m_inst.network.getOrder()); ++u) {
//                 for (int j(0); j < int(nbLayers); ++j) {
//                     IloNumVarArray vars(m_placement.m_env);
//                     IloNumArray vals(m_placement.m_env);
//                     for (const auto& v : m_placement.m_inst.network.getNeighbors(u)) {
//                         vars.add(getF(m_placement.m_inst.edgeToId.at(Graph::Edge(u, v)), j));
//                         vals.add(1.0);
//                         vars.add(getF(m_placement.m_inst.edgeToId.at(Graph::Edge(v, u)), j));
//                         vals.add(-1.0);
//                     }
//                     if (m_placement.m_isNFV[u]) {
//                         if (j > 0) {
//                             vars.add(m_a[n * (j - 1) + u]);
//                             vals.add(-1.0);
//                         }
//                         if (j < nbLayers - 1) {
//                             vars.add(m_a[n * j + u]);
//                             vals.add(1.0);
//                         }
//                     }
//                     m_flowConsCons[n * j + u].setLinearCoefs(vars, vals);
//                 }
//             }

//             //std::cout << "Building link capa cons...\n";
//             for (int e(0); e < int(edges.size()); ++e) {
//                 m_linkCapaCons[e].setUB(m_placement.m_inst.network.getEdgeWeight(edges[e]));
//                 setIloName(m_linkCapaCons[e], "m_linkCapaCons(" + toString(e) + ")");
//             }

//             //std::cout << "Building node capa cons...\n";
//             for (const auto& u : m_placement.m_inst.NFVNodes) {
//                 m_nodeCapaCons[u] = IloRange(m_placement.m_env, 0.0, m_placement.m_inst.nodeCapa[u]);
//                 setIloName(m_nodeCapaCons[u], "m_nodeCapaCons(" + std::to_string(u) + ")");
//             }

//             //std::cout << "Building no loop cons...\n";
//             // for(Graph::Node u(0); u < int(m_placement.m_inst.network.getOrder()); ++u) {
//             // 	IloNumVarArray varsIn(m_placement.m_env);
//             // 	IloNumVarArray varsOut(m_placement.m_env);
//             // 	IloNumArray vals(m_placement.m_env);
//             // 	for(int j(0); j < int(nbLayers); ++j) {
//             // 		for(const auto& v : m_placement.m_inst.network.getNeighbors(u)) {
//             // 			varsOut.add(getF(m_placement.m_inst.edgeToId.at(Graph::Edge(u, v)), j));
//             // 			varsIn.add(getF(m_placement.m_inst.edgeToId.at(Graph::Edge(v, u)), j));
//             // 			vals.add(1.0);
//             // 		}
//             // 	}
//             // 	m_maxIn[u].setLinearCoefs(varsIn, vals);
//             // 	m_maxOut[u].setLinearCoefs(varsOut, vals);
//             // }

//             for (int e(0); e < m; ++e) {
//                 for (int j(0); j < int(nbLayers); ++j) {
//                     setIloName(getF(e, j), "f" + toString(std::make_tuple(edges[e], j)));
//                 }
//             }

//             for (const auto& u : m_placement.m_inst.NFVNodes) {
//                 for (int j(0); j < int(nbLayers - 1); ++j) {
//                     setIloName(m_a[n * j + u], "a" + toString(std::make_tuple(j, u)));
//                 }
//             }
//         }

//         inline IloNumVar getF(const int _e, const int _j) const {
//             assert(0 <= _e && _e < m_placement.m_inst.network.size());
//             return m_f[m * _j + _e];
//         }

//         inline IloNumVar getF(const int _e, const int _j) {
//             assert(0 <= _e && _e < m_placement.m_inst.network.size());
//             return m_f[m * _j + _e];
//         }

//         void updateModel(const DemandID& _i) {
//             m_solver.end();
//             int nbFunctions = m_placement.m_inst.demands[m_demandID].functions.size();
//             m_flowConsCons[n * 0 + std::get<0>(m_placement.m_inst.demands[m_demandID])].setBounds(0.0, 0.0);
//             m_flowConsCons[n * nbFunctions + std::get<1>(m_placement.m_inst.demands[m_demandID])].setBounds(0.0, 0.0);

//             m_demandID = _i;
//             nbFunctions = m_placement.m_inst.demands[m_demandID].functions.size();
//             m_flowConsCons[n * 0 + std::get<0>(m_placement.m_inst.demands[m_demandID])].setBounds(1.0, 1.0);
//             m_flowConsCons[n * nbFunctions + std::get<1>(m_placement.m_inst.demands[m_demandID])].setBounds(-1.0, -1.0);

//             const auto edges = m_placement.m_inst.network.getEdges();
//             for (int e(0); e < int(edges.size()); ++e) {
//                 IloNumVarArray vars(m_placement.m_env);
//                 IloNumArray vals(m_placement.m_env);
//                 for (int j(0); j < int(nbFunctions); ++j) {
//                     vars.add(getF(e, j));
//                     vals.add(std::get<2>(m_placement.m_inst.demands[m_demandID]));
//                 }
//                 for (int j(nbFunctions); j < nbLayers; ++j) {
//                     vars.add(getF(e, j));
//                     vals.add(0);
//                 }
//                 m_linkCapaCons[e].setLinearCoefs(vars, vals);
//             }

//             for (const auto& u : m_placement.m_inst.NFVNodes) {
//                 IloNumVarArray vars(m_placement.m_env);
//                 IloNumArray vals(m_placement.m_env);
//                 for (int j(0); j < int(nbFunctions); ++j) {
//                     vars.add(m_a[n * j + u]);
//                     vals.add(std::get<2>(m_placement.m_inst.demands[m_demandID]) / m_placement.m_funcCharge[j]);
//                 }
//                 for (int j(nbFunctions); j < int(nbLayers - 1); ++j) {
//                     vars.add(m_a[n * j + u]);
//                     vals.add(0);
//                 }
//                 m_nodeCapaCons[u].setLinearCoefs(vars, vals);
//                 setIloName(m_nodeCapaCons[u], "m_nodeCapaCons(" + std::to_string(u) + ")");
//             }
//         }

//         void updateDual(const DemandID& _i, const IloCplex& _sol) {
//             updateModel(_i);
//             // Get dual values
//             const auto& d = std::get<2>(m_placement.m_inst.demands[m_demandID]);
//             const auto& functions = m_placement.m_inst.demands[m_demandID].functions;
//             int nbFunctions = m_placement.m_inst.demands[m_demandID].functions.size();
//             IloNumVarArray vars(m_placement.m_env, m * nbLayers + m_placement.m_inst.NFVNodes.size() * nbFunctions);
//             IloNumArray objCoefs(m_placement.m_env, m * nbLayers + m_placement.m_inst.NFVNodes.size() * nbFunctions);

//             IloNumArray linkCapaDuals(m_placement.m_env);
//             _sol.getDuals(linkCapaDuals, m_placement.m_placementModel.m_linkCapasCons);
//             IloNumArray nodeCapaDuals(m_placement.m_env);
//             _sol.getDuals(nodeCapaDuals, m_placement.m_placementModel.m_nodeCapasCons);

//             int index = 0;
//             for (int e(0); e < m; ++e) {
//                 for (int j(0); j < nbLayers; ++j) {
//                     vars[index] = getF(e, j);
//                     objCoefs[index] = d - d * linkCapaDuals[e];
//                     ++index;
//                 }
//             }

//             for (const auto& u : m_placement.m_inst.NFVNodes) {
//                 for (int j = 0; j < int(functions.size()); ++j) {
//                     objCoefs[index] = -nodeCapaDuals[u] * m_placement.m_inst.nbCores(m_demandID, j);
//                     vars[index] = m_a[n * j + u];
//                     ++index;
//                 }
//                 for (int j(functions.size()); j < int(nbLayers - 1); ++j) {
//                     objCoefs[index] = 0;
//                     vars[index] = m_a[n * j + u];
//                     ++index;
//                 }
//             }
//             m_obj.setConstant(-_sol.getDual(m_placement.m_placementModel.m_onePathCons[m_demandID]));
//             m_obj.setLinearCoefs(vars, objCoefs);
//             m_solver = IloCplex(m_model);
//             m_solver.setOut(m_placement.m_env.getNullStream());
//         }

//         ServicePath getServicePath() const {
// #if defined(LOG_LEVEL) && LOG_LEVEL <= 1
//             std::cout << "getServicePath() -> " << m_placement.m_inst.demands[m_demandID] << '\n';
// #endif
//             const auto edges = m_placement.m_inst.network.getEdges();
//             const auto& funcs = m_placement.m_inst.demands[m_demandID].functions;
//             const auto& t = std::get<1>(m_placement.m_inst.demands[m_demandID]);
//             std::vector<std::vector<function_descriptor>> installs(m_placement.m_inst.network.getOrder());

//             Graph::Node u = std::get<0>(m_placement.m_inst.demands[m_demandID]);
//             int j = 0;
//             Graph::Path path{u};

//             IloNumArray aVal(m_placement.m_env);
//             m_solver.getValues(aVal, m_a);
//             while (u != t || j < funcs.size()) {
//                 if (j < int(funcs.size()) && m_placement.m_isNFV[u] && aVal[n * j + u]) {
//                     installs[u].push_back(funcs[j]);
//                     ++j;
//                 } else {
//                     for (const auto& v : m_placement.m_inst.network.getNeighbors(u)) {
//                         if (m_solver.getValue(getF(m_placement.m_inst.edgeToId.at(Graph::Edge(u, v)), j)) > 0) {
//                             path.push_back(v);
//                             u = v;
//                             break;
//                         }
//                     }
//                 }
//             }

//             // Get localization of functions
//             std::vector<std::pair<Graph::Node, std::vector<function_descriptor>>> finalInstalls;
//             int nbFunctions = 0;
//             for (const auto& u : m_placement.m_inst.NFVNodes) {
//                 if (!installs[u].empty()) {
//                     finalInstalls.emplace_back(u, installs[u]);
//                     nbFunctions += installs[u].size();
//                 }
//             }
//             assert(nbFunctions == funcs.size());
//             return ServicePath(path, finalInstalls);
//         }

//       private:
//         const ChainingPath2& m_placement;
//         const int n;
//         const int m;
//         const int nbLayers;
//         IloModel m_model;

//         IloObjective m_obj;

//         IloNumVarArray m_a;
//         IloNumVarArray m_f;

//         IloRangeArray m_flowConsCons;
//         IloRangeArray m_linkCapaCons;
//         IloRangeArray m_nodeCapaCons;
//         IloRangeArray m_maxIn;
//         IloRangeArray m_maxOut;

//         IloCplex m_solver;
//     };

//   public:
//     ChainingPath2(const DiGraph& _network, const std::vector<int> _nodeCapa, const std::vector<Demand>& _demands, const std::vector<double>& _funcCharge, const std::vector<int>& _NFVNodes)
//         : m_env()
//         , m_inst.network(_network)
//         , m_inst.nodeCapa(_nodeCapa)
//         , m_funcCharge(_funcCharge)
//         , m_inst.demands(_demands)
//         , m_inst.maxChainSize(*std::max_element(m_inst.demands.begin(.functions, m_inst.demands.end(),
//                                                [&](const Demand& _d1, const Demand& _d2) {
//                                                    return _d1.functions > _d2.functions;
//                                                }))
//                               .size())
//         , m_inst.NFVNodes(_NFVNodes)
//         , m_isNFV([&]() {
//             std::vector<int> isNFV(m_inst.network.getOrder(), 0);
//             for (const auto& u : m_inst.NFVNodes) {
//                 isNFV[u] = 1;
//             }
//             return isNFV;
//         }())
//         , m_inst.nbCores([&]() {
//             Matrix<double> mat(m_inst.demands.size(), m_funcCharge.size(), -1);

//             for (int i(0); i < int(m_inst.demands.size()); ++i) {
//                 const auto& funcs = m_inst.demands[i].functions;

//                 for (int j(0); j < int(funcs.size()); ++j) {
//                     mat(i, j) = ceil(std::get<2>(m_inst.demands[i]) / m_funcCharge[funcs[j]]);
//                 }
//             }
//             return mat;
//         }())
//         , m_intObj(-1)
//         , m_fractObj(-1)
//         , m_inst.edgeToId([&]() {
//             std::map<Graph::Edge, int> edgeToId;
//             int i = 0;
//             for (Graph::Edge edge : m_inst.network.getEdges()) {
//                 edgeToId[edge] = i;
//                 ++i;
//             }
//             return edgeToId;
//         }())
//         , m_placementModel(*this)
//         , m_pp(*this) {
//     }

//     void addInitConf(const std::vector<ServicePath>& _paths) {
//         int i = 0;
//         for (auto& path : _paths) {
//             m_placementModel.addPath(path, i);
//             ++i;
//         }
//     }

//     void save(const std::string& _filename, const std::pair<double, double>& _time) {
//         std::ofstream ofs(_filename);
//         ofs << m_intObj << '\t' << m_fractObj << '\n';
//         ofs << m_placementModel.m_paths.size() << std::endl;
//         ofs << _time.first << '\t' << _time.second << std::endl;

//         std::vector<double> linkUsage(m_inst.network.size(), 0);
//         std::vector<int> nodeUsage(m_inst.network.getOrder(), 0);
//         std::vector<IloNumVar> pathsUsed(m_inst.demands.size(), nullptr);

//         for (int i = 0; i < int(m_placementModel.m_paths.size()); ++i) {
//             if (m_placementModel.m_solver.getValue(m_placementModel.m_y[i]) != 0) {
//                 const auto& path = m_placementModel.m_paths[i].first.first;
//                 const auto& funcPlacement = m_placementModel.m_paths[i].first.second;
//                 const auto& demand = m_inst.demands[m_placementModel.m_paths[i].second];
//                 const auto d = std::get<2>(demand);
//                 pathsUsed[m_placementModel.m_paths[i].second] = m_placementModel.m_y[i];

//                 for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
//                     linkUsage[m_inst.edgeToId.at(Graph::Edge(*iteU, *iteV))] += d * m_placementModel.m_solver.getValue(m_placementModel.m_y[i]);
//                 }
//                 int j = 0;
//                 for (const auto& pair_NodeFuncs : funcPlacement) {
//                     for (const auto& func : pair_NodeFuncs.second) {
//                         nodeUsage[pair_NodeFuncs.first] += m_placementModel.m_solver.getValue(m_placementModel.m_y[i]) * m_inst.nbCores(m_placementModel.m_paths[i].second, j);
//                         ++j;
//                     }
//                 }
//             }
//         }
//         for (Graph::Node u = 0; u < m_inst.network.getOrder(); ++u) {
//             ofs << u << '\t' << nodeUsage[u] << '\t' << m_inst.nodeCapa[u] << '\n';
//         }
//         int i = 0;
//         for (const auto& edge : m_inst.network.getEdges()) {
//             ofs << std::fixed << edge.first << '\t' << edge.second << '\t' << linkUsage[i] << '\t' << m_inst.network.getEdgeWeight(edge) << '\n';
//             ++i;
//         }
//         std::cout << "Results saved to :" << _filename << '\n';
//     }

//     void solveInteger() {
//         m_placementModel.solveInteger();
//     }

//     bool solve() {
//         IloCplex mainSolver(m_placementModel.m_model);
//         mainSolver.setOut(m_env.getNullStream());

//         bool reducedCost;
//         std::vector<std::pair<ServicePath, int>> toAdd;
//         toAdd.reserve(m_inst.demands.size());
//         do {
//             reducedCost = false;
//             toAdd.clear();
//             if (mainSolver.solve()) {
// #if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 1
//                 std::cout << m_placementModel.m_model << '\n';
//                 m_placementModel.getNetworkUsage(mainSolver);
// #endif
//                 m_fractObj = mainSolver.getObjValue();
//                 std::cout << "Reduced Main Problem Objective Value = " << m_fractObj << '\n';

//                 for (DemandID i(0); i < int(m_inst.demands.size()); ++i) {
// /* For each demands, search for a shortest path in the augmented graph */
// #if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 2
//                     std::cout << '\r' << "Solving " << i << "th pp" << std::flush;
// #endif
//                     m_pp.updateDual(i, mainSolver);
//                     if (m_pp.m_solver.solve()) {
//                         const auto rc = m_pp.m_solver.getObjValue();
//                         if (rc < -RC_EPS) {
// #if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 2
//                             std::cout << "Reduced cost of " << rc;
// #endif
//                             // std::cout << pp.m_obj << '\n';

//                             const ServicePath sPath = m_pp.getServicePath();
// #if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 2
//                             std::cout << " with " << sPath << '\n';
// #endif
//                             reducedCost = true;
//                             toAdd.emplace_back(sPath, i);
//                         }
//                     } else {
//                         m_pp.m_solver.exportModel("pp.lp");
//                         std::cout << i << "th pp no solution" << '\n';
//                     }
//                 }
//                 std::cout << '\n';
//                 for (auto& p : toAdd) {
//                     m_placementModel.addPath(p.first, p.second);
//                 }
//             } else {
//                 mainSolver.exportModel("rm.lp");
//                 std::cout << "No solution found for RMP" << '\n';
//             }

//         } while (reducedCost);
//         m_intObj = m_placementModel.solveInteger();
//         return m_intObj > 0;
//     }

//   private:
//     const IloEnv m_env { };
//     const DiGraph m_inst.network;
//     const std::vector<int> m_inst.nodeCapa;
//     const std::vector<double> m_funcCharge;
//     const std::vector<Demand> m_inst.demands;

//     int m_inst.maxChainSize;
//     const std::vector<int> m_inst.NFVNodes;
//     const std::vector<int> m_isNFV;
//     const Matrix<double> m_inst.nbCores;
//     double m_intObj;
//     double m_fractObj;

//     const std::map<Graph::Edge, int> m_inst.edgeToId;

//     MasterProblem m_placementModel;
//     PricingProblem m_pp;

//     friend MasterProblem;
// };

// #endif