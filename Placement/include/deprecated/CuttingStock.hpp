// class CuttingStock {
//     class MainProblem {
//         friend CuttingStock;

//       public:
//         MainProblem(CuttingStock& _cuttingStock)
//             : m_cuttingStock(_cuttingStock)
//             , m_cutOpt(_cuttingStock.m_env)
//             , m_rollsUsed(IloAdd(m_cutOpt, IloMinimize(_cuttingStock.m_env)))
//             , m_fill(IloAdd(m_cutOpt, IloRangeArray(_cuttingStock.m_env, m_cuttingStock.m_amount, IloInfinity)))
//             , m_cut(_cuttingStock.m_env) {
//             for (int j = 0; j < m_cuttingStock.m_nWdth; j++) {
//                 m_cut.add(IloNumVar(m_rollsUsed(1) + m_fill[j](int(m_cuttingStock.m_rollWidth / m_cuttingStock.m_size[j]))));
//             }
//             m_fill.setNames("fill");
//             m_cut.setNames("cut");
//         }

//       private:
//         CuttingStock& m_cuttingStock;
//         IloModel m_cutOpt;

//         IloObjective m_rollsUsed;
//         IloRangeArray m_fill;
//         IloNumVarArray m_cut;
//     };

//     class PricingProblem {
//         friend CuttingStock;

//       public:
//         PricingProblem(CuttingStock& _cuttingStock)
//             : m_cuttingStock(_cuttingStock)
//             , m_patGen(m_cuttingStock.m_env)
//             , m_reducedCost(IloAdd(m_patGen, IloMinimize(m_cuttingStock.m_env, 1)))
//             , m_use(m_cuttingStock.m_env, m_cuttingStock.m_nWdth, 0.0, IloInfinity, ILOINT) {
//             m_patGen.add(IloScalProd(m_cuttingStock.m_size, m_use) <= m_cuttingStock.m_rollWidth);
//             m_use.setNames("use");
//         }

//       private:
//         CuttingStock& m_cuttingStock;
//         IloModel m_patGen;

//         IloObjective m_reducedCost;
//         IloNumVarArray m_use;
//     };

//   public:
//     CuttingStock(const std::string& _filename = "cutstock.dat")
//         : m_env()
//         , m_rollWidth()
//         , m_amount(m_env)
//         , m_size(m_env)
//         , m_nWdth()
//         , m_mainProblem(nullptr) {
//         std::ifstream in(_filename);
//         if (in) {
//             in >> m_rollWidth;
//             std::cout << "m_rollWidth: " << m_rollWidth << std::endl;
//             in >> m_size;
//             std::cout << "m_size: " << m_size << std::endl;
//             in >> m_amount;
//             std::cout << "m_amount: " << m_amount << std::endl;
//             m_nWdth = m_size.getSize();
//         } else {
//             std::cerr << "No such file: " << _filename << std::endl;
//             throw(1);
//         }
//         m_mainProblem = new MainProblem(*this);
//     }

//     ~CuttingStock() {
//         delete m_mainProblem;
//     }

//     const CuttingStock& operator=(const CuttingStock&) = delete;
//     CuttingStock(const CuttingStock&) = delete;

//     void solve() {
//         IloCplex cutSolver(m_mainProblem->m_cutOpt);
//         cutSolver.setOut(m_env.getNullStream());
//         PricingProblem pp(*this);
//         IloCplex patSolver(pp.m_patGen);
//         patSolver.setOut(m_env.getNullStream());

//         IloNumArray price(m_env, m_nWdth);
//         IloNumArray newPatt(m_env, m_nWdth);
//         while (true) {
//             /* OPTIMIZE OVER CURRENT PATTERNS */

//             cutSolver.solve();
//             std::cout << "Obj value=" << cutSolver.getObjValue() << " with " << m_mainProblem->m_cut.getSize() << " cuts." << std::endl;
//             /* FIND AND ADD A NEW PATTERN */

//             for (int i = 0; i < m_nWdth; ++i) {
//                 // std::cout << "dual["<<i<<"]=" << cutSolver.getDual(m_mainProblem->m_fill[i]) << std::endl;
//                 price[i] = -cutSolver.getDual(m_mainProblem->m_fill[i]);
//             }
//             pp.m_reducedCost.setLinearCoefs(pp.m_use, price);

//             patSolver.solve();
//             if (patSolver.getObjValue() > -RC_EPS) {
//                 break;
//             }
//             std::cout << "Reduced cost of " << patSolver.getObjValue() << std::endl;
//             patSolver.getValues(newPatt, pp.m_use);
//             m_mainProblem->m_cut.add(IloNumVar(m_mainProblem->m_rollsUsed(1) + m_mainProblem->m_fill(newPatt)));
//         }

//         m_mainProblem->m_cutOpt.add(IloConversion(m_env, m_mainProblem->m_cut, ILOINT));

//         cutSolver.solve();
//         std::cout << "Solution status: " << cutSolver.getStatus() << std::endl;
//     }

//     friend PricingProblem;
//     friend MainProblem;

//   private:
//     IloEnv m_env { };
//     IloNum m_rollWidth;
//     IloNumArray m_amount;
//     IloNumArray m_size;

//     IloInt m_nWdth;

//     MainProblem* m_mainProblem;
// };