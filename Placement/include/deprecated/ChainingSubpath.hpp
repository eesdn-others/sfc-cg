#ifndef CHAININGABHISHEK_PATH_HPP
#define CHAININGABHISHEK_PATH_HPP

#include <chrono>
#include <cmath>
#include <ilcplex/ilocplex.h>
#include <iostream>
#include <map>
#include <tuple>

#include "DiGraph.hpp"
#include "SFC.hpp"
#include "ShortestPath.hpp"
#include "cplex_utility.hpp"
#include "utility.hpp"

#define RC_EPS 1.0e-6

struct Configuration {
    Configuration(const Graph::Path& _path = Graph::Path(),
        const std::vector<Graph::Node>& _localisation = std::vector<Graph::Node>(),
        const std::vector<DemandID>& _demands = std::vector<DemandID>(),
        const int& _chainID = -1)
        : path(_path)
        , localisation(_localisation)
        , demands(_demands)
        , chainID(_chainID) {}
    Graph::Path path;
    std::vector<Graph::Node> localisation;
    std::vector<DemandID> demands;
    int chainID;
};

std::ostream& operator<<(std::ostream& _out, const Configuration& _conf) {
    return _out << "{path: " << _conf.path << ", localisation:" << _conf.localisation << ", demands:" << _conf.demands << ", chainID:" << _conf.chainID << "}";
}

bool operator==(const Configuration& _c1, const Configuration& _c2) {
    return _c1.path == _c2.path && _c1.localisation == _c2.localisation && _c1.demands == _c2.demands && _c1.chainID == _c2.chainID;
}

class ChainingSubpath {
    class ReducedMainProblem {
        friend ChainingSubpath;

      public:
        ReducedMainProblem(const ChainingSubpath& _placement)
            : m_placement(_placement)
            , m_env()
            , m_model(m_env)
            , m_fStart([&]() {
                IloNumVarArray f(m_env, m_placement.m_inst.demands.size() * m_placement.m_inst.network.size(), 0.0, IloInfinity);
                for (int i = 0; i < m_placement.m_inst.demands.size(); ++i) {
                    for (auto e = 0; e < m_placement.m_inst.network.size()); ++e; {
                        setIloName(f[getIndexF(i, e)],
                            "fStart" + toString(std::make_tuple(m_placement.m_inst.edges[e], m_placement.m_inst.demands[i])));
                    }
                }
                return IloAdd(m_model, f);
            }())
            , m_fEnd([&]() {
                IloNumVarArray f(m_env, m_placement.m_inst.demands.size() * m_placement.m_inst.network.size(), 0.0, IloInfinity);
                for (int i = 0; i < m_placement.m_inst.demands.size(); ++i) {
                    for (auto e = 0; e < m_placement.m_inst.network.size()); ++e; {
                        setIloName(f[getIndexF(i, e)],
                            "fEnd" + toString(std::make_tuple(m_placement.m_inst.edges[e], m_placement.m_inst.demands[i])));
                    }
                }
                return IloAdd(m_model, f);
            }())
            , m_z(IloAdd(m_model, IloNumVarArray(m_env)))
            , m_obj([&]() {
                IloNumArray vals(m_env);
                IloNumVarArray vars(m_env);
                for (int i = 0; i < m_placement.m_inst.demands.size(); ++i) {
                    for (auto e = 0; e < m_placement.m_inst.network.size()); ++e; {
                        vars.add(m_fStart[m_placement.m_inst.network.size() * i + e]);
                        vals.add(std::get<2>(m_placement.m_inst.demands[i]));
                        vars.add(m_fEnd[m_placement.m_inst.network.size() * i + e]);
                        vals.add(std::get<2>(m_placement.m_inst.demands[i]));
                    }
                }
                return IloAdd(m_model, IloMinimize(m_env, IloScalProd(vars, vals)));
            }())
            ,
            /***************** Constraints *****************/
            m_startFlowConservationCons1([&]() {
                IloRangeArray flowConservationCons(m_env, m_placement.m_inst.demands.size() * m_placement.m_inst.network.getOrder());

                for (int i = 0; i < m_placement.m_inst.demands.size(); ++i) {
                    for (Graph::Node u = 0; u < m_placement.m_inst.network.getOrder(); ++u) {
                        if (u != std::get<0>(m_placement.m_inst.demands[i])) {
                            IloNumVarArray vars(m_env);
                            IloNumArray vals(m_env);

                            for (const auto v : m_placement.m_inst.network.getNeighbors(u)) {
                                vars.add(m_fStart[getIndexF(i, m_placement.m_inst.edgeToId(u, v))]);
                                vals.add(1.0);
                                vars.add(m_fStart[getIndexF(i, m_placement.m_inst.edgeToId(v, u))]);
                                vals.add(-1.0);
                            }
                            flowConservationCons[m_placement.m_inst.demands.size() * u + i] =
                                IloRange(m_env, 0, IloScalProd(vars, vals), 0);
                            setIloName(flowConservationCons[m_placement.m_inst.demands.size() * u + i], "fStartCons1" + toString(std::make_pair(u, i)));
                        }
                    }
                }

                for (int i = 0; i < m_placement.m_inst.demands.size(); ++i) {
                    Graph::Node u = std::get<0>(m_placement.m_inst.demands[i]);
                    IloNumVarArray vars(m_env);
                    IloNumArray vals(m_env);

                    for (const auto v : m_placement.m_inst.network.getNeighbors(u)) {
                        vars.add(m_fStart[getIndexF(i, m_placement.m_inst.edgeToId(u, v))]);
                        vals.add(1.0);
                    }
                    flowConservationCons[m_placement.m_inst.demands.size() * u + i] = IloRange(m_env, 1, IloScalProd(vars, vals), 1);
                    setIloName(flowConservationCons[m_placement.m_inst.demands.size() * u + i], "fStartCons1" + toString(std::make_pair(u, i)));
                }

                return IloAdd(m_model, flowConservationCons);
            }())
            , m_startFlowConservationCons2([&]() {
                IloRangeArray flowConservationCons(m_env, m_placement.m_inst.demands.size() * m_placement.m_inst.network.getOrder());

                for (int i = 0; i < m_placement.m_inst.demands.size(); ++i) {
                    for (Graph::Node u = 0; u < m_placement.m_inst.network.getOrder(); ++u) {
                        if (u != std::get<0>(m_placement.m_inst.demands[i])) {
                            IloNumVarArray vars(m_env);
                            IloNumArray vals(m_env);

                            for (const auto v : m_placement.m_inst.network.getNeighbors(u)) {
                                vars.add(m_fStart[getIndexF(i, m_placement.m_inst.edgeToId(v, u))]);
                                vals.add(-1.0);
                            }
                            flowConservationCons[m_placement.m_inst.demands.size() * u + i] =
                                IloRange(m_env, -IloInfinity, IloScalProd(vars, vals), 0);
                            setIloName(flowConservationCons[m_placement.m_inst.demands.size() * u + i], "fStartCons2" + toString(std::make_pair(u, i)));
                        }
                    }
                }

                return IloAdd(m_model, flowConservationCons);
            }())
            , m_endFlowConservationCons1([&]() {
                IloRangeArray flowConservationCons(m_env, m_placement.m_inst.demands.size() * m_placement.m_inst.network.getOrder());

                for (int i = 0; i < m_placement.m_inst.demands.size(); ++i) {
                    for (Graph::Node u = 0; u < m_placement.m_inst.network.getOrder(); ++u) {
                        if (u != std::get<1>(m_placement.m_inst.demands[i])) {
                            IloNumVarArray vars(m_env);
                            IloNumArray vals(m_env);

                            for (const auto& v : m_placement.m_inst.network.getNeighbors(u)) {
                                vars.add(m_fEnd[getIndexF(i, m_placement.m_inst.edgeToId(u, v))]);
                                vals.add(1.0);
                                vars.add(m_fEnd[getIndexF(i, m_placement.m_inst.edgeToId(v, u))]);
                                vals.add(-1.0);
                            }
                            flowConservationCons[m_placement.m_inst.demands.size() * u + i] = IloRange(m_env, 0, IloScalProd(vars, vals), 0);
                            setIloName(flowConservationCons[m_placement.m_inst.demands.size() * u + i], "fEndCons1" + toString(std::make_pair(u, i)));
                        }
                    }
                }

                for (int i = 0; i < m_placement.m_inst.demands.size(); ++i) {
                    Graph::Node u = std::get<1>(m_placement.m_inst.demands[i]);
                    IloNumVarArray vars(m_env);
                    IloNumArray vals(m_env);

                    for (const auto v : m_placement.m_inst.network.getNeighbors(u)) {
                        vars.add(m_fEnd[getIndexF(i, m_placement.m_inst.edgeToId(v, u))]);
                        vals.add(1.0);
                    }
                    flowConservationCons[m_placement.m_inst.demands.size() * u + i] = IloRange(m_env, 1, IloScalProd(vars, vals), 1);
                    setIloName(flowConservationCons[m_placement.m_inst.demands.size() * u + i], "fEndCons1" + toString(std::make_pair(u, i)));
                }

                return IloAdd(m_model, flowConservationCons);
            }())
            , m_endFlowConservationCons2([&]() {
                IloRangeArray flowConservationCons(m_env, m_placement.m_inst.demands.size() * m_placement.m_inst.network.getOrder());

                for (int i = 0; i < m_placement.m_inst.demands.size(); ++i) {
                    for (Graph::Node u = 0; u < m_placement.m_inst.network.getOrder(); ++u) {
                        if (u != std::get<1>(m_placement.m_inst.demands[i])) {
                            IloNumVarArray vars(m_env);
                            IloNumArray vals(m_env);

                            for (const auto& v : m_placement.m_inst.network.getNeighbors(u)) {
                                vars.add(m_fEnd[getIndexF(i, m_placement.m_inst.edgeToId(u, v))]);
                                vals.add(-1.0);
                            }
                            flowConservationCons[m_placement.m_inst.demands.size() * u + i] = IloRange(m_env, -IloInfinity, IloScalProd(vars, vals), 0);
                            setIloName(flowConservationCons[m_placement.m_inst.demands.size() * u + i], "fEndCons2" + toString(std::make_pair(u, i)));
                        }
                    }
                }

                return IloAdd(m_model, flowConservationCons);
            }())
            , m_nodeCapasCons([&]() {
                IloRangeArray nodeCapasCons(m_env, m_placement.m_inst.NFVNodes.size());
                for (const auto& u : m_placement.m_inst.NFVNodes) {
                    const int i = m_placement.m_inst.NFVIndices[u];
                    nodeCapasCons[i] = IloAdd(m_model, IloRange(m_env, 0.0, m_placement.m_inst.nodeCapa[u]));
                    setIloName(nodeCapasCons[i], "nodeCapasCons" + std::to_string(u));
                }
                return nodeCapasCons;
            }())
            , m_linkCapasCons([&]() {
                IloRangeArray linkCapaCons(m_env, m_placement.m_inst.network.size());
                int e = 0;
                for (const Graph::Edge& edge : m_placement.m_inst.edges) {
                    IloNumVarArray vars(m_env);
                    IloNumArray vals(m_env);
                    for (int i = 0; i < m_placement.m_inst.demands.size(); ++i) {
                        vars.add(m_fStart[getIndexF(i, e)]);
                        vals.add(std::get<2>(m_placement.m_inst.demands[i]));
                        vars.add(m_fEnd[getIndexF(i, e)]);
                        vals.add(std::get<2>(m_placement.m_inst.demands[i]));
                    }
                    // vals.add(-1.0);
                    linkCapaCons[e] = IloRange(m_env,
                        0.0, IloScalProd(vars, vals), m_placement.m_inst.network.getEdgeWeight(edge));
                    setIloName(linkCapaCons[e], "linkCapas" + toString(edge));
                    ++e;
                }
                return IloAdd(m_model, linkCapaCons);
            }())
            , m_oneConfig([&]() {
                IloRangeArray oneConfig(m_env, m_placement.m_inst.demands.size());
                for (DemandID i = 0; i < m_placement.m_inst.demands.size()); ++i; {
                    oneConfig[i] = IloRange(m_env, 1.0, 1.0);
                }
                return IloAdd(m_model, oneConfig);
            }())
            , m_solver([&]() {
                IloCplex solver(m_model);
                // #if defined(NDEBUG)
                solver.setOut(m_env.getNullStream());
                // #endif
                solver.setParam(IloCplex::Threads, 1);
                return solver;
            }())
            , m_configs() {
        }

        ~ReducedMainProblem() {
            m_env.end();
        }

        ReducedMainProblem& operator=(const ReducedMainProblem&) = delete;
        ReducedMainProblem(const ReducedMainProblem&) = delete;

        void addConfiguration(const Configuration& _config) {
#if !defined(NDEBUG)
            std::cout << "addConfiguration" << _config << '\n';
#endif
            assert(!_config.path.empty());
            assert(!_config.localisation.empty());
            assert(!_config.demands.empty());
            assert(_config.chainID != -1);
            // Assert no double column
            assert([&]() {
                const auto ite = std::find(m_configs.begin(), m_configs.end(), _config);
                if (ite == m_configs.end()) {
                    return true;
                } else {
                    std::cout << _config << " is already present!" << '\n';
                    return false;
                }
            }());
            IloNumColumn column(m_env);

            /***************************************** Node constraints ******************************************************/
            IloNumArray nodeCharge(m_env, m_placement.m_inst.NFVNodes.size());
            for (IloInt i = 0; i < nodeCharge.getSize(); ++i) {
                nodeCharge[i] = 0.0;
            }
            int j = 0;
            for (const auto& u : _config.localisation) {
                for (const auto i : _config.demands) { // @TODO: Can be precomputed
                    nodeCharge[m_placement.m_inst.NFVIndices[u]] += m_placement.m_inst.nbCores(i, j);
                }
                ++j;
            }
            column += m_nodeCapasCons(nodeCharge);
            /****************************************************************************************************************/

            /*********************************************** Link constraints ***********************************************/
            IloNumArray linkCharge(m_env, m_placement.m_inst.network.size());
            for (int e = 0; e < linkCharge.getSize()); ++e; {
                linkCharge[e] = IloNum(0.0);
            }
            const double totalLinkCharge = std::accumulate(_config.demands.begin(), _config.demands.end(), 0.0,
                [&](const double& __charge, const DemandID& __i) {
                    return __charge + std::get<2>(m_placement.m_inst.demands[__i]);
                });

            for (auto iteU = _config.path.begin(), iteV = std::next(iteU); iteV != _config.path.end(); ++iteU, ++iteV) {
                linkCharge[m_placement.m_inst.edgeToId(*iteU, *iteV)] += totalLinkCharge;
            }
            column += m_linkCapasCons(linkCharge);
            /****************************************************************************************************************/

            /**************************************** Flow conservation constraints ****************************************/
            for (const auto& i : _config.demands) {
                const int s = std::get<0>(m_placement.m_inst.demands[i]),
                          t = std::get<1>(m_placement.m_inst.demands[i]);
                /* Start flow conservation */
                column += m_startFlowConservationCons1[m_placement.m_inst.demands.size() * _config.path.front() + i](1.0);
                if (s != _config.path.front()) {
                    column += m_startFlowConservationCons2[m_placement.m_inst.demands.size() * _config.path.front() + i](1.0);
                }

                /* End flow conservation */
                if (t == _config.path.back()) {
                    column += m_endFlowConservationCons1[m_placement.m_inst.demands.size() * _config.path.back() + i](1.0);
                } else {
                    column += m_endFlowConservationCons1[m_placement.m_inst.demands.size() * _config.path.back() + i](-1.0);
                    column += m_endFlowConservationCons2[m_placement.m_inst.demands.size() * _config.path.back() + i](1.0);
                }
            }
            //***************************************************************************************************************/

            /**************************************  One config per demand constraints***************************************/
            for (const auto& i : _config.demands) {
                column += m_oneConfig[i](1.0);
            }

            //***************************************************************************************************************/

            // @TODO: Add to function limit

            // Add to objective function
            column += m_obj((_config.path.size() - 1) * totalLinkCharge);

            IloNumVar zVar = IloNumVar(column);
            setIloName(zVar, "z" + toString(std::make_tuple(_config.path, _config.localisation, _config.demands)));
            m_z.add(zVar);
            m_configs.emplace_back(_config);
        }

        int getIndexX(const int _u, const int _f) const {
            return m_placement.m_inst.NFVIndices[_u] * m_placement.m_funcCharge.size() + _f;
        }

        int getIndexF(const int _i, const int _e) {
            assert(0 <= _i && _i < m_placement.m_inst.demands.size());
            assert(0 <= _e && _e < m_placement.m_inst.network.size());
            return m_placement.m_inst.network.size() * _i + _e;
        }

        bool checkReducedCosts() const {
            for (int k = 0; k < m_z.getSize()); ++k; {
                double solverRC = m_solver.getReducedCost(m_z[k]);
                // std::cout << "Checking reducedCost of " << m_configs[k] << '\n';
                // Get RC using duals
                double myRC = 0.0;
                IloNumArray linkCapaDuals(m_env);
                m_solver.getDuals(linkCapaDuals, m_placement.m_mainProblem.m_linkCapasCons);
                IloNumArray nodeCapaDuals(m_env);
                m_solver.getDuals(nodeCapaDuals, m_placement.m_mainProblem.m_nodeCapasCons);

                IloNumArray startFlowConservationDual1(m_env);
                m_solver.getDuals(startFlowConservationDual1, m_placement.m_mainProblem.m_startFlowConservationCons1);
                IloNumArray endFlowConservationDual1(m_env);
                m_solver.getDuals(endFlowConservationDual1, m_placement.m_mainProblem.m_endFlowConservationCons1);

                IloNumArray oneConfigDual(m_env);
                m_solver.getDuals(oneConfigDual, m_placement.m_mainProblem.m_oneConfig);

                // Link constraints
                for (auto iteU = m_configs[k].path.begin(), iteV = std::next(iteU); iteV != m_configs[k].path.end(); ++iteU, ++iteV) {
                    for (const auto& i : m_configs[k].demands) {
                        const double d = std::get<2>(m_placement.m_inst.demands[i]);
                        myRC += d - d * linkCapaDuals[m_placement.m_inst.edgeToId(*iteU, *iteV)];
#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
                        std::cout << "myRC(link) +=" << std::get<2>(m_placement.m_inst.demands[i]) - std::get<2>(m_placement.m_inst.demands[i]) * linkCapaDuals[m_placement.m_inst.edgeToId(*iteU, *iteV)] << '\n';
#endif
                    }
                }
                //***************************************************************************************************************/

                /******************************************** One config per demand ********************************************/
                for (const auto& i : m_configs[k].demands) {
                    myRC += -oneConfigDual[i];
#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
                    std::cout << "myRC(oneConf)[" << i << "] +=" << -oneConfigDual[i] << '\n';
#endif
                }
                /***************************************************************************************************************/

                /*********************************************** Node constraints ***********************************************/
                for (int j = 0; j < m_configs[k].localisation.size()); ++j; {
                    const Graph::Node u = m_configs[k].localisation[j];
                    for (const auto& i : m_configs[k].demands) {
#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
                        std::cout << "myRC(node) +=" << -nodeCapaDuals[m_placement.m_inst.NFVIndices[u]] * m_placement.m_inst.nbCores(i, j) << '\n';
#endif
                        myRC += -nodeCapaDuals[m_placement.m_inst.NFVIndices[u]] * m_placement.m_inst.nbCores(i, j);
                    }
                }
                //***************************************************************************************************************/

                /**************************************** Flow conservation constraints ****************************************/
                for (const auto& i : m_configs[k].demands) {
                    const int endValue = m_configs[k].localisation.back() == std::get<1>(m_placement.m_inst.demands[i]) ? 1.0 : -1.0;
#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
                    std::cout << "myRC(fStartCons) +=" << -startFlowConservationDual1[m_placement.m_inst.demands.size() * m_configs[k].localisation.front() + i] << '\n';
                    std::cout << "myRC(fEndCons) +=" << -endFlowConservationDual1[m_placement.m_inst.demands.size() * m_configs[k].localisation.back() + i] * endValue << '\n';

#endif
                    myRC += -startFlowConservationDual1[m_placement.m_inst.demands.size() * m_configs[k].localisation.front() + i]
                            - endFlowConservationDual1[m_placement.m_inst.demands.size() * m_configs[k].localisation.back() + i] * endValue;
                    if (m_configs[k].localisation.front() != std::get<0>(m_placement.m_inst.demands[i])) {
                        myRC += -m_solver.getDual(m_placement.m_mainProblem.m_startFlowConservationCons2[m_placement.m_inst.demands.size() * m_configs[k].localisation.front() + i]);
                    }
                    if (m_configs[k].localisation.back() != std::get<1>(m_placement.m_inst.demands[i])) {
                        myRC += -m_solver.getDual(m_placement.m_mainProblem.m_endFlowConservationCons2[m_placement.m_inst.demands.size() * m_configs[k].localisation.back() + i]);
                    }
                }
                /***************************************************************************************************************/

                // std::cout << myRC << " vs " << solverRC << '\n';
                if (abs(myRC - solverRC) > RC_EPS) {
                    return false;
                }
            }
            return true;
        }

        double solveInteger() {
            m_model.add(IloConversion(m_env, m_z, ILOBOOL));
            m_model.add(IloConversion(m_env, m_fEnd, ILOBOOL));
            m_model.add(IloConversion(m_env, m_fStart, ILOBOOL));
            if (m_solver.solve()) {
                auto obj = m_solver.getObjValue();
                std::cout << "Final obj value: " << obj << "\t# Paths: " << m_configs.size() << '\n';
                getNetworkUsage(m_solver);
                return obj;
            } else {
                return -1.0;
            }
        }

        void getNetworkUsage(const IloCplex& _sol) const {
            std::vector<double> linkUsage(m_placement.m_inst.network.size(), 0);
            std::vector<int> nodeUsage(m_placement.m_inst.network.getOrder(), 0);

            for (int k = 0; k < m_configs.size()); ++k; {
                if (_sol.getValue(m_z[k]) > 0.0) {
                    std::cout << m_configs[k] << "->" << _sol.getValue(m_z[k]) << "is set\n";
                    const double totalLinkCharge = std::accumulate(m_configs[k].demands.begin(), m_configs[k].demands.end(), 0.0,
                        [&](const double& __charge, const DemandID& __i) {
                            return __charge + std::get<2>(m_placement.m_inst.demands[__i]);
                        });
                    for (auto iteU = m_configs[k].path.begin(), iteV = std::next(iteU); iteV != m_configs[k].path.end(); ++iteU, ++iteV) {
                        linkUsage[m_placement.m_inst.edgeToId(*iteU, *iteV)] += totalLinkCharge;
                    }

                    int j = 0;
                    for (const auto& u : m_configs[k].localisation) {
                        for (const auto i : m_configs[k].demands) { // @TODO: Can be precomputed
                            nodeUsage[u] += m_placement.m_inst.nbCores(i, j);
                        }
                        ++j;
                    }
                }
            }

            std::cout << "Node usage: \n";
            for (const auto& u : m_placement.m_inst.NFVNodes) {
                std::cout << '\t' << u << " -> " << nodeUsage[u] << " / " << m_placement.m_inst.nodeCapa[u] << '\n';
                assert(nodeUsage[u] <= m_placement.m_inst.nodeCapa[u]);
            }
            std::cout << "Link usage: \n";
            int i = 0;
            for (const auto& edge : m_placement.m_inst.network.getEdges()) {
                std::cout << std::fixed << '\t' << edge << " -> " << linkUsage[i] << " / " << m_placement.m_inst.network.getEdgeWeight(edge) << '\n';
                assert(m_placement.m_inst.network.getEdgeWeight(edge) - linkUsage[i] > -1.0e-6
                       || (std::cerr << std::fixed << m_linkCapasCons[i] << '\n'
                                     << m_placement.m_inst.network.getEdgeWeight(edge) - linkUsage[i] << '\n',
                              false));
                ++i;
            }
        }

      private:
        const ChainingSubpath& m_placement;
        IloEnv m_env { };
        IloModel m_model;

        IloNumVarArray m_fStart;
        IloNumVarArray m_fEnd;
        IloNumVarArray m_z;

        IloObjective m_obj;

        IloRangeArray m_startFlowConservationCons1;
        IloRangeArray m_startFlowConservationCons2;
        IloRangeArray m_endFlowConservationCons1;
        IloRangeArray m_endFlowConservationCons2;

        IloRangeArray m_nodeCapasCons;
        IloRangeArray m_linkCapasCons;
        IloRangeArray m_oneConfig;

        IloCplex m_solver;

        std::vector<Configuration> m_configs;
    };

    class PricingProblem {
        friend ChainingSubpath;

      public:
        PricingProblem(const ChainingSubpath& _chainPlacement, const int _c)
            : m_placement(_chainPlacement)
            , m_env()
            , m_c(_c)
            , n(m_placement.m_inst.network.getOrder())
            , m(m_placement.m_inst.network.size())
            , m_model(m_env)
            , m_obj(IloAdd(m_model, IloMinimize(m_env)))
            , m_a([&]() { // jth function is installed for
                IloNumVarArray a(m_env, m_placement.m_chains[m_c].size() * m_placement.m_inst.NFVNodes.size());
                for (const auto& u : m_placement.m_inst.NFVNodes) {
                    for (int j(0); j < m_placement.m_chains[m_c].size()); ++j; {
                        a[getIndexA(u, j)] = IloAdd(m_model,
                            IloNumVar(m_env, 0, 1, ILOBOOL));
                        setIloName(a[getIndexA(u, j)], "a" + toString(std::make_tuple(u, j)));
                    }
                }
                return a;
            }())
            , m_f([&]() {
                IloNumVarArray f(m_env, m * m_placement.m_chains[m_c].size(), 0, 1, ILOBOOL);
                for (int e(0); e < m; ++e) {
                    for (int j(0); j < m_placement.m_chains[m_c].size()); ++j; {
                        setIloName(f[getIndexF(e, j)], "f" + toString(std::make_tuple(m_placement.m_inst.edges[e], j)));
                    }
                }
                return IloAdd(m_model, f);
            }())
            , m_b([&]() { // jth function is installed for
                IloNumVarArray b(m_env, m_placement.m_chains[m_c].size() * m_placement.m_inst.NFVNodes.size() * m_placement.m_inst.demands.size());
                for (const auto& u : m_placement.m_inst.NFVNodes) {
                    for (int j(0); j < m_placement.m_chains[m_c].size()); ++j; {
                        for (const auto& i : m_placement.m_inst.demandsPerChain[m_c]) {
                            b[getIndexB(u, i, j)] = IloAdd(m_model,
                                IloNumVar(m_env, 0, 1, ILOBOOL));
                            setIloName(b[getIndexB(u, i, j)], "b" + toString(std::make_tuple(u, i, j)));
                        }
                    }
                }
                return b;
            }())
            , m_g([&]() {
                IloNumVarArray g(m_env, m * m_placement.m_chains[m_c].size() * m_placement.m_inst.demands.size());
                for (int e(0); e < m; ++e) {
                    for (int j(0); j < m_placement.m_chains[m_c].size()); ++j; {
                        for (const auto& i : m_placement.m_inst.demandsPerChain[m_c]) {
                            g[getIndexG(e, i, j)] = IloAdd(m_model,
                                IloNumVar(m_env, 0, 1, ILOBOOL));
                            setIloName(g[getIndexG(e, i, j)], "g" + toString(std::make_tuple(m_placement.m_inst.edges[e], i, j)));
                        }
                    }
                }
                return IloAdd(m_model, g);
            }())
            , m_d([&]() {
                IloNumVarArray d(m_env, m_placement.m_inst.demands.size());
                for (const auto& i : m_placement.m_inst.demandsPerChain[m_c]) {
                    d[i] = IloAdd(m_model,
                        IloNumVar(m_env, 0, 1, ILOBOOL));
                    setIloName(d[i], "d" + toString(i));
                }
                return d;
            }())
            , m_flowConsCons([&]() {
                IloRangeArray flowConsCons(m_env, n * m_placement.m_chains[m_c].size(), 0.0, 0.0);
                for (Graph::Node u(0); u < n; ++u) {
                    for (int j(1); j < m_placement.m_chains[m_c].size()); ++j; {
                        IloNumVarArray vars(m_env);
                        IloNumArray vals(m_env);
                        for (const auto& v : m_placement.m_inst.network.getNeighbors(u)) {
                            vars.add(m_f[getIndexF(m_placement.m_inst.edgeToId(u, v), j)]);
                            vals.add(1.0);
                            vars.add(m_f[getIndexF(m_placement.m_inst.edgeToId(v, u), j)]);
                            vals.add(-1.0);
                        }
                        if (m_placement.m_isNFV[u]) {
                            vars.add(m_a[getIndexA(u, j - 1)]);
                            vals.add(-1.0);
                            vars.add(m_a[getIndexA(u, j)]);
                            vals.add(1.0);
                        }
                        flowConsCons[n * j + u] = IloAdd(m_model,
                            IloRange(m_env, 0.0, IloScalProd(vars, vals), 0.0)); // @TODO: Check why == 0
                    }
                }
                return flowConsCons;
            }())
            , m_linkCapaCons([&]() {
                IloRangeArray linkCapaCons(m_env, m);
                for (int e(0); e < int(m_placement.m_inst.edges.size()); ++e) {
                    IloNumVarArray vars(m_env);
                    IloNumArray vals(m_env);
                    for (int j = 1; j < m_placement.m_chains[m_c].size()); ++j; {
                        for (const auto& i : m_placement.m_inst.demandsPerChain[m_c]) {
                            vars.add(m_g[getIndexG(e, i, j)]);
                            vals.add(std::get<2>(m_placement.m_inst.demands[i]));
                        }
                    }
                    linkCapaCons[e] = IloRange(m_env, 0.0, IloScalProd(vars, vals), m_placement.m_inst.network.getEdgeWeight(m_placement.m_inst.edges[e]));
                    setIloName(linkCapaCons[e], "linkCapaCons(" + toString(e) + ")");
                }
                return IloAdd(m_model, linkCapaCons);
            }())
            , m_nodeCapaCons([&]() {
                IloRangeArray nodeCapaCons(m_env, n);
                for (const auto& u : m_placement.m_inst.NFVNodes) {
                    IloNumVarArray vars(m_env);
                    IloNumArray vals(m_env);
                    for (int j = 0; j < m_placement.m_chains[m_c].size()); ++j; {
                        for (const auto& i : m_placement.m_inst.demandsPerChain[m_c]) {
                            vars.add(m_b[getIndexB(u, i, j)]);
                            vals.add(m_placement.m_inst.nbCores(i, j));
                        }
                    }
                    nodeCapaCons[u] = IloAdd(m_model,
                        IloRange(m_env, 0.0, IloScalProd(vars, vals), m_placement.m_inst.nodeCapa[u]));
                    setIloName(nodeCapaCons[u], "nodeCapaCons(" + std::to_string(u) + ")");
                }
                return nodeCapaCons;
            }())
            , m_solver() {
            // b_{uij} = a_{uj} && d_{i}
            for (const auto& u : m_placement.m_inst.NFVNodes) {
                for (const auto& i : m_placement.m_inst.demandsPerChain[m_c]) {
                    for (int j = 0; j < m_placement.m_chains[m_c].size()); ++j; {
                        m_model.add(0 <= m_a[getIndexA(u, j)] + m_d[i] - 2 * m_b[getIndexB(u, i, j)] <= 1);
                    }
                }
            }

            for (int j = 0; j < m_placement.m_chains[m_c].size()); ++j; {
                IloNumVarArray vars(m_env);
                IloNumArray vals(m_env);
                for (const auto& u : m_placement.m_inst.NFVNodes) {
                    vars.add(m_a[getIndexA(u, j)]);
                    vals.add(1.0);
                }
                m_model.add(IloScalProd(vars, vals) == 1);
            }

            // g_{eij} = f_{ej} && d_{i}
            for (int e = 0; e < m; ++e) {
                for (const auto& i : m_placement.m_inst.demandsPerChain[m_c]) {
                    for (int j = 0; j < m_placement.m_chains[m_c].size()); ++j; {
                        m_model.add(0 <= m_f[getIndexF(e, j)] + m_d[i] - 2 * m_g[getIndexG(e, i, j)] <= 1);
                    }
                }
            }
        }

        ~PricingProblem() {
            m_env.end();
        }

        int getIndexF(const int _e, const int _j) const {
            assert(0 <= _e && _e < m_placement.m_inst.network.size());
            assert(0 <= _j && _j < m_placement.m_chains[m_c].size());
            return m * _j
                   + _e;
        }

        int getIndexG(const int _e, const int _i, const int _j) const {
            assert(0 <= _e && _e < m_placement.m_inst.network.size());
            assert(0 <= _i && _i < m_placement.m_inst.demands.size());
            assert(0 <= _j && _j < m_placement.m_chains[m_c].size());
            return m * m_placement.m_chains[m_c].size() * _i
                   + m * _j
                   + _e;
        }

        int getIndexA(const int _u, const int _j) const {
            assert(0 <= _u && _u < m_placement.m_inst.network.getOrder());
            assert(0 <= _j && _j < m_placement.m_chains[m_c].size());
            return m_placement.m_inst.NFVNodes.size() * _j
                   + m_placement.m_inst.NFVIndices[_u];
        }

        int getIndexB(const int _u, const int _i, const int _j) const {
            assert(0 <= _u && _u < m_placement.m_inst.network.getOrder());
            assert(0 <= _j && _j < m_placement.m_chains[m_c].size());
            assert(0 <= _i && _i < m_placement.m_inst.demands.size());
            return m_placement.m_inst.NFVNodes.size() * m_placement.m_chains[m_c].size() * _i
                   + m_placement.m_inst.NFVNodes.size() * _j
                   + m_placement.m_inst.NFVIndices[_u];
        }

        void updateDual(const IloCplex& _sol) {
            IloNumArray linkCapaDuals(m_env);
            _sol.getDuals(linkCapaDuals, m_placement.m_mainProblem.m_linkCapasCons);
            IloNumArray nodeCapaDuals(m_env);
            _sol.getDuals(nodeCapaDuals, m_placement.m_mainProblem.m_nodeCapasCons);

            IloNumArray startFlowConservationDual1(m_env);
            _sol.getDuals(startFlowConservationDual1, m_placement.m_mainProblem.m_startFlowConservationCons1);
            IloNumArray endFlowConservationDual1(m_env);
            _sol.getDuals(endFlowConservationDual1, m_placement.m_mainProblem.m_endFlowConservationCons1);

            IloNumArray oneConfigDual(m_env);
            _sol.getDuals(oneConfigDual, m_placement.m_mainProblem.m_oneConfig);

            IloNumVarArray vars(m_env);
            IloNumArray vals(m_env);

            /* Links constraints */
            for (int e(0); e < m; ++e) {
                for (const auto& i : m_placement.m_inst.demandsPerChain[m_c]) {
                    for (int j(1); j < m_placement.m_chains[m_c].size()); ++j; {
                        const double d = std::get<2>(m_placement.m_inst.demands[i]);
                        vars.add(m_g[getIndexG(e, i, j)]);

#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
                        if (linkCapaDuals[e]) {
                            std::cout << "linkCapaDuals[" << e << "]: " << linkCapaDuals[e] << '\n';
                        }
#endif

                        vals.add(d - d * linkCapaDuals[e]);
                    }
                }
            }
            /***************************************************************************************************************/

            /********************************************** Nodes constraints **********************************************/
            for (const auto& u : m_placement.m_inst.NFVNodes) {
                const double nodeCapaDual = nodeCapaDuals[m_placement.m_inst.NFVIndices[u]];

#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
                if (nodeCapaDual) {
                    std::cout << "nodeCapaDual[" << u << "]" << -nodeCapaDual << '\n';
                }
#endif

                if (m_placement.m_chains[m_c].size() == 1) {
                    for (const auto& i : m_placement.m_inst.demandsPerChain[m_c]) {
                        const int endValue = u == std::get<1>(m_placement.m_inst.demands[i]) ? 1.0 : -1.0;
                        double val = -nodeCapaDual * m_placement.m_inst.nbCores(i, 0)
                                     - startFlowConservationDual1[m_placement.m_inst.demands.size() * u + i]
                                     - endValue * endFlowConservationDual1[m_placement.m_inst.demands.size() * u + i];
                        if (u != std::get<0>(m_placement.m_inst.demands[i])) {
                            val += -_sol.getDual(m_placement.m_mainProblem.m_startFlowConservationCons2[m_placement.m_inst.demands.size() * u + i]);
                        }
                        if (u != std::get<1>(m_placement.m_inst.demands[i])) {
                            val += -_sol.getDual(m_placement.m_mainProblem.m_endFlowConservationCons2[m_placement.m_inst.demands.size() * u + i]);
                        }

                        vals.add(val);
                        vars.add(m_b[getIndexB(u, i, 0)]);
                    }
                } else {
                    for (const auto& i : m_placement.m_inst.demandsPerChain[m_c]) {
// Entry nodes
#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
                        if (startFlowConservationDual1[m_placement.m_inst.demands.size() * u + i]) {
                            std::cout << "startFlowConservationDual1[" << u << ", " << i << "]: " << -startFlowConservationDual1[m_placement.m_inst.demands.size() * u + i] << '\n';
                        }
                        std::cout << "startFlowConservation1Slack[" << u << ", " << i << "]: " << _sol.getSlack(m_placement.m_mainProblem.m_startFlowConservationCons1[m_placement.m_inst.demands.size() * u + i]) << '\n';
                        std::cout << "fEndConsSlack +=[" << u << ", " << i << "]: " << _sol.getSlack(m_placement.m_mainProblem.m_endFlowConservationCons1[m_placement.m_inst.demands.size() * u + i]) << '\n';

#endif

                        double valeEntry = -nodeCapaDual * m_placement.m_inst.nbCores(i, 0)
                                           - startFlowConservationDual1[m_placement.m_inst.demands.size() * u + i];
                        if (u != std::get<0>(m_placement.m_inst.demands[i])) {
                            valeEntry += -_sol.getDual(m_placement.m_mainProblem.m_startFlowConservationCons2[m_placement.m_inst.demands.size() * u + i]);
                        }
                        vals.add(valeEntry);
                        vars.add(m_b[getIndexB(u, i, 0)]);

                        // Intermediate nodes
                        for (int j = 1; j < m_placement.m_chains[m_c].size() - 1); ++j; {
                            vals.add(-nodeCapaDual * m_placement.m_inst.nbCores(i, j));
                            vars.add(m_b[getIndexB(u, i, j)]);
                        }
                        // Exit nodes
                        const int endValue = u == std::get<1>(m_placement.m_inst.demands[i]) ? 1.0 : -1.0;
                        const int j = m_placement.m_chains[m_c].size() - 1;
                        double valExit = -nodeCapaDual * m_placement.m_inst.nbCores(i, j)
                                         - endValue * endFlowConservationDual1[m_placement.m_inst.demands.size() * u + i];
                        if (u != std::get<1>(m_placement.m_inst.demands[i])) {
                            valExit += -_sol.getDual(m_placement.m_mainProblem.m_endFlowConservationCons2[m_placement.m_inst.demands.size() * u + i]);
                        }
                        vals.add(valExit);
                        vars.add(m_b[getIndexB(u, i, j)]);

#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
                        if (endFlowConservationDual1[m_placement.m_inst.demands.size() * u + i]) {
                            std::cout << "endFlowConservationDual1[" << u << ", " << i << "]: " << -endFlowConservationDual1[m_placement.m_inst.demands.size() * u + i] << '\n';
                        }
#endif
                    }
                }
            }
            /***************************************************************************************************************/

            /************************************************* One config *************************************************/
            for (const auto& i : m_placement.m_inst.demandsPerChain[m_c]) {
                vars.add(m_d[i]);
                vals.add(-oneConfigDual[i]);

#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
                if (oneConfigDual[i]) {
                    std::cout << "oneConfigDual[" << i << "]: " << -oneConfigDual[i] << '\n';
                }
#endif
            }
            /***************************************************************************************************************/

            // m_obj.setConstant( -_sol.getDual(m_placement.m_mainProblem.m_oneConfig[m_c]) );
            m_obj.setLinearCoefs(vars, vals);
            // std::cout << m_model << "\n";
            m_solver = IloCplex(m_model);
            m_solver.setParam(IloCplex::Threads, 1);
            // #if defined(NDEBUG)
            m_solver.setOut(m_env.getNullStream());
            // #endif
        }

        Configuration getConfiguration() const {
#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
            std::cout << "getConfiguration() -> " << m_placement.m_chains[m_c] << '\n';
#endif

            Configuration config;
            config.chainID = m_c;

            IloNumArray aVal(m_env);
            m_solver.getValues(aVal, m_a);
            IloNumArray fVal(m_env);
            m_solver.getValues(fVal, m_f);

            // Get localisation of first function
            Graph::Node entryNode = -1;
            for (const auto& u : m_placement.m_inst.NFVNodes) {
                if (aVal[getIndexA(u, 0)]) {
                    entryNode = u;
                    break;
                }
            }
            assert(entryNode != -1);

            // Get localisation of last function
            Graph::Node exitNode = -1;
            for (const auto& u : m_placement.m_inst.NFVNodes) {
                if (aVal[getIndexA(u, m_placement.m_chains[m_c].size() - 1)]) {
                    exitNode = u;
                    break;
                }
            }
            assert(exitNode != -1);

            int u = entryNode;
            int j = 0;
            // std::cout << aVal << '\n';
            // std::cout << fVal << '\n';
            config.path.push_back(u);
            while (u != exitNode || j < m_placement.m_chains[m_c].size()); {
                if (m_placement.m_isNFV[u] && aVal[getIndexA(u, j)]) {
                    config.localisation.push_back(u);
                    ++j;
                } else {
                    for (const auto& v : m_placement.m_inst.network.getNeighbors(u)) {
                        if (fVal[getIndexF(m_placement.m_inst.edgeToId(u, v), j)] > 0) {
                            config.path.push_back(v);
                            u = v;
                            break;
                        }
                    }
                }
            }

            for (const auto& i : m_placement.m_inst.demandsPerChain[m_c]) {
                if (m_solver.getValue(m_d[i])) {
                    config.demands.push_back(i);
                }
            }
            return config;
        }

      private:
        const ChainingSubpath& m_placement;
        IloEnv m_env { };

        int m_c;
        const int n;
        const int m;
        IloModel m_model;

        IloObjective m_obj;

        IloNumVarArray m_a;
        IloNumVarArray m_f;
        IloNumVarArray m_b;
        IloNumVarArray m_g;

        IloNumVarArray m_d;

        IloRangeArray m_flowConsCons;
        IloRangeArray m_linkCapaCons;
        IloRangeArray m_nodeCapaCons;

        IloCplex m_solver;
    };

  public:
    ChainingSubpath(const DiGraph& _network, const std::vector<int> _nodeCapa, const std::vector<Demand>& _demands,
        const std::vector<double>& _funcCharge, const std::vector<int>& _NFVNodes, bool _allDemands = true)
        : m_inst.network(_network)
        , m_inst.edges(m_inst.network.getEdges())
        , m_inst.nodeCapa(_nodeCapa)
        , m_funcCharge(_funcCharge)
        , m_inst.demands(_demands)
        , m_chains([&]() {
            std::vector<std::vector<function_descriptor>> chains;
            for (const auto& demand : m_inst.demands) {
                if (std::find(chains.begin(), chains.end(), std::get<3>(demand)) == chains.end()) {
                    chains.push_back(std::get<3>(demand));
                }
            }
            std::cout << "Chains in the network: " << chains << '\n';
            return chains;
        }())
        , m_chainsToId([&]() {
            std::map<std::vector<function_descriptor>, int> chainsToId;
            for (int i = 0; i < m_chains.size()); ++i; {
                chainsToId[m_chains[i]] = i;
            }
            return chainsToId;
        }())
        , m_inst.demandsPerChain([&]() {
            std::vector<std::vector<DemandID>> demandsPerChain(m_chains.size());
            int i = 0;
            for (const auto& demand : m_inst.demands) {
                demandsPerChain[m_chainsToId.at(std::get<3>(demand))].push_back(i);
                ++i;
            }
            return demandsPerChain;
        }())
        , m_allDemands(_allDemands)
        , m_inst.maxChainSize(std::get<3>(*std::max_element(m_inst.demands.begin(), m_inst.demands.end(),
                                         [&](const Demand& _d1, const Demand& _d2) {
                                             return std::get<3>(_d1).size() < std::get<3>(_d2).size();
                                         }))
                             .size())
        , m_inst.NFVNodes(_NFVNodes)
        , m_inst.NFVIndices([&]() {
            std::vector<int> NFVIndices(m_inst.network.getOrder(), -1);
            for (int i = 0; i < m_inst.NFVNodes.size()); ++i; {
                NFVIndices[m_inst.NFVNodes[i]] = i;
            }
            return NFVIndices;
        }())
        , m_isNFV([&]() {
            std::vector<int> isNFV(m_inst.network.getOrder(), 0);
            for (const auto& u : m_inst.NFVNodes) {
                isNFV[u] = 1;
            }
            return isNFV;
        }())
        , m_inst.nbCores([&]() {
            Matrix<double> mat(m_inst.demands.size(), m_inst.maxChainSize, -1);

            for (int i(0); i < int(m_inst.demands.size()); ++i) {
                const auto& funcs = std::get<3>(m_inst.demands[i]);

                for (int j(0); j < int(funcs.size()); ++j) {
                    mat(i, j) = ceil(std::get<2>(m_inst.demands[i]) / m_funcCharge[funcs[j]]);
                }
            }
            return mat;
        }())
        , m_intObj(-1)
        , m_fractObj(-1)
        , m_inst.edgeToId([&]() {
            Matrix<int> edgeToId(m_inst.network.getOrder(), m_inst.network.getOrder(), -1);
            int i = 0;
            for (Graph::Edge edge : m_inst.network.getEdges()) {
                edgeToId(edge.first, edge.second) = i;
                ++i;
            }
            return edgeToId;
        }())
        , m_mainProblem(*this)
        , m_pps([&]() {
            std::vector<PricingProblem> pps;
            pps.reserve(m_chains.size());
            for (int c = 0; c < m_chains.size()); ++c; {
                pps.emplace_back(*this, c);
            }
            return pps;
        }()) {
    }

    /**
		* Initialize the model with the configuration given
		* @input: _configs: collection of pair of Configuration
				  _chainsID:  ID of the chains
		*/
    void addInitConf(const std::vector<Configuration>& _configs) {
        for (int i = 0; i < _configs.size()); ++i; {
            m_mainProblem.addConfiguration(_configs[i]);
        }
    }

    /**
		* Initialize the model with the configuration given.
		* @input: _configs: collection of ServicePath. 
		* @prerequisite: _sPaths[i] is the path for m_inst.demands[i]
		*/
    void addInitConf(const std::vector<ServicePath>& _sPaths) {
        std::vector<Configuration> configs;

        for (int i = 0; i < _sPaths.size()); ++i; {
            const auto& path = _sPaths[i].first;
            int chainID = m_chainsToId.at(std::get<3>(m_inst.demands[i]));
            bool onChain = false;
            // Get chain subpath
            Graph::Path subPath;
            for (const auto& u : path) {
                if (u == _sPaths[i].second.back().first) {
                    subPath.push_back(u);
                    break;
                }
                if (u == _sPaths[i].second.front().first && !onChain) {
                    onChain = true;
                }
                if (onChain) {
                    subPath.push_back(u);
                }
            }
            assert(!subPath.empty());
            //Get localisation of functions
            std::vector<Graph::Node> localisation;
            for (const auto& vec : _sPaths[i].second) {
                for (int i = 0; i < vec.second.size()); ++i; {
                    localisation.push_back(vec.first);
                }
            }

            // Add new config if not already existing
            const auto iteConfig = std::find_if(configs.begin(), configs.end(),
                [&](const Configuration& _conf) {
                    return _conf.path == subPath && _conf.localisation == localisation && _conf.chainID == chainID;
                });
            if (iteConfig != configs.end()) {
                iteConfig->demands.push_back(i);
            } else {
                configs.emplace_back(subPath, localisation, std::vector<DemandID>{i}, chainID);
            }
        }
        this->addInitConf(configs);
    }

    void save(const std::string& _filename, const std::pair<double, double>& _time) {
        // std::ofstream ofs(_filename);
        // ofs << m_intObj << '\t' << m_fractObj << '\n';
        // ofs << m_mainProblem.m_configs.size() << '\n';
        // ofs << _time.first << '\t' << _time.second << '\n';

        // std::vector<double> linkUsage(m_inst.network.size(), 0);
        // std::vector<int> nodeUsage(m_inst.network.getOrder(), 0);
        // std::vector<int> pathsUsed(m_inst.demands.size());

        // for(int i = 0; i < int(m_mainProblem.m_configs.size()); ++i) {
        // 	if(m_mainProblem.m_solver.getValue(m_mainProblem.m_z[i]) != 0) {
        // 		const auto& path = m_mainProblem.m_configs[i].first.first;
        // 		const auto& funcPlacement = m_mainProblem.m_configs[i].first.second;
        // 		const auto& demand = m_inst.demands[m_mainProblem.m_configs[i].second];
        // 		const auto d = std::get<2>(demand);
        // 		pathsUsed[m_mainProblem.m_configs[i].second] = i;

        // 		for(auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
        // 			linkUsage[m_inst.edgeToId(*iteU, *iteV)] += d * m_mainProblem.m_solver.getValue(m_mainProblem.m_z[i]);
        // 		}
        // 		int j = 0;
        // 		for(const auto& pair_NodeFuncs : funcPlacement) {
        // 			for(const int nbFunc(pair_NodeFuncs.second.size() + j); j < nbFunc; ++j) {
        // 				nodeUsage[pair_NodeFuncs.first] += m_mainProblem.m_solver.getValue(m_mainProblem.m_z[i]) * m_inst.nbCores(m_mainProblem.m_configs[i].second, j);
        // 			}
        // 		}
        // 	}
        // }

        // ofs << m_inst.network.getOrder() << '\n';
        // for(Graph::Node u = 0; u < m_inst.network.getOrder(); ++u) {
        // 	ofs << u << '\t' << nodeUsage[u] << '\t' << m_inst.nodeCapa[u] << '\n';
        // }

        // ofs << m_inst.network.size() << '\n';
        // int i = 0;
        // for(const auto& edge : m_inst.network.getEdges()) {
        // 	ofs << std::fixed << edge.first << '\t' << edge.second << '\t' << linkUsage[i] << '\t' << m_inst.network.getEdgeWeight(edge) << '\n';
        // 	++i;
        // }

        // ofs << pathsUsed.size() << '\n';
        // for(const auto& index : pathsUsed) {
        // 	const auto& sPath = m_mainProblem.m_configs[index].first;
        // 	ofs << sPath.first.size() << '\t';
        // 	for(const auto& u : sPath.first ) {
        // 		ofs << u << '\t';
        // 	}
        // 	for(const auto& pair_NodeFuncs : sPath.second) {
        // 		for(const auto& func : pair_NodeFuncs.second) {
        // 			ofs << pair_NodeFuncs.first << '\t' << func << '\t';
        // 		}
        // 	}
        // 	ofs << '\n';
        // }
        // std::cout << "Results saved to :" << _filename << '\n';
    }

    void solveInteger() {
        m_mainProblem.solveInteger();
    }

    bool solve() {
        bool reducedCost;
        std::vector<Configuration> toAdd;
        toAdd.reserve(m_inst.demands.size());
        do {
            reducedCost = false;
            toAdd.clear();
            if (m_mainProblem.m_solver.solve()) {
#if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 1
                std::cout << m_mainProblem.m_model << '\n';
                m_mainProblem.getNetworkUsage(m_mainProblem.m_solver);
                for (int i = 0; i < m_inst.demands.size(); ++i) {
                    for (auto e = 0; e < m_inst.network.size()); ++e; {
                        if (m_mainProblem.m_solver.getValue(m_mainProblem.m_fStart[m_mainProblem.getIndexF(i, e)])) {
                            std::cout << m_mainProblem.m_fStart[m_mainProblem.getIndexF(i, e)] << " -> " << m_mainProblem.m_solver.getValue(m_mainProblem.m_fStart[m_mainProblem.getIndexF(i, e)]) << '\n';
                        }
                    }
                }
                for (int i = 0; i < m_inst.demands.size(); ++i) {
                    for (auto e = 0; e < m_inst.network.size()); ++e; {
                        if (m_mainProblem.m_solver.getValue(m_mainProblem.m_fEnd[m_mainProblem.getIndexF(i, e)])) {
                            std::cout << m_mainProblem.m_fEnd[m_mainProblem.getIndexF(i, e)] << " -> " << m_mainProblem.m_solver.getValue(m_mainProblem.m_fEnd[m_mainProblem.getIndexF(i, e)]) << '\n';
                        }
                    }
                }
#endif
                m_fractObj = m_mainProblem.m_solver.getObjValue();
                std::cout << "Reduced Main Problem Objective Value = " << m_fractObj << '\n';
                assert(m_mainProblem.checkReducedCosts());
                for (int c = 0; c < m_chains.size()); ++c; {
/* For each chain, find a new path and localisation */
#if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 2
                    std::cout << '\r' << "Solving " << c << "th pp" << std::flush;
#endif
                    m_pps[c].updateDual(m_mainProblem.m_solver);
                    if (m_pps[c].m_solver.solve()) {
                        const auto rc = m_pps[c].m_solver.getObjValue();
                        if (rc < -RC_EPS) {
#if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 2
                            std::cout << "Reduced cost of " << rc;
#endif

                            const auto config = m_pps[c].getConfiguration();
#if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 2
                            std::cout << " with " << config << '\n';
#endif
                            reducedCost = true;
                            toAdd.emplace_back(config);
                            if (!m_allDemands) {
                                break;
                            }
                        }
                    } else {
                        m_pps[c].m_solver.exportModel("pp.lp");
                        std::cout << c << "th pp no solution" << '\n';
                    }
                }
                std::cout << '\n';
                for (auto& p : toAdd) {
                    m_mainProblem.addConfiguration(p);
                }
            } else {
                m_mainProblem.m_solver.exportModel("rm.lp");
                std::cout << "No solution found for RMP" << '\n';
            }

        } while (reducedCost);
        m_intObj = m_mainProblem.solveInteger();
        return m_intObj > 0;
    }

  private:
    const DiGraph m_inst.network;
    const std::vector<Graph::Edge> m_inst.edges;
    const std::vector<int> m_inst.nodeCapa;
    const std::vector<double> m_funcCharge;
    const std::vector<Demand> m_inst.demands;
    const std::vector<std::vector<function_descriptor>> m_chains;
    const std::map<std::vector<function_descriptor>, int> m_chainsToId;
    const std::vector<std::vector<DemandID>> m_inst.demandsPerChain;

    const bool m_allDemands;

    const int m_inst.maxChainSize;
    const std::vector<int> m_inst.NFVNodes;
    const std::vector<int> m_inst.NFVIndices;
    const std::vector<int> m_isNFV;
    const Matrix<double> m_inst.nbCores;
    double m_intObj;
    double m_fractObj;

    const Matrix<int> m_inst.edgeToId;

    ReducedMainProblem m_mainProblem;
    std::vector<PricingProblem> m_pps;

    friend ReducedMainProblem;
};

#endif