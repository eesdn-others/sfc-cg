// #ifndef ENERGY_PARTITION_HPP
// #define ENERGY_PARTITION_HPP

// #include <cmath>
// #include <ilcplex/ilocplex.h>
// #include <iostream>
// #include <map>
// #include <set>
// #include <tuple>

// #include "DiGraph.hpp"
// #include "ShortestPath.hpp"
// #include "utility.hpp"

// #define RC_EPS 1.0e-6
// #define ALL_EDGES 0

// template <typename Ilo>
// void setIloName(const Ilo& _ilo, const std::string& _str) {
//     _ilo.setName(_str.c_str());
// }

// typedef int function_descriptor;
// typedef std::tuple<int, int, double> Demand;
// typedef int Node;

// class EnergyPartition {
//     class PlacementModel {
//         friend EnergyPartition;

//       private:
//         EnergyPartition& m_placement;
//         IloModel m_model;

//         IloNumVarArray m_x;
//         IloNumVarArray m_y;

//         IloObjective m_obj;

//         IloRangeArray m_flowConservation;
//         IloRangeArray m_oneSetPerEdge;
//         IloRangeArray m_activeLink1;
//         IloRangeArray m_activeLink2;
//         // IloRangeArray m_bothLinks;

//         IloCplex m_solver;
//         std::vector<std::pair<std::vector<int>, Graph::Edge>> m_sets;
//         std::vector<int> m_arcToEdge;

//       public:
//         PlacementModel(EnergyPartition& _placement)
//             : m_placement(_placement)
//             , m_model(m_placement.m_env)
//             , m_x(IloNumVarArray(m_placement.m_env, m_placement.m_inst.network.size(), -IloInfinity, IloInfinity))
//             , m_y(IloNumVarArray(m_placement.m_env))
//             , m_obj(IloAdd(m_model, IloMinimize(m_placement.m_env)))
//             , m_flowConservation(IloAdd(m_model, IloRangeArray(m_placement.m_env, m_placement.m_inst.demands.size() * m_placement.m_inst.network.getOrder(), 0.0, 0.0)))
//             , m_oneSetPerEdge(IloAdd(m_model, IloRangeArray(m_placement.m_env, m_placement.m_inst.network.size(), 1.0, 1.0)))
//             , m_activeLink1(IloAdd(m_model, IloRangeArray(m_placement.m_env, m_placement.m_inst.network.size(), 0.0, IloInfinity)))
//             , m_activeLink2(IloAdd(m_model, IloRangeArray(m_placement.m_env, m_placement.m_inst.network.size(), 0.0, IloInfinity)))
//             ,
//             // m_oneEdge( IloAdd(m_model, IloRangeArray(m_placement.m_env, m_placement.m_inst.network.size(), 0.0, 0.0)) ),
//             m_solver()
//             , m_sets()
//             , m_arcToEdge(m_placement.m_inst.network.size()) {
//             int e = 0;
//             for (Graph::Edge arc : m_placement.m_inst.network.getEdges()) {
//                 if (arc.first < arc.second) {
//                     m_arcToEdge[e] = e;
//                 } else {
//                     m_arcToEdge[e] = m_placement.m_arcToId[Graph::Edge(arc.second, arc.first)];
//                 }
//                 ++e;
//             }

//             e = 0;
//             for (Graph::Edge arc : m_placement.m_inst.network.getEdges()) {
//                 if (arc.first < arc.second) {
//                     m_obj.setLinearCoef(m_x[m_arcToEdge[e]], 1.0);
//                     setIloName(m_activeLink1[m_arcToEdge[e]], "m_activeLink1" + toString(arc));
//                     setIloName(m_activeLink2[m_arcToEdge[e]], "m_activeLink2" + toString(arc));
//                     setIloName(m_x[m_arcToEdge[e]], "x" + toString(arc));
//                     m_activeLink1[m_arcToEdge[e]].setLinearCoef(m_x[m_arcToEdge[e]], 1.0);
//                     m_activeLink1[m_arcToEdge[e]].setLB(1.0);
//                     m_activeLink2[m_arcToEdge[e]].setLinearCoef(m_x[m_arcToEdge[e]], 1.0);
//                     m_activeLink2[m_arcToEdge[e]].setLB(1.0);
//                 }
//                 setIloName(m_oneSetPerEdge[e], "m_oneSetPerEdge" + toString(arc));

//                 IloNumColumn col = m_oneSetPerEdge[e](1.0);
//                 if (arc.first < arc.second) {
//                     col += m_activeLink1[m_arcToEdge[e]](1.0);
//                 } else {
//                     col += m_activeLink2[m_arcToEdge[e]](1.0);
//                 }
//                 IloNumVar y0 = IloNumVar(col);
//                 setIloName(y0, "y0" + toString(arc));
//                 m_y.add(y0);
//                 m_sets.emplace_back();

//                 // m_bothLinks[e].setLinearCoef(m_x[e], 1.0);
//                 // Graph::Edge brother = Graph::Edge(arc.second, arc.first); // The other link between the two nodes
//                 // m_bothLinks[e].setLinearCoef(m_x[m_placement.m_arcToId[brother]], -1.0);
//                 ++e;
//             }

//             for (int i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                 for (Node u = 0; u < m_placement.m_inst.network.getOrder(); ++u) {
//                     int s = std::get<0>(m_placement.m_inst.demands[i]),
//                         t = std::get<1>(m_placement.m_inst.demands[i]);

//                     int value = u == s ? 1 : (u == t ? -1 : 0);
//                     int j = m_placement.m_inst.network.getOrder() * i + u;
//                     m_flowConservation[j].setUB(value);
//                     m_flowConservation[j].setLB(value);
//                     m_flowConservation[j].setName(std::string("flowConservation" + toString(std::make_pair(u, i))).c_str());
//                 }
//             }
//             m_solver = IloCplex(m_model);
//             m_solver.setOut(m_placement.m_env.getNullStream());
//         }

//         PlacementModel& operator=(const PlacementModel&) = delete;
//         PlacementModel(const PlacementModel&) = delete;

//         void addSetCheck(const std::vector<int>& _set, const Graph::Edge& _edge) {
//             if (std::find(m_sets.begin(), m_sets.end(), std::make_pair(_set, _edge)) == m_sets.end()) {
//                 addSet(_set, _edge);
//             }
//         }

//         void addSet(const std::vector<int>& _set, const Graph::Edge& _edge) {
// #ifndef NDEBUG
//             std::cout << "addSet" << _set << " on " << _edge << std::endl;
//             assert([&]() {
//                 auto ite = std::find(m_sets.begin(), m_sets.end(), std::make_pair(_set, _edge));
//                 if (ite == m_sets.end()) {
//                     return true;
//                 } else {
//                     std::cout << _set << " is already present!" << std::endl;
//                     return false;
//                 }
//             }());
// #endif

//             IloNumColumn col(m_oneSetPerEdge[m_placement.m_arcToId[_edge]](1.0));
//             for (int i : _set) {
//                 col += m_flowConservation[m_placement.m_inst.network.getOrder() * i + _edge.first](1.0);
//                 col += m_flowConservation[m_placement.m_inst.network.getOrder() * i + _edge.second](-1.0);
//             }
//             // col += m_activeLink[m_placement.m_arcToId[_edge]](-1.0);

//             IloNumVar yVar = IloNumVar(col);
//             yVar.setName(std::string("y" + toString(std::make_pair(_set, _edge))).c_str());
//             m_y.add(yVar);
//             m_sets.emplace_back(_set, _edge);
//         }

//         void solveInteger() {
//             m_model.add(IloConversion(m_placement.m_env, m_x, ILOBOOL));
//             m_model.add(IloConversion(m_placement.m_env, m_y, ILOBOOL));
//             // std::cout << m_model << std::endl;
//             m_solver.exportModel("lp.lp");
//             if (m_solver.solve()) {
//                 std::cout << "Final obj value: " << m_solver.getObjValue() << "\t# Paths: " << m_sets.size() << std::endl;
//                 std::cout << 100 * (1 - m_solver.getObjValue() / double(m_placement.m_inst.network.size())) << std::endl;
//                 std::cout << "Active links: " << std::endl;

//                 for (Graph::Edge arc : m_placement.m_inst.network.getEdges()) {
//                     if (arc.first < arc.second) {
//                         if (m_solver.getValue(m_x[m_placement.m_arcToId[arc]])) {
//                             std::cout << '\t' << arc << std::endl;
//                         }
//                     }
//                 }
//                 std::cout << "Active sets:" << std::endl;
//                 for (int i = 0; i < m_y.getSize(); ++i) {
//                     if (m_solver.getValue(m_y[i])) {
//                         std::cout << '\t' << m_sets[i] << std::endl;
//                     }
//                 }

//             } else {
//                 std::cout << "No integer solution found!" << std::endl;
//             }
//         }
//     };

//     class PricingProblem {
//         friend EnergyPartition;

//       private:
//         EnergyPartition& m_placement;
//         Graph::Edge m_arc;
//         int n;
//         int m;
//         IloModel m_model;

//         IloObjective m_obj;

//         IloBoolVarArray m_f;
//         IloBoolVarArray m_x;

// #if ALL_EDGES
//         IloRangeArray m_capaCons;
// #else
//         IloRange m_capaCons;
// #endif
//         // IloRangeArray m_activeLink;

//       public:
//         PricingProblem(const Graph::Edge& _arc, EnergyPartition& _chainPlacement)
//             : m_placement(_chainPlacement)
//             , m_arc(_arc)
//             , n(m_placement.m_inst.network.getOrder())
//             , m(m_placement.m_inst.network.size())
//             , m_model(m_placement.m_env)
//             , m_obj(IloAdd(m_model, IloMinimize(m_placement.m_env)))
//             , m_f(IloAdd(m_model, IloBoolVarArray(m_placement.m_env, m_placement.m_inst.network.size() * m_placement.m_inst.demands.size())))
//             , m_x(IloAdd(m_model, IloBoolVarArray(m_placement.m_env, m_placement.m_inst.network.size())))
//             ,
// #if ALL_EDGES
//             m_capaCons(IloAdd(m_model, IloRangeArray(m_placement.m_env, m_placement.m_inst.network.size(), 0.0, IloInfinity)))
// #else
//             m_capaCons(IloAdd(m_model, IloRange(m_placement.m_env, 0.0, 0.0)))
// #endif
//         // m_activeLink( IloAdd(m_model, IloRangeArray(m_placement.m_env, m_placement.m_inst.network.getOrder() * m_placement.m_inst.demands.size(), 0.0, 0.0)) )
//         {
//             auto listEdges = m_placement.m_inst.network.getEdges();
//             std::vector<Graph::Edge> edges{listEdges.begin(), listEdges.end()};

// #if ALL_EDGES
//             int e = 0;
//             for (auto& arc : m_placement.m_inst.network.getEdges()) {
//                 // m_capaCons[e].setUB(m_placement.m_inst.network.getEdgeWeight(arc));
//                 m_capaCons[e].setLinearCoef(m_x[e], m_placement.m_inst.network.getEdgeWeight(arc));
//                 setIloName(m_x[e], "x" + toString(arc));

//                 for (int i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                     m_capaCons[e].setLinearCoef(m_f[m * i + e], -std::get<2>(m_placement.m_inst.demands[i]));
//                     setIloName(m_f[m * i + e], "f" + toString(std::make_tuple(i, arc)));
//                 }
//                 ++e;
//             }
// // m_model.add(IloSum(m_x) == 1);
// #else
//             m_capaCons.setUB(m_placement.m_inst.network.getEdgeWeight(m_arc));
//             for (int i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                 m_capaCons.setLinearCoef(m_f[i], std::get<2>(m_placement.m_inst.demands[i]));
//                 setIloName(m_f[i], "f" + toString(std::make_tuple(i)));
//             }
// #endif
//         }

// #if !ALL_EDGES
//         void updateArc(const Graph::Edge& _arc) {
//             m_arc = _arc;
//             m_capaCons.setUB(m_placement.m_inst.network.getEdgeWeight(m_arc));
//         }
// #endif

// #if ALL_EDGES
//         void updateDual(const IloCplex& _sol) {
//             int e = 0;
//             for (auto& arc : m_placement.m_inst.network.getEdges()) {
//                 m_obj.setLinearCoef(m_x[e], -_sol.getDual(m_placement.m_placementModel->m_oneSetPerEdge[e])
//                     // + _sol.getDual( m_placement.m_placementModel->m_activeLink[e])
//                     );

//                 for (int i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                     // std::cout << "Demand" << m_placement.m_inst.demands[i] << std::endl;
//                     m_obj.setLinearCoef(m_f[m * i + e],
//                         -_sol.getDual(m_placement.m_placementModel->m_flowConservation[n * i + arc.first])
//                             + _sol.getDual(m_placement.m_placementModel->m_flowConservation[n * i + arc.second]));
// #ifndef NDEBUG
//                     std::cout << '(' << _sol.getDual(m_placement.m_placementModel->m_flowConservation[n * i + arc.first])
//                               << ", " << _sol.getDual(m_placement.m_placementModel->m_flowConservation[n * i + arc.second]) << ") * f"
//                               << std::make_tuple(i, arc) << std::endl;
// #endif
//                     // std::cout << arc << std::endl;
//                     // if(_sol.getDual( m_placement.m_placementModel->m_flowConservation[n * i + arc.first] ) != 0) {
//                     // 	std::cout << "m_flowConservation" << std::make_tuple(i, arc.first) << " -> "
//                     // 		<< - _sol.getDual( m_placement.m_placementModel->m_flowConservation[n * i + arc.first] ) << std::endl;
//                     // }
//                     // if(_sol.getDual( m_placement.m_placementModel->m_flowConservation[n * i + arc.second] ) != 0) {
//                     // 	std::cout << "m_flowConservation" << std::make_tuple(i, arc.second) << " -> "
//                     // 		<< _sol.getDual( m_placement.m_placementModel->m_flowConservation[n * i + arc.second] ) << std::endl;
//                     // }
//                     // if(_sol.getDual( m_placement.m_placementModel->m_oneSetPerEdge[e] ) != 0) {
//                     // 	std::cout << "m_oneSetPerEdge" << arc << " -> "
//                     // 		<< _sol.getDual( m_placement.m_placementModel->m_oneSetPerEdge[e] )<< std::endl;
//                     // }
//                     // if(_sol.getDual( m_placement.m_placementModel->m_activeLink[e] ) != 0) {
//                     // 	std::cout << "m_activeLink" << arc << " -> "
//                     // 		<< _sol.getDual( m_placement.m_placementModel->m_activeLink[e] ) << std::endl;
//                     // }
//                     // std::cout << std::endl;
//                 }
//                 // std::cout << m_obj << std::endl;
//                 ++e;
//             }
// #ifndef NDEBUG
//             std::cout << m_obj << std::endl;
// #endif
//         }
// #else
//         void updateDual(const IloCplex& _sol) {
//             m_obj.setConstant(-_sol.getDual(m_placement.m_placementModel->m_oneSetPerEdge[m_placement.m_arcToId[m_arc]])
//                 // + _sol.getDual( m_placement.m_placementModel->m_activeLink[e])
//                 );

//             for (int i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                 // std::cout << "Demand" << m_placement.m_inst.demands[i] << std::endl;
//                 m_obj.setLinearCoef(m_f[i],
//                     -_sol.getDual(m_placement.m_placementModel->m_flowConservation[n * i + m_arc.first])
//                         + _sol.getDual(m_placement.m_placementModel->m_flowConservation[n * i + m_arc.second]));

// #ifndef NDEBUG
//                 std::cout << '(' << _sol.getDual(m_placement.m_placementModel->m_flowConservation[n * i + m_arc.first])
//                           << ", " << _sol.getDual(m_placement.m_placementModel->m_flowConservation[n * i + m_arc.second]) << ") * f"
//                           << std::make_tuple(i, m_arc) << std::endl;
// #endif
//                 // std::cout << arc << std::endl;
//                 // if(_sol.getDual( m_placement.m_placementModel->m_flowConservation[n * i + arc.first] ) != 0) {
//                 // 	std::cout << "m_flowConservation" << std::make_tuple(i, arc.first) << " -> "
//                 // 		<< - _sol.getDual( m_placement.m_placementModel->m_flowConservation[n * i + arc.first] ) << std::endl;
//                 // }
//                 // if(_sol.getDual( m_placement.m_placementModel->m_flowConservation[n * i + arc.second] ) != 0) {
//                 // 	std::cout << "m_flowConservation" << std::make_tuple(i, arc.second) << " -> "
//                 // 		<< _sol.getDual( m_placement.m_placementModel->m_flowConservation[n * i + arc.second] ) << std::endl;
//                 // }
//                 // if(_sol.getDual( m_placement.m_placementModel->m_oneSetPerEdge[e] ) != 0) {
//                 // 	std::cout << "m_oneSetPerEdge" << arc << " -> "
//                 // 		<< _sol.getDual( m_placement.m_placementModel->m_oneSetPerEdge[e] )<< std::endl;
//                 // }
//                 // if(_sol.getDual( m_placement.m_placementModel->m_activeLink[e] ) != 0) {
//                 // 	std::cout << "m_activeLink" << arc << " -> "
//                 // 		<< _sol.getDual( m_placement.m_placementModel->m_activeLink[e] ) << std::endl;
//                 // }
//                 // std::cout << std::endl;
//             }
// // std::cout << m_obj << std::endl;
// #ifndef NDEBUG
//             std::cout << m_obj << std::endl;
// #endif
//         }
// #endif

// #if ALL_EDGES
//         std::list<std::pair<std::vector<int>, Graph::Edge>> getSets(const IloCplex& _sol) const {
//             std::list<std::pair<std::vector<int>, Graph::Edge>> sets;
//             int e = 0;
//             for (auto& arc : m_placement.m_inst.network.getEdges()) {
//                 std::vector<int> set;
//                 for (int i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                     if (_sol.getValue(m_f[m * i + e])) {
//                         set.push_back(i);
//                     }
//                 }
//                 if (!set.empty()) {
//                     sets.emplace_back(set, arc);
//                 }
//                 ++e;
//             }
//             return sets;
//         }
// #else
//         std::vector<int> getSet(const IloCplex& _sol) const {
//             std::vector<int> set;
//             for (int i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                 if (_sol.getValue(m_f[i])) {
//                     set.push_back(i);
//                 }
//             }
//             return set;
//         }
// #endif
//     };

//   public:
//     EnergyPartition(const DiGraph& _network, const std::vector<Demand>& _demands)
//         : m_env()
//         , m_inst.network(_network)
//         , m_inst.demands(_demands)
//         , m_arcToId()
//         , m_placementModel(nullptr)
//         , m_pp(nullptr) {
//         int i = 0;
//         for (auto& arc : m_inst.network.getEdges()) {
//             m_arcToId[arc] = i;
//             i++;
//         }

//         m_placementModel = new PlacementModel(*this);
//         m_pp = new PricingProblem(Graph::Edge(0, 0), *this);

//         DiGraph tempFlowGraph = m_inst.network; // Graph of flow installed
//         for (auto& arc : tempFlowGraph.getEdges()) {
//             tempFlowGraph.setEdgeWeight(arc, 0);
//         }

//         std::vector<std::vector<int>> sets(m_inst.network.size());

// #if 1
//         std::vector<Graph::Path> paths;
//         paths.reserve(m_inst.demands.size());

//         for (int i = 0; i < int(m_inst.demands.size()); ++i) {
//             auto path = ShortestPath<DiGraph>(&m_inst.network).getShortestPath(std::get<0>(m_inst.demands[i]), std::get<1>(m_inst.demands[i]), [&](const int& __u, const int& __v) { return std::get<2>(m_inst.demands[i]) + tempFlowGraph.getEdgeWeight(__u, __v) < m_inst.network.getEdgeWeight(__u, __v); }, [&](const int& __u, const int& __v) { return 1 + std::get<2>(m_inst.demands[i]) + tempFlowGraph.getEdgeWeight(__u, __v) / m_inst.network.getEdgeWeight(__u, __v); });
//             for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
//                 tempFlowGraph.setEdgeWeight(*iteU, *iteV, tempFlowGraph.getEdgeWeight(*iteU, *iteV) + std::get<2>(m_inst.demands[i]));
//             }
//             paths.push_back(path);
//         }
//         for (int i = 0; i < int(m_inst.demands.size()); ++i) {
//             auto& path = paths[i];
//             for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
//                 sets[m_arcToId[Graph::Edge(*iteU, *iteV)]].push_back(i);
//             }
//         }

//         i = 0;
//         std::cout << "First sets: " << std::endl;
//         for (auto& arc : m_inst.network.getEdges()) {
//             if (!sets[i].empty()) {
//                 std::cout << arc << " -> " << sets[i] << std::endl;
//                 m_placementModel->addSet(sets[i], arc);
//             }
//             ++i;
//         }
// #else
//         /* Bidouille test */
//         sets = std::vector<std::vector<int>>(m_inst.network.size());
//         std::vector<Graph::Path> paths = {{0, 1}, {0, 5, 4, 3, 2}};
//         for (int i = 0; i < int(m_inst.demands.size()); ++i) {
//             auto& path = paths[i];
//             for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
//                 sets[m_arcToId[Graph::Edge(*iteU, *iteV)]].push_back(i);
//             }
//         }

//         i = 0;
//         std::cout << "Second sets: " << std::endl;
//         for (auto& arc : m_inst.network.getEdges()) {
//             if (!sets[i].empty()) {
//                 std::cout << arc << " -> " << sets[i] << std::endl;
//                 m_placementModel->addSetCheck(sets[i], arc);
//             }
//             ++i;
//         }
// #endif

// #ifndef NDEBUG
//         std::cout << m_placementModel->m_model << std::endl;
//         m_placementModel->m_solver.exportModel("lp.lp");
// #endif
//         // std::cin.ignore();
//     }

//     ~EnergyPartition() {
//         delete m_placementModel;
//         delete m_pp;
//     }

//     EnergyPartition(const EnergyPartition&) = delete;
//     EnergyPartition& operator=(const EnergyPartition&) = delete;

//     void solve() {
//         m_placementModel->m_solver.setOut(m_env.getNullStream());
//         std::list<std::pair<std::vector<int>, Graph::Edge>> setsToAdd;
//         do {
//             m_placementModel->m_solver.solve();
//             //std::cout << "########################################################################" << std::endl;
//             //std::cout << "########################################################################" << std::endl;
//             //std::cout << "########################################################################" << std::endl;
//             //std::cout << "########################################################################" << std::endl;

//             std::cout << "Reduced Main Problem Objective Value = " << m_placementModel->m_solver.getObjValue() << std::endl;
// #ifndef NDEBUG
//             std::cout << "Active links: " << std::endl;

//             for (Graph::Edge arc : m_inst.network.getEdges()) {
//                 if (arc.first < arc.second) {
//                     if (m_placementModel->m_solver.getValue(m_placementModel->m_x[m_arcToId[arc]])) {
//                         std::cout << '\t' << arc << std::endl;
//                     }
//                 }
//             }

//             std::cout << "Active sets" << std::endl;
//             for (int i = 0; i < m_placementModel->m_y.getSize(); ++i) {
//                 if (m_placementModel->m_solver.getValue(m_placementModel->m_y[i])) {
//                     std::cout << '\t' << m_placementModel->m_sets[i] << std::endl;
//                 }
//             }
// #endif
//             // std::cin.ignore();

//             setsToAdd.clear();
//             searchSets(setsToAdd);
//             for (auto& p : setsToAdd) {
//                 m_placementModel->addSet(p.first, p.second);
//             }
//         } while (!setsToAdd.empty());

//         m_placementModel->solveInteger();
//     }

//     void searchSets(std::list<std::pair<std::vector<int>, Graph::Edge>>& _sets, double _threshold = -RC_EPS) {
// /* For each demands, search for a shortest path in the augmented graph */
// #if !ALL_EDGES
//         for (auto& arc : m_inst.network.getEdges()) {
//             m_pp->updateArc(arc);
// #endif
//             m_pp->updateDual(m_placementModel->m_solver);

//             IloCplex ppSolver(m_pp->m_model);
//             // ppSolver.exportModel("pp.sav");
//             ppSolver.setOut(m_env.getNullStream());
//             // ppSolver.setParam(IloCplex::RootAlg, IloCplex::Network);
//             if (ppSolver.solve()) {
//                 double reducedCost = ppSolver.getObjValue();
// #ifndef NDEBUG
// #if ALL_EDGES
//                 std::cout << "Solved PP -> " << reducedCost;
// #else
//                 std::cout << "Solved PP for " << arc << " -> " << reducedCost;
// #endif
// #endif
//                 if (reducedCost <= _threshold) {
// #if ALL_EDGES
//                     _sets = m_pp->getSets(ppSolver);
// #ifndef NDEBUG
//                     std::cout << " with " << _sets;
// #endif
// #else
//                 auto set = m_pp->getSet(ppSolver);
// #ifndef NDEBUG
//                 std::cout << " with " << set;
// #endif
//                 _sets.emplace_back(set, arc);
// #endif
//                 }
// #ifndef NDEBUG
//                 std::cout << std::endl;
// #endif
//             }
// #ifndef NDEBUG
//             else {
// #if ALL_EDGES
//                 std::cout << "No solution for PP" << std::endl;
// #else
//                 std::cout << "No solution PP for " << arc << " -> ";
// #endif
//             }
// #endif

//             // std::cin.ignore();
//             ppSolver.end();
// #if !ALL_EDGES
//         }
// #endif
//     }

//   private:
//     IloEnv m_env { };
//     DiGraph m_inst.network;
//     std::vector<Demand> m_inst.demands;

//     std::map<Graph::Edge, int> m_arcToId;

//     PlacementModel* m_placementModel;
//     PricingProblem* m_pp;

//     friend PlacementModel;
// };

// //@Todo: Move to own file
// class ClassicalEnergy {
//   public:
//     ClassicalEnergy(const DiGraph& _network, const std::vector<Demand>& _demands)
//         : m_env()
//         , m_inst.network(_network)
//         , m_inst.demands(_demands)
//         , n(m_inst.network.getOrder())
//         , m(m_inst.network.size())
//         , m_model(m_env)
//         , m_obj(IloAdd(m_model, IloMinimize(m_env)))
//         , m_f(IloAdd(m_model, IloBoolVarArray(m_env, m * m_inst.demands.size())))
//         , m_x(IloAdd(m_model, IloBoolVarArray(m_env, m)))
//         , m_flowConsCons(IloAdd(m_model, IloRangeArray(m_env, n * m_inst.demands.size(), 0.0, 0.0)))
//         , m_linkCapaCons(IloAdd(m_model, IloRangeArray(m_env, m, 0.0, IloInfinity)))
//         ,
//         // m_bothLinks( IloAdd(m_model, IloRangeArray(m_env, m / 2, 0.0, 0.0) ) ),
//         m_noLoopCons1(IloAdd(m_model, IloRangeArray(m_env, n * m_inst.demands.size(), 0.0, 1.0)))
//         , m_noLoopCons2(IloAdd(m_model, IloRangeArray(m_env, n * m_inst.demands.size(), 0.0, 1.0)))
//         , m_sumLinks(IloAdd(m_model, IloRange(m_env, int(m_inst.demands.size()) == n * (n - 1) ? 2 * (m_inst.network.getOrder() - 1) : 0, m_inst.network.size())))
//         ,
//         // m_sumLinks( IloAdd(m_model, IloRange(m_env, 10, 10)) ),
//         m_solver(m_model)
//         , m_arcToId() {

//         auto listEdges = m_inst.network.getEdges();
//         std::vector<Graph::Edge> edges{listEdges.begin(), listEdges.end()};

//         int i = 0;
//         for (int e = 0; e < int(edges.size()); ++e) {
//             m_arcToId[edges[e]] = i++;
//             IloNumColumn inc = m_obj(1.0);
//             inc += m_linkCapaCons[e](m_inst.network.getEdgeWeight(edges[e]));
//             inc += m_sumLinks(1.0);
//             m_x[e] = IloBoolVar(inc);

//             m_x[e].setName(std::string("x" + toString(edges[e])).c_str());
//             m_linkCapaCons[e].setName(std::string("linkCapa" + toString(edges[e])).c_str());
//         }

//         for (auto i = 0; i < int(m_inst.demands.size()); ++i) {
//             int s = std::get<0>(m_inst.demands[i]),
//                 t = std::get<1>(m_inst.demands[i]);
//             double d = std::get<2>(m_inst.demands[i]);

//             for (int e = 0; e < m; ++e) {
//                 IloNumColumn inc(m_env);
//                 inc += m_flowConsCons[n * i + edges[e].first](1.0);
//                 inc += m_flowConsCons[n * i + edges[e].second](-1.0);
//                 inc += m_linkCapaCons[e](-d);
//                 inc += m_noLoopCons1[n * i + edges[e].first](1.0);
//                 inc += m_noLoopCons2[n * i + edges[e].second](1.0);

//                 m_f[m * i + e] = IloBoolVar(inc);
//                 m_f[m * i + e].setName(std::string("f" + toString(std::make_tuple(m_inst.demands[i], edges[e]))).c_str());
//             }
//             // m_nodeCapaCons.setNames("nodeCapa");

//             m_flowConsCons[n * i + s].setUB(1.0);
//             m_flowConsCons[n * i + s].setLB(1.0);

//             m_flowConsCons[n * i + t].setLB(-1.0);
//             m_flowConsCons[n * i + t].setUB(-1.0);
//         }

//         m_flowConsCons.setNames("flowConservation");
//         m_linkCapaCons.setNames("linkCapa");

//         for (auto i = 0; i < int(m_inst.demands.size()); ++i) {
//             for (int e = 0; e < m; ++e) {
//                 m_noLoopCons1[n * i + edges[e].first].setName(std::string("m_noLoopCons1" + toString(m_inst.demands[i]) + "_" + toString(edges[e])).c_str());
//                 m_noLoopCons2[n * i + edges[e].first].setName(std::string("m_noLoopCons2" + toString(m_inst.demands[i]) + "_" + toString(edges[e])).c_str());
//                 // //std::cout << m_noLoopCons1[n * i + edges[e].first] << std::endl;
//                 // //std::cout << m_noLoopCons2[n * i + edges[e].first] << std::endl;
//             }
//         }
//         m_solver.setParam(IloCplex::RootAlg, IloCplex::Network);
//         m_solver.setOut(m_env.getNullStream());
//         m_solver.exportModel("clp.lp");
//     }

//     void solve() {
//         m_solver.solve();
//         std::cout << "Optimal obj value: " << m_solver.getObjValue() << std::endl;
//         std::cout << 100 * (1 - m_solver.getObjValue() / double(m_inst.network.size())) << std::endl;
//     }

//     std::vector<Graph::Path> getPaths() const {
//         auto listEdges = m_inst.network.getEdges();
//         std::vector<Graph::Edge> edges{listEdges.begin(), listEdges.end()};
//         std::vector<Graph::Path> paths;
//         paths.reserve(m_inst.demands.size());

//         for (int i = 0; i < int(m_inst.demands.size()); ++i) {
//             // //std::cout << m_inst.demands[i] << std::endl;
//             std::vector<int> parent(m_inst.network.getOrder(), -1);
//             parent[std::get<0>(m_inst.demands[i])] = std::get<0>(m_inst.demands[i]);

//             for (int e = 0; e < m; ++e) {
//                 if (m_solver.getValue(m_f[m * i + e])) {
//                     // //std::cout << "m_f" << edges[e] << std::endl;
//                     assert(parent[edges[e].second] == -1 || [&]() {
//                         //std::cout << "Already have a parent: " << parent[edges[e].second] << " vs. " << edges[e].first << std::endl;
//                         return false;
//                     }());
//                     parent[edges[e].second] = edges[e].first;
//                 }
//             }
//             // //std::cout << "\t parents ->" << parent << std::endl;
//             int u = std::get<1>(m_inst.demands[i]);
//             Graph::Path path{u};
//             while (u != std::get<0>(m_inst.demands[i])) {
//                 u = parent[u];
//                 path.push_front(u);
//                 // //std::cout << "path.push_front("<<u<<");"<< std::endl;
//             }
//             paths.push_back(path);
//         }
//         return paths;
//     }

//   private:
//     IloEnv m_env { };
//     const DiGraph m_inst.network;
//     const std::vector<Demand> m_inst.demands;

//     int n;
//     int m;
//     IloModel m_model;

//     IloObjective m_obj;

//     IloBoolVarArray m_f;
//     IloBoolVarArray m_x;

//     IloRangeArray m_flowConsCons;
//     IloRangeArray m_linkCapaCons;
//     // IloRangeArray m_bothLinks;
//     IloRangeArray m_noLoopCons1;
//     IloRangeArray m_noLoopCons2;
//     IloRange m_sumLinks;

//     IloCplex m_solver;

//     std::map<Graph::Edge, int> m_arcToId;
// };

// #endif