#ifndef CHAINING_PATH_FUNCOCC_BENDERS_HPP
#define CHAINING_PATH_FUNCOCC_BENDERS_HPP

#include <algorithm>
#include <chrono>
#include <cmath>
#include <ilcplex/ilocplex.h>
#include <iostream>
#include <map>
#include <thread>
#include <tuple>

#include "DiGraph.hpp"
#include "SFC.hpp"
#include "ShortestPath.hpp"
#include "cplex_utility.hpp"
#include "utility.hpp"

#define RC_EPS 1.0e-6

class ChainingPathOccLimitBenders {
    class SubProblem;

    class MasterProblem {
      public:
        MasterProblem(const ChainingPathOccLimitBenders& _placement)
            : m_placement(_placement)
            , m_env()
            , m_model(m_env)
            , m_b([&]() {
                IloNumVarArray b(m_env, m_placement.m_funcCharge.size() * m_placement.m_inst.NFVNodes.size());
                for (int f = 0; f < m_placement.m_funcCharge.size()); ++f; {
                    for (const auto& u : m_placement.m_inst.NFVNodes) {
                        b[getIndexB(u, f)] = IloAdd(m_model, IloNumVar(m_env, 0.0, 1.0, ILOBOOL));
                        setIloName(b[getIndexB(u, f)], "b[u=" + std::to_string(u) + ", f=" + std::to_string(f) + "]");
                        ;
                    }
                }
                return b;
            }())
            , m_z(IloAdd(m_model, IloNumVar(m_env, 0.0, IloInfinity, "z")))
            , m_obj([&]() {
                IloObjective obj = IloMinimize(m_env);
                obj.setLinearCoef(m_z, 1.0);
                return IloAdd(m_model, obj);
            }())
            , m_nbLicencesCons([&]() {
                // Compute minimum nb licences
                std::vector<int> nbCores(m_placement.m_funcCharge.size(), 0);
                for (int i = 0; i < m_placement.m_inst.demands.size(); ++i) {
                    for (int j = 0; j < std::get<3>(m_placement.m_inst.demands[i]).size()); ++j; {
                        nbCores[std::get<3>(m_placement.m_inst.demands[i])[j]] += m_placement.m_inst.nbCores(i, j);
                    }
                }
                auto copyCapas = m_placement.m_inst.nodeCapa;
                std::sort(copyCapas.begin(), copyCapas.end(), [&](const int __i1, const int __i2) {
                    return __i2 > __i1;
                });

                std::vector<int> minLic(m_placement.m_funcCharge.size(), 0);
                for (int f = 0; f < m_placement.m_funcCharge.size()); ++f; {
                    while (nbCores[f] > 0) {
                        if (copyCapas[minLic[f]] != -1) {
                            nbCores[f] -= copyCapas[minLic[f]];
                            ++minLic[f];
                        } else {
                            minLic[f] = m_placement.m_nbLicenses + 1;
                            break;
                        }
                    }
                }

                IloRangeArray nbLicensesCons(m_env, m_placement.m_funcCharge.size());
                for (int f = 0; f < m_placement.m_funcCharge.size()); ++f; {
                    IloNumVarArray vars(m_env);
                    for (const auto& u : m_placement.m_inst.NFVNodes) {
                        vars.add(m_b[getIndexB(u, f)]);
                    }
                    nbLicensesCons[f] = IloAdd(m_model, IloRange(m_env, m_placement.m_nbLicenses, IloSum(vars), m_placement.m_nbLicenses));
                    setIloName(nbLicensesCons[f], "nbLicensesCons(" + toString(f) + ")");
                }
                return nbLicensesCons;
            }())
            , m_benders(IloRangeArray(m_env))
            , m_solver([&]() {
                IloCplex solver(m_model);
                solver.setOut(m_env.getNullStream());
                solver.setParam(IloCplex::Threads, 1);
                return solver;
            }()) {}

        bool solve() {
            return m_solver.solve();
        }

        IloNumArray getBValues() const {
            IloNumArray bValues(m_env);
            m_solver.getValues(m_b, bValues);
            return bValues;
        }

        double getObjValue() const {
            return m_solver.getObjValue();
        }

      private:
        inline int getIndexB(const int _u, const int _f) {
            assert(0 <= _f && _f < m_placement.m_funcCharge.size());
            assert(0 <= _u && _u < m_placement.m_inst.network.getOrder());
            assert(m_placement.m_inst.NFVIndices[_u] != -1);
            return m_placement.m_inst.NFVNodes.size() * _f + m_placement.m_inst.NFVIndices[_u];
        }

        const ChainingPathOccLimitBenders& m_placement;
        IloEnv m_env { };
        IloModel m_model;

        IloNumVarArray m_b;
        IloNumVar m_z;

        IloObjective m_obj;

        IloRangeArray m_nbLicencesCons;
        IloRangeArray m_benders;

        IloCplex m_solver;

        friend SubProblem;
    };

    class SubProblem {
        class ReducedMainProblem {
            friend SubProblem;

          public:
            ReducedMainProblem(const SubProblem& _placement)
                : m_placement(_placement)
                , m_env()
                , m_model(m_env)
                , m_y(IloAdd(m_model, IloNumVarArray(m_env)))
                , m_b([&]() {
                    IloNumVarArray b(m_env, m_placement.m_funcCharge.size() * m_placement.m_inst.NFVNodes.size());
                    for (int f = 0; f < m_placement.m_funcCharge.size()); ++f; {
                        for (const auto& u : m_placement.m_inst.NFVNodes) {
                            b[getIndexB(u, f)] = IloAdd(m_model, IloNumVar(m_env, 0.0, IloInfinity));
                            setIloName(b[getIndexB(u, f)], "b[u=" + std::to_string(u) + ", f=" + std::to_string(f) + "]");
                            ;
                        }
                    }
                    return b;
                }())
                , m_obj(IloAdd(m_model, IloMinimize(m_env)))
                , m_onePathCons([&]() {
                    IloRangeArray onePathCons(m_env, m_placement.m_inst.demands.size(), 1.0, 1.0);
                    for (DemandID i = 0; i < m_placement.m_inst.demands.size()); ++i; {
                        setIloName(onePathCons[i], "onePathCons" + toString(m_placement.m_inst.demands[i]));
                    }
                    return IloAdd(m_model, onePathCons);
                }())
                , m_linkCapasCons([&]() {
                    IloRangeArray linkCapaCons(m_env, m_placement.m_inst.network.size(), 0.0, 0.0);
                    int e = 0;
                    for (const Graph::Edge& edge : m_placement.m_inst.edges) {
                        linkCapaCons[e].setBounds(0.0, m_placement.m_inst.network.getEdgeWeight(edge));
                        setIloName(linkCapaCons[e], "linkCapas" + toString(edge));
                        ++e;
                    }
                    return IloAdd(m_model, linkCapaCons);
                }())
                , m_nodeCapasCons([&]() {
                    IloRangeArray nodeCapasCons(m_env, m_placement.m_inst.NFVNodes.size());
                    for (const auto& u : m_placement.m_inst.NFVNodes) {
                        const int i = m_placement.m_inst.NFVIndices[u];
                        nodeCapasCons[i] = IloAdd(m_model, IloRange(m_env, 0.0, m_placement.m_inst.nodeCapa[u]));
                        setIloName(nodeCapasCons[i], "nodeCapasCons" + std::to_string(u));
                    }
                    return nodeCapasCons;
                }())
                , m_funcLicensesCons([&]() {
                    // Compute minimum nb licences
                    std::vector<int> nbCores(m_placement.m_funcCharge.size(), 0);
                    for (int i = 0; i < m_placement.m_inst.demands.size(); ++i) {
                        for (int j = 0; j < std::get<3>(m_placement.m_inst.demands[i]).size()); ++j; {
                            nbCores[std::get<3>(m_placement.m_inst.demands[i])[j]] += m_placement.m_inst.nbCores(i, j);
                        }
                    }
                    auto copyCapas = m_placement.m_inst.nodeCapa;
                    std::sort(copyCapas.begin(), copyCapas.end(), [&](const int __i1, const int __i2) {
                        return __i2 > __i1;
                    });

                    std::vector<int> minLic(m_placement.m_funcCharge.size(), 0);
                    for (int f = 0; f < m_placement.m_funcCharge.size()); ++f; {
                        while (nbCores[f] > 0) {
                            if (copyCapas[minLic[f]] != -1) {
                                nbCores[f] -= copyCapas[minLic[f]];
                                ++minLic[f];
                            } else {
                                minLic[f] = m_placement.m_nbLicenses + 1;
                                break;
                            }
                        }
                    }

                    IloRangeArray funcLicensesCons(m_env, m_placement.m_funcCharge.size());
                    for (int f = 0; f < m_placement.m_funcCharge.size()); ++f; {
                        IloNumVarArray vars(m_env);
                        for (const auto& u : m_placement.m_inst.NFVNodes) {
                            vars.add(m_b[getIndexB(u, f)]);
                        }
                        funcLicensesCons[f] = IloAdd(m_model, IloRange(m_env, minLic[f], IloSum(vars), m_placement.m_nbLicenses));
                        // std::cout << funcLicensesCons[f] << '\n';
                        setIloName(funcLicensesCons[f], "funcLicensesCons(" + toString(f) + ")");
                    }
                    return funcLicensesCons;
                }())
                , m_pathFuncCons([&]() {
                    IloRangeArray pathFuncCons(m_env, m_placement.m_inst.demands.size() * m_placement.m_funcCharge.size() * m_placement.m_inst.NFVNodes.size());
                    for (int i = 0; i < m_placement.m_inst.demands.size(); ++i) {
                        for (int f = 0; f < m_placement.m_funcCharge.size()); ++f; {
                            for (const auto& u : m_placement.m_inst.NFVNodes) {
                                pathFuncCons[getIndexPathFunc(i, f, u)] = IloRange(m_env, 0.0, m_b[getIndexB(u, f)], IloInfinity);
                            }
                        }
                    }
                    return IloAdd(m_model, pathFuncCons);
                }())
                , m_pathFuncConsBIGM(m_placement.m_funcCharge.size() * m_placement.m_inst.NFVNodes.size(), 1.0)
                , m_solver([&]() {
                    IloCplex solver(m_model);
                    solver.setOut(m_env.getNullStream());
                    solver.setParam(IloCplex::Threads, 1);
                    return solver;
                }())
                , m_paths() {}

            inline int getIndexB(const int _u, const int _f) {
                assert(0 <= _f && _f < m_placement.m_funcCharge.size());
                assert(0 <= _u && _u < m_placement.m_inst.network.getOrder());
                assert(m_placement.m_inst.NFVIndices[_u] != -1);
                return m_placement.m_inst.NFVNodes.size() * _f + m_placement.m_inst.NFVIndices[_u];
            }

            inline int getIndexPathFunc(const int _i, const int _f, const int _u) const {
                assert(0 <= _i && _i < m_placement.m_inst.demands.size());
                assert(0 <= _f && _f < m_placement.m_funcCharge.size());
                assert(0 <= _u && _u < m_placement.m_inst.network.getOrder());
                assert(m_placement.m_inst.NFVIndices[_u] != -1);

                return m_placement.m_inst.NFVNodes.size() * m_placement.m_funcCharge.size() * _i
                       + m_placement.m_inst.NFVNodes.size() * _f
                       + m_placement.m_inst.NFVIndices[_u];
            }

            ~ReducedMainProblem() {
                // m_env.end();
            }

            ReducedMainProblem& operator=(const ReducedMainProblem&) = delete;
            ReducedMainProblem(const ReducedMainProblem&) = delete;

            void addPath(const ServicePath& _sPath, const int _i) {
                const auto pair = std::make_pair(_sPath, _i);
#if !defined(NDEBUG)
                std::cout << "addPath" << pair << '\n';
#endif
                assert([&]() {
                    const auto ite = std::find(m_paths.begin(), m_paths.end(), pair);
                    if (ite == m_paths.end()) {
                        return true;
                    } else {
                        std::cout << pair << " is already present!" << '\n';
                        return false;
                    }
                }());

                const auto& path = _sPath.first;
                const auto& funcPlacement = _sPath.second;

                const auto& demand = m_placement.m_inst.demands[_i];
                const auto d = std::get<2>(demand);

                IloNumArray linkCharge(m_env, m_placement.m_inst.network.size());
                for (int e = 0; e < m_placement.m_inst.network.size(); ++e) {
                    linkCharge[e] = 0;
                }
                for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
                    linkCharge[m_placement.m_inst.edgeToId(*iteU, *iteV)] += d;
                } /****************************************************************************************************/

                /***************************************** Node constraints *****************************************/
                IloNumArray nodeCharge(m_env, m_placement.m_inst.NFVNodes.size());
                for (IloInt i = 0; i < nodeCharge.getSize(); ++i) {
                    nodeCharge[i] = IloNum(0.0);
                }

                IloNumArray funcPath(m_env, m_placement.m_funcCharge.size() * m_placement.m_inst.NFVNodes.size());
                IloRangeArray pathFuncCons(m_env, m_placement.m_funcCharge.size() * m_placement.m_inst.NFVNodes.size());

                for (int f = 0; f < m_placement.m_funcCharge.size()); ++f; {
                    for (const auto& u : m_placement.m_inst.NFVNodes) {
                        const int index = m_placement.m_inst.NFVIndices[u];
                        funcPath[m_placement.m_inst.NFVNodes.size() * f + index] = IloNum(0.0);
                        pathFuncCons[m_placement.m_inst.NFVNodes.size() * f + index] = m_pathFuncCons[getIndexPathFunc(_i, f, u)];
                    }
                }

                int j = 0;
                for (const auto& pair_NodeFuncs : funcPlacement) {
                    for (const int nbFunc = pair_NodeFuncs.second.size() + j; j < nbFunc; ++j) {
                        // Add to node capa constraints
                        const int index = m_placement.m_inst.NFVIndices[pair_NodeFuncs.first];
                        nodeCharge[index] += m_placement.m_inst.nbCores(_i, j);
                        // Add to placement constraint
                        const int f = std::get<3>(m_placement.m_inst.demands[_i])[j];
                        funcPath[m_placement.m_inst.NFVNodes.size() * f + index] = -1.0;
                    }
                }
                IloNumColumn inc = m_onePathCons[_i](1.0)
                                   + m_obj(d * (path.size() - 1))
                                   + m_linkCapasCons(linkCharge)
                                   + m_nodeCapasCons(nodeCharge)
                                   + pathFuncCons(funcPath);

                auto yVar = IloNumVar(inc);
                setIloName(yVar, "y" + toString(std::make_tuple(demand, _sPath)));
                m_y.add(yVar);
                m_paths.emplace_back(_sPath, _i);
            }

            void addPath(const int _i) {
                const auto& demand = m_placement.m_inst.demands[_i];
                const auto d = std::get<2>(demand);

                IloNumColumn cplexCol(m_onePathCons[_i](1.0));
                cplexCol += m_obj(d * m_placement.m_inst.network.getOrder());

                auto yVar = IloNumVar(cplexCol);
                setIloName(yVar, "y" + toString(demand));
                m_y.add(yVar);
            }

            void suppressDummyColumns() {
                for (int i = 0; i < m_placement.m_inst.demands.size(); ++i) {
                    m_y[i].setBounds(0.0, 0.0);
                }
            }

            void enableDummyColumns() {
                for (int i = 0; i < m_placement.m_inst.demands.size(); ++i) {
                    m_y[i].setBounds(0.0, IloInfinity);
                }
            }

            double solveInteger() {
                // Convert from real to bool
                std::cout << "Converting y...\n";
                for (int i = 0; i < m_paths.size(); ++i) {
                    m_model.add(IloConversion(m_env, m_y[i + m_placement.m_inst.demands.size()], ILOBOOL));
                }
                std::cout << "Converting a...\n";
                m_model.add(IloConversion(m_env, m_b, ILOBOOL));
                std::cout << "Setting a priorities...\n";
                for (int i = 0; i < m_b.getSize(); ++i) {
                    m_solver.setPriority(m_b[i], 10.0);
                }
                m_solver.setOut(std::cout);
                std::cout << "Solving...\n";
                if (m_solver.solve()) {
                    // Solution found
                    auto obj = m_solver.getObjValue();
                    std::cout << "Final obj value: " << obj << "\t# Paths: " << m_paths.size() << '\n';
                    getNetworkUsage(m_solver);
                    return obj;
                }
                return -1.0;
            }

            double getObjValue() const {
                return m_solver.getObjValue();
            }

            void setBBounds(const IloNumArray& _b) {
                for (int f = 0; f < m_placement.m_funcCharge.size()); ++f; {
                    std::cout << "Function " << f << " installed on :";
                    for (const auto& u : m_placement.m_inst.NFVNodes) {
                        if (_b[getIndexB(u, f)]) {
                            std::cout << u << ", ";
                        }
                    }
                    std::cout << '\n';
                }
                for (const auto& u : m_placement.m_inst.NFVNodes) {
                    for (int f(0); f < m_placement.m_funcCharge.size()); ++f; {
                        if (_b[getIndexB(u, f)] == 1.0) {
                            m_b[getIndexB(u, f)].setBounds(1.0, 1.0);
                        } else {
                            m_b[getIndexB(u, f)].setBounds(0.0, 0.0);
                        }
                    }
                }
            }

            void getNetworkUsage(const IloCplex& _sol) const {
                std::vector<double> linkUsage(m_placement.m_inst.network.size(), 0);
                std::vector<int> nodeUsage(m_placement.m_inst.network.getOrder(), 0);
                std::vector<IloNumVar> pathsUsed(m_placement.m_inst.demands.size(), nullptr);

                for (int i = 0; i < m_paths.size(); ++i) {
                    auto y = m_y[i + m_placement.m_inst.demands.size()];
                    if (_sol.getValue(y) > RC_EPS) {
                        if (_sol.getValue(y) != 1.0) {
                            std::cout << y << " -> " << _sol.getValue(y) << '\n';
                        }
                        const auto& path = m_paths[i].first.first;
                        const auto& funcPlacement = m_paths[i].first.second;
                        const auto& demand = m_placement.m_inst.demands[m_paths[i].second];
                        const auto d = std::get<2>(demand);
                        pathsUsed[m_paths[i].second] = y;

                        for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
                            linkUsage[m_placement.m_inst.edgeToId(*iteU, *iteV)] += d * _sol.getValue(y);
                        }
                        int j = 0;
                        for (const auto& pair_NodeFuncs : funcPlacement) {
                            for (const int nbFunc = pair_NodeFuncs.second.size() + j; j < nbFunc; ++j) {
                                nodeUsage[pair_NodeFuncs.first] += _sol.getValue(y) * m_placement.m_inst.nbCores(m_paths[i].second, j);
                            }
                        }
                    }
                }
                std::cout << "Node usage: \n";
                for (const auto& u : m_placement.m_inst.NFVNodes) {
                    std::cout << '\t' << u << " -> " << nodeUsage[u] << " / " << m_placement.m_inst.nodeCapa[u] << '\n';
                    assert(nodeUsage[u] <= m_placement.m_inst.nodeCapa[u]);
                }
                std::cout << "Link usage: \n";
                int i = 0;
                for (const auto& edge : m_placement.m_inst.network.getEdges()) {
                    std::cout << std::fixed << '\t' << edge << " -> " << linkUsage[i] << " / " << m_placement.m_inst.network.getEdgeWeight(edge) << '\n';
                    assert(m_placement.m_inst.network.getEdgeWeight(edge) - linkUsage[i] > -1.0e-6
                           || (std::cerr << std::fixed << m_linkCapasCons[i] << '\n'
                                         << m_placement.m_inst.network.getEdgeWeight(edge) - linkUsage[i] << '\n',
                                  false));
                    ++i;
                }

                std::cout << "Function placement: \n";
                for (int f = 0; f < m_placement.m_funcCharge.size()); ++f; {
                    std::cout << "\tfunction " << f << " on: (";
                    for (const auto& u : m_placement.m_inst.NFVNodes) {
                        const int i = m_placement.m_inst.NFVIndices[u];
                        if (_sol.getValue(m_b[m_placement.m_inst.NFVNodes.size() * f + i]) > RC_EPS) {
                            std::cout << u << " (" << _sol.getValue(m_b[m_placement.m_inst.NFVNodes.size() * f + i]) << "), ";
                        }
                    }
                    std::cout << ")\n";
                }
            }

          private:
            const SubProblem& m_placement;
            IloEnv m_env { };
            IloModel m_model;

            IloNumVarArray m_y;
            IloNumVarArray m_b;

            IloObjective m_obj;

            IloRangeArray m_onePathCons;
            IloRangeArray m_linkCapasCons;
            IloRangeArray m_nodeCapasCons;
            IloRangeArray m_funcLicensesCons;
            IloRangeArray m_pathFuncCons;

            std::vector<int> m_pathFuncConsBIGM;

            IloCplex m_solver;

            std::vector<std::pair<ServicePath, int>> m_paths;
        };

        class PricingProblemLP {
            friend SubProblem;

          public:
            PricingProblemLP(const SubProblem& _chainPlacement)
                : m_placement(_chainPlacement)
                , m_env()
                , m_demandID(0)
                , n(m_placement.m_inst.network.getOrder())
                , m(m_placement.m_inst.network.size())
                , nbLayers(m_placement.m_inst.maxChainSize + 1)
                , m_model(m_env)
                , m_obj(IloAdd(m_model, IloMinimize(m_env)))
                , m_a([&]() {
                    IloNumVarArray a(m_env, m_placement.m_inst.NFVNodes.size() * (nbLayers - 1));
                    for (const auto& u : m_placement.m_inst.NFVNodes) {
                        for (int j = 0; j < nbLayers - 1); ++j; {
                            a[getIndexA(u, j)] = IloAdd(m_model,
                                IloNumVar(m_env, 0, 1, ILOBOOL));
                            setIloName(a[getIndexA(u, j)], "a" + toString(std::make_tuple(j, u)));
                        }
                    }
                    return a;
                }())
                , m_b([&]() {
                    IloNumVarArray b(m_env, m_placement.m_inst.NFVNodes.size() * m_placement.m_funcCharge.size());
                    for (const auto& u : m_placement.m_inst.NFVNodes) {
                        for (int f(0); f < m_placement.m_funcCharge.size()); ++f; {
                            b[getIndexB(u, f)] = IloAdd(m_model, IloNumVar(m_env, 0, 1, ILOBOOL));
                            setIloName(b[getIndexB(u, f)], "b" + toString(std::make_tuple(f, u)));
                        }
                    }
                    return b;
                }())
                , m_f([&]() {
                    IloNumVarArray f(m_env, m * nbLayers, 0, 1, ILOBOOL);
                    for (int e = 0; e < m; ++e) {
                        for (int j = 0; j < nbLayers); ++j; {
                            setIloName(f[getIndexF(e, j)], "f" + toString(std::make_tuple(m_placement.m_inst.edges[e], j)));
                        }
                    }
                    return IloAdd(m_model, f);
                }())
                , m_flowConsCons([&]() {
                    IloRangeArray flowConsCons(m_env, n * nbLayers, 0.0, 0.0);
                    for (Graph::Node u = 0; u < m_placement.m_inst.network.getOrder()); ++u; {
                        for (int j = 0; j < nbLayers); ++j; {
                            IloNumVarArray vars(m_env);
                            IloNumArray vals(m_env);
                            for (const auto& v : m_placement.m_inst.network.getNeighbors(u)) {
                                vars.add(m_f[getIndexF(m_placement.m_inst.edgeToId(u, v), j)]);
                                vals.add(1.0);
                                vars.add(m_f[getIndexF(m_placement.m_inst.edgeToId(v, u), j)]);
                                vals.add(-1.0);
                            }
                            if (m_placement.m_isNFV[u]) {
                                if (j > 0) {
                                    vars.add(m_a[getIndexA(u, j - 1)]);
                                    vals.add(-1.0);
                                }
                                if (j < nbLayers - 1) {
                                    vars.add(m_a[getIndexA(u, j)]);
                                    vals.add(1.0);
                                }
                            }
                            flowConsCons[n * j + u] = IloAdd(m_model,
                                IloRange(m_env, 0.0, IloScalProd(vars, vals), 0.0));
                        }
                    }
                    return flowConsCons;
                }())
                , m_linkCapaCons([&]() {
                    IloRangeArray linkCapaCons(m_env, m, 0.0, 0.0);
                    for (int e = 0; e < m_placement.m_inst.edges.size()); ++e; {
                        linkCapaCons[e].setUB(m_placement.m_inst.network.getEdgeWeight(m_placement.m_inst.edges[e]));
                        setIloName(linkCapaCons[e], "linkCapaCons(" + toString(e) + ")");
                    }
                    return IloAdd(m_model, linkCapaCons);
                }())
                , m_nodeCapaCons([&]() {
                    IloRangeArray nodeCapaCons(m_env, n);
                    for (const auto& u : m_placement.m_inst.NFVNodes) {
                        nodeCapaCons[u] = IloAdd(m_model,
                            IloRange(m_env, 0.0, m_placement.m_inst.nodeCapa[u]));
                        setIloName(nodeCapaCons[u], "nodeCapaCons(" + std::to_string(u) + ")");
                    }
                    return nodeCapaCons;
                }())
                , m_funcNodeUsageCons([&]() {
                    IloRangeArray funcNodeUsageCons(m_env, m_placement.m_inst.NFVNodes.size() * m_placement.m_funcCharge.size());
                    for (const auto& u : m_placement.m_inst.NFVNodes) {
                        for (int f(0); f < m_placement.m_funcCharge.size()); ++f; {
                            funcNodeUsageCons[getIndexB(u, f)] = IloAdd(m_model, IloRange(m_env, 0.0, IloNum(m_placement.m_inst.maxChainSize) * m_b[getIndexB(u, f)], IloInfinity));
                            setIloName(funcNodeUsageCons[getIndexB(u, f)], "funcNodeUsageCons" + toString(std::make_tuple(f, u)));
                        }
                    }
                    return funcNodeUsageCons;
                }())
                , m_nbLicensesCons([&]() {
                    IloRangeArray nbLicensesCons(m_env, m_placement.m_funcCharge.size());
                    for (int f(0); f < m_placement.m_funcCharge.size()); ++f; {
                        IloNumVarArray vars(m_env);
                        for (const auto& u : m_placement.m_inst.NFVNodes) {
                            vars.add(m_b[getIndexB(u, f)]);
                        }
                        nbLicensesCons[f] = IloAdd(m_model, IloRange(m_env, 0.0, IloSum(vars), m_placement.m_nbLicenses));
                    }
                    return nbLicensesCons;
                }())
                , m_solver(m_model) {}

            ~PricingProblemLP() {
                // m_env.end();
            }

            int getIndexF(const int _e, const int _j) const {
                assert(0 <= _e && _e < m_placement.m_inst.network.size());
                assert(0 <= _j && _j < nbLayers);
                return m * _j + _e;
            }

            int getIndexA(const int _u, const int _j) const {
                assert(0 <= _u && _u < m_placement.m_inst.network.getOrder());
                assert(m_placement.m_inst.NFVIndices[_u] != -1);
                assert(0 <= _j && _j < nbLayers);
                return m_placement.m_inst.NFVNodes.size() * _j + m_placement.m_inst.NFVIndices[_u];
            }

            int getIndexB(const int _u, const int _f) const {
                assert(0 <= _u && _u < m_placement.m_inst.network.getOrder());
                assert(m_placement.m_inst.NFVIndices[_u] != -1);
                assert(0 <= _f && _f < m_placement.m_funcCharge.size());
                return m_placement.m_inst.NFVNodes.size() * _f + m_placement.m_inst.NFVIndices[_u];
            }

            void updateModel(const DemandID& _i) {
                m_solver.end();
                int nbFunctions = std::get<3>(m_placement.m_inst.demands[m_demandID]).size();
                m_flowConsCons[n * 0 + std::get<0>(m_placement.m_inst.demands[m_demandID])].setBounds(0.0, 0.0);
                m_flowConsCons[n * nbFunctions + std::get<1>(m_placement.m_inst.demands[m_demandID])].setBounds(0.0, 0.0);

                m_demandID = _i;
                nbFunctions = std::get<3>(m_placement.m_inst.demands[m_demandID]).size();
                m_flowConsCons[n * 0 + std::get<0>(m_placement.m_inst.demands[m_demandID])].setBounds(1.0, 1.0);
                m_flowConsCons[n * nbFunctions + std::get<1>(m_placement.m_inst.demands[m_demandID])].setBounds(-1.0, -1.0);

                for (int e = 0; e < m_placement.m_inst.edges.size()); ++e; {
                    IloNumVarArray vars(m_env);
                    IloNumArray vals(m_env);
                    for (int j = 0; j <= nbFunctions); ++j; {
                        vars.add(m_f[getIndexF(e, j)]);
                        vals.add(std::get<2>(m_placement.m_inst.demands[m_demandID]));
                    }
                    for (int j(nbFunctions + 1); j < nbLayers; ++j) {
                        vars.add(m_f[getIndexF(e, j)]);
                        vals.add(0);
                    }
                    m_linkCapaCons[e].setLinearCoefs(vars, vals);
                }

                for (const auto& u : m_placement.m_inst.NFVNodes) {
                    IloNumVarArray vars(m_env);
                    IloNumArray vals(m_env);
                    for (int j = 0; j < nbFunctions); ++j; {
                        vars.add(m_a[getIndexA(u, j)]);
                        vals.add(m_placement.m_inst.nbCores(m_demandID, j));
                    }
                    for (int j = nbFunctions; j < nbLayers - 1); ++j; {
                        vars.add(m_a[getIndexA(u, j)]);
                        vals.add(0);
                    }
                    m_nodeCapaCons[u].setLinearCoefs(vars, vals);
                    setIloName(m_nodeCapaCons[u], "m_nodeCapaCons(" + std::to_string(u) + ")");
                }

                //Function node usage
                for (const auto& u : m_placement.m_inst.NFVNodes) {
                    for (int f(0); f < m_placement.m_funcCharge.size()); ++f; {
                        IloNumVarArray vars(m_env);
                        IloNumArray vals(m_env);
                        for (int j = 0; j < nbFunctions); ++j; {
                            vars.add(m_a[getIndexA(u, j)]);
                            if (std::get<3>(m_placement.m_inst.demands[m_demandID])[j] == f) {
                                vals.add(-1.0);
                            } else {
                                vals.add(0.0);
                            }
                        }
                        for (int j = nbFunctions; j < nbLayers - 1); ++j; {
                            vars.add(m_a[getIndexA(u, j)]);
                            vals.add(0.0);
                        }
                        m_funcNodeUsageCons[getIndexB(u, f)].setLinearCoefs(vars, vals);
                    }
                }
            }

            void updateDual(const DemandID& _i, const IloNumArray& _linkCapaDuals, const IloNumArray& _nodeCapaDuals, const IloNumArray& _pathFuncDuals, const IloNum& _demandDual) {
                updateModel(_i);
                const auto& d = std::get<2>(m_placement.m_inst.demands[m_demandID]);
                const auto& functions = std::get<3>(m_placement.m_inst.demands[m_demandID]);
                const int nbFunctions = functions.size();
                IloNumVarArray vars(m_env);
                IloNumArray vals(m_env);

                for (int e = 0; e < m; ++e) {
                    for (int j = 0; j <= nbFunctions; ++j) {
                        vars.add(m_f[getIndexF(e, j)]);
                        vals.add(d - d * _linkCapaDuals[e]);
                    }
                }

                for (const auto& u : m_placement.m_inst.NFVNodes) {
                    const int index = m_placement.m_inst.NFVIndices[u];
                    for (int j = 0; j < functions.size()); ++j; {
                        vars.add(m_a[getIndexA(u, j)]);
                        vals.add(
                            -_nodeCapaDuals[index] * m_placement.m_inst.nbCores(m_demandID, j));
                    }
                }
                for (const auto& u : m_placement.m_inst.NFVNodes) {
                    for (int f(0); f < m_placement.m_funcCharge.size()); ++f; {
                        vars.add(m_b[getIndexB(u, f)]);
                        vals.add(_pathFuncDuals[m_placement.m_mainProblem.getIndexPathFunc(m_demandID, f, u)]);
                    }
                }
                m_obj.setConstant(-_demandDual);
                m_obj.setLinearCoefs(vars, vals);
                m_solver = IloCplex(m_model);
                m_solver.setParam(IloCplex::Threads, 1);
                m_solver.setOut(m_env.getNullStream());
            }

            void setBBounds(const IloNumArray& _b) {
                for (const auto& u : m_placement.m_inst.NFVNodes) {
                    for (int f(0); f < m_placement.m_funcCharge.size()); ++f; {
                        if (_b[getIndexB(u, f)] == 1.0) {
                            m_b[getIndexB(u, f)].setBounds(1.0, 1.0);
                        } else {
                            m_b[getIndexB(u, f)].setBounds(0.0, 0.0);
                        }
                    }
                }
            }

            void resetBBounds() {
                for (const auto& u : m_placement.m_inst.NFVNodes) {
                    for (int f(0); f < m_placement.m_funcCharge.size()); ++f; {
                        m_b[getIndexB(u, f)].setBounds(0.0, 1.0);
                    }
                }
            }

            bool solve() {
                std::cout << "\r                                            ";
                std::cout << "\rSolving for " << m_demandID;
                return m_solver.solve();
            }

            double getObjValue() const {
                return m_solver.getObjValue();
            }

            ServicePath getServicePath() const {
#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
                std::cout << "getServicePath() -> " << m_placement.m_inst.demands[m_demandID] << '\n';
#endif
                const auto& funcs = std::get<3>(m_placement.m_inst.demands[m_demandID]);
                const auto& t = std::get<1>(m_placement.m_inst.demands[m_demandID]);
                std::vector<std::pair<Graph::Node, std::vector<function_descriptor>>> installs;

                Graph::Node u = std::get<0>(m_placement.m_inst.demands[m_demandID]);
                int j = 0;
                Graph::Path path{u};

                IloNumArray aVal(m_env);
                m_solver.getValues(aVal, m_a);

                while (u != t || j < funcs.size()); {
                    if (j < funcs.size()) && m_placement.m_isNFV[u] && aVal[getIndexA(u, j)]; {
                        if (installs.empty() || installs.back().first != u) {
                            installs.emplace_back(u, std::vector<function_descriptor>());
                        }
                        installs.back().second.push_back(funcs[j]);
                        ++j;
                    } else {
                        for (const auto& v : m_placement.m_inst.network.getNeighbors(u)) {
                            if (m_solver.getValue(m_f[getIndexF(m_placement.m_inst.edgeToId(u, v), j)]) > 0) {
                                path.push_back(v);
                                u = v;
                                break;
                            }
                        }
                    }
                }

                assert(path.back() == t);
                return ServicePath(path, installs);
            }

          private:
            const SubProblem& m_placement;
            IloEnv m_env { };

            int m_demandID;
            const int n;
            const int m;
            const int nbLayers;
            IloModel m_model;

            IloObjective m_obj;

            IloNumVarArray m_a;
            IloNumVarArray m_b;
            IloNumVarArray m_f;

            IloRangeArray m_flowConsCons;
            IloRangeArray m_linkCapaCons;
            IloRangeArray m_nodeCapaCons;

            IloRangeArray m_funcNodeUsageCons;
            IloRangeArray m_nbLicensesCons;

            IloCplex m_solver;
        };

      public:
        SubProblem(ChainingPathOccLimitBenders& _problem, const DiGraph& _network, const std::vector<int> _nodeCapa, const std::vector<Demand>& _demands, const std::vector<double>& _funcCharge, const std::vector<int>& _NFVNodes, const int _nbLicenses, bool _allDemands = true)
            : m_problem(_problem)
            , m_inst.network(_network)
            , m_inst.edges(m_inst.network.getEdges())
            , m_inst.nodeCapa(_nodeCapa)
            , m_funcCharge(_funcCharge)
            , m_inst.demands(_demands)
            , m_allDemands(_allDemands)
            , m_nbLicenses(_nbLicenses)
            , m_inst.maxChainSize(std::get<3>(*std::max_element(m_inst.demands.begin(), m_inst.demands.end(),
                                             [&](const Demand& _d1, const Demand& _d2) {
                                                 return std::get<3>(_d1).size() < std::get<3>(_d2).size();
                                             }))
                                 .size())
            , m_inst.NFVNodes(_NFVNodes)
            , m_inst.NFVIndices([&]() {
                std::vector<int> NFVIndices(m_inst.network.getOrder(), -1);
                for (int i = 0; i < m_inst.NFVNodes.size()); ++i; {
                    NFVIndices[m_inst.NFVNodes[i]] = i;
                }
                return NFVIndices;
            }())
            , m_isNFV([&]() {
                std::vector<int> isNFV(m_inst.network.getOrder(), 0);
                for (const auto& u : m_inst.NFVNodes) {
                    isNFV[u] = 1;
                }
                return isNFV;
            }())
            , m_inst.nbCores([&]() {
                Matrix<double> mat(m_inst.demands.size(), m_inst.maxChainSize, -1);

                for (int i(0); i < m_inst.demands.size()); ++i; {
                    const auto& funcs = std::get<3>(m_inst.demands[i]);

                    for (int j = 0; j < funcs.size()); ++j; {
                        mat(i, j) = ceil(std::get<2>(m_inst.demands[i]) / m_funcCharge[funcs[j]]);
                    }
                }
                return mat;
            }())
            , m_intObj(-1)
            , m_fractObj(-1)
            , m_inst.edgeToId([&]() {
                Matrix<int> edgeToId(m_inst.network.getOrder(), m_inst.network.getOrder(), -1);
                int i = 0;
                for (Graph::Edge edge : m_inst.network.getEdges()) {
                    edgeToId(edge.first, edge.second) = i;
                    ++i;
                }
                return edgeToId;
            }())
            , m_mainProblem(*this)
            , m_pps([&]() {
                std::vector<PricingProblemLP> pps;
                int concurentThreadsSupported = std::max(1u, std::thread::hardware_concurrency());
                pps.reserve(concurentThreadsSupported);
                for (int i = 0; i < concurentThreadsSupported; ++i) {
                    pps.emplace_back(*this);
                }
                return pps;
            }()) {
            addInitConf();
        }

        void addInitConf(const std::vector<ServicePath>& _paths) {
            for (int i = 0; i < _paths.size(); ++i) {
                m_mainProblem.addPath(_paths[i], i);
            }
        }

        // Add dummy variables for every demands
        void addInitConf() {
            for (int i = 0; i < m_inst.demands.size(); ++i) {
                m_mainProblem.addPath(i);
            }
        }

        void setBBounds(const IloNumArray& _b) {
            m_mainProblem.setBBounds(_b);
            for (auto& pp : m_pps) {
                pp.setBBounds(_b);
            }
        }

        void addBendersCuts() {
            IloNumArray onePathDuals(m_mainProblem.m_env);
            IloNumArray linkCapasDuals(m_mainProblem.m_env);
            IloNumArray nodeCapasDuals(m_mainProblem.m_env);
            IloNumArray funcLicensesDuals(m_mainProblem.m_env);
            IloNumArray pathFuncDuals(m_mainProblem.m_env);
            // std::cout << m_mainProblem.m_solver.getStatus()  << '\n';
            if (m_mainProblem.m_solver.getStatus() == IloAlgorithm::Infeasible) {
                m_mainProblem.m_solver.dualFarkas(m_mainProblem.m_onePathCons, onePathDuals);
                m_mainProblem.m_solver.dualFarkas(m_mainProblem.m_linkCapasCons, linkCapasDuals);
                m_mainProblem.m_solver.dualFarkas(m_mainProblem.m_nodeCapasCons, nodeCapasDuals);
                m_mainProblem.m_solver.dualFarkas(m_mainProblem.m_funcLicensesCons, funcLicensesDuals);
                m_mainProblem.m_solver.dualFarkas(m_mainProblem.m_pathFuncCons, pathFuncDuals);
                double constant = 0.0;
                for (DemandID i = 0; i < m_inst.demands.size()); ++i; {
                    constant += onePathDuals[i];
                }
                std::cout << "Constant part equals " << constant << '\n';
                int e = 0;
                for (const Graph::Edge& edge : m_inst.edges) {
                    constant += -m_inst.network.getEdgeWeight(edge) * linkCapasDuals[e];
                    ++e;
                }
                std::cout << "Constant part equals " << constant << '\n';
                for (const auto& u : m_inst.NFVNodes) {
                    constant += -m_inst.nodeCapa[u] * nodeCapasDuals[m_inst.NFVIndices[u]];
                }
                std::cout << "Constant part equals " << constant << '\n';
                for (int f = 0; f < m_funcCharge.size()); ++f; {
                    constant += m_nbLicenses * funcLicensesDuals[f];
                }
                std::cout << "Constant part equals " << constant << '\n';
                for (int i = 0; i < m_inst.demands.size(); ++i) {
                    for (int f = 0; f < m_funcCharge.size()); ++f; {
                        for (const auto& u : m_inst.NFVNodes) {
                            constant += pathFuncDuals[m_mainProblem.getIndexPathFunc(i, f, u)];
                        }
                    }
                }
                std::cout << "Constant part equals " << constant << '\n';
                IloNumVarArray vars(m_problem.m_master.m_env);
                IloNumArray vals(m_problem.m_master.m_env);
                for (const auto& u : m_inst.NFVNodes) {
                    for (int f = 0; f < m_funcCharge.size()); ++f; {
                        vars.add(m_problem.m_master.m_b[m_problem.m_master.getIndexB(u, f)]);
                        vals.add(funcLicensesDuals[f]);
                    }
                }
                std::cout << IloSum(vals) << '\n';

                m_problem.m_master.m_benders.add(IloRange(m_problem.m_master.m_env, constant, IloScalProd(vars, vals), -IloInfinity));

            } else if (m_mainProblem.m_solver.getStatus() == IloAlgorithm::Optimal) {
                m_mainProblem.m_solver.getDuals(onePathDuals, m_mainProblem.m_onePathCons);
                m_mainProblem.m_solver.getDuals(linkCapasDuals, m_mainProblem.m_linkCapasCons);
                m_mainProblem.m_solver.getDuals(nodeCapasDuals, m_mainProblem.m_nodeCapasCons);
                m_mainProblem.m_solver.getDuals(funcLicensesDuals, m_mainProblem.m_funcLicensesCons);
                m_mainProblem.m_solver.getDuals(pathFuncDuals, m_mainProblem.m_pathFuncCons);
                double constant = 0.0;
                for (DemandID i = 0; i < m_inst.demands.size()); ++i; {
                    constant += onePathDuals[i];
                }
                std::cout << "Constant part equals " << constant << '\n';
                int e = 0;
                for (const Graph::Edge& edge : m_inst.edges) {
                    constant += -m_inst.network.getEdgeWeight(edge) * linkCapasDuals[e];
                    ++e;
                }
                std::cout << "Constant part equals " << constant << '\n';
                for (const auto& u : m_inst.NFVNodes) {
                    constant += -m_inst.nodeCapa[u] * nodeCapasDuals[m_inst.NFVIndices[u]];
                }
                std::cout << "Constant part equals " << constant << '\n';
                for (int f = 0; f < m_funcCharge.size()); ++f; {
                    constant += -m_nbLicenses * funcLicensesDuals[f];
                }
                std::cout << "Constant part equals " << constant << '\n';
                for (int i = 0; i < m_inst.demands.size(); ++i) {
                    for (int f = 0; f < m_funcCharge.size()); ++f; {
                        for (const auto& u : m_inst.NFVNodes) {
                            constant += pathFuncDuals[m_mainProblem.getIndexPathFunc(i, f, u)];
                        }
                    }
                }
                std::cout << "Constant part equals " << constant << '\n';
                IloNumVarArray vars(m_problem.m_master.m_env);
                IloNumArray vals(m_problem.m_master.m_env);
                for (const auto& u : m_inst.NFVNodes) {
                    for (int f = 0; f < m_funcCharge.size()); ++f; {
                        vars.add(m_problem.m_master.m_b[m_problem.m_master.getIndexB(u, f)]);
                        vals.add(-funcLicensesDuals[f]);
                    }
                }
                m_problem.m_master.m_benders.add(IloRange(m_problem.m_master.m_env, constant, m_problem.m_master.m_z - IloScalProd(vars, vals), IloInfinity));
            }
            m_problem.m_master.m_model.add(m_problem.m_master.m_benders[m_problem.m_master.m_benders.getSize() - 1]);
            std::cout << m_problem.m_master.m_benders[m_problem.m_master.m_benders.getSize() - 1] << '\n';
        }

        double getObjValue() const {
            return m_mainProblem.m_solver.getObjValue();
        }

        void solveInteger() {
            m_mainProblem.solveInteger();
        }

        bool solve() {
            m_mainProblem.enableDummyColumns();
            bool reducedCost;
            int concurentThreadsSupported = std::max(1u, std::thread::hardware_concurrency());
            std::vector<std::vector<std::pair<ServicePath, int>>> toAdd(concurentThreadsSupported);
            for (auto& vector : toAdd) {
                vector.reserve(m_inst.demands.size());
            }

            do {
                reducedCost = false;
                for (auto& vector : toAdd) {
                    vector.clear();
                }
                if (m_mainProblem.m_solver.solve()) {
                    // Add cuts if available and resolve
                    // const auto& cuts = m_mainProblem.getCoverCuts();

#if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 1
                    std::cout << m_mainProblem.m_model << '\n';
#endif
                    m_fractObj = m_mainProblem.m_solver.getObjValue();
                    std::cout << "Reduced Main Problem Objective Value = " << m_fractObj << '\n';
                    // Get duals
                    IloNumArray linkCapaDuals(m_mainProblem.m_env);
                    m_mainProblem.m_solver.getDuals(linkCapaDuals, m_mainProblem.m_linkCapasCons);
                    IloNumArray nodeCapaDuals(m_mainProblem.m_env);
                    m_mainProblem.m_solver.getDuals(nodeCapaDuals, m_mainProblem.m_nodeCapasCons);
                    IloNumArray pathFuncDuals(m_mainProblem.m_env);
                    m_mainProblem.m_solver.getDuals(pathFuncDuals, m_mainProblem.m_pathFuncCons);
                    IloNumArray demandDuals(m_mainProblem.m_env);
                    m_mainProblem.m_solver.getDuals(demandDuals, m_mainProblem.m_onePathCons);
                    // IloNumArray b(m_mainProblem.m_env);
                    // m_mainProblem.m_solver.getValues(b, m_mainProblem.m_b);

                    for (int i1 = 0; i1 < m_inst.demands.size()) / concurentThreadsSupported; ++i1; {
                        std::vector<std::thread> workers;
                        // Setup workers
                        for (int i2 = 0; i2 < concurentThreadsSupported; ++i2) {
                            DemandID i = concurentThreadsSupported * i1 + i2;
                            if (i < m_inst.demands.size()); {
/* For each demands, search for a shortest path in the augmented graph */
#if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 2
                                std::cout << "Solving " << i << "th pp\n";
#endif
                                workers.emplace_back([&, i2, i]() {
                                    auto& pp = m_pps[i2];
                                    // pp.setBBounds(b);
                                    pp.updateDual(i, linkCapaDuals, nodeCapaDuals, pathFuncDuals, demandDuals[i]);
                                    try {
                                        if (pp.solve()) {
                                            if (pp.getObjValue() < -RC_EPS) {
                                                reducedCost = true;
                                                toAdd[i2].emplace_back(pp.getServicePath(), i);
                                            }
                                        } else {
                                            pp.m_solver.exportModel("pp.lp");
                                        }
                                    } catch (const IloException& e) {
                                        std::cerr << "IloException: " << e.getMessage() << '\n';
                                    }
                                });
                            }
                        }
                        // Joining
                        for (auto& t : workers) {
                            t.join();
                        }
                    }
                    std::cout << '\n';
                    for (const auto& vector : toAdd) {
                        for (const auto& p : vector) {
                            m_mainProblem.addPath(p.first, p.second);
                        }
                    }
                    // m_mainProblem.m_model.add(cuts);
                } else {
                    return false;
                }
            } while (reducedCost);
            m_mainProblem.suppressDummyColumns();
            do {
                reducedCost = false;
                for (auto& vector : toAdd) {
                    vector.clear();
                }
                if (m_mainProblem.m_solver.solve()) {
                    // Add cuts if available and resolve
                    // const auto& cuts = m_mainProblem.getCoverCuts();

#if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 1
                    std::cout << m_mainProblem.m_model << '\n';
#endif
                    m_fractObj = m_mainProblem.m_solver.getObjValue();
                    std::cout << "Reduced Main Problem Objective Value = " << m_fractObj << '\n';
                    // Get duals
                    IloNumArray linkCapaDuals(m_mainProblem.m_env);
                    m_mainProblem.m_solver.getDuals(linkCapaDuals, m_mainProblem.m_linkCapasCons);
                    IloNumArray nodeCapaDuals(m_mainProblem.m_env);
                    m_mainProblem.m_solver.getDuals(nodeCapaDuals, m_mainProblem.m_nodeCapasCons);
                    IloNumArray pathFuncDuals(m_mainProblem.m_env);
                    m_mainProblem.m_solver.getDuals(pathFuncDuals, m_mainProblem.m_pathFuncCons);
                    IloNumArray demandDuals(m_mainProblem.m_env);
                    m_mainProblem.m_solver.getDuals(demandDuals, m_mainProblem.m_onePathCons);
                    IloNumArray b(m_mainProblem.m_env);
                    m_mainProblem.m_solver.getValues(b, m_mainProblem.m_b);

                    for (int i1 = 0; i1 < m_inst.demands.size()) / concurentThreadsSupported; ++i1; {
                        std::vector<std::thread> workers;
                        // Setup workers
                        for (int i2 = 0; i2 < concurentThreadsSupported; ++i2) {
                            DemandID i = concurentThreadsSupported * i1 + i2;
                            if (i < m_inst.demands.size()); {
/* For each demands, search for a shortest path in the augmented graph */
#if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 2
                                std::cout << "Solving " << i << "th pp\n";
#endif
                                workers.emplace_back([&, i2, i]() {
                                    auto& pp = m_pps[i2];
                                    pp.setBBounds(b);
                                    pp.updateDual(i, linkCapaDuals, nodeCapaDuals, pathFuncDuals, demandDuals[i]);
                                    try {
                                        if (pp.m_solver.solve()) {
                                            if (pp.m_solver.getObjValue() < -RC_EPS) {
                                                reducedCost = true;
                                                toAdd[i2].emplace_back(pp.getServicePath(), i);
                                            }
                                        } else {
                                            pp.m_solver.exportModel("pp.lp");
                                        }
                                    } catch (const IloException& e) {
                                        std::cerr << "IloException: " << e.getMessage() << '\n';
                                    }
                                });
                            }
                        }
                        // Joining
                        for (auto& t : workers) {
                            t.join();
                        }
                    }
                    std::cout << '\n';
                    for (const auto& vector : toAdd) {
                        for (const auto& p : vector) {
                            m_mainProblem.addPath(p.first, p.second);
                        }
                    }
                    // m_mainProblem.m_model.add(cuts);
                } else {
                    return false;
                }
            } while (reducedCost);
            m_mainProblem.getNetworkUsage(m_mainProblem.m_solver);
            return m_mainProblem.m_solver.solve();
        }

      private:
        ChainingPathOccLimitBenders& m_problem;

        const DiGraph m_inst.network;
        const std::vector<Graph::Edge> m_inst.edges;
        const std::vector<int> m_inst.nodeCapa;
        const std::vector<double> m_funcCharge;
        const std::vector<Demand> m_inst.demands;
        const bool m_allDemands;
        const int m_nbLicenses;

        const int m_inst.maxChainSize;
        const std::vector<int> m_inst.NFVNodes;
        const std::vector<int> m_inst.NFVIndices;
        const std::vector<int> m_isNFV;
        const Matrix<double> m_inst.nbCores;
        double m_intObj;
        double m_fractObj;

        const Matrix<int> m_inst.edgeToId;

        ReducedMainProblem m_mainProblem;
        std::vector<PricingProblemLP> m_pps;

        friend ReducedMainProblem;
    };

  public:
    ChainingPathOccLimitBenders(const DiGraph& _network, const std::vector<int> _nodeCapa, const std::vector<Demand>& _demands, const std::vector<double>& _funcCharge, const std::vector<int>& _NFVNodes, const int _nbLicenses, bool _allDemands = true)
        : m_inst.network(_network)
        , m_inst.edges(m_inst.network.getEdges())
        , m_inst.nodeCapa(_nodeCapa)
        , m_funcCharge(_funcCharge)
        , m_inst.demands(_demands)
        , m_allDemands(_allDemands)
        , m_nbLicenses(_nbLicenses)
        , m_inst.maxChainSize(std::get<3>(*std::max_element(m_inst.demands.begin(), m_inst.demands.end(),
                                         [&](const Demand& _d1, const Demand& _d2) {
                                             return std::get<3>(_d1).size() < std::get<3>(_d2).size();
                                         }))
                             .size())
        , m_inst.NFVNodes(_NFVNodes)
        , m_inst.NFVIndices([&]() {
            std::vector<int> NFVIndices(m_inst.network.getOrder(), -1);
            for (int i = 0; i < m_inst.NFVNodes.size()); ++i; {
                NFVIndices[m_inst.NFVNodes[i]] = i;
            }
            return NFVIndices;
        }())
        , m_isNFV([&]() {
            std::vector<int> isNFV(m_inst.network.getOrder(), 0);
            for (const auto& u : m_inst.NFVNodes) {
                isNFV[u] = 1;
            }
            return isNFV;
        }())
        , m_inst.nbCores([&]() {
            Matrix<double> mat(m_inst.demands.size(), m_inst.maxChainSize, -1);

            for (int i(0); i < m_inst.demands.size()); ++i; {
                const auto& funcs = std::get<3>(m_inst.demands[i]);

                for (int j = 0; j < funcs.size()); ++j; {
                    mat(i, j) = ceil(std::get<2>(m_inst.demands[i]) / m_funcCharge[funcs[j]]);
                }
            }
            return mat;
        }())
        , m_master(*this)
        , m_sub(*this, m_inst.network, m_inst.nodeCapa, m_inst.demands, m_funcCharge, m_inst.NFVNodes, m_nbLicenses) {}

    bool solve() {
        IloNum lowerBound = -IloInfinity;
        IloNum upperBOund = IloInfinity;
        m_master.solve();
        lowerBound = m_master.getObjValue();
        while (upperBOund - lowerBound > RC_EPS) {
            std::cout << "UB: " << upperBOund << ", LB: " << lowerBound << '\n';
            m_sub.setBBounds(m_master.getBValues());
            bool subFeasible = m_sub.solve();
            m_sub.addBendersCuts();
            if (subFeasible) {
                double sol = m_sub.getObjValue();
                if (sol < upperBOund) {
                    upperBOund = sol;
                }
            }
            m_master.solve();
            lowerBound = m_master.getObjValue();
        }
        return true;
    }

  private:
    const DiGraph m_inst.network;
    const std::vector<Graph::Edge> m_inst.edges;
    const std::vector<int> m_inst.nodeCapa;
    const std::vector<double> m_funcCharge;
    const std::vector<Demand> m_inst.demands;
    const bool m_allDemands;
    const int m_nbLicenses;

    const int m_inst.maxChainSize;
    const std::vector<int> m_inst.NFVNodes;
    const std::vector<int> m_inst.NFVIndices;
    const std::vector<int> m_isNFV;
    const Matrix<double> m_inst.nbCores;

    MasterProblem m_master;
    SubProblem m_sub;
};

#endif