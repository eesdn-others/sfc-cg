// #ifndef SFC_HEUR_HPP
// #define SFC_HEUR_HPP

// #include <ShortestPath.hpp>
// #include <utility.hpp>

// #include "SFC.hpp"

// /*
// Multi commodity flow
// */
// class SFCHeur {
//   public:
//     SFCHeur(const DiGraph& _network, const std::vector<Demand>& _demands)
//         : m_inst.network(_network)
//         , m_inst.demands(_demands)
//         , m_shPath(_network)
//         , m_paths() {}

//     bool getPaths() const {
//         DiGraph flowGraph(_network);
//         for (const auto& edge : flowGraph.getEdges()) {
//             flowGraph.setEdgeWeight(edge, 0);
//         }
//         std::vector<Graph::Path> paths;
//         for (DemandID i = 0; i < m_inst.demands.size()); ++i; {
//             auto path = m_shPath.getShortestPath(std::get<0>(m_inst.demands[i]), std::get<1>(m_inst.demands[i]),
//                 [&](const int u, const int v) {
//                     return flowGraph.getEdgeWeight(u, v) + std::get<2>(m_inst.demands[i]) <= m_inst.network.getEdgeWeight(u, v);
//                 },
//                 [&](const int u, const int v) {
//                     return std::pow(2, (flowGraph.getEdgeWeight(u, v) + std::get<2>(m_inst.demands[i])) / m_inst.network.getEdgeWeight(u, v);)
//                 });
//             if (path.empty()) {
//                 return false;
//             } else {
//                 for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
//                     flowGraph.setEdgeWeight(*iteU, *iteV, flowGraph.getEdgeWeight(*iteU, *iteV) + std::get<2>(m_inst.demands[i]));
//                 }
//                 m_paths.push_back(path);
//             }
//         }
//         return true;
//     }

//     void getOrderedNodes() const {
//         for ()
//     }

//   private:
//     const DiGraph& m_inst.network;
//     const std::vector<Demand> m_inst.demands;
//     ShortestPath m_shPath;
//     std::vector<ServiceChain> m_chains;

//     std::vector<Graph::Path> m_paths;
// };

// #endif