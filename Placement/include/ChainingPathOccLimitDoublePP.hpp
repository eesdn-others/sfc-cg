#ifndef CHAINING_PATH_FUNCOCC_DOUBLEPP_HPP
#define CHAINING_PATH_FUNCOCC_DOUBLEPP_HPP

#include <algorithm>
#include <chrono>
#include <cmath>
#include <ilcplex/ilocplex.h>
#include <iostream>
#include <map>
#include <thread>
#include <tuple>

#include <ShortestPath.hpp>
#include <ThreadPool.hpp>
#include <cplex_utility.hpp>
#include <optional>
#include <utility.hpp>

#include "SFC.hpp"

#define RC_EPS 1.0e-6

namespace SFC::OccLim {
struct FunctionLocation {
    /*FunctionLocation(function_descriptor _f, std::vector<char> _locations)
    : f(_f)
    , locations(std::move(_locations))
    {}*/

    function_descriptor f;
    std::vector<char> locations;
};

class ChainingPathOccFuncDoublePP {
  private:
    class ReducedMainProblem {
        friend ChainingPathOccFuncDoublePP;

      public:
        ReducedMainProblem(const Instance& _inst);
        ReducedMainProblem& operator=(const ReducedMainProblem&) = delete;
        ReducedMainProblem(const ReducedMainProblem&) = delete;
        ReducedMainProblem& operator=(ReducedMainProblem&&) = delete;
        ReducedMainProblem(ReducedMainProblem&&) = delete;
        ~ReducedMainProblem();

        double getObjValue() const;
        void addPath(const ServicePath& _sPath);
        void addLocation(const FunctionLocation& _loc);
        void updateNbLicenses();
        bool checkReducedCosts() const;
        double solveInteger();
        bool solve();
        void getNetworkUsage() const;
        void updateDuals() const;
        std::vector<ServicePath> getServicePaths() const;

      private:
        const Instance* m_inst;
        IloEnv m_env = IloEnv();
        IloModel m_model = IloModel(m_env);
        IloModel m_integerConversions = IloModel(m_env);

        IloNumVarArray m_dummyPaths;
        IloNumVarArray m_y = IloNumVarArray(m_env); /// Path variables
        IloNumVarArray m_dummyLocations;
        IloNumVarArray m_z = IloNumVarArray(m_env); /// Location variables

        IloObjective m_obj; /// Minimizing the bandwidth

        IloRangeArray m_onePathCons;     /// At least one path per constraints
        IloRangeArray m_linkCapasCons;   /// Link capacity constraints
        IloRangeArray m_nodeCapasCons;   /// Node capacity constraints
        IloRangeArray m_oneLocationCons; /// At least one location per function
        IloRangeArray m_pathFuncCons;

        IloNumArray m_onePathDuals = IloNumArray(m_env);
        IloNumArray m_linkCapasDuals = IloNumArray(m_env);
        IloNumArray m_nodeCapasDuals = IloNumArray(m_env);
        IloNumArray m_oneLocationDuals = IloNumArray(m_env);
        IloNumArray m_pathFuncDuals = IloNumArray(m_env);

        IloNumArray m_linkCharge;
        IloNumArray m_nodeCharge;
        IloNumArray m_funcPath;
        IloRangeArray m_pathFuncCons_Utility;

        IloCplex m_solver = IloCplex(m_model);

        std::vector<ServicePath> m_paths;
        std::vector<FunctionLocation> m_localisations;

        int getIndexB(const Graph::Node _u, const function_descriptor _f) const;
        int getIndexPathFunc(const int _i, const function_descriptor _f,
            const Graph::Node _u) const;
    };

    class PathPricingProblem {
        friend ChainingPathOccFuncDoublePP;

      public:
        PathPricingProblem(const Instance& _inst);
        PathPricingProblem& operator=(const PathPricingProblem&) = default;
        PathPricingProblem(const PathPricingProblem&) = default;
        PathPricingProblem& operator=(PathPricingProblem&&) = default;
        PathPricingProblem(PathPricingProblem&&) = default;
        ~PathPricingProblem();

        void updateNbLicenses();
        void setDemandID(const int _i);
        void updateDual(const ReducedMainProblem& _rmp);
        bool solve();
        double getObjValue() const;
        ServicePath getServicePath() const;

      private:
        const Instance* m_inst;
        IloEnv m_env = IloEnv();

        int m_demandID = 0;
        const int n;
        const int m;
        const int nbLayers;
        IloModel m_model = IloModel(m_env);
        IloObjective m_obj;
        /// Variables
        IloNumVarArray m_a;
        IloNumVarArray m_b;
        IloNumVarArray m_f;
        /// Constraints
        IloRangeArray m_flowConsCons;
        IloRangeArray m_linkCapaCons;
        IloRangeArray m_nodeCapaCons;
        IloRangeArray m_funcNodeUsageCons;
        IloRangeArray m_nbLicensesCons;
        IloCplex m_solver; /// Solver

        int getIndexF(const int _e, const int _j) const;
        int getIndexA(const Graph::Node _u, const int _j) const;
        int getIndexB(const Graph::Node _u, const function_descriptor _f) const;

        void updateModelsCoefficients();
    };

    class LocationPricingProblem {
      public:
        LocationPricingProblem(const Instance& _inst);
        void setFunction(const function_descriptor _f);
        FunctionLocation getLocations() const;
        bool solve(const ReducedMainProblem& _rmp);
        double getObjValue() const;

      private:
        const Instance* m_inst;
        function_descriptor m_f;
        FunctionLocation m_funcLocation;
        double m_objValue;
    };

  public:
    ChainingPathOccFuncDoublePP(const Instance& _inst);
    double getObjValue() const;
    void addInitConf(const std::vector<ServicePath>& _paths);
    std::vector<ServicePath> getServicePaths() const;
    bool solve();
    bool solveInteger();
    void printSolution() const;
    void save(const std::string& _filename, const std::pair<double, double>& _time);

  private:
    const Instance* m_inst;

    double m_intObj = -1;
    double m_fractObj = -1;

    ReducedMainProblem m_mainProblem;
    ThreadPool m_pool;
    std::map<std::thread::id, int> m_threadMap;
    std::vector<PathPricingProblem> m_ppps;
    std::vector<LocationPricingProblem> m_lpps;

    friend ReducedMainProblem;
};

} // namespace SFC::OccLim
#endif
