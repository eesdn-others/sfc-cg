#ifndef BRANCH_AND_BOUND_HPP
#define BRANCH_AND_BOUND_HPP

#include <iostream>
#include <limits>
#include <list>
#include <vector>

template <typename Problem>
class BranchAndBound {
  private:
    Problem& m_problem;
    std::list<typename Problem::BBNode> m_nodes;

  public:
    explicit BranchAndBound(Problem& _problem);
    bool solve();
};

template <typename Problem>
BranchAndBound<Problem>::BranchAndBound(Problem& _problem)
    : m_problem(_problem)
    , m_nodes() {
}

template <typename Problem>
bool BranchAndBound<Problem>::solve() {
    // First solve on root node
    typename Problem::BBNode bestNode;
    double bestValue = std::numeric_limits<double>::max();
    if (m_problem.solve()) {
        if (m_problem.isInteger()) {
            std::cout << "Solution at root node is integer, gg no re\n";
        }
        for (const auto& node : m_problem.getNodes()) {
            m_nodes.emplace_back(bestNode, node);
        }
        // Then start branch and bound
        std::cout << "Starting branch and bound\n";
        while (!m_nodes.empty()) {
            std::cout << "Current best solution: " << bestValue << '\n';
            typename Problem::BBNode& currentNode = m_nodes.back();
            m_problem.applyNode(currentNode);
            // Found solution
            if (m_problem.solve()) {
                double currentValue = m_problem.getObjValue();
                if (currentValue < bestValue) {
                    if (m_problem.isInteger()) {
                        // Current node is new best node because it's integer and better than the current best
                        bestNode = currentNode;
                        bestValue = currentValue;
                    } else {
                        for (const auto& node : m_problem.getNodes()) {
                            m_nodes.emplace_back(currentNode, node);
                        }
                    }
                }
            }
            m_nodes.pop_back();
        }
        m_problem.resetBBounds();
        return m_problem.solveInteger();
    }
    std::cerr << "Problem is infeasible!\n";
    return false;
}
#endif
