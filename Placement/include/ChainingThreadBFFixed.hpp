#ifndef CHAINING_PATH_THREAD_BF_HPP
#define CHAINING_PATH_THREAD_BF_HPP

#include <algorithm>
#include <chrono>
#include <cmath>
#include <ilcplex/ilocplex.h>
#include <iostream>
#include <map>
#include <optional>
#include <thread>
#include <tuple>

#include "DiGraph.hpp"
#include "SFC.hpp"
#include "ShortestPathBF.hpp"
#include "ThreadPool.hpp"
#include "cplex_utility.hpp"
#include "utility.hpp"

#define RC_EPS 1.0e-6

namespace SFC::Placed {
class ChainingPathThreadedBFFixed {
    class ReducedMainProblem {
        friend ChainingPathThreadedBFFixed;

      public:
        explicit ReducedMainProblem(const ChainingPathThreadedBFFixed& _placement);
        ReducedMainProblem(const ReducedMainProblem&) = default;
        ReducedMainProblem& operator=(const ReducedMainProblem&) = default;
        ReducedMainProblem(ReducedMainProblem&&) = default;
        ReducedMainProblem& operator=(ReducedMainProblem&&) = default;
        ~ReducedMainProblem();
        void addPath(const ServicePath& _sPath);
        void addPath(int _i);
        double solveInteger();
        void getDuals();
        bool solve();
        double getObjValue() const;
        void getNetworkUsage() const;
        std::vector<ServicePath> getServicePaths() const;

      private:
        ChainingPathThreadedBFFixed const* m_placement;
        IloEnv m_env = IloEnv();
        IloModel m_model = IloModel(m_env);

        IloNumVarArray m_dummyPaths;
        IloNumVarArray m_y;

        IloObjective m_obj;

        IloRangeArray m_onePathCons;
        IloRangeArray m_linkCapasCons;
        IloRangeArray m_nodeCapasCons;

        IloNumArray m_onePathDuals = IloNumArray(m_env);
        IloNumArray m_linkCapasDuals = IloNumArray(m_env);
        IloNumArray m_nodeCapasDuals = IloNumArray(m_env);

        IloNumArray m_linkCharge;
        IloNumArray m_nodeCharge;

        IloCplex m_solver;

        std::vector<ServicePath> m_paths = std::vector<ServicePath>();
    };

    class PricingProblemBF {
        friend ChainingPathThreadedBFFixed;

      public:
        explicit PricingProblemBF(
            const ChainingPathThreadedBFFixed& _chainPlacement);
        PricingProblemBF(const PricingProblemBF&) = default;
        PricingProblemBF& operator=(const PricingProblemBF&) = default;
        PricingProblemBF(PricingProblemBF&&) = default;
        PricingProblemBF& operator=(PricingProblemBF&&) = default;
        ~PricingProblemBF() = default;

        void updateDual(int _i);
        bool solve();
        double getObjValue() const;
        ServicePath getServicePath() const;

      private:
        ChainingPathThreadedBFFixed const* m_placement;
        int n;
        int m;
        int nbLayers;

        int m_demandID = std::numeric_limits<int>::max();

        DiGraph m_layeredGraph;
        ShortestPathBellmanFord<DiGraph> m_bf;
        Graph::Path m_path;
        double m_objValue;
    };

  public:
    ChainingPathThreadedBFFixed(Instance& _inst);
    ChainingPathThreadedBFFixed(const ChainingPathThreadedBFFixed&) = default;
    ChainingPathThreadedBFFixed&
    operator=(const ChainingPathThreadedBFFixed&) = default;
    ChainingPathThreadedBFFixed(ChainingPathThreadedBFFixed&&) = default;
    ChainingPathThreadedBFFixed&
    operator=(ChainingPathThreadedBFFixed&&) = default;
    ~ChainingPathThreadedBFFixed() = default;

    void addInitConf(const std::vector<ServicePath>& _paths);
    void save(const std::string& _filename,
        const std::pair<double, double>& _time);
    bool solveInteger();
    double getObjValue() const;
    bool solve();
    std::vector<ServicePath> getServicePaths() const;

  private:
    SFC::Placed::Instance& m_inst;
    double m_intObj = -1;
    double m_fractObj = -1;

    ReducedMainProblem m_mainProblem;
    ThreadPool m_pool;
    std::map<std::thread::id, int> m_threadID;
    std::vector<PricingProblemBF> m_pps;

    void addInitConf();

    friend ReducedMainProblem;
};

ChainingPathThreadedBFFixed::ChainingPathThreadedBFFixed(Instance& _inst)
    : m_inst(_inst)
    , m_mainProblem(*this)
    , m_pool(std::max(1u, std::thread::hardware_concurrency()))
    , m_threadID([&]() {
        std::map<std::thread::id, int> pps;
        int i = 0;
        for (const auto& id : m_pool.getIds()) {
            pps.emplace(id, i);
            ++i;
        }
        return pps;
    }())
    , m_pps([&]() {
        std::vector<PricingProblemBF> pps;
        pps.reserve(m_threadID.size());
        for (int i = 0; i < m_threadID.size(); ++i) {
            pps.emplace_back(*this);
        }
        return pps;
    }()) {
    addInitConf();
}

void ChainingPathThreadedBFFixed::addInitConf(const std::vector<ServicePath>& _paths) {
    for (int i = 0; i < _paths.size(); ++i) {
        m_mainProblem.addPath(_paths[i]);
    }
}

void ChainingPathThreadedBFFixed::save(const std::string& _filename, const std::pair<double, double>& _time) {
    std::ofstream ofs(_filename);
    ofs << m_intObj << '\t' << m_fractObj << '\n';
    ofs << m_mainProblem.m_paths.size() << std::endl;
    ofs << _time.first << '\t' << _time.second << std::endl;

    std::vector<double> linkUsage(m_inst.network.size(), 0);
    std::vector<int> nodeUsage(m_inst.network.getOrder(), 0);
    std::vector<int> pathsUsed(m_inst.demands.size());

    IloNumArray yVals(m_mainProblem.m_env);
    m_mainProblem.m_solver.getValues(yVals, m_mainProblem.m_y);

    for (int k = 0; k < m_mainProblem.m_paths.size(); ++k) {
        if (IloRound(yVals[k]) > 0) {
            const auto & [ nPath, locations, demandID ] = m_mainProblem.m_paths[k];

            pathsUsed[demandID] = k;

            for (auto iteU = nPath.begin(), iteV = std::next(iteU); iteV != nPath.end();
                 ++iteU, ++iteV) {
                linkUsage[m_inst.edgeToId(*iteU, *iteV)] += m_inst.demands[demandID].d;
            }
            for (int j = 0; j < m_mainProblem.m_paths[k].locations.size(); ++j) {
                nodeUsage[locations[j]] += m_inst.nbCores(demandID, j);
            }
        }
    }

    std::cout << pathsUsed << '\n';

    ofs << m_inst.network.getOrder() << '\n';
    for (Graph::Node u = 0; u < m_inst.network.getOrder(); ++u) {
        ofs << u << '\t' << nodeUsage[u] << '\t' << m_inst.nodeCapa[u] << '\n';
    }

    ofs << m_inst.network.size() << '\n';
    int i = 0;
    for (const auto& edge : m_inst.network.getEdges()) {
        ofs << std::fixed << edge.first << '\t' << edge.second << '\t'
            << linkUsage[i] << '\t' << m_inst.network.getEdgeWeight(edge) << '\n';
        ++i;
    }

    ofs << pathsUsed.size() << '\n';
    for (const auto& k : pathsUsed) {
        const auto & [ nPath, locations, demandID ] = m_mainProblem.m_paths[k];

        ofs << nPath.size() << '\t';
        for (const auto& u : nPath) {
            ofs << u << '\t';
        }
        for (int j = 0; j < locations.size(); ++j) {
            ofs << locations[j] << '\t' << m_inst.demands[demandID].functions[j] << '\t';
        }
        ofs << '\n';
    }
    std::cout << "Results saved to :" << _filename << '\n';
}

double ChainingPathThreadedBFFixed::getObjValue() const { return m_intObj; }

std::vector<ServicePath> ChainingPathThreadedBFFixed::getServicePaths() const {
    return m_mainProblem.getServicePaths();
}

bool ChainingPathThreadedBFFixed::solveInteger() {
    if (!solve()) {
        return false;
    }
    m_intObj = m_mainProblem.solveInteger();
    return m_intObj > 0.0;
}

bool ChainingPathThreadedBFFixed::solve() {
    std::cout << "ChainingPathThreadedBFFixed::solve()\n";
    bool reducedCost;
    std::vector<std::future<std::optional<ServicePath>>> futures;
    futures.reserve(m_inst.demands.size());
    do {
        futures.clear();
        reducedCost = false;
        if (m_mainProblem.solve()) {
            m_fractObj = m_mainProblem.getObjValue();
            std::cout << "Reduced Main Problem Objective Value = " << m_fractObj
                      << ", #cols:" << m_mainProblem.m_paths.size() << '\n';
            m_mainProblem.getDuals();
            std::cout << "Searching for paths..." << std::flush;
            for (int i = 0; i < m_inst.demands.size(); ++i) {
                futures.emplace_back(m_pool.enqueue([&, this, i]() -> std::optional<ServicePath> {
                    // PricingProblem pp(*this);
                    auto& pp = m_pps[m_threadID.at(std::this_thread::get_id())];
                    pp.updateDual(i);
                    if (pp.solve() && pp.getObjValue() < -RC_EPS) { // Return empty Service Path if not
                                                                    // solved or reduced cost not
                                                                    // negative
                        return pp.getServicePath();
                    }
                    return std::nullopt;
                }));
            }
            std::cout << "getting paths..." << std::flush;
            for (auto& fut : futures) {
                auto sPath = fut.get();
                if (sPath.has_value()) {
                    reducedCost = true;
                    m_mainProblem.addPath(*sPath);
                }
            }
            std::cout << "done!\n";
        } else {
            m_mainProblem.m_solver.exportModel("rm.lp");
            std::cout << "No solution found for RMP" << '\n';
        }
    } while (reducedCost);
    return m_fractObj > 0.0;
}

void ChainingPathThreadedBFFixed::addInitConf() {
    for (int i = 0; i < m_inst.demands.size(); ++i) {
        m_mainProblem.addPath(i);
    }
}

// ####################################################################################
// ####################################################################################
// ####################################################################################

ChainingPathThreadedBFFixed::ReducedMainProblem::ReducedMainProblem(
    const ChainingPathThreadedBFFixed& _placement)
    : m_placement(&_placement)
    , m_dummyPaths(IloAdd(m_model, IloNumVarArray(m_env)))
    , m_y(IloAdd(m_model, IloNumVarArray(m_env)))
    , m_obj(IloAdd(m_model, IloMinimize(m_env)))
    , m_onePathCons([&]() {
        IloRangeArray onePathCons(m_env, m_placement->m_inst.demands.size(),
            1.0, 1.0);
        for (int i = 0; i < m_placement->m_inst.demands.size(); ++i) {
            setIloName(onePathCons[i],
                "onePathCons" + toString(m_placement->m_inst.demands[i]));
        }
        return IloAdd(m_model, onePathCons);
    }())
    , m_linkCapasCons([&]() {
        IloRangeArray linkCapaCons(m_env, m_placement->m_inst.network.size(),
            0.0, 0.0);
        int e = 0;
        for (const Graph::Edge& edge : m_placement->m_inst.edges) {
            linkCapaCons[e].setBounds(
                0.0, m_placement->m_inst.network.getEdgeWeight(edge));
            setIloName(linkCapaCons[e], "linkCapas" + toString(edge));
            ++e;
        }
        return IloAdd(m_model, linkCapaCons);
    }())
    , m_nodeCapasCons([&]() {
        IloRangeArray nodeCapasCons(m_env, m_placement->m_inst.NFVNodes.size());
        for (const auto& u : m_placement->m_inst.NFVNodes) {
            const int i = m_placement->m_inst.NFVIndices[u];
            nodeCapasCons[i] = IloAdd(
                m_model, IloRange(m_env, 0.0, m_placement->m_inst.nodeCapa[u]));
            setIloName(nodeCapasCons[i], "nodeCapasCons" + std::to_string(u));
        }
        return nodeCapasCons;
    }())
    , m_linkCharge(m_env, m_placement->m_inst.network.size())
    , m_nodeCharge(m_env, m_placement->m_inst.NFVNodes.size())
    , m_solver([&]() {
        IloCplex solver(m_model);
        solver.setOut(m_env.getNullStream());
        return solver;
    }())
    , m_paths() {}

ChainingPathThreadedBFFixed::ReducedMainProblem::~ReducedMainProblem() {
    m_env.end();
}

std::vector<ServicePath>
ChainingPathThreadedBFFixed::ReducedMainProblem::getServicePaths() const {
    std::vector<ServicePath> sPaths(m_placement->m_inst.demands.size());
    for (int i = 0; i < m_paths.size(); ++i) {
        auto y = m_y[i];
        if (m_solver.getValue(y) > RC_EPS) {
            sPaths[m_paths[i].demand] = m_paths[i];
        }
    }
    return sPaths;
}

void ChainingPathThreadedBFFixed::ReducedMainProblem::addPath(
    const ServicePath& _sPath) {
    assert([&]() {
        const auto ite = std::find(m_paths.begin(), m_paths.end(), _sPath);
        if (ite != m_paths.end()) {
            std::cout << _sPath << " is already present!" << '\n';
            return false;
        }
        return true;
    }());

    const auto & [ nPath, locations, demandID ] = _sPath;

    IloNumArray linkCharge(m_env, m_placement->m_inst.network.size());
    for (int e(0); e < m_placement->m_inst.network.size(); ++e) {
        linkCharge[e] = 0;
    }
    for (auto iteU = nPath.begin(), iteV = std::next(iteU); iteV != nPath.end();
         ++iteU, ++iteV) {
        linkCharge[m_placement->m_inst.edgeToId(*iteU, *iteV)] += m_placement->m_inst.demands[demandID].d;
    }

    /*************************************************************************
    **************************** Node constraints ****************************
    *************************************************************************/

    IloNumArray nodeCharge(m_env, m_placement->m_inst.NFVNodes.size());

    for (IloInt i = 0; i < nodeCharge.getSize(); ++i) {
        nodeCharge[i] = IloNum(0.0);
    }

    for (int j = 0; j < locations.size(); ++j) {
        nodeCharge[m_placement->m_inst.NFVIndices[locations[j]]] += m_placement->m_inst.nbCores(demandID, j);
    }

    m_y.add(m_onePathCons[demandID](1.0) + m_obj(m_placement->m_inst.demands[demandID].d * (nPath.size() - 1))
            + m_linkCapasCons(linkCharge) + m_nodeCapasCons(nodeCharge));

    setIloName(m_y[m_y.getSize() - 1],
        "y" + toString(std::make_tuple(m_placement->m_inst.demands[demandID], _sPath)));
    m_paths.emplace_back(_sPath);
}

void ChainingPathThreadedBFFixed::ReducedMainProblem::addPath(
    const int _i) {
    const auto& demand = m_placement->m_inst.demands[_i];
    m_dummyPaths.add(m_onePathCons[_i](1.0) + m_obj(demand.d * m_placement->m_inst.network.getOrder()));
    setIloName(m_dummyPaths[m_dummyPaths.getSize() - 1], "y" + toString(demand));
}

double ChainingPathThreadedBFFixed::ReducedMainProblem::solveInteger() {
    std::cout << "ChainingPathThreadedBFFixed::ReducedMainProblem::solveInteger\n";
    m_model.add(IloConversion(m_env, m_y, ILOBOOL));
    for (int i = 0; i < m_placement->m_inst.demands.size(); ++i) {
        m_model.add(IloRange(m_env, 0.0, IloNumExprArg(m_dummyPaths[i]), 0.0));
    }

    m_solver.setOut(std::cout);
    if (!m_solver.solve()) {
        return -1.0;
    }
    auto obj = m_solver.getObjValue();
    std::cout << "Final obj value: " << obj << "\t# Paths: " << m_paths.size()
              << '\n';
    getNetworkUsage();
    return obj;
}

void ChainingPathThreadedBFFixed::ReducedMainProblem::getDuals() {
    m_solver.getDuals(m_linkCapasDuals, m_linkCapasCons);
    m_solver.getDuals(m_nodeCapasDuals, m_nodeCapasCons);
    m_solver.getDuals(m_onePathDuals, m_onePathCons);
}

bool ChainingPathThreadedBFFixed::ReducedMainProblem::solve() {
    return m_solver.solve();
}

void ChainingPathThreadedBFFixed::ReducedMainProblem::getNetworkUsage() const {
    std::vector<double> linkUsage(m_placement->m_inst.network.size(), 0);
    std::vector<int> nodeUsage(m_placement->m_inst.network.getOrder(), 0);
    // std::vector<IloNumVar> pathsUsed(m_placement->m_inst.demands.size(),
    // nullptr);

    for (int k = 0; k < m_paths.size(); ++k) {
        const auto& yVal = m_solver.getValue(m_y[k]);
        if (yVal > RC_EPS) {
            const auto & [ nPath, locations, demandID ] = m_paths[k];
            // pathsUsed[m_paths[k].second] =
            // m_y[k];

            for (auto iteU = nPath.begin(), iteV = std::next(iteU); iteV != nPath.end();
                 ++iteU, ++iteV) {
                linkUsage[m_placement->m_inst.edgeToId(*iteU, *iteV)] +=
                    m_placement->m_inst.demands[demandID].d * yVal;
            }

            for (int j = 0; j < locations.size(); ++j) {
                nodeUsage[locations[j]] += m_placement->m_inst.nbCores(demandID, j);
            }
        }
    }
    std::cout << "Node usage: \n";
    for (const auto& u : m_placement->m_inst.NFVNodes) {
        std::cout << '\t' << u << " -> " << nodeUsage[u] << " / "
                  << m_placement->m_inst.nodeCapa[u] << '\n';
        assert(nodeUsage[u] <= m_placement->m_inst.nodeCapa[u]);
    }
    std::cout << "Link usage: \n";
    int i = 0;
    for (const auto& edge : m_placement->m_inst.network.getEdges()) {
        std::cout << std::fixed << '\t' << edge << " -> " << linkUsage[i] << " / "
                  << m_placement->m_inst.network.getEdgeWeight(edge) << '\n';
        assert(m_placement->m_inst.network.getEdgeWeight(edge) - linkUsage[i] > -1.0e-6
               || (std::cerr << std::fixed << m_linkCapasCons[i] << '\n'
                             << m_placement->m_inst.network.getEdgeWeight(edge) - linkUsage[i] << '\n',
                      false));
        ++i;
    }
}

double ChainingPathThreadedBFFixed::ReducedMainProblem::getObjValue() const {
    return m_solver.getObjValue();
}

// ####################################################################################
// ####################################################################################
// ####################################################################################
ChainingPathThreadedBFFixed::PricingProblemBF::PricingProblemBF(
    const ChainingPathThreadedBFFixed& _chainPlacement)
    : m_placement(&_chainPlacement)
    , n(m_placement->m_inst.network.getOrder())
    , m(m_placement->m_inst.network.size())
    , nbLayers(m_placement->m_inst.maxChainSize + 1)
    , m_layeredGraph([&]() {
        DiGraph layeredGraph(n * nbLayers);
        for (const auto& u : m_placement->m_inst.NFVNodes) {
            for (int j = 0; j < nbLayers - 1; ++j) {
                layeredGraph.addEdge(n * j + u, n * (j + 1) + u);
            }
        }
        for (int j = 0; j < nbLayers; ++j) {
            for (const auto& edge : m_placement->m_inst.network.getEdges()) {
                layeredGraph.addEdge(n * j + edge.first, n * j + edge.second);
            }
        }
        return layeredGraph;
    }())
    , m_bf(m_layeredGraph)
    , m_path()
    , m_objValue(0) {}

void ChainingPathThreadedBFFixed::PricingProblemBF::updateDual(
    const int _i) {

    m_demandID = _i;
    // Update func placement
    for (const auto& u : m_placement->m_inst.NFVNodes) {
        for (int j = 0; j < nbLayers - 1; ++j) {
            m_layeredGraph.removeEdge(n * j + u, n * (j + 1) + u);
        }
    }

    const int demandNbLayers =
        m_placement->m_inst.demands[m_demandID].functions.size() + 1;
    for (const auto& u : m_placement->m_inst.NFVNodes) {
        for (int j = 0; j < demandNbLayers - 1; ++j) {
            if (m_placement->m_inst.funcPlacement(
                    u, m_placement->m_inst.demands[m_demandID].functions[j])) {
                m_layeredGraph.addEdge(
                    n * j + u, n * (j + 1) + u,
                    -m_placement->m_mainProblem
                            .m_nodeCapasDuals[m_placement->m_inst.NFVIndices[u]]
                        * m_placement->m_inst.nbCores(m_demandID, j));
            }
        }
        for (int j = demandNbLayers - 1; j < nbLayers - 1; ++j) {
            m_layeredGraph.removeEdge(n * j + u, n * (j + 1) + u);
        }
    }

    const auto edges = m_placement->m_inst.network.getEdges();
    for (int e = 0; e < m; ++e) {
        for (int j = 0; j < demandNbLayers; ++j) {
            m_layeredGraph.addEdge(
                n * j + edges[e].first, n * j + edges[e].second,
                m_placement->m_inst.demands[_i].d * (1 - m_placement->m_mainProblem.m_linkCapasDuals[e]));
        }
        for (int j = demandNbLayers; j < nbLayers; ++j) {
            m_layeredGraph.removeEdge(n * j + edges[e].first,
                n * j + edges[e].second);
        }
    }
}

bool ChainingPathThreadedBFFixed::PricingProblemBF::solve() {
    m_bf.clear();
    m_path = m_bf.getShortestPath(
        m_placement->m_inst.demands[m_demandID].s,
        n * (m_placement->m_inst.demands[m_demandID].functions.size()) + m_placement->m_inst.demands[m_demandID].t);
    m_objValue =
        -m_placement->m_mainProblem.m_onePathDuals[m_demandID] + m_bf.getDistance(n * (m_placement->m_inst.demands[m_demandID].functions.size()) + m_placement->m_inst.demands[m_demandID].t);
    return !m_path.empty();
}

double ChainingPathThreadedBFFixed::PricingProblemBF::getObjValue() const {
    return m_objValue;
}

ServicePath
ChainingPathThreadedBFFixed::PricingProblemBF::getServicePath() const {
    return SFC::getServicePath(m_path, n, m_demandID);
}
} // namespace SFC::Placed

#endif
