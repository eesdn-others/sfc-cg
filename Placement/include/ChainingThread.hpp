// #ifndef CHAINING_PATH_THREAD_HPP
// #define CHAINING_PATH_THREAD_HPP

// #include <algorithm>
// #include <chrono>
// #include <cmath>
// #include <ilcplex/ilocplex.h>
// #include <iostream>
// #include <map>
// #include <optional>
// #include <thread>
// #include <tuple>

// #include "DiGraph.hpp"
// #include "SFC.hpp"
// #include "ShortestPath.hpp"
// #include "ThreadPool.hpp"
// #include "cplex_utility.hpp"
// #include "utility.hpp"

// #define RC_EPS 1.0e-6

// namespace SFC {
// class ChainingPathThreaded {
//     class ReducedMainProblem {
//         friend ChainingPathThreaded;

//       public:
//         explicit ReducedMainProblem(const ChainingPathThreaded& _placement);
//         ReducedMainProblem(const ReducedMainProblem&) = default;
//         ReducedMainProblem& operator=(const ReducedMainProblem&) = default;
//         ReducedMainProblem(ReducedMainProblem&&) = default;
//         ReducedMainProblem& operator=(ReducedMainProblem&&) = default;
//         ~ReducedMainProblem();

//         void addPath(const ServicePath& _sPath);
//         void addPath(const int _i);
//         double solveInteger();
//         void getNetworkUsage() const;
//         void getDuals();
//         bool solve();
//         double getObjValue();

//       private:
//         const ChainingPathThreaded* m_placement;
//         IloEnv m_env{};
//         IloModel m_model;

//         IloNumVarArray m_y;

//         IloObjective m_obj;

//         IloRangeArray m_onePathCons;
//         IloRangeArray m_linkCapasCons;
//         IloRangeArray m_nodeCapasCons;

//         IloNumArray m_onePathDuals;
//         IloNumArray m_linkCapasDuals;
//         IloNumArray m_nodeCapasDuals;

//         IloNumArray m_linkCharge;
//         IloNumArray m_nodeCharge;

//         IloCplex m_solver;

//         std::vector<ServicePath> m_paths;
//     };

//   public:
//     ChainingPathThreaded(Instance& _inst);
//     void addInitConf(const std::vector<ServicePath>& _paths);
//     void addInitConf();
//     void save(const std::string& _filename,
//         const std::pair<double, double>& _time);
//     bool solveInteger();
//     bool solve();

//   private:
//     Instance& m_inst;
//     double m_intObj;
//     double m_fractObj;

//     ReducedMainProblem m_mainProblem;
//     ThreadPool m_pool;
//     std::map<std::thread::id, int> m_threadID;
//     std::vector<PricingProblemLP> m_pps;

//     friend ReducedMainProblem;
// };

// ChainingPathThreaded::ChainingPathThreaded(Instance& _inst)
//     : m_inst(_inst)
//     , m_intObj(-1)
//     , m_fractObj(-1)
//     , m_mainProblem(*this)
//     , m_pool(std::max(1u, std::thread::hardware_concurrency()))
//     , m_threadID([&]() {
//         std::map<std::thread::id, int> pps;
//         int i = 0;
//         for (const auto& id : m_pool.getIds()) {
//             pps.emplace(id, i++);
//         }
//         return pps;
//     }())
//     , m_pps([&]() {
//         std::vector<PricingProblemLP> pps;
//         int concurentThreadsSupported = m_pool.size();
//         pps.reserve(concurentThreadsSupported);
//         for (int i = 0; i < concurentThreadsSupported; ++i) {
//             pps.emplace_back(*this);
//         }
//         return pps;
//     }()) {}

// void ChainingPathThreaded::addInitConf(const std::vector<ServicePath>& _paths) {
//     for (int i = 0; i < _paths.size(); ++i) {
//         m_mainProblem.addPath(_paths[i]);
//     }
// }

// void ChainingPathThreaded::addInitConf() {
//     for (int i = 0; i < m_inst.demands.size(); ++i) {
//         m_mainProblem.addPath(i);
//     }
// }

// void ChainingPathThreaded::save(const std::string& _filename,
//     const std::pair<double, double>& _time) {
//     std::ofstream ofs(_filename);
//     ofs << m_intObj << '\t' << m_fractObj << '\n';
//     ofs << m_mainProblem.m_paths.size() << std::endl;
//     ofs << _time.first << '\t' << _time.second << std::endl;

//     std::vector<double> linkUsage(m_inst.network.size(), 0);
//     std::vector<int> nodeUsage(m_inst.network.getOrder(), 0);
//     std::vector<int> pathsUsed(m_inst.demands.size());

//     for (int k = 0; k < m_mainProblem.m_paths.size(); ++k) {
//         if (m_mainProblem.m_solver.getValue(m_mainProblem.m_y[k]) != 0) {
//             const auto & [ nPath, locations, demandID ] = m_mainProblem.m_paths[k];
//             // const auto& path = m_mainProblem.m_paths[k].first.first;
//             // const auto& funcPlacement = m_mainProblem.m_paths[k].first.second;
//             // const auto& demand = m_inst.demands[m_mainProblem.m_paths[k].second];
//             // const auto d = demand.d;
//             pathsUsed[demandID] = k;

//             for (auto iteU = nPath.begin(), iteV = std::next(iteU); iteV != nPath.end();
//                  ++iteU, ++iteV) {
//                 linkUsage[m_inst.edgeToId(*iteU, *iteV)] +=
//                     m_inst.demands[demandID].d * m_mainProblem.m_solver.getValue(m_mainProblem.m_y[k]);
//             }
//             for (int j = 0; j < m_mainProblem.m_paths[k].locations.size(); ++j) {
//                 nodeUsage[m_mainProblem.m_paths[k].locations[j]] += m_mainProblem.m_solver.getValue(m_mainProblem.m_y[k])
//                                                                     * m_inst.nbCores(m_mainProblem.m_paths[k].demand, j);
//             }
//         }
//     }

//     ofs << m_inst.network.getOrder() << '\n';
//     for (Graph::Node u = 0; u < m_inst.network.getOrder(); ++u) {
//         ofs << u << '\t' << nodeUsage[u] << '\t' << m_inst.nodeCapa[u] << '\n';
//     }

//     ofs << m_inst.network.size() << '\n';
//     int i = 0;
//     for (const auto& edge : m_inst.network.getEdges()) {
//         ofs << std::fixed << edge.first << '\t' << edge.second << '\t'
//             << linkUsage[i] << '\t' << m_inst.network.getEdgeWeight(edge) << '\n';
//         ++i;
//     }

//     ofs << pathsUsed.size() << '\n';
//     for (const auto& index : pathsUsed) {
//         ofs << m_mainProblem.m_paths[index].nPath.size() << '\t';
//         for (const auto& u : m_mainProblem.m_paths[index].nPath) {
//             ofs << u << '\t';
//         }
//         for (int j = 0; j < m_mainProblem.m_paths[index].locations.size(); ++j) {
//             ofs << m_mainProblem.m_paths[index].locations[j] << '\t';
//         }
//         ofs << '\n';
//     }
//     std::cout << "Results saved to :" << _filename << '\n';
// }

// bool ChainingPathThreaded::solveInteger() {
//     if (!solve()) {
//         return false;
//     }
//     m_intObj = m_mainProblem.solveInteger();
//     return m_intObj != -1;
// }

// bool ChainingPathThreaded::solve() {
//     bool reducedCost;
//     do {
//         reducedCost = false;
//         if (m_mainProblem.solve()) {
//             m_fractObj = m_mainProblem.getObjValue();
//             std::cout << "Reduced Main Problem Objective Value = " << m_fractObj << ", #cols:" << m_mainProblem.m_paths.size() << '\n';
//             m_mainProblem.getDuals();
//             // Try to get new path for each demand
//             // Using a thread pool
//             std::cout << "Searching for paths..." << std::flush;
//             std::vector<std::future<std::optional<ServicePath>>> futures;
//             futures.reserve(m_inst.demands.size());
//             for (int i = 0; i < m_inst.demands.size(); ++i) {
//                 futures.emplace_back(
//                     m_pool.enqueue([&, this, i]() -> std::optional<ServicePath> {
//                         auto& pp = m_pps[m_threadID.at(std::this_thread::get_id())];
//                         pp.updateDual(i);
//                         if (pp.solve() && pp.getObjValue() < -RC_EPS) {
//                             return pp.getServicePath();
//                         }
//                         return std::nullopt;
//                     }));
//             }
//             std::cout << "getting paths..." << std::flush;
//             for (auto& fut : futures) {
//                 auto sPath = fut.get();
//                 if (sPath.has_value()) {
//                     reducedCost = true;
//                     m_mainProblem.addPath(sPath.value());
//                 }
//             }
//             std::cout << "done!\n";
//         } else {
//             m_mainProblem.m_solver.exportModel("rm.lp");
//             std::cout << "No solution found for RMP" << '\n';
//         }
//     } while (reducedCost);
//     return m_fractObj != -1; // Why -1
// }

// // #####################################################################
// // #####################################################################
// // #####################################################################

// ChainingPathThreaded::ReducedMainProblem::ReducedMainProblem(
//     const ChainingPathThreaded& _placement)
//     : m_placement(&_placement)
//     , m_env()
//     , m_model(m_env)
//     , m_y(IloAdd(m_model, IloNumVarArray(m_env)))
//     , m_obj(IloAdd(m_model, IloMinimize(m_env)))
//     , m_onePathCons([&]() {
//         IloRangeArray onePathCons(m_env, m_placement->m_inst.demands.size(), 1.0,
//             1.0);
//         for (int i = 0; i < m_placement->m_inst.demands.size(); ++i) {
//             setIloName(onePathCons[i],
//                 "onePathCons" + toString(m_placement->m_inst.demands[i]));
//         }
//         return IloAdd(m_model, onePathCons);
//     }())
//     , m_linkCapasCons([&]() {
//         IloRangeArray linkCapaCons(m_env, m_placement->m_inst.network.size(),
//             0.0, 0.0);
//         int e = 0;
//         for (const Graph::Edge& edge : m_placement->m_inst.edges) {
//             linkCapaCons[e].setBounds(
//                 0.0, m_placement->m_inst.network.getEdgeWeight(edge));
//             setIloName(linkCapaCons[e], "linkCapas" + toString(edge));
//             ++e;
//         }
//         return IloAdd(m_model, linkCapaCons);
//     }())
//     , m_nodeCapasCons([&]() {
//         IloRangeArray nodeCapasCons(m_env, m_placement->m_inst.NFVNodes.size());
//         for (const auto& u : m_placement->m_inst.NFVNodes) {
//             const int i = m_placement->m_inst.NFVIndices[u];
//             nodeCapasCons[i] = IloAdd(
//                 m_model, IloRange(m_env, 0.0, m_placement->m_inst.nodeCapa[u]));
//             setIloName(nodeCapasCons[i], "nodeCapasCons" + std::to_string(u));
//         }
//         return nodeCapasCons;
//     }())
//     , m_onePathDuals(m_env)
//     , m_linkCapasDuals(m_env)
//     , m_nodeCapasDuals(m_env)
//     , m_linkCharge(m_env, m_placement->m_inst.network.size())
//     , m_nodeCharge(m_env, m_placement->m_inst.NFVNodes.size())
//     , m_solver([&]() {
//         IloCplex solver(m_model);
//         solver.setOut(m_env.getNullStream());
//         solver.setParam(IloCplex::Threads, 1);
//         return solver;
//     }())
//     , m_paths() {}

// ChainingPathThreaded::ReducedMainProblem::~ReducedMainProblem() {
//     m_env.end();
// }

// void ChainingPathThreaded::ReducedMainProblem::addPath(const ServicePath& _sPath) {
//     assert([&]() {
//         const auto ite = std::find(m_paths.begin(), m_paths.end(), _sPath);
//         if (ite == m_paths.end()) {
//             return true;
//         } else {
//             std::cout << _sPath << " is already present!" << '\n';
//             return false;
//         }
//     }());

//     const auto & [ nPath, locations, demandID ] = _sPath;

//     for (int e = 0; e < m_placement->m_inst.network.size(); ++e) {
//         m_linkCharge[e] = 0;
//     }
//     for (auto iteU = _sPath.nPath.begin(), iteV = std::next(iteU); iteV != _sPath.nPath.end(); ++iteU, ++iteV) {
//         m_linkCharge[m_placement->m_inst.edgeToId(*iteU, *iteV)] += m_placement->m_inst.demands[demandID].d;
//     }
//     /****************************************************************************************************/
//     /***************************************** Node constraints *****************************************/
//     for (IloInt i = 0; i < m_nodeCharge.getSize(); ++i) {
//         m_nodeCharge[i] = IloNum(0.0);
//     }

//     for (int j = 0; j < _sPath.locations.size(); ++j) {
//         m_nodeCharge[m_placement->m_inst.NFVIndices[_sPath.locations[j]]] += m_placement->m_inst.nbCores(_sPath.demand, j);
//     }

//     IloNumColumn inc = m_onePathCons[demandID](1.0)
//                        + m_obj(m_placement->m_inst.demands[demandID].d * (_sPath.nPath.size() - 1))
//                        + m_linkCapasCons(m_linkCharge)
//                        + m_nodeCapasCons(m_nodeCharge);

//     auto yVar = IloNumVar(inc);
//     // setIloName(yVar, "y" + toString(_sPath));
//     m_y.add(yVar);
//     m_paths.emplace_back(_sPath);
// }

// void ChainingPathThreaded::ReducedMainProblem::addPath(const int _i) {
//     const auto& demand = m_placement->m_inst.demands[_i];
//     const auto d = demand.d;

//     IloNumColumn cplexCol =
//         m_onePathCons[_i](1.0) + m_obj(2 * d * m_placement->m_inst.network.size());

//     auto yVar = IloNumVar(cplexCol);
//     setIloName(yVar, "yDummy" + toString(demand));
//     m_y.add(yVar);
// }

// double ChainingPathThreaded::ReducedMainProblem::solveInteger() {
//     m_model.add(IloConversion(m_env, m_y, ILOBOOL));
//     if (m_solver.solve()) {
//         auto obj = m_solver.getObjValue();
//         std::cout << "Final obj value: " << obj << "\t# Paths: " << m_paths.size() << '\n';
//         getNetworkUsage();
//         return obj;
//     } else {
//         return -1.0;
//     }
// }

// void ChainingPathThreaded::ReducedMainProblem::getNetworkUsage() const {
//     std::vector<double> linkUsage(m_placement->m_inst.network.size(), 0);
//     std::vector<int> nodeUsage(m_placement->m_inst.network.getOrder(), 0);
//     std::vector<IloNumVar> pathsUsed(m_placement->m_inst.demands.size(), nullptr);

//     for (int i = 0; i < m_paths.size(); ++i) {
//         if (m_solver.getValue(m_y[i]) > RC_EPS) {
//             const auto & [ nPath, locations, demandID ] = m_paths[i];
//             pathsUsed[demandID] = m_y[i];
//             for (auto iteU = nPath.begin(), iteV = std::next(iteU); iteV != nPath.end();
//                  ++iteU, ++iteV) {
//                 linkUsage[m_placement->m_inst.edgeToId(*iteU, *iteV)] +=
//                     m_placement->m_inst.demands[demandID].d * m_solver.getValue(m_y[i]);
//             }
//             for (int j = 0; j < locations.size(); ++j) {
//                 nodeUsage[locations[j]] +=
//                     m_solver.getValue(m_y[i]) * m_placement->m_inst.nbCores(demandID, j);
//             }
//         }
//     }
//     std::cout << "Node usage: \n";
//     for (const auto& u : m_placement->m_inst.NFVNodes) {
//         std::cout << '\t' << u << " -> " << nodeUsage[u] << " / "
//                   << m_placement->m_inst.nodeCapa[u] << '\n';
//         assert(nodeUsage[u] <= m_placement->m_inst.nodeCapa[u]);
//     }
//     std::cout << "Link usage: \n";
//     int i = 0;
//     for (const auto& edge : m_placement->m_inst.network.getEdges()) {
//         std::cout << std::fixed << '\t' << edge << " -> " << linkUsage[i] << " / "
//                   << m_placement->m_inst.network.getEdgeWeight(edge) << '\n';
//         assert(m_placement->m_inst.network.getEdgeWeight(edge) - linkUsage[i] > -1.0e-6 || (std::cerr << std::fixed << m_linkCapasCons[i] << '\n'
//                                                                                                       << m_placement->m_inst.network.getEdgeWeight(edge) - linkUsage[i] << '\n',
//                                                                                                false));
//         ++i;
//     }
// }

// void ChainingPathThreaded::ReducedMainProblem::getDuals() {
//     m_solver.getDuals(m_linkCapasDuals, m_linkCapasCons);
//     m_solver.getDuals(m_nodeCapasDuals, m_nodeCapasCons);
//     m_solver.getDuals(m_onePathDuals, m_onePathCons);
// }

// bool ChainingPathThreaded::ReducedMainProblem::solve() {
//     return m_solver.solve();
// }

// double ChainingPathThreaded::ReducedMainProblem::getObjValue() {
//     return m_solver.getObjValue();
// }

// // #####################################################################
// // #####################################################################
// // #####################################################################

// ChainingPathThreaded::PricingProblemLP::PricingProblemLP(
//     const ChainingPathThreaded& _chainPlacement)
//     : m_placement(&_chainPlacement)
//     , m_env()
//     , m_demandID(0)
//     , n(m_placement->m_inst.network.getOrder())
//     , m(m_placement->m_inst.network.size())
//     , nbLayers(m_placement->m_inst.maxChainSize + 1)
//     , m_model(m_env)
//     , m_obj(IloAdd(m_model, IloMinimize(m_env)))
//     , m_a([&]() {
//         IloNumVarArray a(m_env,
//             m_placement->m_inst.NFVNodes.size() * (nbLayers - 1));
//         for (const auto& u : m_placement->m_inst.NFVNodes) {
//             for (int j = 0; j < nbLayers - 1; ++j) {
//                 a[getIndexA(u, j)] =
//                     IloAdd(m_model, IloNumVar(m_env, 0, 1, ILOBOOL));
//                 setIloName(a[getIndexA(u, j)],
//                     "a" + toString(std::make_tuple(j, u)));
//             }
//         }
//         return a;
//     }())
//     , m_f([&]() {
//         IloNumVarArray f(m_env, m * nbLayers, 0, 1, ILOBOOL);
//         for (int e = 0; e < m; ++e) {
//             for (int j = 0; j < nbLayers; ++j) {
//                 setIloName(f[getIndexF(e, j)],
//                     "f" + toString(std::make_tuple(m_placement->m_inst.edges[e], j)));
//             }
//         }
//         return IloAdd(m_model, f);
//     }())
//     , m_flowConsCons([&]() {
//         IloRangeArray flowConsCons(m_env, n * nbLayers, 0.0, 0.0);
//         for (Graph::Node u(0); u < m_placement->m_inst.network.getOrder(); ++u) {
//             for (int j = 0; j < nbLayers; ++j) {
//                 // IloNumExpr expr(m_env);
//                 IloNumVarArray vars(m_env);
//                 IloNumArray vals(m_env);
//                 for (const auto& v : m_placement->m_inst.network.getNeighbors(u)) {
//                     vars.add(m_f[getIndexF(m_placement->m_inst.edgeToId(u, v), j)]);
//                     vals.add(1.0);
//                     vars.add(m_f[getIndexF(m_placement->m_inst.edgeToId(v, u), j)]);
//                     vals.add(-1.0);
//                 }
//                 if (m_placement->m_inst.isNFV[u]) {
//                     if (j > 0) {
//                         vars.add(m_a[getIndexA(u, j - 1)]);
//                         vals.add(-1.0);
//                     }
//                     if (j < nbLayers - 1) {
//                         vars.add(m_a[getIndexA(u, j)]);
//                         vals.add(1.0);
//                     }
//                 }
//                 flowConsCons[n * j + u] = IloAdd(
//                     m_model, IloRange(m_env, 0.0, IloScalProd(vars, vals), 0.0));
//                 vars.end();
//                 vals.end();
//             }
//         }

//         return flowConsCons;
//     }())
//     , m_linkCapaCons([&]() {
//         IloRangeArray linkCapaCons(m_env, m, 0.0, 0.0);
//         for (int e = 0; e < m_placement->m_inst.edges.size(); ++e) {
//             linkCapaCons[e].setUB(m_placement->m_inst.network.getEdgeWeight(
//                 m_placement->m_inst.edges[e]));
//             setIloName(linkCapaCons[e], "linkCapaCons(" + toString(e) + ")");
//         }
//         return IloAdd(m_model, linkCapaCons);
//     }())
//     , m_nodeCapaCons([&]() {
//         IloRangeArray nodeCapaCons(m_env, n);
//         for (const auto& u : m_placement->m_inst.NFVNodes) {
//             nodeCapaCons[u] = IloAdd(
//                 m_model, IloRange(m_env, 0.0, m_placement->m_inst.nodeCapa[u]));
//             setIloName(nodeCapaCons[u],
//                 "nodeCapaCons(" + std::to_string(u) + ")");
//         }
//         return nodeCapaCons;
//     }())
//     , m_solver([&]() {
//         IloCplex solver(m_env);
//         solver.setParam(IloCplex::Threads, 1);
//         solver.setOut(m_env.getNullStream());
//         solver.setWarning(m_env.getNullStream());
//         return solver;
//     }()) {}

// ChainingPathThreaded::PricingProblemLP::~PricingProblemLP() { m_env.end(); }

// int ChainingPathThreaded::PricingProblemLP::getIndexF(
//     const int _e, const int _j) const {
//     assert(_e < m_placement->m_inst.network.size());
//     assert(_j < nbLayers);
//     return m * _j + _e;
// }

// int ChainingPathThreaded::PricingProblemLP::getIndexA(
//     const Graph::Node _u, const int _j) const {
//     assert(_u < m_placement->m_inst.network.getOrder());
//     assert(_j < nbLayers);
//     return m_placement->m_inst.NFVNodes.size() * _j + m_placement->m_inst.NFVIndices[_u];
// }

// bool ChainingPathThreaded::PricingProblemLP::solve() {
//     return m_solver.solve();
// }

// double ChainingPathThreaded::PricingProblemLP::getObjValue() const {
//     return m_solver.getObjValue();
// }

// void ChainingPathThreaded::PricingProblemLP::updateModel(const int _i) {
//     m_solver.clearModel();
//     int nbFunctions =
//         m_placement->m_inst.demands[m_demandID].functions.size();
//     m_flowConsCons[n * 0 + m_placement->m_inst.demands[m_demandID].s].setBounds(
//         0.0, 0.0);
//     m_flowConsCons[n * nbFunctions + m_placement->m_inst.demands[m_demandID].t]
//         .setBounds(0.0, 0.0);

//     m_demandID = _i;
//     nbFunctions = m_placement->m_inst.demands[m_demandID].functions.size();
//     m_flowConsCons[n * 0 + m_placement->m_inst.demands[m_demandID].s].setBounds(
//         1.0, 1.0);
//     m_flowConsCons[n * nbFunctions + m_placement->m_inst.demands[m_demandID].t]
//         .setBounds(-1.0, -1.0);

//     for (int e = 0; e < m_placement->m_inst.edges.size(); ++e) {
//         IloExpr expr(m_env);
//         for (int j = 0; j <= nbFunctions; ++j) {
//             expr += m_f[getIndexF(e, j)] * m_placement->m_inst.demands[m_demandID].d;
//         }
//         m_linkCapaCons[e].setExpr(expr);
//         expr.end();
//     }
//     for (const auto& u : m_placement->m_inst.NFVNodes) {
//         IloExpr expr(m_env);
//         for (int j = 0; j < nbFunctions; ++j) {
//             expr += m_a[getIndexA(u, j)] * m_placement->m_inst.nbCores(m_demandID, j);
//         }
//         m_nodeCapaCons[u].setExpr(expr);
//         expr.end();
//     }
// }

// void ChainingPathThreaded::PricingProblemLP::updateDual(const int _i) {
//     updateModel(_i);
//     const auto& d = m_placement->m_inst.demands[m_demandID].d;
//     const auto& functions = m_placement->m_inst.demands[m_demandID].functions;
//     const auto nbFunctions = functions.size();

//     IloNumExpr expr(m_env, -m_placement->m_mainProblem.m_onePathDuals[_i]);
//     for (int e = 0; e < m; ++e) {
//         for (int j = 0; j <= nbFunctions; ++j) {
//             expr += m_f[getIndexF(e, j)] * (d - d * m_placement->m_mainProblem.m_linkCapasDuals[e]);
//         }
//     }
//     for (const auto& u : m_placement->m_inst.NFVNodes) {
//         for (int j = 0; j < functions.size(); ++j) {
//             expr += m_a[getIndexA(u, j)] * (-m_placement->m_mainProblem.m_nodeCapasDuals[m_placement->m_inst.NFVIndices[u]] * m_placement->m_inst.nbCores(m_demandID, j));
//         }
//     }
//     m_obj.setExpr(expr);
//     expr.end();
//     m_solver.extract(m_model);
// }

// ServicePath ChainingPathThreaded::PricingProblemLP::getServicePath() const {

//     const auto& funcs = m_placement->m_inst.demands[m_demandID].functions;
//     const auto& t = m_placement->m_inst.demands[m_demandID].t;

//     Graph::Node u = m_placement->m_inst.demands[m_demandID].s;
//     ServicePath retval{{u}, {}, m_demandID};
//     int j = 0;

//     IloNumArray aVal(m_env);
//     m_solver.getValues(aVal, m_a);

//     while (u != t || j < funcs.size()) {
//         if (j < funcs.size() && m_placement->m_inst.isNFV[u] && aVal[getIndexA(u, j)]) {
//             retval.locations.push_back(u);
//             ++j;
//         } else {
//             for (const auto& v : m_placement->m_inst.network.getNeighbors(u)) {
//                 if (m_solver.getValue(
//                         m_f[getIndexF(m_placement->m_inst.edgeToId(u, v), j)])
//                     > 0) {
//                     retval.nPath.push_back(v);
//                     u = v;
//                     break;
//                 }
//             }
//         }
//     }

//     assert(retval.nPath.back() == t);
//     return retval;
// }
// } // namespace SFC
// #endif
