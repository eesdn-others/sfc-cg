import os

def isSolved(instanceName, method, percent, nbReplic):
	return os.path.isfile("./results/occlim/{instanceName}_{method}_{percent:.6f}_{nbReplic}.res".format(instanceName=instanceName, method=method, percent=percent, nbReplic=nbReplic))

# os.path.isfile("./results/occlim/{instanceName}_{method}_{percent:.6f}_{nbReplic}.res".format(instanceName=instanceName, method=method, percent=1.000000, nbReplic=nbReplic))

# randoms
def printInstanceGeneration(load=1000000000, pairType="uniform", nbFunctions=5, nbNodes=10,
	chainSize=5, edgeProba=0.100000, seed=0, chainType="cisco"):
	string = "erdosrenyi_{nbNodes}_{edgeProba:.6f}_erdosrenyi_{nbNodes}_{edgeProba:.6f}_{load}_{pairType}_{chainType}_{seed}"
	return string.format(load=int(load), pairType=pairType, nbNodes=nbNodes, edgeProba=float(edgeProba), seed=seed, chainType=chainType)
