import math
import operator
# import all variables initialized in gen_instance
from sage.all import *
import sys
sys.path.append("../../MyPython")
from utility import path_edge_iterator
import sndlib

# True if the graph is given in undirected form
INDIRECT = True
# name of the instance
name = sys.argv[1]
scale_factor = float(sys.argv[2])

# possible values of traffic demand

# key: a function,
# value: fraction of cores used by the function per unit of flow
# in the formulation is delta
delta = [1 / 100000.0, 1 / 100000.0, 1 / 100000.0, 1 / 10000.0, 1 / 1000.0, 1 / 1000.0]

# functions
functions = [0, 1, 2, 3, 4, 5]

# key: demand,
# value: bandwidth demand, chain tuple
# in the formulation is D_st
d, demands = sndlib.getSFCNetwork(name, folder="./instances/energy", p=scale_factor)
# sort demands per chain
for (s, t), (charge, chain) in demands.iteritems():


# max cores per node
node_capacity = sndlib.getNodeCapacities(name, folder="./instances/energy")

# keep track of the edges that are eliminated from the graph
edge_eliminated = {}

charge_edges = {}

# graph

solution = ({}, {})



# TODO:avoid read from file at each traffic level iteration


#
# to export data in the format you use
#

# name = sys.argv[1]
# percent = float(sys.argv[2])

#
#TODO: optimize computation of the shortest paths
# returns a dictionary with the shortest path for each demand
# d = original graph
# d' = copy of d used to calculate all the shortest path, reducing the capacity of the links at each iteration
# d'' = copy of d' where I remove all links with capacity smaller than the charge
# May raise an exception if a path does not exist, in this case backtracking will be applied
#

def get_shortest_paths(d1):
    d2 = d1.copy()

    demands_paths = {}

    for demand in demands:
        source = demand[1]
        dest = demand[2]
        charge = demands[demand]

        # note: shortest path on g'' where g'' contains just the edges greater than the charge requested
        d3 = d2.copy()

        for (u, v, cap) in d3.edges():
            if cap < charge:
                d3.delete_edge(u, v)

        path = d3.shortest_path(source, dest)

        if len(path) == 0:
            raise ValueError('No Path exists - backtracking')

        demands_paths[demand] = path
        for i in range(len(path) - 1):
            old_cap = d2.edge_label(path[i], path[i + 1])
            d2.set_edge_label(path[i], path[i + 1], old_cap - charge)

    return demands_paths

#
# return True if all the edges have been tried
#
def tried_all_edges():
    return all(value == True for value in edge_eliminated.values())


#
# return the energy used calculated as:
# link (u,v) energy = utilization * 100 + 100 if used, 100 if unused and (v,u) used, 0 otherwise
# node energy = 100*core used
#

def get_energy_used(charge_edges, node_demands):
    energy_used = 0

    # nodeEnergy = 0
    # energy used from cores
    for key in node_demands:
        cores_used_node = 0
        for dem in node_demands[key]:
            for fun in demands_functions[dem]:
                cores_used_node += demands[dem] * delta[fun]
        energy_used += math.ceil(cores_used_node) * 100
        # nodeEnergy += math.ceil(cores_used_node) * 100
    # print "nodeEnergy: ", nodeEnergy

    # linkEnergy = 0
    # energy used from links
    for (u, v, cap) in d.edges():
        if charge_edges[(u, v)] > 0 or charge_edges[(v, u)] > 0:
            energy_used += 100
            # linkEnergy += 100

    for (u, v, cap) in d.edges():
        energy_used += (100 * charge_edges[(u, v)] / float(cap))
        # linkEnergy += (100 * charge_edges[(u, v)] / float(cap))
    # print "linkEnergy: ", linkEnergy

    return energy_used

#
# return a dict with the charge on each edges
#
def get_charge_edges(demands_path):
    charge_edges = {}
    for (u, v, cap) in d.edges():
        charge_edges[(u, v)] = 0

    for key in demands_path.keys():
        for i in range(len(demands_path[key]) - 1):
            s1 = demands_path[key][i]
            t1 = demands_path[key][i + 1]
            charge = demands[key]
            charge_edges[(s1, t1)] += charge
    return charge_edges


#
# just in the first iteration
# remove all links with charge = 0
#
def remove_unused_links(d1, charge_edges):
    for (u, v, cap) in d1.edges():
        if charge_edges[(u, v)] == 0:
            d1.delete_edge((u, v))
            edge_eliminated[(u, v)] = True


#
# return the less used link
#
def get_less_used_link(d1, charge_edges):
    # first iteration: remove all unused edges
    if get_less_used_link.firstCall:
        remove_unused_links(d1, charge_edges)
        get_less_used_link.firstCall = False

    min_key = None
    min_value = float("inf")
    for (u, v, cap) in d1.edges():
        usage = charge_edges[(u, v)] / float(cap)
        # print (u,v),"--",charge_edges[(u,v)],"--",float(cap)
        if usage < min_value and not edge_eliminated[(u, v)]:
            min_key = (u, v)
            min_value = usage
    edge_eliminated[min_key] = True
    return min_key


# keep track of the number of times the function is called
get_less_used_link.firstCall = True



# def export_nico_format():
#     # with open(sys.path[0] + "/output/" + name + "_topo.txt", "w") as f:
#     #     for key in demands.keys():
#     #         f.write(str(key[0]) + "\t" + str(key[1]) + "\t" + str(key[2]) + "\t\t" + str(demands[key]) + "\n")
#     # f.close()
#     print "Exporting to ", "./instances/energy/" + name + "_nodeCapa.txt"
#     with open("./instances/energy/" + name + "_nodeCapa.txt", "w") as f:
#         for u in xrange(d.order()):
#             f.write(str(u) + "\t" + str(node_capacity[str(u)]) + "\n")
#     f.close()

#     print "Exporting to ", "./instances/energy/" + name +"_demand.txt"
#     with open("./instances/energy/" + name +"_demand.txt", "w") as f:
#         for (i, s, t), chain in demands_functions.items():
#             f.write("\t".join([str(v) for v in [s, t, "{:.10f}".format(demands[i, s, t])] + chain])+"\n")

def saveResults():
    filename = "./results/energy/{}_{:.6f}_5.init".format(name, scale_factor)
    with open(filename, "w") as f:
        for k in demands.keys():
            # assert len(solution[0]) == len(solution[1])
            path = solution[0][k]
            loc = solution[1][k]
            f.write('\t'.join([str(v) for v in [len(path)] + path + [loc]]) + '\n')
    print "Saved to {}".format(filename)

delta = {0: 1 / 100000.0, 1: 1 / 100000.0, 2: 1 / 100000.0, 3: 1 / 10000.0, 4: 1 / 1000.0, 5: 1 / 1000.0}



# generate instance and initialize variables based on the traffic level
gen()

if scale_factor == 1.0:
    export_nico_format()

# last eliminated edge, needed in case of backtracking
last_eliminated = None

# keep track of the last value for the energy used
last_energy = float("inf")

d1 = d.copy()

# at most |E| times
# until i try all edges
while not tried_all_edges():
    # keep track of the cores used by the assignment during the iteration
    cores_used_iteration = {}
    for u in d.vertices():
        cores_used_iteration[u] = 0
    locations = {}

    try:
        demands_path = get_shortest_paths(d1)
    except ValueError as err:
        # print err
        # if it doesn't find the paths in the original problem
        if last_eliminated == None:
            raise ValueError('Unfeasible edge capacities')
        # else backtracking
        else:
            d1.add_edge(last_eliminated[0], last_eliminated[1],
                        d.edge_label(last_eliminated[0], last_eliminated[1]))

    node_demands = [list() for u in d.vertices() ]

    #
    # for each chain I create a list with the path and one with the demands that use that chain
    #
    nbCores = 0
    for chain in chains:  # [[0, 1, 2, 3, 4]]:
        # print chain
        demands_chain = [] # Set of paths using chain
        demands_key = [] # Set of demands
        # i put all the path relatively to that chain in a list
        for key in demands_functions:
            if demands_functions[key] == chain:
                # path of demands that ask for the chain
                demands_chain.append(demands_path[key])
                # demands that ask for the chain
                demands_key.append(key)

        #
        # here I cover all demands that use that chain
        #
        nodes_chain = []
        for el1 in demands_chain: # el1 is a path
            for el2 in el1: # el2 is a node
                nodes_chain.append(el2)
        # l contains all the nodes of the demands that use that chain
        # count repetition and take the max until all the paths of that demands are covered
        nodes_rep = {}
        for el in nodes_chain: # el is node
            if el not in nodes_rep.keys():
                nodes_rep[el] = 0
        for el in nodes_chain:
            nodes_rep[el] += 1

        # most used nodes by the demands that have the same chain
        sorted_nodes_rep = sorted(nodes_rep.items(), key=operator.itemgetter(1), reverse=True)

        old_size_demands = len(demands_key)

        # most used node by the paths of the selected chain
        for j in range(len(sorted_nodes_rep)):
            selected_node = sorted_nodes_rep[j][0]
            # computing cores left that can be assigned


            cores_left = node_capacity[selected_node] - cores_used_iteration[selected_node]
            # assign all demands that fit in the core left

            indexes_to_remove = []
            for ind in range(len(demands_chain)):
                # print demands_key[ind]
                if selected_node in demands_chain[ind]:
                    cores_requested = 0
                    for fun in demands_functions[demands_key[ind]]:
                        cores_requested += demands[demands_key[ind]] * delta[fun]

                    if cores_requested <= cores_left:
                        node_demands[selected_node].append(demands_key[ind])
                        cores_left -= cores_requested
                        indexes_to_remove.append(ind)
                        cores_used_iteration[selected_node] += cores_requested 
                        nbCores += cores_requested
                        locations[demands_key[ind]] = selected_node
                        # print "Installing {} on node {}".format(ind, selected_node)
            # print cores_requested
            # for u in d.vertices():
                # print u, "(", nodes_rep[u], ")->", cores_used_iteration[u], '/', node_capacity[u], ", ", 
            # print

            # delete assigned demands
            for index in sorted(indexes_to_remove, reverse=True):
                del demands_chain[index]
                del demands_key[index]

        # print demands_key

        new_size_demands = len(demands_key)

        if new_size_demands == old_size_demands or len(demands_key) > 0:
            # for u in d.vertices():
            #     print u, "(", nodes_rep[u], ")->", cores_used_iteration[u], '/', node_capacity[u], ", ", 
            if last_eliminated == None:
                raise ValueError('Unfeasible node capacities')
            # else backtracking
            else:
                d1.add_edge(last_eliminated[0], last_eliminated[1],
                            d.edge_label(last_eliminated[0], last_eliminated[1]))
            break

    if not(new_size_demands == old_size_demands or len(demands_key) > 0):
        # computing charge on edges and energy used
        charge_edges = get_charge_edges(demands_path)
        energy_used = get_energy_used(charge_edges, node_demands)

        # if the energy is increasing from last iteration I put the edge again in the graph
        if energy_used < last_energy:
            last_energy = energy_used
            # assert (len(demands_path) == len(locations))
            solution = (demands_path, locations)
            # saveResults()
        elif energy_used > last_energy:
            d1.add_edge(last_eliminated[0], last_eliminated[1],
                        d.edge_label(last_eliminated[0], last_eliminated[1]))

        # less used link
        edge_to_remove = get_less_used_link(d1, charge_edges)

        # remove less used link and store it, in case of backtracking
        d1.delete_edge(edge_to_remove)
        last_eliminated = (edge_to_remove[0], edge_to_remove[1])
        # break

print scale_factor, "----->", last_energy
# charges = get_charge_edges(demands_path)
# for edge in d.edges(labels=None):
#     print edge, " -> ", charges[edge]

# for node in node_demands:
#     cores_used_node = 0
#     for dem in node_demands[node]:
#         for fun in demands_functions[dem]:
#             cores_used_node += demands[dem] * delta[fun]
#     print node, " -> ", math.ceil(cores_used_node), '/', node_capacity[node]
saveResults()
# break
