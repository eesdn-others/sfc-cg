import csv
import sys

orders = {"atlanta": 15, "internet2": 10, "germany50": 50}

name = sys.argv[1]
factor = float(sys.argv[2])

capas = []
print "./instances/{}_nodeCapa.txt".format(name)
with open("./instances/{}_nodeCapa.txt".format(name), "r") as f:
	datareader = csv.reader(f, delimiter='\t')
	for row in datareader:
		capas.append((int(row[0]), int(row[1])))

print capas
with open("./instances/{}_nodeCapa.txt".format(name), "w") as f:
	dataWriter = csv.writer(f, delimiter='\t')
	for node, capa in capas:
		if capa != -1:
			capa = int(capa*factor)
		dataWriter.writerow([node, capa])