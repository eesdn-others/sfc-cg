import matplotlib.pyplot as plt
import matplotlib
import SFC
import SFC_generators
from sage.all import *

load("./metrics.sage")

labels = {
	1: "0\%",
	1.01: "1\%",
	1.05: "5\%",
	1.1: "10\%",
}

linestyles = {
	1: "-",
	1.01: "--",
	1.05: ":",
	1.1: "-.",
}

markers = {
	"heur_pf": "x",
	"lowerBoud": "+"
}

# ratio = 1
# instanceName = "erdosrenyi_10_0.100000_erdosrenyi_10_0.100000_1000000000_population_0.000000_cisco_0"
# prefix = "./instances/occlim/"
# nwFile = prefix + instanceName + "_topo.txt"
# demandFile = prefix + instanceName + "_demand.txt"
# model = "heur_pf"
# percent = float(1.0)
# g = loadNetwork(nwFile)
# dem, func = loadDemandsAndFunctionReq(demandFile)
# LB = getLowerBound(g, dem)
# allNode, _, _, _, _, _, _, _, _, _ = getCGMetrics("./results/occlim/{instanceName}_{model}_{percent:.6f}_{nbReplic}.res".format(instanceName=instanceName, model=model, percent=percent, nbReplic=g.order()))

def average(llist):
	return sum(llist) / float(len(llist)) if len(llist) > 0 else -1

def latexify(fig_width=None, fig_height=None, columns=1, fontsize=20):
	assert(columns in [1,2])
	fig_widths = [3.39, 6.9]
	if fig_width is None:
	    fig_width = fig_widths[columns-1] # width in inches

	if fig_height is None:
	    fig_height = fig_width*((sqrt(5)-1.0)/2.0) # height in inches

	MAX_HEIGHT_INCHES = 8.0
	if fig_height > MAX_HEIGHT_INCHES:
	    print("WARNING: fig_height too large:" + fig_height + 
	          "so will reduce to" + MAX_HEIGHT_INCHES + "inches.")
	    fig_height = MAX_HEIGHT_INCHES

	params = {'backend': 'pdf',
	          # 'text.latex.preamble': ['\usepackage{gensymb}'],
	          'axes.labelsize': 10, # fontsize for x and y labels (was 10)
	          'axes.titlesize': 10,
	          'font.size': fontsize, # was 10
	          'legend.fontsize': 10, # was 10
	          'xtick.labelsize': 10,
	          'ytick.labelsize': 10,
	          'text.usetex': True,
	          'figure.figsize': [fig_width, fig_height],
	          'font.family': 'serif', 
	}
	matplotlib.rcParams.update(params)

def plotCriticalLicences_vs_nbNodes(nbNodesRange=SFC_generators.NB_NODES_RANGE, model="heur_pf", critRatios=[1, 1.01, 1.05, 1.1]):
	print "nbNodes"
	legendsLines = []
	legendsLabels = []

	plt.clf()
	fig, ax1 = plt.subplots()
	for critRatio in critRatios:
		ySeed = [list(filter(lambda x: x != -1, [getCriticalNbLicense(SFC.printInstanceGeneration(nbNodes=nbNodes, seed=seed), ratio=critRatio) for seed in SFC_generators.SEEDS])) 
			for nbNodes in nbNodesRange]
		ySeed = list(filter(lambda x: x != -1, ySeed))
		print ySeed
		line, = ax1.plot(nbNodesRange, list(map(average, ySeed)), marker=markers[model], linestyle=linestyles[critRatio]) 
		print list(map(average, ySeed))
		legendsLines.append(line)
		legendsLabels.append(labels[critRatio])
	
	ax1.set_xlabel("\# Nodes")
	ax1.set_xlim(min(nbNodesRange), max(nbNodesRange))

	ax1.set_ylabel("Critical number of licenses")
	ax1.set_ylim(0, max(nbNodesRange))

	ax2 = ax1.twinx()
	yBWSeed = [list(filter(lambda x: x != -1e-6, [getMinimumBandwidth(SFC.printInstanceGeneration(nbNodes=nbNodes, seed=seed)) / float(1e6) for seed in SFC_generators.SEEDS])) for nbNodes in nbNodesRange]
	print "bandwidth: ", yBWSeed
	line, = ax2.plot(nbNodesRange, list(map(average, yBWSeed)), linestyle="-", color="black")
	legendsLines.append(line)
	legendsLabels.append("Min. bandwidth")
	ax2.set_ylabel("Minimum bandwidth required")
	ax2.set_ylim(1000, 3000)

	plt.legend(legendsLines, legendsLabels, loc='upper center', bbox_to_anchor=(0.5, 1.45),
          ncol=3, fancybox=True, shadow=True)

	# plt.legend(loc="best", ncol=2)
	plt.savefig("figures/occlim/random_n_critLicense.pdf", bbox_inches='tight')

def plotCriticalLicences_vs_edgeProba(edgeProbaRange=SFC_generators.PROBABILITY_RANGE, model="heur_pf", critRatios=[1, 1.01, 1.05, 1.1]):
	print "edgeProba"
	legendsLines = []
	legendsLabels = []

	plt.clf()
	fig, ax1 = plt.subplots()
	for critRatio in critRatios:
		yRatioSeed = [list(filter(lambda x: x != -1, [getCriticalNbLicense(SFC.printInstanceGeneration(edgeProba=edgeProba, seed=seed), ratio=critRatio) for seed in SFC_generators.SEEDS]))
			for edgeProba in edgeProbaRange]
		print yRatioSeed
		line, = ax1.plot(edgeProbaRange, list(map(average, yRatioSeed)), marker=markers[model], linestyle=linestyles[critRatio])
		legendsLines.append(line)
		legendsLabels.append(labels[critRatio])

	ax1.set_xlabel("Edge probability")
	ax1.set_xlim(0.1, 1)
	ax1.set_ylabel("Critical number of licenses")
	ax1.set_ylim(0, 10)	
	# plt.legend(loc="best", ncol=2)

	ax2 = ax1.twinx()
	yBWSeed = [list(filter(lambda x: x != -1e-6, [getMinimumBandwidth(SFC.printInstanceGeneration(edgeProba=edgeProba, seed=seed)) / float(1e6) for seed in SFC_generators.SEEDS])) for edgeProba in edgeProbaRange]
	print "bandwidth: ", yBWSeed
	line, = ax2.plot(edgeProbaRange, list(map(average, yBWSeed)), label="Min. band", linestyle="-", color="black")
	legendsLines.append(line)
	legendsLabels.append("Min. bandwidth")
	ax2.set_ylabel("Minimum bandwidth required")
	ax2.set_ylim(1000, 3000)

	plt.legend(legendsLines, legendsLabels, loc='upper center', bbox_to_anchor=(0.5, 1.45),
          ncol=3, fancybox=True, shadow=True)
	plt.savefig("figures/occlim/random_p_critLicense.pdf", bbox_inches='tight')

def plotCriticalLicences_vs_load(loadRange=SFC_generators.LOAD_RANGE, model="heur_pf", critRatios=[1, 1.01, 1.05, 1.1]):
	print "load"
	legendsLines = []
	legendsLabels = []

	xRange = [load / float(1e6) for load in loadRange]
	plt.clf()
	fig, ax1 = plt.subplots()
	for critRatio in critRatios:
		yRatioSeed = [list(filter(lambda x: x != -1, [getCriticalNbLicense(SFC.printInstanceGeneration(load=load, seed=seed), ratio=critRatio) for seed in SFC_generators.SEEDS]))
			for load in loadRange]
		print yRatioSeed
		line, = ax1.plot(xRange, list(map(average, yRatioSeed)), label=labels[critRatio], marker=markers[model], linestyle=linestyles[critRatio])
		legendsLines.append(line)
		legendsLabels.append(labels[critRatio])

	ax1.set_xlabel("Load (Gb)")
	ax1.set_xscale("log")
	ax1.set_xticks(xRange)
	
	ax1.set_ylabel("Critical number of licenses")
	ax1.set_ylim(0, 10)
	# plt.legend(loc="best", ncol=2)

	ax2 = ax1.twinx()
	yNbDemands = [list(filter(lambda x: x != -1, [getNbDemands(SFC.printInstanceGeneration(load=load, seed=seed)) for seed in SFC_generators.SEEDS])) for load in loadRange]
	print "bandwidth: ", yNbDemands
	line, = ax2.plot(xRange, list(map(average, yNbDemands)), label="Min. band", linestyle="-", color="black")

	legendsLines.append(line)
	legendsLabels.append("\# demands")

	ax2.set_ylabel("\# Demands")
	ax2.set_xscale("log")

	plt.legend(legendsLines, legendsLabels, loc='upper center', bbox_to_anchor=(0.5, 1.45),
          ncol=3, fancybox=True, shadow=True)
	plt.savefig("figures/occlim/random_load_critLicense.pdf", bbox_inches='tight')

def plotCriticalLicences_vs_ratio(ratioRange=SFC_generators.HETERO_RATIO_RANGE, model="heur_pf", critRatios=[1, 1.01, 1.05, 1.1]):
	print "ratio"
	legendsLines = []
	legendsLabels = []

	plt.clf()
	fig, ax1 = plt.subplots()
	for critRatio in critRatios:
		ySeed = [list(filter(lambda x: x != -1, [getCriticalNbLicense(SFC.printInstanceGeneration(pairType=SFC_generators.getPopulationName(ratio=ratio), seed=seed), ratio=critRatio) for seed in SFC_generators.SEEDS]))
			for ratio in ratioRange]
		print ySeed
		line, = ax1.plot(ratioRange, list(map(average, ySeed)), label=labels[critRatio], marker=markers[model], linestyle=linestyles[critRatio])
		legendsLines.append(line)
		legendsLabels.append(labels[critRatio])
	
	ax1.set_xlabel("$\\alpha$")

	ax1.set_ylabel("Critical number of licenses")
	ax1.set_ylim(0, 10)
	
	ax2 = ax1.twinx()
	yBWSeed = [list(filter(lambda x: x != -1e-6, [getMinimumBandwidth(SFC.printInstanceGeneration(pairType=SFC_generators.getPopulationName(ratio=ratio), seed=seed)) / float(1e6) for seed in SFC_generators.SEEDS])) 
		for ratio in ratioRange]
	print "bandwidth: ", yBWSeed
	line, = ax2.plot(ratioRange, list(map(average, yBWSeed)), label="Min. band", linestyle="-", color="black")
	legendsLines.append(line)
	legendsLabels.append("Min. bandwidth")
	ax2.set_ylabel("Minimum bandwidth required")
	ax2.set_ylim(1000, 3000)

	# plt.legend(loc="best", ncol=2)

	plt.legend(legendsLines, legendsLabels, loc='upper center', bbox_to_anchor=(0.5, 1.45),
          ncol=3, fancybox=True, shadow=True)
	plt.savefig("figures/occlim/random_ratio_critLicense.pdf", bbox_inches='tight')

def plotCriticalLicences_vs_nbChains(nbChainsRange=SFC_generators.NB_CHAINS_RANGE, model="heur_pf", critRatios=[1, 1.01, 1.05, 1.1]):
	print "nbChains"
	legendsLines = []
	legendsLabels = []

	plt.clf()
	fig, ax1 = plt.subplots()
	for critRatio in critRatios:
		ySeed = [list(filter(lambda x: x != -1, [getCriticalNbLicense(SFC.printInstanceGeneration(chainType=SFC_generators.getUniformName(nbChains=nbChains), seed=seed), ratio=critRatio) for seed in SFC_generators.SEEDS]))
			for nbChains in nbChainsRange]
		print ySeed
		line, = ax1.plot(nbChainsRange, list(map(average, ySeed)), label=labels[critRatio], marker=markers[model], linestyle=linestyles[critRatio])
		legendsLines.append(line)
		legendsLabels.append(labels[critRatio])
	
	ax1.set_xlabel("\# Chains")
	ax1.set_ylabel("Critical number of licenses")
	ax1.set_ylim(0, 10)

	# plt.legend(loc="best", ncol=2)

	ax2 = ax1.twinx()
	yBWSeed = [list(filter(lambda x: x != -1e-6, [getMinimumBandwidth(SFC.printInstanceGeneration(chainType=SFC_generators.getUniformName(nbChains=nbChains), seed=seed)) / float(1e6) for seed in SFC_generators.SEEDS]))
		for nbChains in nbChainsRange]	
	print "bandwidth: ", yBWSeed
	line, = ax2.plot(nbChainsRange, list(map(average, yBWSeed)), label="Min. band", linestyle="-", color="black")
	legendsLines.append(line)
	legendsLabels.append("Min. bandwidth")
	ax2.set_ylabel("Minimum bandwidth required")
	ax2.set_ylim(1000, 3000)
	
	plt.legend(legendsLines, legendsLabels, loc='upper center', bbox_to_anchor=(0.5, 1.45),
          ncol=3, fancybox=True, shadow=True)
	plt.savefig("figures/occlim/random_nbChains_critLicense.pdf", bbox_inches='tight')

def plotCriticalLicences_vs_nbFunctions(nbFunctionsRange=SFC_generators.NB_FUNCTIONS_RANGE, model="heur_pf", critRatios=[1, 1.01, 1.05, 1.1]):
	print "nbFunctions"
	legendsLines = []
	legendsLabels = []

	plt.clf()
	fig, ax1 = plt.subplots()
	for critRatio in critRatios:
		ySeed = [list(filter(lambda x: x != -1, [getCriticalNbLicense(SFC.printInstanceGeneration(chainType=SFC_generators.getUniformName(nbFunctions=nbFunctions), seed=seed), ratio=critRatio) for seed in SFC_generators.SEEDS]))
			for nbFunctions in nbFunctionsRange]
		print ySeed
		line, = ax1.plot(nbFunctionsRange, list(map(average, ySeed)), label=labels[critRatio], marker=markers[model], linestyle=linestyles[critRatio])
		legendsLines.append(line)
		legendsLabels.append(labels[critRatio])
	
	ax1.set_xlabel("\# functions")

	ax1.set_ylabel("Critical number of licenses")
	ax1.set_ylim(0, 10)
	# plt.legend(loc="best", ncol=2)
	
	ax2 = ax1.twinx()
	yBWSeed = [list(filter(lambda x: x != -1e-6, [getMinimumBandwidth(SFC.printInstanceGeneration(chainType=SFC_generators.getUniformName(nbFunctions=nbFunctions), seed=seed)) / float(1e6) for seed in SFC_generators.SEEDS]))
		for nbFunctions in nbFunctionsRange]
	print "bandwidth: ", yBWSeed
	line, = ax2.plot(nbFunctionsRange, list(map(average, yBWSeed)), label="Min. band", linestyle="-", color="black")
	legendsLines.append(line)
	legendsLabels.append("Min. bandwidth")
	ax2.set_ylabel("Minimum bandwidth required")
	ax2.set_ylim(1000, 3000)

	plt.legend(legendsLines, legendsLabels, loc='upper center', bbox_to_anchor=(0.5, 1.45),
          ncol=3, fancybox=True, shadow=True)
	plt.savefig("figures/occlim/random_nbFunctions_critLicense.pdf", bbox_inches='tight')

def plotCriticalLicences_vs_chainSize(chainSizeRange=SFC_generators.CHAIN_SIZE_RANGE, model="heur_pf", critRatios=[1, 1.01, 1.05, 1.1]):
	print "chainSize"
	legendsLines = []
	legendsLabels = []

	plt.clf()
	fig, ax1 = plt.subplots()
	for critRatio in critRatios:
		ySeed = [list(filter(lambda x: x != -1, [getCriticalNbLicense(SFC.printInstanceGeneration(chainType=SFC_generators.getUniformName(chainSize=chainSize), seed=seed), ratio=critRatio) for seed in SFC_generators.SEEDS])) 
			for chainSize in chainSizeRange]
		ySeed = list(filter(lambda x: x != -1, ySeed))
		print ySeed
		line, = ax1.plot(chainSizeRange, list(map(average, ySeed)), label=labels[critRatio], marker=markers[model], linestyle=linestyles[critRatio])
		legendsLines.append(line)
		legendsLabels.append(labels[critRatio])
	
	ax1.set_xlabel("Chains size")

	ax1.set_ylabel("Critical number of licenses")
	ax1.set_ylim(0, 10)
	# plt.legend(loc="upper left", ncol=2)
	
	ax2 = ax1.twinx()
	yBWSeed = [list(filter(lambda x: x != -1e-6, [getMinimumBandwidth(SFC.printInstanceGeneration(chainType=SFC_generators.getUniformName(chainSize=chainSize), seed=seed)) / float(1e6) for seed in SFC_generators.SEEDS]))
		for chainSize in chainSizeRange]
	print "bandwidth: ", yBWSeed
	line, = ax2.plot(chainSizeRange, list(map(average, yBWSeed)), label="Min. band", linestyle="-", color="black")
	legendsLines.append(line)
	legendsLabels.append("Min. bandwidth")
	ax2.set_ylabel("Minimum bandwidth required")
	ax2.set_ylim(1000, 3000)
	
	plt.legend(legendsLines, legendsLabels, loc='upper center', bbox_to_anchor=(0.5, 1.45),
          ncol=3, fancybox=True, shadow=True)
	plt.savefig("figures/occlim/random_chainSize_critLicense.pdf", bbox_inches='tight')

if __name__ == '__main__':
	latexify()
	plotCriticalLicences_vs_nbNodes()
	plotCriticalLicences_vs_edgeProba()
	plotCriticalLicences_vs_nbChains()
	plotCriticalLicences_vs_chainSize()
	plotCriticalLicences_vs_nbFunctions()
	plotCriticalLicences_vs_ratio()
	plotCriticalLicences_vs_load()