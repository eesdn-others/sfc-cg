import sys

GRAPH_ORDERS = {
	"atlanta": 15,
	"internet2": 10,
	"germany50": 50,
	"ta2": 65
}
 
if __name__ == '__main__':
	genAll([sys.argv[1]] if sys.argv[1] != "all" else GRAPH_ORDERS.keys())

def genAll(names, nbThreads):
	for name in names:
		for i in range(1, GRAPH_ORDERS[name]+1):
			instName = "{}_emb_{}".format(name, i)
			print "./LP {} -m pathThreadBF -j {}".format(instName, nbThreads)
