#include "MyTimer.hpp"
#include "tclap/CmdLine.hpp"

// #include "AugmentedGraph.hpp"
#include "ChainingThread.hpp"
#include "ChainingThreadBF.hpp"
#include "SFC.hpp"

struct Param {
    std::string model;
    std::string name;
    int nbThreads;
    double factor;
    double percentDemand;
};

Param getParams(int argc, char** argv);
Param getParams(int argc, char** argv) {
    TCLAP::CmdLine cmd("Solve the SFC problem with column generation", ' ', "1.0");

    TCLAP::ValuesConstraint<std::string> vCons({"pathThreadBF", "pathThreadBF_noclear", "pathThread"});
    TCLAP::ValueArg<std::string> modelName("m", "model", "Specify the model used",
        false, "pathThreadBF", &vCons);
    cmd.add(&modelName);

    TCLAP::UnlabeledValueArg<std::string> networkName(
        "network", "Specify the network name", true, "", "network");
    cmd.add(&networkName);

    TCLAP::ValueArg<double> demandFactor(
        "d", "demandFactor", "Specify the multiplicative factor for the demands",
        false, 1, "double");
    cmd.add(&demandFactor);

    TCLAP::ValueArg<int> nbThreads(
        "j", "nbThreads", "Specify the number of threads",
        false, 1, "int");
    cmd.add(&nbThreads);

    TCLAP::ValueArg<double> percentDemand(
        "p", "percentDemand",
        "Specify the percentage of demand used (0 <= p <= 100)", false, 100.0,
        "double");
    cmd.add(&percentDemand);

    cmd.parse(argc, argv);

    return {modelName.getValue(), networkName.getValue(), nbThreads.getValue(),
        demandFactor.getValue(), percentDemand.getValue()};
}

int main(int argc, char** argv) {
    std::cout << std::fixed;
    try {
        auto params = getParams(argc, argv);

        const DiGraph<double> network = SFC::loadNetwork("./instances/" + params.name + "_topo.txt");
        const auto[allDemands, funcCharge] = SFC::loadDemands("./instances/" + params.name + "_demand.txt", params.factor);
        auto nodeCapas = SFC::loadNodeCapa("./instances/" + params.name + "_nodeCapa.txt");
        std::cout << "Files loaded...\n";

        const int nbDemands =
            static_cast<int>(std::ceil(params.percentDemand * allDemands.size() / 100.0));
        if (params.percentDemand < 100.0) {
            std::cout << "Using only " << nbDemands << " / " << allDemands.size()
                      << " demands.\n";
        }
        std::vector<SFC::Demand> demands = std::vector<SFC::Demand>(allDemands.begin(),
            allDemands.begin() + nbDemands);

        int totalNbCores = 0;
        for (const auto& demand : demands) {
            for (int j = 0; j < demand.functions.size(); ++j) {
                totalNbCores += ceil(demand.d / funcCharge[demand.functions[j]]);
            }
        }
        std::cout << totalNbCores << " needed cores!" << '\n';

        std::vector<int> NFVNodes;
        for (Graph::Node u(0); u < network.getOrder(); ++u) {
            if (nodeCapas[u] != -1) {
                NFVNodes.push_back(u);
            }
        }
        int availableCores = 0;
        for (const auto& u : NFVNodes) {
            availableCores += nodeCapas[u];
        }
        std::cout << availableCores << " cores available !\n";

        SFC::Instance inst(network, nodeCapas, demands, funcCharge, NFVNodes);
        Time timer;
        timer.start();
        // std::cout << "Getting initial configuration...\n";
        // SFC::AugmentedGraph augGraph(inst);
        // /* Find first initial configuration */
        // const auto sPaths = augGraph.getInitialConfiguration(demands);
        // // augGraph.showNetworkUsage();

        // if (sPaths.size() < demands.size()) {
        //     std::cout << "No initial configuration\n";
        //     return 1;
        // }
        std::string filename =
            epsilon_equal<double>()(params.percentDemand, 100)
                ? "./results/" + params.name + "_" + params.model + "_" + std::to_string(params.factor) + ".res"
                : "./results/" + params.name + "_" + std::to_string(nbDemands) + "_" + params.model + "_" + std::to_string(params.factor) + ".res";
        if (params.model == "pathThreadBF_noclear") {
            ColumnGenerationModel<SFC::Instance, SFC::PlacementDWD_Path> css(&inst);
            try {
                if (css.solve<SFC::PricingProblemBF>(params.nbThreads)) {
                    css.getSolution().save(filename, timer.get());
                }
            } catch (IloException& e) {
                std::cout << e.getMessage() << '\n';
            }
        } else if (params.model == "pathThreadBF") {
            ColumnGenerationModel<SFC::Instance, SFC::PlacementDWD_Path> css(&inst);
            try {
                if (css.solve<SFC::PricingProblemBF>(params.nbThreads)) {
                    css.getSolution().save(filename, timer.get());
                }
            } catch (IloException& e) {
                std::cout << e.getMessage() << '\n';
            }
        } else if (params.model == "pathThread") {
            ColumnGenerationModel<SFC::Instance, SFC::PlacementDWD_Path> css(&inst);
            try {
                if (css.solve<SFC::PricingProblemLP>(params.nbThreads)) {
                    css.getSolution().save(filename, timer.get());
                }
            } catch (IloException& e) {
                std::cout << e.getMessage() << '\n';
            }
        } else {
            std::cout << "No method specified!\n";
        }
    } catch (const TCLAP::ArgException& e) {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << '\n';
    }
}
