
#include <MyTimer.hpp>

#include "AugmentedGraph.hpp"

std::string getDemandFile(const std::string& name, int _i);
std::vector<Demand> loadDemands(const std::string& _filename, double _percent);
std::vector<int> loadNodeCapa(const std::string& _filename,
    const double _percent = 1.0);

// DiGraph loadNetwork(const std::string& _filename);
// std::vector<double> loadFunctions(const std::string& _filename);
// void saveNodeCapas(const std::vector<int>& _nodeCapas, const std::string&
// _filename); void saveTopo(const DiGraph& _graph, const std::string&
// _filename);

int main(int argc, char** argv) {
    std::cout << std::fixed;
    if (argc < 4) {
        std::cerr << "Missing arguments: {name} {percent} {funcCoef}\n";
    } else {
        const std::string name = argv[1];
        const double percent = std::stod(argv[2]);
        const int funcCoef = std::stoi(argv[3]);

        const DiGraph network = loadNetwork("./instances/" + name + "_topo.txt");
        // const std::string demandFile = getDemandFile(name, numDemand);
        // const std::vector<Demand> demands = loadDemands(demandFile, percent);
        const std::vector<Demand> demands = [&]() {
            auto demands =
                loadDemands("./instances/" + name + "_demand.txt", percent);
            // std::sort(demands.begin(), demands.end(), [&](const Demand& _d1, const
            // Demand& _d2) {
            //     return std::get<2>(_d1) < std::get<2>(_d2);
            // });
            return demands;
        }();
        const std::vector<double> funcCharge =
            loadFunctions("./instances/func" + std::to_string(funcCoef) + ".txt");
        std::vector<int> nodeCapas =
            loadNodeCapa("./instances/" + name + "_nodeCapa.txt");
        std::cout << "Files loaded...\n";

        int totalNbCores = 0;
        for (int i(0); i < int(demands.size()); ++i) {
            const auto& funcs = std::get<3>(demands[i]);

            for (int j(0); j < int(funcs.size()); ++j) {
                totalNbCores += ceil(std::get<2>(demands[i]) / funcCharge[funcs[j]]);
            }
        }
        std::cout << totalNbCores << " needed cores!" << '\n';

        std::vector<int> NFVNodes;
        for (Graph::Node u(0); u < network.getOrder(); ++u) {
            if (nodeCapas[u] != -1) {
                NFVNodes.push_back(u);
            }
        }
        int availableCores = 0;
        for (const auto& u : NFVNodes) {
            availableCores += nodeCapas[u];
        }
        std::cout << availableCores << " cores available !\n";

        Time timer;
        timer.start();
        std::cout << "Getting initial configuration...\n";
        AugmentedGraph augGraph(network, funcCharge, NFVNodes, nodeCapas);
        /*Find first initial configuration*/
        auto sPaths = augGraph.getInitialConfiguration(demands);
        augGraph.showNetworkUsage();

        if (sPaths.empty()) {
            std::cout << "No initial configuration\n";
            return -1;
        } else {
            return 1;
        }
    }
    return 1;
}

// std::vector<int> loadNodeCapa(const std::string& _filename, const double
// _percent) {
//     std::vector<int> nodeCapas;
//     std::ifstream ifs(_filename, std::ifstream::in);
//     if(!ifs) {
//         std::cerr << _filename << " does not exists!\n";
//         exit(-1);
//     } else {
//         std::cout << "Opening :" << _filename << '\n';
//     }

//     while (ifs.good()) {
//         std::string line;
//         std::getline(ifs, line);
//         if(line != "") {
//             int u, capa;
//             std::stringstream lineStream(line);
//             lineStream >> u >> capa;
//             nodeCapas.emplace_back(capa * _percent);
//         }
//     }
//     return nodeCapas;
// }

// std::vector<Demand> loadDemands(const std::string& _filename, const double
// _percent) { 	std::vector<Demand> demands;
//     std::ifstream ifs(_filename, std::ifstream::in);
//     if(!ifs) {
//         std::cerr << _filename << " does not exists!\n";
//         exit(-1);
//     } else {
//         std::cout << "Opening :" << _filename << '\n';
//     }

//     while (ifs.good()) {
//         std::string line;
//         std::getline(ifs, line);
//         if(line != "") {
//             int s, t;
//             double d;
//             std::stringstream lineStream(line);
//             lineStream >> s >> t >> d;
//             FunctionDescriptor f;
//             std::vector<FunctionDescriptor> functions;
//             while(lineStream.good()) {
//             	lineStream >> f;
//             	functions.push_back(f);
//             }
//             functions.pop_back(); // Dirty hack
//             demands.emplace_back(s, t, d*_percent, functions);
//         }
//     }
//     return demands;
// }

// std::string getDemandFile(const std::string& _name, const int _n) {
//     std::ifstream ifs( "./instances/" + _name + "_demands.txt" );
//     if(!ifs) {
//         std::cerr << ("./instances/" + _name + "_demands.txt") << " does not
//         exists!\n"; exit(-1);
//     }
//     std::string filename;
//     int i = 0;
//     while(ifs.good()) {
//         std::string line;
//         std::getline(ifs, line);
//         if(i >= _n) {
//             filename = line;
//             break;
//         }
//         ++i;
//     }
//     return "./instances/" + filename;
// }

// DiGraph loadNetwork(const std::string& _filename) {
// 	const auto network = DiGraph::loadFromFile(_filename);
// 	return std::get<0>(network);
// }

// std::vector<double> loadFunctions(const std::string& _filename) {
// 	std::ifstream ifs(_filename, std::ifstream::in);
//     if(!ifs) {
//         std::cerr << _filename << " does not exists!\n";
//         exit(-1);
//     }
//     std::vector<double> functions;
// 	while(ifs.good()) {
// 		std::string line;
// 		std::getline(ifs, line);
// 		if(line != "") {
// 			int f;
// 			double cpu;
// 			std::stringstream lineStream(line);
//             lineStream >> f >> cpu;
//             functions.push_back(cpu);
// 		}
// 	}
// 	return functions;
// }