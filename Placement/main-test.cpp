// #include "ChainingSubset.hpp"

#include "MyRandom.hpp"
#include "RadixSort.hpp"
#include <MyTimer.hpp>
#include <algorithm>
#include <array>
#include <iostream>
#include <limits>

int main(int argc, char** argv) {
    std::cout << "Generating random vector..." << std::flush;
    using Integer = unsigned int;
    std::vector<Integer> tab(std::stoi(argv[1]));
    // std::vector<Integer> tab {{495, 496, 236, 9, 101, 496, 65, 504}};
    Time timer;
    for (auto& v : tab) {
        v = MyRandom::getInstance().getIntUniform<Integer>(
            std::numeric_limits<Integer>::min(),
            std::numeric_limits<Integer>::min());
    }
    std::cout << "done\n";
    auto v1 = tab;
    auto v2 = tab;
    auto v3 = tab;
    auto v4 = tab;

    timer.start();
    RightRadixSort<Integer>::sort(v1.begin(), v1.end());
    timer.show();
    std::cout << "v1 is sorted ? " << std::is_sorted(v1.begin(), v1.end()) << '\n'
              << std::flush;
    // std::cout << "tab included in v1 ? " << std::all_of(tab.begin(), tab.end(),
    // [&](const Integer _v){ return std::find(v1.begin(), v1.end(), _v) !=
    // v1.end(); }) << '\n'  << std::flush;

    timer.start();
    CountingRightRadixSort<Integer>::sort(v2.begin(), v2.end());
    timer.show();
    std::cout << "v2 is sorted ? " << std::is_sorted(v2.begin(), v2.end()) << '\n'
              << std::flush;
    // std::cout << "tab included in v2 ? " << std::all_of(tab.begin(), tab.end(),
    // [&](const Integer _v){ return std::find(v2.begin(), v2.end(), _v) !=
    // v2.end(); }) << '\n' << std::flush;

    timer.start();
    std::sort(v3.begin(), v3.end());
    timer.show();
    std::cout << "v3 is sorted ? " << std::is_sorted(v3.begin(), v3.end()) << '\n'
              << std::flush;
    // std::cout << "tab included in v3 ? " << std::all_of(tab.begin(), tab.end(),
    // [&](const Integer _v){ return std::find(v3.begin(), v3.end(), _v) !=
    // v3.end(); }) << '\n' << std::flush;

    timer.start();
    CountingRightRadixSort<Integer>::sort(v4);
    timer.show();
    std::cout << "v4 is sorted ? " << std::is_sorted(v4.begin(), v4.end()) << '\n'
              << std::flush;
    // std::cout << "tab included in v4 ? " << std::all_of(tab.begin(), tab.end(),
    // [&](const Integer _v){ return std::find(v4.begin(), v4.end(), _v) !=
    // v4.end(); }) << '\n' << std::flush;
    return 0;
}