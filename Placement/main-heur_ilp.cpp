#include <MyTimer.hpp>
#include <tclap/CmdLine.h>

// Path
#include "SFCHeur.hpp"

std::string getDemandFile(const std::string& name, int _i);
std::vector<Demand> loadDemands(const std::string& _filename, double _percent);
std::vector<int> loadNodeCapa(const std::string& _filename,
    const double _percent = 1.0);

DiGraph loadNetwork(const std::string& _filename);
std::vector<double> loadFunctions(const std::string& _filename);
void saveNodeCapas(const std::vector<int>& _nodeCapas,
    const std::string& _filename);
void saveTopo(const DiGraph& _graph, const std::string& _filename);

struct Param {
    std::string model;
    std::string name;
    double factor;
    int funcCoef;
    double percentDemand;
    int replicationLimit;
};

Param getParams(int argc, char** argv) {
    TCLAP::CmdLine cmd("Solve the SFC problem with column generation", ' ',
        "1.0");

    TCLAP::ValuesConstraint<std::string> vCons({"path"});
    TCLAP::ValueArg<std::string> modelName("m", "model", "Specify the model used",
        false, "path", &vCons);
    cmd.add(&modelName);

    TCLAP::UnlabeledValueArg<std::string> networkName(
        "network", "Specify the network name", true, "", "network");
    cmd.add(&networkName);

    TCLAP::ValueArg<double> demandFactor(
        "d", "demandFactor", "Specify the multiplicative factor for the demands",
        false, 1, "int");
    cmd.add(&demandFactor);

    TCLAP::ValueArg<int> replicationLimit(
        "r", "replicationLimit",
        "Specify the maximum amount of replication in functions", true, 3, "int");
    cmd.add(&replicationLimit);

    TCLAP::ValueArg<int> functionChages(
        "c", "functionChages", "Specify the index for the function charge file",
        false, 4, "int");
    cmd.add(&functionChages);

    TCLAP::ValueArg<double> percentDemand("p", "percentDemand",
        "Specify the percentage of demand used",
        false, -1, "double");
    cmd.add(&percentDemand);

    cmd.parse(argc, argv);

    return {modelName.getValue(), networkName.getValue(),
        demandFactor.getValue(), functionChages.getValue(),
        percentDemand.getValue(), replicationLimit.getValue()};
}

int main(int argc, char** argv) {
    std::cout << std::fixed;
    try {
        auto params = getParams(argc, argv);

        const DiGraph network =
            loadNetwork("./instances/" + params.name + "_topo.txt");
        // const std::string demandFile = getDemandFile(params.name, numDemand);
        // const std::vector<Demand> demands = loadDemands(demandFile,
        // params.factor);
        const std::vector<Demand> allDemands = loadDemands(
            "./instances/" + params.name + "_demand.txt", params.factor);
        const std::vector<double> funcCharge = loadFunctions(
            "./instances/func" + std::to_string(params.funcCoef) + ".txt");
        std::vector<int> nodeCapas =
            loadNodeCapa("./instances/" + params.name + "_nodeCapa.txt");
        std::cout << "Files loaded...\n";

        std::vector<Demand> demands;
        const int nbDemands =
            ceil(allDemands.size() * params.percentDemand / 100.0);
        if (params.percentDemand != -1) {
            demands = std::vector<Demand>(allDemands.begin(),
                allDemands.begin() + nbDemands);
        } else {
            demands = allDemands;
        }

        int totalNbCores = 0;
        for (int i(0); i < int(demands.size()); ++i) {
            const auto& funcs = std::get<3>(demands[i]);

            for (int j(0); j < int(funcs.size()); ++j) {
                totalNbCores += ceil(std::get<2>(demands[i]) / funcCharge[funcs[j]]);
            }
        }
        std::cout << totalNbCores << " needed cores!" << '\n';

        std::vector<int> NFVNodes;
        for (Graph::Node u(0); u < network.getOrder(); ++u) {
            if (nodeCapas[u] != -1) {
                NFVNodes.push_back(u);
            }
        }
        int availableCores = 0;
        for (const auto& u : NFVNodes) {
            availableCores += nodeCapas[u];
        }
        std::cout << availableCores << " cores available !\n";

        Time timer;
        timer.start();
        std::cout << "Getting initial configuration...\n";
        SFCHeur sfcHeur(network, demands);
        /*Find first initial configuration*/
        auto sPaths = sfcHeur.solve();

        if (!sPaths.empty()) {
            std::cout << sPaths << '\n';
        } else {
            std::cout << "No initial configuration\n";
            return -1;
        }

    } catch (IloException& e) {
        std::cout << e.getMessage() << '\n';
    } catch (const TCLAP::ArgException& e) {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << '\n';
    }
    return 1;
}

std::vector<int> loadNodeCapa(const std::string& _filename,
    const double _percent) {
    std::vector<int> nodeCapas;
    std::ifstream ifs(_filename, std::ifstream::in);
    if (!ifs) {
        std::cerr << _filename << " does not exists!\n";
        exit(-1);
    } else {
        std::cout << "Opening :" << _filename << '\n';
    }

    while (ifs.good()) {
        std::string line;
        std::getline(ifs, line);
        if (line != "") {
            int u, capa;
            std::stringstream lineStream(line);
            lineStream >> u >> capa;
            nodeCapas.emplace_back(capa * _percent);
        }
    }
    return nodeCapas;
}

std::vector<Demand> loadDemands(const std::string& _filename,
    const double _percent) {
    std::vector<Demand> demands;
    std::ifstream ifs(_filename, std::ifstream::in);
    if (!ifs) {
        std::cerr << _filename << " does not exists!\n";
        exit(-1);
    } else {
        std::cout << "Opening :" << _filename << '\n';
    }

    while (ifs.good()) {
        std::string line;
        std::getline(ifs, line);
        if (line != "") {
            int s, t;
            double d;
            std::stringstream lineStream(line);
            lineStream >> s >> t >> d;
            FunctionDescriptor f;
            std::vector<FunctionDescriptor> functions;
            while (lineStream.good()) {
                lineStream >> f;
                functions.push_back(f);
            }
            functions.pop_back(); // Dirty hack
            demands.emplace_back(s, t, d * _percent, functions);
        }
    }
    return demands;
}

std::string getDemandFile(const std::string& _name, const int _n) {
    std::ifstream ifs("./instances/" + _name + "_demands.txt");
    if (!ifs) {
        std::cerr << ("./instances/" + _name + "_demands.txt")
                  << " does not exists!\n";
        exit(-1);
    }
    std::string filename;
    int i = 0;
    while (ifs.good()) {
        std::string line;
        std::getline(ifs, line);
        if (i >= _n) {
            filename = line;
            break;
        }
        ++i;
    }
    return "./instances/" + filename;
}

DiGraph loadNetwork(const std::string& _filename) {
    const auto network = DiGraph::loadFromFile(_filename);
    return std::get<0>(network);
}

std::vector<double> loadFunctions(const std::string& _filename) {
    std::ifstream ifs(_filename, std::ifstream::in);
    if (!ifs) {
        std::cerr << _filename << " does not exists!\n";
        exit(-1);
    }
    std::vector<double> functions;
    while (ifs.good()) {
        std::string line;
        std::getline(ifs, line);
        if (line != "") {
            int f;
            double cpu;
            std::stringstream lineStream(line);
            lineStream >> f >> cpu;
            functions.push_back(cpu);
        }
    }
    return functions;
}