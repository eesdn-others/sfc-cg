#include "ChainingPathOccLimit.hpp"

namespace SFC::OccLim {

    Placement_DWD::Placement_DWD(const Instance* _inst)
    : m_inst(_inst)
    , m_y(IloAdd(m_model, IloNumVarArray(m_env)))
    , m_b([&]() {
        IloNumVarArray b(m_env, m_inst->funcCharge.size() * m_inst->NFVNodes.size());
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            for (const auto& u : m_inst->NFVNodes) {
                b[getIndexB(u, f)] = IloAdd(m_model, IloNumVar(m_env, 0.0, 1.0));
                // setIloName(b[getIndexB(u, f)], "b[u=" + std::to_string(u) + ", f=" + std::to_string(f) + "]");
            }
        }
        IloAdd(m_integerConversions, IloConversion(m_env, b, ILOBOOL));
        return b;
    }())
    , m_obj(IloAdd(m_model, IloMinimize(m_env)))
    , m_onePathCons([&]() {
        IloRangeArray retval(m_env, m_inst->demands.size());
        std::generate(begin(retval), end(retval), [&, i=0]() mutable {
            return IloRange(m_env, 1.0, 1.0, std::string("onePathCons" + toString(m_inst->demands[i++])).c_str());
        });
        return IloAdd(m_model, retval);
    }())
    , m_linkCapasCons([&]() {
        IloRangeArray linkCapaCons(m_env, m_inst->network.size(),
            0.0, 0.0);
        int e = 0;
        for (const Graph::Edge& edge : m_inst->edges) {
            linkCapaCons[e].setBounds(
                0.0, m_inst->network.getEdgeWeight(edge));
            // setIloName(linkCapaCons[e], "linkCapas" + toString(edge));
            ++e;
        }
        return IloAdd(m_model, linkCapaCons);
    }())
    , m_nodeCapasCons([&]() {
        IloRangeArray nodeCapasCons(m_env, m_inst->NFVNodes.size());
        for (const auto& u : m_inst->NFVNodes) {
            const int i = m_inst->NFVIndices[u];
            nodeCapasCons[i] = IloAdd(
                m_model, IloRange(m_env, 0.0, m_inst->nodeCapa[u]));
            // setIloName(nodeCapasCons[i], "nodeCapasCons" + std::to_string(u));
        }
        return nodeCapasCons;
    }())
    , m_nbLicensesCons([&]() {
        // // Compute minimum nb licences
        // std::vector<int> nbCores(m_inst->funcCharge.size(), 0);
        // for (int i = 0; i < m_inst->demands.size(); ++i) {
        //     for (int j = 0;
        //          j < m_inst->demands[i].functions.size(); ++j) {
        //         nbCores[m_inst->demands[i].functions[j]] +=
        //             m_inst->nbCores(i, j);
        //     }
        // }
        // auto copyCapas = m_inst->nodeCapa;
        // std::sort(copyCapas.begin(), copyCapas.end(),
        //     [&](const int __i1, const int __i2) { return __i1 > __i2; });

        // std::vector<int> minLic(m_inst->funcCharge.size(), 0);
        // for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
        //     while (nbCores[f] > 0) {
        //         if (copyCapas[minLic[f]] != 0) {
        //             nbCores[f] -= copyCapas[minLic[f]];
        //             ++minLic[f];
        //         } else {
        //             minLic[f] = IloInfinity;
        //             break;
        //         }
        //     }
        // }

        IloRangeArray funcLicensesCons(m_env,
            m_inst->funcCharge.size());
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            IloNumVarArray vars(m_env);
            for (const auto& u : m_inst->NFVNodes) {
                vars.add(m_b[getIndexB(u, f)]);
            }
            funcLicensesCons[f] =
                IloAdd(m_model, IloRange(m_env, -IloInfinity, IloSum(vars),
                                    m_inst->nbLicenses));
            // setIloName(funcLicensesCons[f],
            //     "funcLicensesCons(" + toString(f) + ")");
            vars.end();
        }
        return funcLicensesCons;
    }())
    , m_pathFuncCons([&]() {
        IloRangeArray pathFuncCons(m_env,
            m_inst->demands.size() * m_inst->funcCharge.size() * m_inst->NFVNodes.size());
        for (int i = 0; i < m_inst->demands.size(); ++i) {
            for (int f = 0; f < m_inst->funcCharge.size();
                 ++f) {
                for (const auto& u : m_inst->NFVNodes) {
                    pathFuncCons[getIndexPathFunc(i, f, u)] =
                        IloRange(m_env, 0.0, m_b[getIndexB(u, f)], IloInfinity);
                    // setIloName(pathFuncCons[getIndexPathFunc(i, f, u)],
                    //     "pathFuncCons" + toString(std::make_tuple(i, f, u)));
                }
            }
        }
        return IloAdd(m_model, pathFuncCons);
    }())
    , m_dummyPaths([&]() {
        IloNumVarArray retval(m_env);
        for (int i = 0; i < m_inst->demands.size(); ++i) {
            retval.add(m_onePathCons[i](1.0) + m_obj(2 * m_inst->demands[i].d * m_inst->network.getOrder()));
            // setIloName(retval[retval.getSize() - 1], "dummyPaths" + toString(m_inst->demands[i]));
        }
        return retval;
    }())
    , m_linkCharge(m_env, m_inst->network.size())
    , m_nodeCharge(m_env, m_inst->NFVNodes.size())
    , m_funcPath(m_env, m_inst->funcCharge.size() * m_inst->NFVNodes.size())
    , m_pathFuncCons_Utility(m_env, m_inst->funcCharge.size() * m_inst->NFVNodes.size())
    , m_solver([&]() {
        IloCplex retval(m_model);
        retval.setOut(retval.getEnv().getNullStream());
        retval.setWarning(retval.getEnv().getNullStream());
        retval.setParam(IloCplex::Threads, 1);
        retval.setParam(IloCplex::RootAlg, IloCplex::Concurrent);
        return retval;
    }()) {}

double Placement_DWD::getObjValue() const {
    return m_fractObj;
}

Placement_DWD::Solution Placement_DWD::getSolution() const {
    return Placement_DWD::Solution(*m_inst, getIntegerObjValue(), getRelaxationObjValue(),
        getServicePaths(), m_paths.size());
}

int Placement_DWD::getIndexB(
    const Graph::Node _u, const function_descriptor _f) const {
    assert(_f < m_inst->funcCharge.size());
    assert(_u < m_inst->network.getOrder());
    return m_inst->NFVNodes.size() * _f + m_inst->NFVIndices[_u];
}

double Placement_DWD::getReducedCost(const SFC::ServicePath& _sPath) const {
    return getReducedCost(_sPath, normalDuals);
}

int Placement_DWD::getIndexPathFunc(
    const int _i, const function_descriptor _f,
    const Graph::Node _u) const {
    assert(_i < m_inst->demands.size());
    assert(_f < m_inst->funcCharge.size());
    assert(_u < m_inst->network.getOrder());

    return m_inst->NFVNodes.size() * m_inst->funcCharge.size() * _i + m_inst->NFVNodes.size() * _f + m_inst->NFVIndices[_u];
}

void Placement_DWD::addColumns(const std::vector<Column>& _sPaths) {
    // std::cout << "Adding: " << _sPath << '\n';
    IloNumColumnArray columns(m_env);
    columns.setSize(_sPaths.size());
    int idx = 0;
    for(const auto& _sPath : _sPaths) {
        assert([&]() {
            const auto ite = std::find(m_paths.begin(), m_paths.end(), _sPath);
            if (ite != m_paths.end()) {
                std::cout << _sPath << " is already present!" << '\n';
                return false;
            }
            return true;
        }());

        std::fill(begin(m_linkCharge), end(m_linkCharge), 0.0);
        for (auto iteU = _sPath.nPath.begin(), iteV = std::next(iteU); iteV != _sPath.nPath.end();
             ++iteU, ++iteV) {
            m_linkCharge[m_inst->edgeToId(*iteU, *iteV)] +=
                m_inst->demands[_sPath.demand].d;
        }
        /****************************************************************************************************/

        /***************************************** Node constraints
       * *****************************************/
        std::fill(begin(m_nodeCharge), end(m_nodeCharge), 0.0);
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            for (const auto& u : m_inst->NFVNodes) {
                const int index = m_inst->NFVIndices[u];
                m_funcPath[m_inst->NFVNodes.size() * f + index] = IloNum(0.0);
                m_pathFuncCons_Utility[m_inst->NFVNodes.size() * f + index] =
                    m_pathFuncCons[getIndexPathFunc(_sPath.demand, f, u)];
            }
        }

        for (int j = 0; j < _sPath.locations.size(); ++j) {
            // Add to node capa constraints
            const int index = m_inst->NFVIndices[_sPath.locations[j]];
            m_nodeCharge[index] += m_inst->nbCores(_sPath.demand, j);
            // Add to placement constraint
            m_funcPath[m_inst->NFVNodes.size() * m_inst->demands[_sPath.demand].functions[j] + index] = -1.0;
        }

        columns[idx] = IloNumColumn(m_env);
        columns[idx] += m_onePathCons[_sPath.demand](1.0);
        columns[idx] += m_obj(m_inst->demands[_sPath.demand].d * (_sPath.nPath.size() - 1));
        columns[idx] += m_linkCapasCons(m_linkCharge);
        columns[idx] += m_nodeCapasCons(m_nodeCharge);
        columns[idx] += m_pathFuncCons_Utility(m_funcPath);
        ++idx;
    }
    m_y.add(IloNumVarArray(m_env, columns));
    // setIloName(m_y[m_y.getSize() - 1], "y" + toString(std::make_tuple(m_inst->demands[demandID], _sPath)));
    m_paths.reserve(m_paths.size() + _sPaths.size());
    m_paths.insert(m_paths.end(), _sPaths.begin(), _sPaths.end());
}

double Placement_DWD::getLowerBound(const std::vector<Column>& _columns, const DualValues& _dualValues) const {
    auto lowerBound = 0.0;
    for (const auto& sPath : _columns) {
        lowerBound += getReducedCost(sPath, _dualValues);
    }

    std::cout << "rc:" << lowerBound << '\t';
    lowerBound += getDualSumRHS(_dualValues);
    lowerBound += getAdditionalVariable(_dualValues);
    // std::cout << "=> " << lowerBound;
    return lowerBound;
}

double Placement_DWD::getLowerBound(const DualValues& _dualValues) const {
    auto lowerBound = 0.0;
    // std::cout << "rc:" << lowerBound << '\t';
    lowerBound += getDualSumRHS(_dualValues);
    lowerBound += getAdditionalVariable(_dualValues);
    // std::cout << "=> " << lowerBound;
    return lowerBound;
}

double Placement_DWD::getStabilizedLowerBound(const std::vector<Column>& _columns) const {
    // std::cout << "\ngetStabilizedLowerBound: ";
    return getLowerBound(_columns, stabDuals);
}

double Placement_DWD::getBestLowerBound(const double _bestLowerBound, const std::vector<Column>& _columns) {
    const double LB_st = getLowerBound(_columns, stabDuals);
    std::cout << "Stabilized LB: " << LB_st << '\n';
    if (LB_st > _bestLowerBound) {
        bestDuals.m_onePathDuals = stabDuals.m_onePathDuals;
        bestDuals.m_linkCapasDuals = stabDuals.m_linkCapasDuals;
        bestDuals.m_nodeCapasDuals = stabDuals.m_nodeCapasDuals;
        bestDuals.m_nbLicensesDuals = stabDuals.m_nbLicensesDuals;
        bestDuals.m_pathFuncDuals = stabDuals.m_pathFuncDuals;
        return LB_st;
    }
    return _bestLowerBound;
}

double Placement_DWD::getReducedCost(const ServicePath& _sPath, const DualValues& _dualValues) const {
    const auto & [ nPath, locations, demand ] = _sPath;
    double retval = -_dualValues.m_onePathDuals[demand];
    for (auto iteU = nPath.begin(), iteV = std::next(iteU);
         iteV != nPath.end(); ++iteU, ++iteV) {
        retval += m_inst->demands[demand].d * (1 - _dualValues.m_linkCapasDuals[m_inst->edgeToId(*iteU, *iteV)]);
    }
    for (int j = 0; j < locations.size(); ++j) {
        retval += -m_inst->nbCores(demand, j) * _dualValues.m_nodeCapasDuals[locations[j]];
    }

    const auto getFuncPathIndex = [&](auto&& u, auto&& f) {
        return m_inst->NFVNodes.size() * f + m_inst->NFVIndices[u];
    };
    IloNumArray funcPath(m_env, m_inst->funcCharge.size() * m_inst->NFVNodes.size());
    for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
        for (const auto& u : m_inst->NFVNodes) {
            funcPath[getFuncPathIndex(u, f)] = IloNum(0.0);
        }
    }

    for (int j = 0; j < locations.size(); ++j) {
        const int u = locations[j];
        const int f = m_inst->demands[demand].functions[j];
        funcPath[getFuncPathIndex(u, f)] = 1.0;
    }

    for (const auto& u : m_inst->NFVNodes) {
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            retval += funcPath[getFuncPathIndex(u, f)]
                      * _dualValues.m_pathFuncDuals[getIndexPathFunc(demand, f, u)];
        }
    }
    funcPath.end();
    return retval;
}

bool Placement_DWD::checkReducedCosts() const {
    IloNumArray funcPath(m_env, m_inst->funcCharge.size() * m_inst->NFVNodes.size());
    for (int k = 0; k < m_paths.size(); ++k) {
        const double solverRC =
            m_solver.getReducedCost(m_y[k]);
        const double myRC = getReducedCost(m_paths[k], normalDuals);
        if (!epsilon_equal<double>()(solverRC, myRC)) {
            std::cerr << "myRC: " << myRC << ", solverRC: " << solverRC << '\n';
            return false;
        }
    }
    return true;
}

bool Placement_DWD::solveInteger() {
    m_solver.clearModel();
    IloModel integerModel(m_env);
    integerModel.add(m_model);
    integerModel.add(m_integerConversions);
    for (int i = 0; i < m_inst->demands.size(); ++i) {
        integerModel.add(IloRange(m_env, 0.0, IloNumExprArg(m_dummyPaths[i]), 0.0));
    }
    m_solver.extract(integerModel);
    m_solver.setOut(std::cout);

    // Setting priorities for location variables
    for (IloInt i = 0; i < m_b.getSize(); ++i) {
        m_solver.setPriority(m_b[i], 1.0);
    }

    if (m_solver.solve() == IloFalse) {
        std::cout << "No solution found!\n";
        m_solver.exportModel("IMP.lp");
        return false;
    }
    // Solution found
    m_intObj = m_solver.getObjValue();
    std::cout << "Final obj value: " << m_intObj << "\t# Paths: " << m_paths.size() << '\n';
    return true;
}

double Placement_DWD::getDualSumRHS(const DualValues& _dualValues) const {
    double retval = 0.0;
    for (const auto u : m_inst->NFVNodes) {
        retval += _dualValues.m_nodeCapasDuals[m_inst->NFVIndices[u]] * m_inst->nodeCapa[u];
    }
    std::cout << "nc:" << retval << '\t';

    for (int e = 0; e < m_inst->network.size(); ++e) {
        retval += _dualValues.m_linkCapasDuals[e] * m_inst->network.getEdgeWeight(m_inst->network.getEdges()[e]);
    }
    std::cout << "lc:" << retval << '\t';

    std::accumulate(begin(_dualValues.m_onePathDuals), end(_dualValues.m_onePathDuals), 0.0);
    std::cout << "op:" << retval << '\t';

    retval += m_inst->nbLicenses 
        * std::accumulate(begin(_dualValues.m_nbLicensesDuals), end(_dualValues.m_nbLicensesDuals), 0.0);
    std::cout << "nl:" << retval << '\t';
    return retval;
}

double Placement_DWD::getAdditionalVariable(const DualValues& _dualValues) const {
    IloNumArray bVal(m_env, m_inst->funcCharge.size() * m_inst->NFVNodes.size());
    for (int i = 0; i < m_inst->demands.size(); ++i) {
        for (const auto& u : m_inst->NFVNodes) {
            for (const auto& f : m_inst->demands[i].functions) {
                bVal[getIndexB(u, f)] +=  _dualValues.m_pathFuncDuals[getIndexPathFunc(i, f, u)];
            }
        }
    }
    for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
        for (const auto& u : m_inst->NFVNodes) {
            bVal[getIndexB(u, f)] += -_dualValues.m_nbLicensesDuals[f];
        }
    }

    double retval = 0.0;
    for(IloInt i = 0; i < bVal.getSize(); ++i) {
        if (bVal[i] < 0) {
            retval += bVal[i];
        }
    }
    // std::cout << "nl:" << retval << "]\t";
    return retval;
}

bool Placement_DWD::solve() {
    m_solver.extract(m_model);
    if (m_solver.solve() == IloFalse) {
        return false;
    }
    m_fractObj = m_solver.getObjValue();
    getDuals();
    assert(checkReducedCosts());
    m_solver.clear();
    return true;
}

void Placement_DWD::getDuals() {
    m_solver.getDuals(normalDuals.m_linkCapasDuals, m_linkCapasCons);
    m_solver.getDuals(normalDuals.m_nodeCapasDuals, m_nodeCapasCons);
    m_solver.getDuals(normalDuals.m_pathFuncDuals, m_pathFuncCons);
    m_solver.getDuals(normalDuals.m_onePathDuals, m_onePathCons);
    m_solver.getDuals(normalDuals.m_nbLicensesDuals, m_nbLicensesCons);

    for (auto i = 0; i < normalDuals.m_onePathDuals.getSize(); ++i) {
        stabDuals.m_onePathDuals[i] = m_alpha * normalDuals.m_onePathDuals[i]
                                      + ((1 - m_alpha) * bestDuals.m_onePathDuals[i]);
    }
    for (auto i = 0; i < normalDuals.m_linkCapasDuals.getSize(); ++i) {
        stabDuals.m_linkCapasDuals[i] = m_alpha * normalDuals.m_linkCapasDuals[i]
                                        + ((1 - m_alpha) * bestDuals.m_linkCapasDuals[i]);
    }
    for (auto i = 0; i < normalDuals.m_nodeCapasDuals.getSize(); ++i) {
        stabDuals.m_nodeCapasDuals[i] = m_alpha * normalDuals.m_nodeCapasDuals[i]
                                        + ((1 - m_alpha) * bestDuals.m_nodeCapasDuals[i]);
    }
    for (auto i = 0; i < normalDuals.m_pathFuncDuals.getSize(); ++i) {
        stabDuals.m_pathFuncDuals[i] = m_alpha * normalDuals.m_pathFuncDuals[i]
                                       + ((1 - m_alpha) * bestDuals.m_pathFuncDuals[i]);
    }
    for (auto i = 0; i < normalDuals.m_nbLicensesDuals.getSize(); ++i) {
        stabDuals.m_nbLicensesDuals[i] = m_alpha * normalDuals.m_nbLicensesDuals[i]
                                      + ((1 - m_alpha) * bestDuals.m_nbLicensesDuals[i]);
    }
}

std::vector<ServicePath>
Placement_DWD::getServicePaths() const {
    std::vector<ServicePath> retval(m_inst->demands.size());
    IloNumArray yVals(m_env);
    m_solver.getValues(m_y, yVals);
    for (int k = 0; k < m_paths.size(); ++k) {
        if (epsilon_equal<double>()(yVals[k], 1.0)) {
            retval[m_paths[k].demand] = m_paths[k];
        }
    }
    return retval;
}
} // namespace SFC::OccLim
