#include "AugmentedGraph.hpp"
#include "BinaryHeap.hpp"
#include "utility.hpp"

namespace SFC {

AugmentedGraph::AugmentedGraph(const Instance& _inst)
    : m_inst(&_inst)
    , m_nbLayers(m_inst->funcCharge.size() + 1)
    , m_layeredGraph(_inst)
    , m_flowGraph([&]() {
        DiGraph flowGraph(m_inst->network);
        for (const auto& edge : flowGraph.getEdges()) {
            flowGraph.setEdgeWeight(edge, 0);
        }
        return flowGraph;
    }())
    , m_nodeUsage(m_inst->network.getOrder(), 0)
    , m_funcPlacement(m_inst->network.getOrder(), m_inst->funcCharge.size(), 1) {
}

AugmentedGraph::AugmentedGraph(const Instance& _inst, Matrix<char> _funcPlacement)
    : AugmentedGraph(_inst) {
    m_funcPlacement = std::move(_funcPlacement);
}

void AugmentedGraph::removeNodeCapa(const Graph::Node _node) {
    for (int fi = 0; fi < m_inst->funcCharge.size(); ++fi) {
        m_layeredGraph.removeCrossLayerLink(_node, fi);
        //m_inst->network.getOrder() * fi + _node, m_inst->network.getOrder() * (fi + 1) + _node);
    }
}

void AugmentedGraph::showNetworkUsage() const {
    std::cout << m_nodeUsage << '\n';
    std::cout << m_flowGraph.getEdges() << std::endl;
}

std::vector<Graph::Node> AugmentedGraph::getNodesSortedByUsage() const {
    std::vector<Graph::Node> nodes(m_inst->nodeCapa.size());
    std::generate(nodes.begin(), nodes.end(), [i = 0]() mutable {
        return i++;
    });
    std::sort(nodes.begin(), nodes.end(),
        [&](const Graph::Node _u, const Graph::Node _v) {
            return m_nodeUsage[_u] < m_nodeUsage[_v];
        });
    return nodes;
}

std::vector<ServicePath>
AugmentedGraph::solve() {
    std::vector<ServicePath> initConf;
    initConf.reserve(m_inst->demands.size());
    for (int demandID = 0; demandID < m_inst->demands.size(); ++demandID) {
        /* For each demands, search for a shortest path in the augmented graph */
        Graph::Path path = getAugmentedPath(m_inst->demands[demandID]);
        if (path.empty()) {
            return initConf;
        }
        // Add service path
        initConf.emplace_back(SFC::getServicePath(path, m_inst->network.getOrder(), demandID));
        updateCapacities(initConf.back());
    }
    return initConf;
}

void AugmentedGraph::updateCapacities(const ServicePath& _sPath) {
    for (int j = 0; j < _sPath.locations.size(); ++j) {
        const auto& u = _sPath.locations[j];
        m_nodeUsage[u] += m_inst->nbCores(_sPath.demand, j);
        assert(m_nodeUsage[u] <= m_inst->nodeCapa[u]
               || (std::cout << "m_nodeUsage[" << u << "] is full (" << m_nodeUsage[u] << '/' << m_inst->nodeCapa[u] << ")\n",
                      false));
    }
    for (auto iteU = std::begin(_sPath.nPath), iteV = std::next(iteU);
         iteV != std::end(_sPath.nPath); ++iteU, ++iteV) {
        const Graph::Edge edge(*iteU, *iteV);
        m_flowGraph.setEdgeWeight(edge,
            m_flowGraph.getEdgeWeight(edge) + m_inst->demands[_sPath.demand].d);
        assert(m_flowGraph.getEdgeWeight(edge) < m_inst->network.getEdgeWeight(edge)
               || (std::cout << "m_flowGraph[" << edge << "] is full ("
                             << m_flowGraph.getEdgeWeight(edge) << '/' << m_inst->network.getEdgeWeight(edge) << ")\n",
                      false));
    }
}

std::vector<ServicePath> AugmentedGraph::getInitialConfigurationNoCapa(
    const std::vector<Demand>& _demands) {
    std::vector<ServicePath> initConf;
    initConf.reserve(_demands.size());

    for (const auto& demand : _demands) {
        /* For each demands, search for a shortest path in the augmented graph */
        Graph::Path path = getAugmentedPathNoCapa(demand);

        if (path.empty()) {
            std::cerr << "No path found for " << demand << '\n';
            return {};
        }
        // Add service path
        updateCapacities(initConf.back());
    }
    return initConf;
}

Graph::Path AugmentedGraph::getAugmentedPath(const Demand& _demand) const {
    const LayeredGraph::Node t {_demand.t, _demand.functions.size()};
    
    std::vector<double> distance(m_layeredGraph.getOrder(),
        std::numeric_limits<int>::max());
    
    std::vector<LayeredGraph::Node> parent(m_layeredGraph.getOrder(), {-1, -1});
    
    enum NodeState {
        Visited = 2, // Node has been visited by the algorithm, distance is minimum.
        InHeap = 1,  // Node hasn't need visited, distance can be reduced.
        Free = 0     // Neighbors of the node hasn't been visited yet, no distance
                     // computed.
    };
    std::vector<NodeState> color(m_layeredGraph.getOrder(), NodeState::Free);

    using Heap = BinaryHeap<LayeredGraph::Node, std::function<bool(Graph::Node, Graph::Node)>>;
    Heap heap(m_layeredGraph.getOrder(),
        [&](const Graph::Node u, const Graph::Node v) {
            return distance[u] < distance[v];
        });

    std::vector<Heap::Handle*> handles(m_layeredGraph.getOrder());

    Graph::Node realNode = getRealNode({_demand.s, 0});
    parent[realNode] = {_demand.s, 0};
    distance[realNode] = 0;
    handles[realNode] = heap.push({_demand.s, 0});
    color[realNode] = NodeState::InHeap;

    while (t != heap.top()) {
        if (heap.empty()) {
            return {}; // return an empty path if no path is found
        }
        const LayeredGraph::Node [u, j] = heap.top();
        const Graph::Node realU = getRealNode(u);
        heap.pop();

        color[u] = NodeState::Visited;

        // Same layer
        for() {
            
        }
        for (Graph::Node realV : m_layeredGraph.getNeighbors(realU)) {
            const LayeredGraph::Node v {
                realV / m_inst->network.getOrder(),
                realV % m_inst->network.getOrder()
            };

            if (color[realV] != NodeState::Visited) {
                /* Check tmp usage */
                /* Check if in-layer or between layers */
                double weight = -1.0;
                if (u.layer == v.layer) {
                    /* Same layer => Check enough bandwidth */
                    const double tmpLinkUsage = checkCurrentLinkUsage(layerU, _demand, parent);
                    double charge = tmpLinkUsage + m_flowGraph.getEdgeWeight(baseU, baseV) + _demand.d;
                    if (charge > m_inst->network.getEdgeWeight(baseU, baseV)) {
                        continue;
                    }
                    weight = 1 + charge / m_inst->network.getEdgeWeight(baseU, baseV);
                } else {
                    //             Different layer
                    // => Check number of layers
                    // => Check that function can be used on realU
                    if (v.layer <= _demand.functions.size() && m_funcPlacement(baseU, _demand.functions[layerU]) == 1) {
                        //=> Check enough node capacity
                        int tmpNodeUsage = 0;
                        for (Graph::Node tmpNode = realU; tmpNode != _demand.s;
                             tmpNode = parent[tmpNode]) {
                            const Graph::Node baseTMP = tmpNode % m_inst->network.getOrder();

                            const Graph::Node baseParent = parent[tmpNode] % m_inst->network.getOrder();
                            const int layerParent = parent[tmpNode] / m_inst->network.getOrder(); //

                            if (baseParent == baseTMP && baseParent == baseU) {
                                tmpNodeUsage += ceil(
                                    _demand.d / m_inst->funcCharge[_demand.functions[layerParent]]);
                            }
                        }
                        double charge =
                            m_nodeUsage[baseU] + ceil(_demand.d / m_inst->funcCharge[_demand.functions[layerU]]) + tmpNodeUsage;
                        if (charge > m_inst->nodeCapa[baseU]) {
                        } else {
                            weight = 1 + charge / double(m_inst->nodeCapa[baseU]);
                        }
                    }
                }
                if (weight != -1.0) {
                    const auto distT = distance[realU] + weight;
                    if (distT < distance[realV]) {
                        distance[realV] = distT;
                        parent[realV] = realU;
                        if (color[realV] == NodeState::InHeap) {
                            // Update heap
                            heap.decrease(handles[realV]);
                        } else {
                            // Add node to heap
                            handles[realV] = heap.push(v);
                            color[realV] = NodeState::InHeap;
                        }
                    }
                }
            }
        }
    }

    // Path is found, build it
    Graph::Path resPath;
    Graph::Node node = t;
    while (node != _demand.s) {
        resPath.push_back(node);
        node = parent[node];
    }
    resPath.push_back(node);
    std::reverse(resPath.begin(), resPath.end());
    return resPath;
}

double checkCurrentLinkUsage(const Graph::Node _currentNode, const Demand& _demand,
    const std::vector<Graph::Node>& _parent) const {
    for (Graph::Node tmpNode = _currentNode; tmpNode != m_layeredGraph.getRealNode({_demand.s, 0});
         tmpNode = _parent[tmpNode]) {
        const Graph::Node baseTMP = tmpNode % m_inst->network.getOrder();
        const int layerTMP = tmpNode / m_inst->network.getOrder();

        const Graph::Node baseParent = _parent[tmpNode] % m_inst->network.getOrder();
        const int layerParent = _parent[tmpNode] / m_inst->network.getOrder();
        if (layerParent == layerTMP && baseU == baseParent && baseV == baseTMP) {
            tmpLinkUsage += _demand.d;
        }
    }
}

Graph::Path
AugmentedGraph::getAugmentedPathNoCapa(const Demand& _demand) const {
    const Graph::Node t =
        m_inst->network.getOrder() * _demand.functions.size() + _demand.t;

    std::vector<double> distance(m_layeredGraph.getOrder(),
        std::numeric_limits<double>::max());
    std::vector<Graph::Node> parent(m_layeredGraph.getOrder(), -1);
    enum NodeState {
        Visited = 2, // Node has been visited by the algorithm, distance is minimum.
        InHeap = 1,  // Node hasn't need visited, distance can be reduced.
        Free = 0     // Neighbors of the node hasn't been visited yet, no distance
                     // computed.
    };
    std::vector<NodeState> color(m_layeredGraph.getOrder(), NodeState::Free);

    std::vector<typename BinaryHeap<
        Graph::Node, std::function<bool(Graph::Node, Graph::Node)>>::Handle*>
        handles(m_layeredGraph.getOrder());
    BinaryHeap<Graph::Node, std::function<bool(Graph::Node, Graph::Node)>> heap(
        m_layeredGraph.getOrder(),
        [&](const Graph::Node u, const Graph::Node v) { return distance[u] < distance[v]; });

    parent[_demand.s] = _demand.s;
    distance[_demand.s] = 0;
    handles[_demand.s] = heap.push(_demand.s);
    color[_demand.s] = NodeState::InHeap;

    Graph::Path resPath;
    while (t != heap.top()) {
        if (heap.empty()) {
            return resPath;
        }
        auto u = heap.top();
        heap.pop();
        color[u] = NodeState::Visited;
        // std::cout << u << " -> " << m_layeredGraph.getNeighbors(u) << '\n';
        for (Graph::Node v : m_layeredGraph.getNeighbors(u)) {
            if (color[v] != NodeState::Visited) {
                /* Check if in-layer or between layers */
                int layerU = u / m_inst->network.getOrder(),
                    layerV = v / m_inst->network.getOrder();
                Graph::Node baseU = u % m_inst->network.getOrder(),
                            baseV = v % m_inst->network.getOrder();
                double weight = 0.0;
                if (layerU == layerV) {
                    /* Same layer => Check enough bandwidth */
                    double tmpLinkUsage(0);
                    for (Graph::Node tmpNode = u; tmpNode != _demand.s;
                         tmpNode = parent[tmpNode]) {
                        const Graph::Node baseTMP = tmpNode % m_inst->network.getOrder();
                        const int layerTMP = tmpNode / m_inst->network.getOrder();

                        const Graph::Node baseParent = parent[tmpNode] % m_inst->network.getOrder();
                        const int layerParent = parent[tmpNode] / m_inst->network.getOrder();
                        if (layerParent == layerTMP && baseU == baseParent && baseV == baseTMP) {
                            tmpLinkUsage += _demand.d;
                        }
                    }
                    weight = tmpLinkUsage + m_flowGraph.getEdgeWeight(baseU, baseV)
                             + _demand.d;
                } else {
                    // Different layer
                    // => Check number of layers
                    // => Check that function can be used on u
                    if (layerV > _demand.functions.size() || !m_funcPlacement(u, _demand.functions[layerU])) {
                        continue;
                    }
                    /* Check temp node usage from used path */
                    Graph::Node tmpNodeUsage = 0;
                    for (Graph::Node tmpNode = u; tmpNode != _demand.s;
                         tmpNode = parent[tmpNode]) {
                        const Graph::Node baseTMP = tmpNode % m_inst->network.getOrder();
                        // const int layerTMP = tmpNode / m_inst->network.getOrder();

                        const Graph::Node baseParent = parent[tmpNode] % m_inst->network.getOrder();
                        const int layerParent = parent[tmpNode] / m_inst->network.getOrder();
                        if (baseParent == baseTMP && baseParent == baseU) {
                            tmpNodeUsage +=
                                (_demand.d / m_inst->funcCharge[_demand.functions[layerParent]]);
                        }
                    }
                    /* => Check enough node capacity */
                    weight = m_nodeUsage[baseU] + (_demand.d / m_inst->funcCharge[_demand.functions[layerV - 1]]) + tmpNodeUsage;
                }
                auto distT = distance[u] + weight;
                if (distT < distance[v]) {
                    distance[v] = distT;
                    parent[v] = u;
                    if (color[v] == NodeState::InHeap) {
                        /* Update heap */
                        heap.decrease(handles[v]);
                    } else {
                        /* Add node to heap */
                        handles[v] = heap.push(v);
                        color[v] = NodeState::InHeap;
                    }
                }
            }
        }
    }

    /* Path is found, build it */
    Graph::Node i = t;
    while (i != _demand.s) {
        resPath.push_back(i);
        i = parent[i];
    }
    resPath.push_back(i);
    std::reverse(resPath.begin(), resPath.end());
    return resPath;
}
} // namespace SFC