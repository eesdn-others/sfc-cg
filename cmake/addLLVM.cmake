# FindLLVM.cmake
#
# Finds the LLVM library
#
# This will define the following variables
#
#    LLVM_FOUND
#    LLVM_INCLUDE_DIRS
#
# and the following imported targets
#
#     LLVM::llvm
#
# Author: Nicolas Huin - nicolasjfhuin@gmail.com

find_package(LLVM 5.0  REQUIRED)

if(LLVM_FOUND AND NOT TARGET LLVM::llvm)
	message(STATUS "Found LLVM in ${LLVM_INCLUDE_DIRS}")
    add_library(LLVM::llvm INTERFACE IMPORTED)
    set_target_properties(LLVM::llvm PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${LLVM_INCLUDE_DIRS}"
        INTERFACE_LINK_LIBRARIES "${LLVM_LIBRARIES}"
    )
endif()

